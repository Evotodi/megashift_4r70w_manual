/*************************************************************************
**************************************************************************
**   4L60Egpio - V0.960
**
**   (C) 2007 - B. A. Bowling And A. C. Grippo
**
**   This header must appear on all derivatives of this code. Any code 
**   modifications made for any reason become the exclusive property of 
**   Bowling and Grippo. 
**   All code revisions must be shared free of charge and without further 
**   restriction on the forums at http://www.msefi.com  This code, or any 
**   derivative of it, may not be used in any form, in whole or in part, 
**   on non-Bowling and Grippo hardware under any cicrumstances.
**   No commercial use of this code, in whole or in part, is permitted without 
**   express written permmission from Bowling and Grippo
**
***************************************************************************
**************************************************************************/

/**
ABOUT THIS CODE

This code is intended primarily as a tutorial in C for embedded use on HSC12 processors. 
The example application is the control of a General Motors 4L60E automotive transmission. 

This code contains many inefficiencies, and has many areas that are implemented 
in ways that experienced programmers would consider naive. Most abstractions have been 
avoided to make the code as easy to follow as possible for the beginning embedded programmer. 
The code is written in a manner that a novice programmer might do, so that it can be understood 
more easily by those just starting out with embedded code on the HCS12.  

For example, the shift controls could easily be incorporated into a few small functions with 
table look-ups to specify the necessary actions. While this would make it easier to adapt the 
code to different transmissions, and would make the code smaller and faster, it would also make 
the logic and program flow less obvious to novices. Instead, the shift controls are explicitly 
coded, and no attempt has been made to genealize them. Because this application is not 'timing 
critical', and because we have plenty of room left, this pedagogical approach isn't a problem 
with the current code. The user is invited to make any improvements they desire, of course. 
**/

/**
 TO DO:

 - finish error code scheme
 - speedo output signal (ratio of VSS) on PT4
 - add shift mode switch to PT5 (open is auto, ground is manual)
 - add option to use MAP or TPS for load 
   (also option to use CAN w/ MegaSquirt-II or EGT1 independently)
**/

/**************************************************************************
***************************************************************************
**
**   The MegaShift 4L60E controller is a dedicated controller for the 
**   electronically shifted General Motors RWD 4-speed automatic transmission. 
**   It uses the GPIO hardware with the MC9S12C64 processor from Bowling and Grippo 
**   (http://www.megasquirt.info). 
**   
**   The 4L60E code has the following features: 
**   
**   - Fully configurable automatic or manual mode, 
**   - Provisions for 'paddle' shifters on steering wheel  
**   - LED display of current gear, 
**   - Manual/Auto LED indicator, 
**   - Torque converter clutch lock-up indicator.
**   
**   In Auto mode: 
**   
**   - Gear is selected according to a 4 forward gear by 12 mph x 12 kpa 
**     shift table which selects the gear based on the current vehicle speed and load, 
**   - TCC engagement if load is below a threshold (in 4th gear), 
**   - TCC engagement in 2nd and 3rd gear if trans temp is high,
**   - In shift_mode==2, the trans can go directly to the target gear from the table, 
**     otherwise it shifts sequentially until it reaches the target gear.  
**   
**   In Manual mode: 
**   
**   - Full manual control, unless (optionally) rpm exceeds user-specified redline or drops below 
**     user-specified minimum, 
**   - TCC engagement only if transmission temperature is high (>180�F) and load is low. 
**   - The torque converter clutch always unlocks during downshifts. It unlocks under 
**     certain conditions during upshifts, depending on the vehicle speed and engine load. 
**     It is safe to upshift under light load while the TCC is locked. Most OEM systems 
**     keep the torque converter clutch applied during light throttle upshifts. 
**   
**   Note that MegaShift-4L60E only controls the forward gears. You still require a 
**   mechanical shifter to place the transmission into Park, Reverse, or Neutral. 
**   
**   As much as possible, the code for MegaShift-4L60E is based on the standard 
**   MegaSquirt code.
**   
**   The transmission temperature is sensed by a built in temperature sensor that 
**   has the standard GM sensor response curve. 
**   
**   The two MegaSquirt PWM channels are used for the pressure control solenoid and 
**   the 3/2 shift solenoid. All other controls are simple on/off switches. 
**   
**************************************************************************
**************************************************************************/

/*************************************************************************

Codewarrior Compiler for 4L60E Code for GPIO
--------------------------------------------

This code was developed entirely on Codewarrior 4.6 Special Edition. This compiler (actually a 
compiler/linker/locator) is available for free from Freescale (which used to be called Motorola):

http://www.freescale.com/webapp/sps/site/prod_summary.jsp?code=CWS-H12-STDED-CX

You have to register to get the software. (Note that the link may eventually become stale. If it 
does, start at www.freescale.com and search for 'codewarrior special edition')

The 'Special Edition' of Codewarrior has a 32 Kbyte code compile limit, 
and a maximum of 32 files (the number of files and code size are listed on the bottom 
left side of the Freescale Codewarrior IDE - integrated development environment). 
This code project is well under those limits at 23 files and ~20 Kbytes.

The files required to create a loadable S19 file for the GPIO board using Codewarrior are:

- main.c         (this file)
- reset.asm
- cltfactor.inc  (the file that is used to convert the trans temp ADC count into degrees Farenheit)
- loadfactor.inc (the file that is used to convert the load signal, MAP/TPS/MAF, into 'load' for 
                  the controller)
- Flash.asm
- flash.h
- hcs12def.h
- Monbitor_linker.prm (memory layout file)

Note that only main.c, Monitor_linker.prm, and loadfactor.inc files are different from the 
2.6 MS-II code. This project does not need the prcessor expert ("beans") enabled in the 
Codewarrior project, the required info is embedded into the above files. If you do decide 
to use the processor expert, you need to set it to 'MC9S12C64MFA' with the small memory model.

**************************************************************************/

/** The 9S12C64 Microcontroller *****************************************

The Freescale 9S12C64 microcontroller used on the GPIO board (and with this 
4L60E code) has a 16 bit, 24 MHz processor. 16 bit means it can address memory 
locations from:
 
  0000 0000 0000 0000 to 1111 1111 1111 1111 binary locations 
= 0 to 65535 decimal 
= 0x0000 to 0xFFFF hexadecimal

Note that in Codewarrior, any number starting with 0x (or 0X) is considered a 
hexadecimal number (example 0xF1B0) - sometimes written in documentation with a 
preceeeding $ or a following h, and any number stating with a 1 to 9 is considered 
a decimal number. C has no native syntax for expressing a binary number in source 
code (though one can be user defined - google it). Hexadecimal is used instead, 
and is convenient because of the smaller size, and each digit corresponds to a 
4 bit binary block.)
 
An easy way to convert between binary, hexadecimal, and decimal is to open the 
Windows calculator (Start/All Programs/Accessories/Calculator) in 'scientific' mode 
(under 'View') then click between the various formats (it has hexadecimal, decimal, 
octal, and binary). The number in the display is converted as you click different 
formats)

The 9S12C64 microcontroller has 3 types of memory:

1) 4 kilobytes of RAM (Ramdom Access Memory) - used for values that need to be changed 
   frequently (all the values that MegaTune normally changes are changed in RAM, except the 
   thermistor and MAF tables). They can then be 'burned to flash with the 'Burn to ECU' button. 
   RAM loses its contents if the power supply to the GPIO main board is interuppted, even very 
   briefly. At start-up these values are copied from the flash memory 
   (below) into the two 1Kbyte inpram. and in2ram. stuctures in RAM.
   
2) 64 kilobytes of flash EEPROM memory - flash EEPROM (Electrically Eraseable Programmable Read 
   Only Memory) memory is retained even if the power is removed for long periods. Flash 
   can be read easily, but is much slower and more difficult to write (since it is erasable only 
   in 1024-byte sectors, and must be erased before even a single bit can be written), 
   so it is used for program instructions and values that don't change 
   much (like the thermistor tables). Note that the compiler may refer to flash as 
   ROM (read only memory), because though it is not 'read-only' it serves much the 
   same purpose as ROM in some other microcontrollers. (Note that rumour has it that the 9S12C64 
   actually has 128 kiloBytes of flash memory, which can be accessed using the paging mechanism
   described below.)
   
3) Registers - used to configure the microcontroller (I/O pins, timer ports, 
   pulse width modulation (PWM), and to store the status of specific operations etc.)
   
The memory is addressed as a number from 0x0000 to 0xFFFF. The memory addresses are arranged 
as:

  0x0000 to 0x03FF - Registers
  0x03FF to 0x3000 - 16K fixed Flash EEPROM (page 0x003D)
  0x3000 to 0x3FFF - RAM (mappable to any 4K boundary)
  0x4000 to 0x7FFF - 16K fixed Flash EEPROM (fixed page 0x003E)
  0x8000 to 0xBFFF - 16K fixed Flash EEPROM (ppage = 0x003C)
  0xC000 to 0xFEFF - 16K fixed Flash EEPROM (fixed page 0x003F)
  0xFF00 to 0xFFFF - Interrupt Vectors (BDM if active)
  
and the memory 'segments' are defined in the monitor_linker.prm file in 
the PRM folder as:

SEGMENTS																		 
    RAM = READ_WRITE 0x3000 TO 0x3FFF;

    unbanked FLASH ROM
    ------------------
    CLT_ROM  = READ_ONLY    0x4000 TO 0x47FF;
    MAT_ROM  = READ_ONLY    0x4800 TO 0x4FFF;
 //   EGO_ROM  = READ_ONLY    0x5000 TO 0x53FF;
 //   MAF_ROM  = READ_ONLY    0x5400 TO 0x5BFF;
    INP_ROM  = READ_ONLY    0x5000 TO 0x63FF;
    OVF_ROM  = READ_ONLY    0x6400 TO 0x6BFF;
    ROM_7000 = READ_ONLY    0x7000 TO 0x7FFF;
    ROM_8000 = READ_ONLY    0x8000 TO 0xBFFF;
    
    banked FLASH ROM
    ----------------
    PAGE_3C  = READ_ONLY  0x3C8000 TO 0x3CBFFF;
    PAGE_3D  = READ_ONLY  0x3D8000 TO 0x3DBFFF;
    ROM_C000 = READ_ONLY    0xC000 TO 0xF77F; 

END

In the monitor_linker.prm file is a section called 'PLACEMENT' which 
contians the user-specified labels (you can a call them whatever you 
want - except for reserved words) that the pragma code_seg directives 
use. This allows us to separate the pragma statements from the memory 
addresses, and makes things like changing to other processor variants, 
etc. much easier. 

PLACEMENT
    _PRESTART, STARTUP,
    VIRTUAL_TABLE_SEGMENT,
    DEFAULT_ROM                  INTO  ROM_7000, PAGE_3C;
    OTHER_ROM                    INTO  PAGE_3D;
    NON_BANKED, RUNTIME,
    COPY                         INTO  ROM_C000;
    ROM_VAR, STRINGS             INTO  CLT_ROM,MAT_ROM,
                                       // EGO_ROM,MAF_ROM,
                                       INP_ROM,OVF_ROM;
    DEFAULT_RAM                  INTO  RAM;
END


'pragma's are directives to the compiler at compile time (not when the code is executed)
that direct the actions of the compiler in a particular portion of a program without 
affecting the program as a whole. Most commonly, we use them to specifiy where the compiler 
(actually the linker) should put things in memory.

The rpm file, pragma directives, and ppage statements in the code work together. The prm file
tells the comiler how the memory is organized. The pragma statments tell the linker where to 
put sections of the code in memnory. The ppage statements tell the running processor where to look 
in memory for code and values, etc.

Note that some apprently diffent memory areaes overlap:

- Flash locations in the range 0xC000 - 0xFFFF are the same as flash 
  locations 0x3F8000 - 0x3FBFFF.
- Flash locations in the range 0x4000 - 0x7FFF are the same as flash 
  locations 0x3E8000 - 0x3EBFFF.
- Flash locations in the range 0x0000 - 0x3FFF are the same as flash 
  locations 0x3D8000 - 0x3DBFFF.

Only the FLASH area 0x0000 - 0x3FFF overlaps with Flash area 0x3D8000 - 
0x3DBFFF. (Not the SFR registers and internal RAM). This is because the 
SFR Registers and internal RAM have higher priority from Flash in the bus 
arbitrator, so when they overlap with some of the Flash memory, they win 
precedence over the Flash that occupies the same address range, and so the 
internal Registers and RAM are accessed as needed, rather than the 
underlying Flash locations.

In other words, Flash portions that are not covered with SFR registers and 
internal RAM are accessible through the 0x0000 - 0x7FFF address range. 
These Flash locations are also accessible through PPAGE pages 0x3D and 0x3E 
in the 0x8000 - 0xBFFF program page window - where no Flash locations are 
usually covered by SFR registers or internal RAM (unless INITRM is set to 
this address range).

For more information on the prm file, ppaged memory, #pragmas and how to use them in C 
programming, see:

http://www.freescale.com/files/microcontrollers/doc/app_note/AN2216.pdf

or search the freescale site (http://www.freescale.com) for document AN2216.pdf 

In general, you won't need to know these unless you are doing a major re-working
of the code. For adding features, etc., you just need to know where specific 
variables are located.

Memory locations for specific variables can be found in the Monitor.map file that 
the linker creates. These locations will be very helpful (i.e., necessary) to write 
the INI file that MegaTune uses.

Also, note that all the variables you want to see in MegaTune, that aren't inputs, need 
to be in the outpc. stucture to be recognized by MegaTune (they also have to be 
set up in the INI file).

If you are looking for a concise introduction to programming the HCS12 (but for the 
D256 variant) try 

https://www.ee.nmt.edu/~rison/ee308_spr06/lectures.html 

(may have a security certificate error, proceed anyways)              

For a complete textbok on programming the HCS12 family of processors, including MegaSquirt-II's 
MC9S12C64MFA, see:

"The HCS12/9S12 : An Introduction to Hardware and Software Interfacing" by Han-Way Huang 

Hardcover: 880 pages 
Publisher: Delmar Learning
Language: English 
ISBN-10: 1401898122 
ISBN-13: 978-1401898120 

(This is the standard 3rd year university electrical engineering textbook on 
this topic. It also includes a CD with freeware compilers and IDEs, among other things.)

 **/
    

/** 4L60E/GPIO Port Assignments ******************************************
    ---------------------------

Function             Port        Circuit  AMP	  In MS-II was:
--------             ----        -------  ---		-------------
Sol A			           PE4          VB3     23			 FP
Sol B						     PM2          VB4     35			 FIdle
Sol32						     PT1          PWM4    34			 INJ1
PC							     PT2					PWM3    33			 PWM1
TCC							     PT3					PWM2		32			 INJ2
LED1						     PM4          GPO1    10			 AccelLED
LED2 						     PM3          GPO2     7			 InjLED
LED3  	  			     PM5          GPO3     8			 WarmLED
LED4  					     PB4          GPO4     9			 IACenable
switchA							 PAD00				GPI1     5			 MAP
switchB							 PAD01				GPI2     6		   IAT
switchC							 PAD03				EGT3    25			 TPS
Mode switch          PT5          VR3     14       IGN (out)
Paddle UP				     PT6          VR2		  15			 IAC1
Paddle DOWN			     PAD06        GPI5		 4			 JS5
VSS							     PT0					VR1			 2			 IRQ (tach)
Temp Sensor			     PAD02				GPI3		30			 CLT
Brake Sense			     PAD07				GPI4     3			 Knock
Speedo Output		     PT4					PWM1		31			 PWM1
Boost Control        PE0					VB1     11			 JP4 (spare)
non-CAN MAP/TPS/MAF  PAD05        EGT1    24       EGO
line pressure sensor PAD04        EGT2    27       ---
*/

/*
The line pressure guage is a 0-500 psi pressure sensor. It is Digi-Key MSP6907-ND for $114 
(www.digikey.com). 
It has:
 - a 0.5-4.5 Volt output (accuracy �5 psi), 
 - just a three wire hookup (5V, ground, and signal).
 - a 1/8 NPT fitting - the same as the trans. However, a 90� elbow, or maybe even some tubing, 
   will be required in most installs to keep the sensor away from the trans tunnel. 
 
The line pressure is available for datalogging the line pressure with the 4L60E code and the 
GPIO.
No part of the code is conditional on the line pressure,  but it is highly recommended to 
prevent damage to the transmission.
*/

/*
 Note that all the standard output transistors recommended for the circuits on the 
 GPIO board (TIP120 in PWMx, VB921 in VBx, ZTX450 in GPOx) are NPN - they are ON 
 (i.e., will allow current to flow) when the base (which is connected to the port pin
 through a 1K Ohm resistor) is pulled high, i.e., set to 1 in software, and 
 OFF if the port pin is set to zero. 
 (Note that if a pull-up circuit is used, 'current flowing' means an external 
 nominal voltage signal of zero (actually about 0.7 Volts) and 'no current flowing' 
 means full pull-up voltage is applied)
*/

/* ------------- CAN Communications -----------------------------------------------

CAN data is requested from the MS-II 8 bytes every can_var_rate (.128 ms tics) of the 
ISR_Timer_Clock(). The data is sent in the CanTxIsr() subroutine, and received in the
CanRxIsr() subroutine. The received data is the entire outpc. structure that MS-II 
normally sends to MegaTune. In the GPIO/4L60E, this data is placed into the msvar 
array, where the rpm and kPa are extracted for use in the program.

Every time a CAN transmit or recive routine is called, the CANtx or CANrx values in the 
outpc. stucture are incremented (up to 65535, then they roll-over). This can be helpful
for de-bugging CAN communications.

*/

/*
Port DDR (data direction register) Configs
------------------------------------------
Each of the I/O port pins on the 9S12C64 processor can be 
configured as either an input or an output. The way you tell 
the processor which should be what is by filling zeros 
or ones in the data direction registers.

0=input, 1=output, x=not assigned (default is input)

For the GPIO, we want:

Bit		 76543210     decimal   hexadecimal
			 --------     -------   -----------
PA     xxxxxxx1			 = 01      = 0x0001
PAD		 00xx0000			 = 00      = 0x0000
PB		 xxx1xxxx			 = 16      = 0x0010
PE		 xxx1xxx1			 = 16      = 0x0011
PM		 xx1111xx			 = 60      = 0x003C
PT		 x0011110			 = 30      = 0x001E

These registers are typically defined with DDRx names. Note that 
the data direction registers are memory addresses, 
but there are convenient names defined for these addresses 
in the hcs12def.h file (open it and look for things like DDRA for 
port A, etc.).

BTW, these user configurable input/output ports are called 
"general purpose I/O" ports which is where the GPIO board gets its name.

*/

/*
Misc. 4L60E Notes:
-----------------

Max ratio stall = 1.85
Minimum Overdrive (4th) speed = 41 mph

--------

VSS is 40 tooth VR on output shaft. Typical frequency is

24" diameter tire = 2 foot * 3.1415 = 6.283 feet = 0.00119 miles/tire rev

The driveshaft and output shaft turn at (1 tire rev)/(drive ratio output shaft rev). 
E.g. 1/3.08

= 0.0003864 mile/rev = 3.864 x 10^-4

So at 10 mph, we have: 

freq = [10 mph � (3600 sec/hr)] � 0.0003864 mile/rev * 40 teeth/rev

     = (10 * 40)/(3600*.0003864) = 287.6 teeth/sec (Hertz) 
     
     = 3.5 milliseconds/tooth
     
     at 100 mph, the frequency is 10x, and the time it 0.1x
     So range for 0 to 200 mph is 0 Hertz to ~6000 Hz.
     
NEED TO ADD TRIP ODOMETER FUNCTION

**/ 

/****************************************************************
******************* TRANSMISSION ERROR CODES ********************
*****************************************************************

Error is an unsigned char (0-255)

   bit    dec  description
   ---    ---  -----------
00000000 = 0   = functioning normally
00000001 = 1   = transmission temperature > 215�F
00000010 = 2   = shift did not happen
00000100 = 4   = shift blocked by over/under rev
00001000 = 8   = VSS input does not match engine input/gear
00010000 = 16  = gear requested exceeds number available
00100000 = 32  =
01000000 = 64  =
10000000 = 128 = 

**/

#include "hcs12def.h"      /* common defines and macros */
                           // This is where things like PORTE are set as names rather than memory addresses.
                           // This is done entirely for programmer convenience, so that you can write 
                           // PORTE instead of  0x0008

#include "flash.h"         // flashburner defines, structures
#include <string.h>        // standard C string handling functions

// #define CAN_TEST

#define FAR_TEXT1_ATTR
#define TEXT1_ATTR
#define EEPROM_ATTR
#define INTERRUPT interrupt
#define POST_INTERRUPT
#define ENABLE_INTERRUPTS  asm cli; // 'cli' is an assembly language directive 
                                    //  meaning 'enable interrupts'
#define DISABLE_INTERRUPTS asm sei; // 'sei' is an assembly language directive 
                                    //  meaning 'disable interrupts'
#define NEAR near
#define VECT_ATTR @0xFF80
                          // vector interrupt space from absolute address 
                          // $FF80 to $FFFF (65456 to 65535 decimal)

#pragma CODE_SEG NON_BANKED /* Interrupt section for this module. 
                               Placement will be in NON_BANKED area. */
                               
extern void near _Startup(void);       /* Startup routine */

/* When the HC9S12 processor 'sees' an interrupt, you can have it automatically
jump to part of the program which tells it what to do outside of the normal flow 
of the program. The code an interrupt jumps to is called an ISR (Interrupt 
Service Routine). The ISR is located at one of a specific set of memory 
locations called Interrupt Vectors that tell the HCS12 the address of the ISR
for each type of interrupt.

The HC9S12 know where to return to because the return address is pushed onto 
the stack before the HC9S12 jumps to the ISR. In assembly language you use 
the RTI (Return from Interrupt) instruction to pull the return address off 
of the stack when the processor exits the ISR.

Even if the ISR changes registers, all registers are pushed onto
stack before jumping to ISR, and pulled off the stack before returning to
program. When the processor executes the RTI instruction at the end of the ISR,
the registers are pulled off of the stack.

To return from the ISR You must return from the ISR using the RTI
instruction (return; in C). The RTI instruction tells the HCS12 to pull all the registers
off of the stack and return to the address where it was processing when
the interrupt occurred.
*/
                               
INTERRUPT void UnimplementedISR(void) POST_INTERRUPT;

INTERRUPT void VSS_timer0(void) POST_INTERRUPT;
INTERRUPT void ISR_TimerOverflow(void) POST_INTERRUPT;
INTERRUPT void ISR_Timer_Clock(void) POST_INTERRUPT;

INTERRUPT void ISR_SCI_Comm(void) POST_INTERRUPT;
INTERRUPT void CanTxIsr(void) POST_INTERRUPT;
INTERRUPT void CanRxIsr(void) POST_INTERRUPT;

typedef void (* NEAR tIsrFunc)(void);
const tIsrFunc _vect[] VECT_ATTR = {      /* Interrupt table placed at 0xFF80 */
        UnimplementedISR,                 /* vector 63 */
        UnimplementedISR,                 /* vector 62 */
        UnimplementedISR,                 /* vector 61 */
        UnimplementedISR,                 /* vector 60 */
        UnimplementedISR,                 /* vector 59 */
        UnimplementedISR,                 /* vector 58 */
        UnimplementedISR,                 /* vector 57 */
        UnimplementedISR,                 /* vector 56 */
        UnimplementedISR,                 /* vector 55 */
        UnimplementedISR,                 /* vector 54 */
        UnimplementedISR,                 /* vector 53 */
        UnimplementedISR,                 /* vector 52 */
        UnimplementedISR,                 /* vector 51 */
        UnimplementedISR,                 /* vector 50 */
        UnimplementedISR,                 /* vector 49 */
        UnimplementedISR,                 /* vector 48 */
        UnimplementedISR,                 /* vector 47 */
        UnimplementedISR,                 /* vector 46 */
        UnimplementedISR,                 /* vector 45 */
        UnimplementedISR,                 /* vector 44 */
        UnimplementedISR,                 /* vector 43 */
        UnimplementedISR,                 /* vector 42 */
        UnimplementedISR,                 /* vector 41 */
        UnimplementedISR,                 /* vector 40 */              
        CanTxIsr,                         /* vector 39 */
        CanRxIsr,                         /* vector 38 */
        CanRxIsr,                         /* vector 37 */
        UnimplementedISR,                 /* vector 36 */
        UnimplementedISR,                 /* vector 35 */
        UnimplementedISR,                 /* vector 34 */
        UnimplementedISR,                 /* vector 33 */
        UnimplementedISR,                 /* vector 32 */
        UnimplementedISR,                 /* vector 31 */
        UnimplementedISR,                 /* vector 30 */
        UnimplementedISR,                 /* vector 29 */
        UnimplementedISR,                 /* vector 28 */
        UnimplementedISR,                 /* vector 27 */
        UnimplementedISR,                 /* vector 26 */
        UnimplementedISR,                 /* vector 25 */
        UnimplementedISR,                 /* vector 24 */
        UnimplementedISR,                 /* vector 23 */
        UnimplementedISR,                 /* vector 22 */
        UnimplementedISR,                 /* vector 21 */
        ISR_SCI_Comm,                     /* vector 20 */      
        UnimplementedISR,                 /* vector 19 */
        UnimplementedISR,                 /* vector 18 */
        UnimplementedISR,                 /* vector 17 */
        ISR_TimerOverflow,                /* vector 16 */
        UnimplementedISR,                 /* vector 15 */
        UnimplementedISR,                 /* vector 14 */
        UnimplementedISR,                 /* vector 13 */
        UnimplementedISR,                 /* vector 12 */
        UnimplementedISR,                 /* vector 11 */
        UnimplementedISR,                 /* vector 10 */
        UnimplementedISR,                 /* vector 09 */
        VSS_timer0,                       /* vector 08 */
        ISR_Timer_Clock,                  /* vector 07 */
        UnimplementedISR,                 /* vector 06 */
        UnimplementedISR,                 /* vector 05 */
        UnimplementedISR,                 /* vector 04 */
        UnimplementedISR,                 /* vector 03 */
        UnimplementedISR,                 /* vector 02 */
        UnimplementedISR,                 /* vector 01 */
        _Startup                          /* Reset vector */
   };
 

// ******************************** FUNCTION PROTOYPES **********************************   
// Functions must be 'prototyped' before they are used. A function protoype
// looks like this:
//
//    void function_name(variable_type variable_name1, variable_type variable_name2, ...);
//
// where:
//       - the first item is the type of the value the function returns 
//         ('void' means the function does not return a value), 
//       - the function_name must be unique and not one of the reserved words,
//       -  a list of the variable passed (preceeded by their data type) is 
//          enclosed is round brackets. 
//
// The names in the function must match those in the function itself, but not necessarily those
// that are passed from the main program (or other function).
//
// Note that the variables passed to the function must match the variable type 
// (int, char, unsigned char, etc.) listed in the function protoypes (below).
//
// Also note that some operations will require different types for the variables being 
// operated on. The can often be temporarily 'cast' to another type using the () operator. 
// This seems confusing, but the gist of it is that if you have a variable that is a char 
// that needs to be a int to be compatible with a specific calculation, you can just 
// add "(int)" in front of it in the code.
//
// For example:
//         int integer_number;            // normally an integer number 
//    		 char letter='A';               
//				 integer_number=(int)letter;    // which works because letter is treated as an int because of cast
 
#pragma CODE_SEG ROM_7000

void switch_page(unsigned char sub_no);

int intrp_1dctable(char sgnx, int x, unsigned char n, unsigned int * x_table, 
  char sgnz, unsigned char * z_table) TEXT1_ATTR;

int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny,
  unsigned int * x_table, int * y_table, unsigned char * z_table) TEXT1_ATTR;
unsigned int intrp_1ditable(unsigned int x, unsigned char nx, 
                  unsigned int * x_table, unsigned int * z_table) TEXT1_ATTR;

unsigned char auto_gear_lookup(int x, int y, unsigned char nx, unsigned char ny,
  unsigned int * x_table, int * y_table, unsigned char * z_table) TEXT1_ATTR; 
  
void blink_gpo3(int flashCount);
void waitAwhile(unsigned short int cycles); 

#pragma CODE_SEG DEFAULT    // end CODE_SEG NON_BANKED for interrupts
   
#include "cltfactor.inc"    // cltfactor.inc defines a 1024 x 1 lookup table.
                            // we set the ADC level to be ten bits later: 
                            // 2�� = 0000 0100 0000 0000 = 1024 = 0x0400 
                            // Each value corresponds to one ADC increment,
                            // giving the temperature*10 in Farenheit 
                            // at that ADC value.
                            // The ADC start at 0 Volts for ADC count value of 0, 
                            // and run to 5.00 Volts at ADC 1024
                            // (so each ADC represent a difference of 5.00/1024
                            // or 0.00488 Volts/count)
#include "sprfactor.inc"    // spare table for future use                          
#include "linefactor.inc"   // linefactor.inc defines a 1024 x 1 lookup table that turns a
                            // 0 to 5 volt signal into transmission line pressure from 0 to 
                            // 500 psi (990 = signal output out-of-range low, 999 = signal 
                            // output out of range high)                         
#include "loadfactor.inc"   // loadfactor.inc defines a 1024 x 1 lookup table that takes a 
                            // 0 to 5 Volt signal, and turns it into a 'load' signal
                            // The input can be MAP, TPS, or MAF
#define NO_TBLES 	16        // tables are cltfactor, sprfactor, presfactor, loadfactor, 
                            // in1flash, in2flash, txbuf, outpc (+blank tables)
#define NO_LOADS 	12        // Number of loads in shift table and pressure control table
#define NO_MPH  	12	      // Number of MPH in shift table auto gear table
#define NO_TEMPS 	10        // number of temps in temperature table(s)
#define NO_GEARS   5        // Number of Gears (1-4 forward gears), 0 for neutral/reverse
#define NPORT      7        // Number of spare ports
#define SER_TOUT   5        // Serial time out in seconds (if corrupt data received)
#define NO_MSVAR_BYTES  112 // Number of variable bytes in the received data from MS-II

// CAN and Serial defined values
#define	MSG_CMD	        0
#define	MSG_REQ	        1
#define	MSG_RSP	        2
#define	MSG_XSUB	      3
#define	MSG_BURN	      4
#define	NO_VAR_BLKS	   16
#define NO_CANMSG      10
#define MAX_CANBOARDS  16

// Error status words: 
//    -bits 0-7 are current errors
//    -bits 8-15 are corresponding latched errors
#define	XMT_ERR		   	0x0101
#define	CLR_XMT_ERR		0xFFFE
#define	XMT_TOUT	  	0x0202
#define	CLR_XMT_TOUT	0xFFFD
#define	RCV_ERR		  	0x0404
#define	CLR_RCV_ERR		0xFFFB
#define	SYS_ERR		    0x0808

// User inputs from MegaTune - 1 set in flash first, then copy into RAM
// To access the RAM variables, use the inpram. prefix

typedef struct {
// In C, variables must be 'declared' before they can be used. 
// Their 'variable type' must be given.
//
// This code uses a number of variable types:
//
//   Type             Bits              Range               MegaTune INI Designation
//   ----             ----              -----               ------------------------
// - char               8            -128 to 127                    S08
// - unsigned char      8               0 to 255                    U08
// .................................................................................
// - int               16          -32768 to 32767                  S16
//   (== short)
// - unsigned int      16               0 to 65535                  U16
//   (== unsigned short int)
// .................................................................................
// - long              32            -2�� to 2�� - 1                S32
//   (== long int)     
// - unsigned long     32               0 to 2�� - 1                U32
//   (== unsigned long int)
// .................................................................................
//
// Variables are declared with a statement like: 
//     data_type variable_name;
//            or
//     data_type variable_name = 16; 
//     (which also initializes the variable)
//
// See the application of this below.
//
// In general, the shortest variable that will do the job
// (spans the range of values needed in any circumstance by the program) 
// uses the least space, leaving more room in memory for 
// other variables and code.
//
// These variable declarations can be prefaced by: 
// - const (doesn't change ever - and can be set as 
//   constant by the compiler)
// - volatile (can be changed externally
//   to the program - by a register result, for example)
// 
// In addiiton to the range of value a variable type can hold, there are issues associated with
// what happens if the range is exceeded. In general, the value 'wraps around'. That is,
// it goes to the next value in the range as if the highest and lowest values were connected.
//
// For example, for an unsigned int, you would have:
// 
// 0,1,2,3,4,.....253,254,255,0,1,2,3....
//
// so adding 1 to 255 = 0, add 2 to 255 = 1, etc. This is often referred to as 'overflow'.
// Similarly, subtracting 1 from 0 is 255, and so on. This is sometimes called 'underflow'.
//
// This is easier to understand if you look at the binary representation that 
// the CPU actually uses:
//
//   255 in binary forum is: 11111111, and this uses 8 bits (short int). Add 1 to it, and you get
//   1+11111111 = 100000000
//
// But when you try to stuff that into the 8 bits of an unsigned int, the most significant 
// bit (MSB - the leftmost) is truncated, so you get 00000000
//
// This applies not just to constants stored in variables, but also to calculations. Suppose 
// you have two variables with values A=10000 and B=20000, and you want to multiply then to get C.
//
// You might expect C = A*B = 10000 * 20000 = 200000000
// However, if C is an unsigned short, then C will equal 20000000 % 255 = 95 
// (% is the modulus, the remainder after dividing the first number by the
// second number as many times as it will go)
// However, if C is an unsigned int, then C will equal 20000000 % 65535 = 11825
// Only if C is a long does it equal the expected value of 20000000.
//
// Note that this wrap-around can also happen in intermediate calculations, so you
// must be aware of this in the order you write assignments if you expect large numbers. 
// For example setting: 
// C = (A/B) * 1000000;
// may not give the same result as: 
// C = (A*1000) / (B*1000); 
// if the intermediate calculations overflow or undeflow (and this can depend on the type 
// of A, B, and C).
// (Note that values grouped in parenthesis are calculated first, creating the intermediate 
// results.)
//
// Also be aware that the compiler (and the HSC12 processor) does not recognized 
// 'floating point' numbers (those with non-zero decimal portions), in either 
// constants or intermediate calculations. 
//
// In another example, suppose you want to multiply A by PI=3.1415926... to get B. 
// You cannot write 
// B = A * 3.14159; (since decimal values are not allowed), 
// instead you have to write something like 
// B = (A * 314159)/100000; (and you can see why things *might* overflow!)
//
// Finally, you should not write
// B = A * (314159/100000); since the intermediate value (314159/100000) will be 
// truncated to 3 (no floating point numbers), and the result will be B = A * 3;
// (Note: also see 'casts'.)
//
// The Codewarrior compiler will often produce a "possible loss of data" warning if
// it can determine that a calculation or variable might overflow, but it is not 
// fool-proof by any means.

unsigned char InputCaptureEdge;    // Bit 0: Input capture: 0 = falling edge, 1 rising
unsigned int vss_table[NO_MPH];    // vehicle speed tables
int LOAD_table[NO_LOADS], temp_table[NO_TEMPS];  // deg x 10 (C or F)
unsigned int gear_table[NO_GEARS]; // gear ratio * 1000

char clt0, cltmult;

int noTCC_temp,           // no TCC lock below this temp (to aid warm-up)
    minTCC_gear;          // minimum gear (1-4) for TCC lock   

unsigned int noTCC_load;  // no TCC lock above this load
char tempFac;		          // temperature adjustment to PWM% for cold trans

unsigned char PulseTol;   // % tolerance for next input pulse

unsigned int axle_ratio;  // rear axle ratio * 1000 (i.e. 3.08 = 3080),
unsigned int tire_diam,   // tire diameter (inches * 100)
         under_rev_limit; // Do not allow upshift if it we result in rpm less than under_rev_limit

unsigned char trans_type, // transmission type, 0=4L60E, 1=4R70W
 shift_mode;              // 0=manual, 1=auto (sequential shift), 2=auto (skip shift)
 
unsigned char Metric_Units, // 0= coolant & mat in deg F; 1= deg C
 error_check;             // 0 = no error checking (stim), 1 = take action on reported errors  
 
int rpm_limit;            // Max engine rpm for downshift

unsigned char debounce;   // Threshold for switch manifold debounce
                          // Initially set to mid-point,
                          // incemented/decremented by one each time through loop
                          // until all switchs are <10% or >90% of debounce value

unsigned char no_teeth;   // divide factor for input VSS pulses (teeth/rev)
 
unsigned int
 TCC_PWM_Pd,			        // TCC PWM period (usec) - keep between 10-25 KHz (100-40 us)
 PC_PWM_Pd,               // PC PWM period (usec) - keep between 10-25 KHz (100-40 us)
 SOL32_PWM_Pd;            // Sol32 PWM period (usec)

unsigned char 
 BatFac,                  // Battery PWM correction factor (% (x10) per volt)
 rpmLF,LOADLF,            // Lag filter coefficients for ADC
 adcLF;  
unsigned long baud;       // baud rate

unsigned char board_type; // board type (1-255) of this board;
                          // type=0, reserved.
                          // type=1, ECU (MS II)
                          // type=2, Router board
                          // type=3, Generic I/O board
                          // type=4, Transmission Controller, ......
unsigned char mycan_id;   // can_id (address) of this board (< MAX_CANBOARDS). Always 0 for ECU.
unsigned int can_var_rate;// rate at which to get CAN variables from MS-II -
                          // 8 bytes (4 words) per can_var_rate, 0.128 ms tics

unsigned char auto_table[NO_LOADS][NO_MPH];   // The target gear for each LOAD & MPH

unsigned char pc_table[NO_LOADS][NO_MPH];     // The PWM% for PC solenoid by load and mph

unsigned int trans_temp_limit; // Apply TCC in all applicable gears above this temp, regardless of load

unsigned int pulse_mile;   // pulses per mile for speedometer

unsigned char rpm_check;   // 0 if no rpm checking (stim or non-CAN), 1 if rpm checking
                                       
} inputs1;

 typedef struct {
  int spare1;   // spare1 in in2ram
  int spare2;   // spare2 in in2ram
  int spare3;   // spare3 in in2ram
  int spare4;   // spare4 in in2ram
  int spare5;   // spare5 in in2ram
  int spare6;   // spare6 in in2ram
  int spare7;   // spare7 in in2ram
  int spare8;   // spare8 in in2ram
  int spare9;   // spare9 in in2ram
  int spare10;  // spare10 in in2ram
  int spare11;  // spare11 in in2ram
  int spare12;  // spare12 in in2ram
  int spare13;  // spare13 in in2ram
  int spare14;  // spare14 in in2ram
  int spare15;  // spare15 in in2ram    
 } inputs2;

#pragma ROM_VAR INP_ROM
// flash copy of inputs - initialized

#define SECTOR_BYTES         1024 // bytes
#define SECTOR_WORDS         ((int)(SECTOR_BYTES/2))
#define N_SECTORS(theStruct) ((int)((sizeof(theStruct)+SECTOR_BYTES-1)/SECTOR_BYTES))
#define N_PADDING(theStruct) ((int)(N_SECTORS(theStruct)*SECTOR_BYTES - sizeof(theStruct)))


// Assign values to the in1flash parameters
const inputs1 in1flash EEPROM_ATTR = {
0,                // input capture edge for VSS
{10,              // vss_table[MPH no = 0] , MPH (KPH), use for 12x12 auto shift table
  20,30,40,50,60,70,80,90,100,120,140},
{45,              // LOAD_table[LOAD/tps no = 0],  kPa, for use in 12x12 auto shift table
  50,55,60,65,70,75,80,85,90,95,100},
{-400,            // temp_table[TEMP no = 0],  deg x 10
    -200,0,200,400,600,800,1000,1300,1600},
{2290,3060,1630,1000,700}, // gear_table[], gear ratios * 1000	 (0th is reverse)
0,100,            // clt0, cltmult        deg (C or F) x 10
800,              // noTCC_temp (degrees x10)
3,                // minTCC_gear
900,              // noTCC_load (kpa x10),
10,			  	      // tempFac for PWM% (+%/100 degrees below 170)
25,               // PulseTol,     % tolerance for next input pulse timing during    
                  // normal running    
3080,2600,        // axle_ratio, tire_diam
1500,             // under_rev_limt - no upshift if it will result in less rpm than this
0,                // trans_type, 0=4L60E
1,                // shift_mode; 0=manual, 1=auto (sequential), 2=auto (skip shift)
0,                // Measure_Units,    0= deg F/inches/mi/hr; 1= deg C/cm/km/hr
0,                // error checking
6500,             // rpm_imit,  Max rpm after downshift
100,              // Switch manifold debounce facor (start at 1/2 of debounce, run until 
                  // less than 0.1*debounce or > 0.9*debounce)
40,               // no_teeth, divide factor for input VSS pulses, 40 teeth for 4L60E VSS
31250,            // TCC_PWM_Pd (us) (=32 Hertz) - not used at present
3413,             // PC_PWM_Pd,  Injector PWM period (us) (=293 Hz)
20000,            // SOL32_PWM_Pd, 3/2 Solenoid period (us) (=50 Hz)
80,               // BatFac,  Battery PWM correction factor (% (x10) per volt)
50,50,75,         // Lag factors
115200,           // baud rate
3,                // board type
1,                // mycan_id of this board, always 0 for ECU, 1 for all others until configured
78,               // can_var_rate; rate at which to get CAN variables from MS-II -
                  // 8 bytes (4 words) per can_var_rate in 0.128 ms tics;
                  // can get all 112 bytes of outpc in 14 x canvarate tics.

/* 12x12 Target Gear Table for Auto Mode **/   
{{1,            // auto_table[LOAD no=0][MPH no=0],  target gear
    2,3,3,4,4,4,4,4,4,4,4},
{1,             // auto_table[LOAD no=1][MPH no=0],  target gear
    2,2,3,3,4,4,4,4,4,4,4},
{1,             // auto_table[LOAD no=2][MPH no=0],  target gear
    2,2,3,3,3,4,4,4,4,4,4},
{1,             // auto_table[LOAD no=3][MPH no=0],  target gear
    2,2,2,3,3,4,4,4,4,4,4},
{1,             // auto_table[LOAD no=4][MPH no=0],  target gear
    1,2,2,2,3,3,4,4,4,4,4},
{1,             // auto_table[LOAD no=5][MPH no=0],  target gear
    1,2,2,2,3,3,3,4,4,4,4},
{1,             // auto_table[LOAD no=6][MPH no=0],  target gear
    1,2,2,2,3,3,3,4,4,4,4},
{1,             // auto_table[LOAD no=7][MPH no=0],  target gear
    1,2,2,2,3,3,3,3,4,4,4},
{1,             // auto_table[LOAD no=8][MPH no=0],  target gear
    1,2,2,2,3,3,3,3,4,4,4},
{1,             // auto_table[LOAD no=9][MPH no=0],  target gear
    1,1,2,2,2,3,3,3,3,4,4},
{1,             // auto_table[LOAD no=10][MPH no=0], target gear
    1,1,2,2,2,2,3,3,3,4,4},
{1,             // auto_table[LOAD no=11][MPH no=0], target gear
    1,1,2,2,2,2,2,3,3,3,4}},

/* 12x12 Pressure Control PWM Table **/   
{{60,             // pc_table[LOAD no=0][MPH no=0], PWM%
    60,60,60,60,60,60,60,60,60,60,60},
{55,             // pc_table[LOAD no=1][MPH no=0],  PWM%
    55,55,55,55,55,55,55,55,55,55,55},
{50,             // pc_table[LOAD no=2][MPH no=0],  PWM%
    50,50,50,50,50,50,50,50,50,50,50},
{45,             // pc_table[LOAD no=3][MPH no=0],  PWM%
    45,45,45,45,45,45,45,45,45,45,45},
{40,             // pc_table[LOAD no=4][MPH no=0],  PWM%
    40,40,40,40,40,40,40,40,40,40,40},
{35,             // pc_table[LOAD no=5][MPH no=0],  PWM%
    35,35,35,35,35,35,35,35,35,35,35},
{30,             // pc_table[LOAD no=6][MPH no=0],  PWM%
    30,30,30,30,30,30,30,30,30,30,30},
{25,             // pc_table[LOAD no=7][MPH no=0],  PWM%
    25,25,25,25,25,25,25,25,25,25,25},
{20,             // pc_table[LOAD no=8][MPH no=0],  PWM%
    20,20,20,20,20,20,20,20,20,20,20},
{15,             // pc_table[LOAD no=9][MPH no=0],  PWM%
    15,15,15,15,15,15,15,15,15,15,15},
{10,             // pc_table[LOAD no=10][MPH no=0], PWM%
    10,10,10,10,10,10,10,10,10,10,10},
{0,             // pc_table[LOAD no=11][MPH no=0],  PWM%
     0,0,0,0,0,0,0,0,0,0,0}},
    
210,     // trans temperature limit
2002,    // pulse/mile for speedometer output
0        // no rpm_check (for stim or non-CAN)
};

// Pad in1flash to end of sector
const unsigned char in1padding[N_PADDING(inputs1)] EEPROM_ATTR ={0}; 

// Put values in in2flash spare variables (spare1 to spare29)
const inputs2 in2flash EEPROM_ATTR = {
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15
};

// Pad in2flash to end of sector    
const unsigned char in2padding[N_PADDING(inputs2)] EEPROM_ATTR ={0}; 
                                // 1024 bytes = 1 block flash
#pragma ROM_VAR DEFAULT

#pragma ROM_VAR OVF_ROM

// IAC stepper motor sequence
const unsigned char IACCoilA[8] = {0,0,1,1,0,0,1,1};
const unsigned char IACCoilB[8] = {1,0,0,1,1,0,0,1};
// array of TC overflow numbers at which to increment seconds counter.
const unsigned int TC_ovfla[60] = {
23,
46,
69,
92,
114,
137,
160,
183,
206,
229,
252,
275,
298,
320,
343,
366,
389,
412,
435,
458,
481,
504,
526,
549,
572,
595,
618,
641,
664,
687,
710,
732,
755,
778,
801,
824,
847,
870,
893,
916,
938,
961,
984,
1007,
1030,
1053,
1076,
1099,
1122,
1144,
1167,
1190,
1213,
1236,
1259,
1282,
1305,
1328,
1350,
1373
};

const char RevNum[20] =  {    // revision no:
  // only change for major rev and/or interface change. (Last char is added by MS-II)
    "4L60/gpio 0.96     "
  // 123456789.123456789. 
},
 Signature[32] = {            // program title.
  // Change this every time you tweak a feature.  (Last char is added by MS-II)
    "**  V0.96 4L60E/gpio by B&G  **"
  // 123456789.123456789.123456789.12
 };

// #pragma ROM_VAR DEFAULT

// RAM copy of inputs
// This is where the RAM variables (copied from flash memory) get 
// their inpram. & in2ram. prefixes (inpram = INPut RAM)
 
inputs1 inpram;
inputs2 in2ram;

// The variables declared below are global, and available to all functions
int last_tps,last_LOAD;
char switchA, switchB, switchC;          // switch states for switch manifold gear determination
unsigned char switchAacc, switchBacc, switchCacc; // switch state debounce variables
unsigned int Fl1OCt_overflow,Fl2OCt_overflow;
unsigned char pulse_no,ICint,ICintmask[2],OCint,OCintmask[2];
int ICt_overflow[2],IgnOCt_overflow[2];
volatile unsigned short *pTIC[2],*pTOC[2];
unsigned long IgnTimerComp[2],dtpred;
unsigned int mms,millisec,burn_flag;
unsigned int TC_ovflow,TC_ov_ix;
unsigned long lmms,t_enable_IC[2],t_chgoff,
  ltch_lmms,
  rcv_timeout,adc_lmms;
unsigned char flocker,tpsaclk;
unsigned char next_adc,first_adc,
	txmode,tble_idx,burn_idx,    // txmode is 1st received character in serial comms, burn_idx is the table to burn
	reinit_flag;
unsigned int speedo_total,speedo_toggle,speedo_counter;
unsigned long ic_period[20],ic_duty[20]; // VSS period and duty cycle 
                                          // array - take the average
unsigned char ic_count;
unsigned int adcval;                                          

/* Clocks:
  	- igncount: counts up each tach pulse, cleared when hit VSSdivide pulses
               (and injection occurs).
  	- asecount: counts up each time igncount = 0 (each VSSdivide pulses).
  	- egocount: counts up each tach pulse, cleared when hits EgoCountCmp
  	- tpsaclk: counts every .1 sec
  	- altcount: flips 0,1,0,1... on each injection, resulting in firing alternate
                injector banks if Alternate option.
*/
unsigned txcnt,txgoal,rxoffset,rxnbytes,rxcnt,kill_ser_t,vfy_fail,
  can_mmsclk,can_varoff;
char kill_ser,vfy_flg;

// CAN variables
unsigned long  cansendclk,canclk,ltch_CAN=0xFFFFFFFF;
char *canvar_blkptr[NO_VAR_BLKS];
unsigned short can_status;
unsigned char can_clr_stat,can_reset,can_id,getCANdat=0,burnCANdat=0,sendCANAdj=0;
struct canmsg {
  /* CAN Xmt mssge ring buffer:
      can[0] is to hold Rx,TxISR messages,
      can[1] for main loop messages (so don't get clobbered by ISR)
      cxno = no msgs in queue waiting to be sent out
      cxno_in = index for inserting a msg in queue (incr after insert)
      cxno_out = index for sending out a msg (incr after load CAN buf)
      msg_type = CMD,REQ,RESP,XSUB (= set value, request value, respond 
          to a request for value, execute a subroutine)
      varblk,varoffset,varbyte = blk no of data structure, byte offset 
          from start of structure, no. bytes of data
      datbuf = the actual data (max of 8 bytes)
      dest = id no. of device to which msg being sent. 
  */
  unsigned char cxno,cxno_in,cxno_out;
  unsigned char cx_msg_type[NO_CANMSG], cx_myvarblk[NO_CANMSG], 
	  cx_destvarblk[NO_CANMSG], cx_dest[NO_CANMSG],cx_varbyt[NO_CANMSG];
  unsigned short cx_myvaroff[NO_CANMSG],cx_destvaroff[NO_CANMSG];
  unsigned char cx_datbuf[NO_CANMSG][8];  // max msg data = 8 bytes
}  can[2];   // end canmsg structure

// pointers for spare port pins
volatile unsigned char *pPTMpin[8], *pPTTpin[8], *pPTApin0;
unsigned char dummyReg,lst_pval[NPORT];

// allocate space in ram for flash burner core
volatile unsigned char RamBurnPgm[36]; 

// rs232 Outputs to PC via Serial Port
typedef struct {
unsigned int seconds,rpm;                 // clock, driveshaft rpm
unsigned int speedo;                      // vehicle speed (x10) 
                                          // (in mph or kph, depending on Metric_Units)
char auto_mode;                           // 0=manual, 1=auto
char upshift_request;                     // shift up - button or other (such as CAN)
char downshift_request;                   // shift down - button or other (such as CAN)
signed char manual_gear;                  // manual valve position determined from switch manifold
signed char current_gear;                 // current gear determined from state of shift solenoids
signed char target_gear;                  // next gear to select
char lock_TCC;                            // 0=unlock torque converter clutch, 1=lock clutch
int engine_rpm;                           // engine rpm (from CAN or estimated from VSS and gear)
unsigned char error;
int LOAD,clt,brake;                       // LOAD - kpa x 10
                                          // clt deg(C/F)x 10
                                          // tps - % x 10
                                          // batt - volts x 10
                                          // brake - volts x 100
unsigned long ic_per_avg, ic_dc_avg;      // average of last 20                                     
int LOAD_short,LOAD_long;                 // LOAD averages
                                          // kPax10 per 0.1 sec
int kpa;																	// LOAD
unsigned char PC_duty;                    // Pressure Control valve duty cyle (% for PWM)
unsigned char converter_slip;
unsigned long loop_count;                 // Main loop counter 
unsigned long vss_teeth;                  // VSS tooth counter
unsigned int linepressure;                // line pressure
unsigned long odo;                        // trip odometer
unsigned int CANtx, CANrx;                // CAN transmit & recieve counters
unsigned int dbug;                        // spare debugging variable brought out in MegaTune
unsigned int vBatt;                       // battery voltage from MS-II via CAN (x10)
unsigned char upbutton, downbutton;       // shift button status
} variables;     // end outpc. structure

variables outpc, txbuf;

// Create a buffer variable to hold the MS-II output variables 
// recieved over CAN for use by GPIO
unsigned char msvar[NO_MSVAR_BYTES];
  // rpm is 2 bytes at offset=6,
  // kPa is 2 bytes at offset=18, and is x10.

#pragma ROM_VAR OVF_ROM

// Define a stucture for the table descriptor that gives the address in RAM, the address 
// in Flash (ROM), and the number of bytes
typedef struct {
   unsigned int *addrRam;
   unsigned int *addrFlash;
   unsigned int  n_bytes;
} tableDescriptor;

// Use the tableDescriptor stucture we just defined to create an array of table descriptions 
// sized NO_TABLES. They will hold the descriptions of the tables (and the indexes are used 
// in MegaTune to 'locate' the table with command like a06, which refers to the 6th table). 
// These are constant. That is, the descriptions - memory locations - of the tables are constant, 
// even if the elements of the tables change values.

const tableDescriptor tables[NO_TBLES] =  {
  //  RAM copy                    FLASH copy                     Table Size 
  {  NULL,                   (unsigned int *)cltfactor_table,  sizeof(cltfactor_table)  },
  {  NULL,                   (unsigned int *)sprfactor_table,  sizeof(sprfactor_table)  },
  {  NULL,                   (unsigned int *)linefactor_table, sizeof(linefactor_table) }, 
  {  NULL,                   (unsigned int *)loadfactor_table, sizeof(loadfactor_table) }, 
  { (unsigned int *)&inpram, (unsigned int *)&in1flash,        sizeof(inputs1)          }, 
  { (unsigned int *)&in2ram, (unsigned int *)&in2flash,        sizeof(inputs2)          },
  { (unsigned int *)&txbuf,   NULL,                            sizeof(txbuf)            },
  { (unsigned int *)&outpc,   NULL,                            sizeof(outpc)            },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        },
  {  NULL,                    NULL,                            0                        }
};


#pragma ROM_VAR DEFAULT  // codewarrior generates error message here, is seemingly OK though

// Calculate and define values for working with tables based on the tables array we just created.
#define tableInit(iTable) (void)memcpy(tables[iTable].addrRam, tables[iTable].addrFlash, tables[iTable].n_bytes)
#define tableByteRam(iTable, iByte)   ((unsigned char *)tables[iTable].addrRam + iByte)
#define tableWordRam(iTable, iWord)   (tables[iTable].addrRam + iWord)
#define tableByteFlash(iTable, iByte) ((unsigned char *)tables[iTable].addrFlash + iByte)
#define tableWordFlash(iTable, iWord) (tables[iTable].addrFlash + iWord)
#define tableBytes(iTable)            (tables[iTable].n_bytes)
#define tableWords(iTable)            ((tables[iTable].n_bytes+1)/2) // Round up

// Prototypes - Note: ISRs (interrupt service routines) are prototyped above.
// These are the function prototypes.

#pragma CODE_SEG MAIN_ROM
void main(void) FAR_TEXT1_ATTR; // the main function, i.e., where the program starts
void tcc_lock (void); 
void tcc_unlock (void);
void reset_gear (void);
void reset_switches(void);

#pragma CODE_SEG DEFAULT
void vss_reset(void);
int get_adc(char chan1); // this is the function that reads the ADC channels
void fburner(unsigned int* progAdr, unsigned int* bufferPtr, 
  					 unsigned int no_words) TEXT1_ATTR; // fburner(pointer to destination, pointer to source, number of words)
// void set_spr_port(char port, char val) TEXT1_ATTR;
// int intrp_2ditable(unsigned int x, int y, unsigned char nx, unsigned char ny,
//        unsigned int * x_table, int * y_table, int * z_table) TEXT1_ATTR;
void CanInit(void) TEXT1_ATTR;
void can_xsub01(void) FAR_TEXT1_ATTR;
void Flash_Init(unsigned long oscclk) TEXT1_ATTR;
void Flash_Erase_Sector(unsigned int *address) FAR_TEXT1_ATTR;
void Flash_Write_Word(unsigned int *address, unsigned int data) TEXT1_ATTR;

#pragma CODE_SEG DEFAULT

extern void reboot(void) FAR_TEXT1_ATTR;
extern void monitor(void) FAR_TEXT1_ATTR;
extern void SpSub(void);
extern void NoOp(void);

// ---------------------------------------------------------------------------------------
// ---------------------------------- Function MAIN --------------------------------------
// ---------------------------------------------------------------------------------------
 
#pragma CODE_SEG MAIN_ROM // Put following code in MAIN_ROM == ppage 0x3C
 
void main(void) {
// This is where the program actually starts. Everything before this was mainly telling the 
// compiler what and where to put values in memory when we load the code using the serial 
// monitor. So the values above exist in the S19 file we will make (that set up both the 
// program and the defaults) but the above doesn't affect the program on a restart of 
// MegaSquirt (which is why the flash memory retains its values).

// First set-up variables, etc., then go into an infinte loop
int ix;
// unsigned int utmp1;
long ltmp;
unsigned long ultmp, ultmp1, ultmp2;
unsigned char tempPC1;
int tempPC2;                   // temp variable to hold PC solenoid duty cycle while we adjust and error check it
int LOADlongcount, LOADshortcount; // Running LOAD averages
unsigned int scount, lcount;  // Loop counts for LOAD
int UP_avg, DWN_avg;          // Shift button 'pressed' status, request shift only 
                              // after reaches user threshold
int LOAD;                     // get from CAN/MS2 or TPS/MAP on EGT1
char SOLAst;                  // Set SOLB state indicator
char SOLBst;                  // Set SOLB state indicator
long VSSdivide;               // Calculated from number of teeth on VSS sensor, etc. 
char lock_TCC = 0;
int engine_rpm = 0;
int upperswitch, lowerswitch; // switch thresholds for switch manifold debounce
long ic_per_acc,ic_dc_acc;    // averaging subtotals
int rev_distance;             // tire revs/mile or revs/km

PPAGE = 0x3C;  // PPAGE register is defined in hcs12def.h 
               // as memory location 0x0030

 /* In order to access the 16K flash blocks in the address range 0x8000�.0xBFFF 
    the PPAGE register (0X0030) must be loaded with the corresponding value for 
    this range:

    PAGE    PAGE Visible with PPAGE Contents
    ----    --------------------------------    
    0x3C    $3C
    0x3D    $3D
    0x3E    $3E
    0x3F    $3F

    For the MC9S12C64, the flash page 3F is also visible in the 0xC000�.0xFFFF range 
    if ROMON is set. 
    For the MC9S12C64, the flash page 3E is also visible in the 0x4000�.0x7FFF range
    if ROMHM is cleared and ROMON is set. 
    For the MC9S12C64, the flash page 3D is also visible in the 0x0000�.0x3FFF range 
    if ROMON is set...  
    
    The main code is responsible for switching PPAGE as needed to select the 
    desired 16K block of memory to be mapped into the "window" from $8000-$BFFF.
    */

  //  Initalize PLL (Phase Locked Loop) - reset default 
  //  is Oscillator clock 8 MHz oscillator, 
  //  PLL freq = 48 MHz, 24 MHz bus, 
  //  divide by 16 for timer of 2/3 usec tic
  PLLCTL &= 0xBF;     // Turn off PLL so can change freq
  SYNR    = 0x02;     // set PLL/ Bus freq to 48/ 24 MHz
  REFDV   = 0x00;
  PLLCTL |= 0x40;     // Turn on PLL
  
  // wait for PLL lock
  while (!(CRGFLG & 0x08)); // Note that & is a bitwise operator, && is the relational operator
  CLKSEL = 0x80;      // select PLL as clock
  
  // wait for clock transition to finish
  for (ix = 0; ix < 60; ix++);
  // open flash programming capability
  Flash_Init(8000);
  // inp_spare used to force inpflash, flashve_table into sectors.
  // Must use in program or it won't use up the entire sector. Codewarrior 
  // ignores the pragma making this stupid statement necessary.
  ix = in1padding[0];
  ix = in2padding[0];
      
  if((int)RamBurnPgm & 0x0001)	{                    // odd address - copy to even one
    (void)memcpy((void *)RamBurnPgm,NoOp,1);         // copy noop to 1st location
    (void)memcpy((void *)&RamBurnPgm[1],SpSub,32);   // copy flashburn core program to RAM
  }  // End if((int)RamBurnPgm & 0x0001)
  else                                               // even address
    (void)memcpy((void *)RamBurnPgm,SpSub,32);       // copy flashburn core program to RAM

  // load all user inputs from Flash to RAM
  // indices are from 'const tableDescriptor tables[NO_TBLES]' and start at 0
  tableInit(4);
  tableInit(5);
  
  // set up Input/Ouput ports (MS-II usage in braces)
  //    - port M2 is SolB (was fast idle solenoid)
  //    - port M3 is LED2 (was inj led)
  //    - port M4 is LED1 (was accel led)
  //    - port M5 is LED3 (was warmup led)
  //    - port E0 is not used (can only be an input, because it is tied to IRQ/XIRQ)
  //    - port E4 is SolA (was fuel pump)
  //    - port P5 is bootload pin (input)
  //    - port T0 is VSS (Tach input)
  //    - port T1 is Sol32 (was INJ1)
  //    - port T2 is PC solenoid (was PWM1)
  //    - port T3 is TCC solenmoid (was INJ2)
  //    - port T4 is Speedo output (was PWM2)
  //    - port T6 is padde upshift (was IAC Coil A)
  //    - port PAD00 is switchA
  //    - port PAD01 is switchB
  //    - port PAD02 is Temp Sensor
  //    - port PAD03 is switchC
  //    - port PAD04 is line pressure sensor
  //    - port PAD05 is non-CAN MAP/TPS/MAF
  //    - port PAD06 is Paddle DOWN
  //    - port PAD07 is Brake Sense
  //    - port T7 is not used (was IAC Coil B)
  //    - port B4 is LED4 (was IAC Enable)
  //    - port A0 is nitrous control (was Knock Enable)

  // ** GPIO/4L60E I/O Port Data Direction Register Settings ***
  //
	//************************************************************
	
  DDRA |= 0x01;    // port A0 - output; 0x01 = 00000001
  DDRB |= 0x10;    // port B4 - output; 0x10 = 00010000
  DDRE |= 0x10;	   // port E4 - output; 0x10 = 00010000     
  DDRM |= 0x3C;    // port M2, M3, M4, M5 are  outputs, full drive by default; 0x3C = 00111100 
  DDRT |= 0x3E;    // port T1, T2, T3, T4, T5 are outputs, T0, T6 are inputs; 0x3E = 00111110
  
  /** Note: ***************************************************  
  serial Tx =  PS1
  serial Rx =  PS0
  CAN    Tx =	 PM1
  CAN    Rx =  PM0
 **************************************************************/ 
   
  //  Set pointers to real port addresses
  for(ix = 0; ix < 8; ix++)  {
    pPTMpin[ix] = pPTM;   // port M
    pPTTpin[ix] = pPTT;   // port T
  } // end for (ix = 0...
  
  pPTApin0 = pPORTA;      // port A pin 0

  // Enable Timer Input Capture for PT0 (Vehicle Speed Sensor - VSS - on circuit VR1)
  // --------------------------------------------------------------------------------
  // To use the Input Capture Function:
  //      - Enable the timer subsystem (set TEN bit of TSCR1)
  //      - Set the prescaler (TSCR2)
  // The 9S12 allows you to slow down the clock which drives the counter.
  // You can slow down the clock by dividing the 24 MHz clock by 2, 4, 8, 16, 32, 64 or 128.
  // You do this by writing to the prescaler bits (PR2:0) of the Timer System Control
  // Register 2 (TSCR2) Register at address 0x004D.
  //
  //   PR  Divide    Freq          Overflow Time
  //   000    1      24 MHz          2.7307 ms
  //   001    2      12 MHz          5.4613 ms
  //   010    4       6 MHz         10.9227 ms
  //   011    8       3 MHz         21.8453 ms
  //   100   16       1.5 MHz       43.6907 ms
  //   101   32       0.75 MHz      87.3813 ms
  //   110   64       0.375 MHz    174.7627 ms
  //   111  128       0.1875 MHz   349.5253 ms
  //
  // The clock ticks are counted in the variable TCNT (which is defined in the hsc12def.h file 
  // at memory location 0x0044). There are 0xFFFF 'tics' (65,536 counts).
  // In our case each 'tic' is 1/0.1875MHz  = 5.333 �s
  //
  // To se up timer:
  //      - Tell the 9S12 that you want to use a particular pin of PORTT for input
  //         capture
  //      - Tell the 9S12 which edge (rising, falling, or either) you want to capture
  //      - Tell the 9S12 if you want an interrupt to be generated when the
  //         cature occurs
  
//  TIOS |= 0x3E; // TIOS = Timer Input capture/Output compare Select register
  TIOS = 0x3E;  // Input Capture or Output Compare Channel Configuration:
                //  - 0 The corresponding channel acts as an input capture
                //  - 1 The corresponding channel acts as an output compare
                // 0x3E = 00111110
                // Timer ch 0 (PT0/IOC0) = IC, ch 1-5 = OC & PWM, (T4 is speedo output) 
                // ch 6,7 = I/O output
  TSCR1 = 0x80; // Timer System Control Register 1, 10000000 = set port T0 to use timer 
                // (Timer Enable (TEN) bit = 1)
  TSCR2 = 0x07; // Timer System Control Register 12, 0x07 = 00000111
                // Set prescaler to divide by 128
                // 349.53 millisecond overflow time, 5.333 �sec period
                // for a 40 tooth input wheel, this is
                // 1/(0.34953*40) = 0.0715 rev/sec, less than 1/2 mile/hour
                // if teeth come is at 2 x 5.333 �sec, this is about
                // 1/(0.00001066*40) = 2344 revs/sec, much faster than your car can go!
  TCTL4 = (TCTL4 | 0x01) & ~0x02; // Capture Falling Edge (TCTL4 = xxxxx10) 
                                  // VRx circuits are designed to trigger on falling edge
                                  // Set edge to capture (EDGxB EDGxA of TCTL 3-4 regs)
                                  // EDGxB EDGxA
                                  //   0     0     Disabled
                                  //   0     1     Rising Edge
                                  //   1     0     Falling Edge
                                  //   1     1     Either Edge
                                  //
                                  //   address  register bit7  bit6  bit5  bit4  bit3  bit2  bit1  bit0
                                  //   0x004A    TCTL3   EDG7B EDG7A EDG6B EDG6A EDG5B EDG5A EDG4B EDG4A
                                  //   0x004B    TCTL4   EDG4B EDG3A EDG2B EDG2A EDG1B EDG1A EDG0B EDG0A
                                  // 0x02 = 00000010, ~0x04 = ~00000100 = 11111011
                                  
  TFLG1 = 0x01;                   // Clear IC0 Flag
  TIE  |= 0x01;                   // Set Timer Interrupt Enable (TIE) on pin 0

  // turn off output pins
  *pPTMpin[2] &= ~0x04;      // 0x04 = 00000100, ~0x04 = 11111011, turn off    Sol B @ PM2
   SOLBst = 0;               // SOLAst, SOLBst = 1 -> P,R,N,1
  *pPTMpin[3] &= ~0x08;      // 0x08 = 00001000, ~0x08 = 11110111, turn off     LED2 @ PM3
  *pPTMpin[4] &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off     LED1 @ PM4
  *pPTMpin[5] &= ~0x20;      // 0x20 = 00100000, ~0x20 = 11011111, turn off     LED3 @ PM5
   PORTE      &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off    Sol A @ PE4
   SOLAst = 0;               // SOLAst, SOLBst = 1 -> P,R,N,1
  *pPTTpin[6] &= ~0xC0;      // 0xC0 = 11000000, ~0xC0 = 00111111, turn off PaddleUp @ PT6, PT7 not used
  *pPTTpin[7] &= ~0xC0;
  *pPTTpin[4] &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off speedo output @ PT4
  *pPTApin0   &= ~0x01;      // no brake signal

  PUCR |= 0x12;              // enable pullups for ports E, B and A;    0x12 = 18 =  00010010

  DDRP  = 0x00;              // port P all inputs
  PERP  = 0xFF;              // enable pullup resistance for port P;    0xFF = 255 = 11111111

  DDRJ &= 0x3F;              // port J pins 0 to 5 outputs,             0x3F = 00111111
  PERJ |= 0xC0;              // enable pullup resistance for port J6,7; 0xC0 = 192 = 11000000 

  DDRS &= 0xF3;              // port S all outputs, except 2,3;         0xF3 = 11110011
  PERS |= 0x0C;              // enable pullup resistance for port S2,3; 0x0C =  12 = 00001100

  reinit_flag = 0;

  // set up CRG RTI Interrupt for .128 ms clock. CRG from 8MHz oscillator.
  mms      = 0;     // 0.128 ms tics
  millisec = 0;     // 1.024 ms clock (8 tics) for adcs
  lmms     = 0;
  cansendclk = 7812;
  ltch_lmms = 0;
  can_mmsclk = 0;   // .128 ms tics
  outpc.seconds = 0;// (1.0035) seconds
  burn_flag  = 0;
  ic_count=0;
 
  RTICTL  = 0x10;   // load timeout register for .128 ms (smallest possible)
  CRGINT |= 0x80;   // enable interrupt
  CRGFLG  = 0x80;   // clear interrupt flag (0 writes have no effect)

  // ------ Set up SCI Serial Communications Interface (RS232) ----------------
  /* To use SCI0 for 8 bit, non-parity communication:

      1. Select the baud rate via SCI0BDL Baud Rate Register, at memory 
         location 0x00C9 - defined in hcs12def.h (set below). 
         SCI baud rate = SCI module clock / (16 x BR) =
                       = 24,000,000/ (16*baud)
                       = 1,500,000/baud
                       = for baud == 115200 this is 13.0  

      2. Enable transmission and/or reception as desired by setting the 
         TIE (Transmitter Interrupt Enable) and RIE (Receiver Full 
         Interrupt Enable Bit) bits in the SCI0CR2 Control Register,
         which is at memory location 0x00CB. 
         This is register is set up as:
         
         bit        7       6      5      4      3     2     1     0
                   TIE    TCIE    RIE    ILIE    TE    RE   RWU   SBK

      3. Tramit and Receive, in interrupt service routine ISR_SCI_Comm():

           - transmit  - poll the TDRE flag and write data to the SCI0DRL register at 
                         address $00D7 when the TDRE flag is set. The data we write 
                         to the SCI0DRL register (0x00CF) is the data that will be 
                         immediately output from the TX pin. 
           - receive   - poll the RDRF register (SCI0SR1). When the RDRF status 
                         register (0x00CC) is set ((SCI0SR1 & 0x20) <> 0), we can then 
                         read the incoming data by reading the SCI1DRL register. The 
                         data we read in from this register is the input via the RX pin. 
*/
// Set the baud rate
  SCI0BDL = (unsigned char)(1500000/inpram.baud);   // = 1500000/115200 = 13
//  SCI0BDH = 0;
  ltmp = (150000000/inpram.baud) - ((long)SCI0BDL*100);
  if(ltmp > 50) SCI0BDL++;   // round up

  SCI0CR1 = 0x00;   // = 00000000 -> parity even -- 0000000x 
                    //               parity disabled
                    //               idle char starts after start bit
                    //               idle line wakeup
                    //               one start bit, eight data, one stop
                    //               only meaningful if LOOPS =1
                    //               SCI enabled in wait mode
                    //               LOOP disabled -- x0000000
                    
  SCI0CR2 = 0x24;   // = 00100100 -> TIE = 0                            -- x0000000, 
                    //               RIE = 1,(enable recieve interuupt) -- 00x00000,
                    //               TE  = 0 (Transmitter Enable Bit)   -- 0000x000, 
                    //               RE  = 1 (Receiver Enable Bit)      -- 00000x00,
                    //  (these will be reconfigured in ISR_SCI_Comm()) 
  txcnt     = 0;
  rxcnt     = 0;
  txmode    = 0;    // receive == 0, transmit > 0
  txgoal    = 0;
  kill_ser  = 0;
  vfy_flg   = 0;
  rcv_timeout = 0xFFFFFFFF;

/* --------------- Using the HCS12 PWM -----------------------------------------------

We want PWM on ports T1, T2, T3, & T4 (T0 is VSS input)
We want:
          Sol32:  T1 = PWM4 =  50 Hz = 0.0200 second period, variable duty cycle
          PC:     T2 = PWM3 = 293 Hz = 0.0034 second period, variable duty cycle
          TCC:    T3 = PWM2 =  50 Hz = 0.0200 second period, variable duty cycle
                  Note that we will only use the TCC in on/off mnode, so freq not important         

1. Choose 8-bit mode (PWMCTL = 0x00)  Can use 16 bit mode, we don't need it.
2. Choose high polarity (PWMPOL = 0xFF)
3. Choose left-aligned (PWMCAE = 0x00)
4. Select clock mode in PWMCLK:
     - PCLKn = 0 for 2N,
     - PCLKn = 1 for 2(N+1) � M,
5. Select N in PWMPRCLK register:
     - PCKA for channels 5, 4, 1, 0;
     - PCKB for channels 7, 6, 3, 2.
6. If PCLKn = 1, select M = 1 to 255
     - PWMSCLA = M for channels 5, 4, 1, 0
     - PWMSCLB = M for channels 7, 6, 3, 2.
7. Select PWMPERn, normally between 100 and 255 (increased values give increased control resolution).
8. Enable desired PWM channels: PWME 1 and 3 for us so PWME |= 0x0A
9. Select PWMDTYn, normally between 0 and PWMPERn. Then
     Duty Cycle n = (PWMDTYn/PWMPERn) � 100%
   Change duty cycle to control speed of motor or intensity of light, etc.
10. For 0% duty cycle, choose PWMDTYn = 0
*/
 
// Set up PWM outputs - PWM 1, 2, 3, & 4
// PT0,1,4,5 are on clkA; 
// PT2,3,6,7 are on clkB
  MODRR    = 0x06; // Make Port T pins 1 & 2 be PWM (0x1E = 00000110), PC and 3/2 SOL (PT3 is on/off TCC)
                   // The MODRR register allows for mapping 
                   // of PWM channels to port T in the absence of port P 
                   // pins for the low pin count processors (like the 
                   // 48 pin MS-II processor).
  PWME     = 0x00; // disable PWMs initially  
  PWMPOL   = 0x1E; // polarity = 1, => go hi when start
 
// Set prescaler to 16. This divides bus clk (= PLLCLK/2 = 24 MHz)
// by 16. This gives 1.5 MHz timer, 1 tic = 2/3 usec.
// PWMPERx should be between 100 and 255, as it is 8 bit number 
// (100 gives us 1% increments, 255 is absolute maximum)
// We need to scale the clocks to get a 'tic' rate that worksd for both 
// the 50 and 293 Hertz requirements 

  PWMCLK   = 0x1E; // select scaled clocks SB, SA
  
  PWMPRCLK = 0x22; // prescale A,B clocks = bus/4 = 24/6 = 6 MHz
                   // 0x22 = 00100010 -> divide clock by 4

  PWMSCLA  = 0xFF; // PWM clk = SA clk/(2*SCLA) = 6MHz/510 = 0.085 msec clk
                   // for  50 Hertz, we will have 235 tic periods
                   
  PWMSCLB  = 0x2A; // PWM clk = SB clk/(2*SCLB) =  6MHz/84 = 0.014 msec clk
                   // for 293 Hertz, we will have 243 tic periods
                   
  PWMCAE   = 0x00; // standard left align pulse:
                   //                            -----
                   //                           |     |______
                   //                             duty
                   //                            <--period--->

//  TSCR2 = 0x04;    // Set to 0x07 above
  TSCR2 |= 0x80;   // enable timer overflow interrupt (TOI)
  pTIC[0] = pTC0;
  ICintmask[0] = 0x01;
  pTOC[0] = pTC5;

  TCTL2 |= 0x88;   // bit OM1,3,5 = 1. OC output line high or 
	                 // low iaw OL1,3,5 (not toggle or disable)
  TCTL1 |= 0x08;
  TSCR2 |= 0x80;   // enable Timer Overflow Interrupt (TOI)
  CFORC |= 0x20;   // force output in OL5 onto OC pin

  // set PWM period (usec) for 3/2 solenoid 
  PWMPER1 = (unsigned char) ((((long)inpram.SOL32_PWM_Pd*11765)+500000)/1000000);
                                                  // (period and duty cycle constant)
                                                  // 90% PWM% except 0% in first and 3/2 downshift
                                                  // 11.765 is (1/0.085) ticks/millisecond
                                                  // nominal value is 50 Hertz = 20 milliseconds 
                                                  // -> PWMPER1 = 20 * 11.765 = 235 tics
                                                  // 1000000 is because SOL32_PWM_Pd is in usec, not msec
                                                  //   *and* 11765 is supposed to be 11.765
                                                  // +500000 is for proper rounding (rather than truncate)
 
  // set PWM period (usec) for pressure control solenoid                                                       

  PWMPER2 = (unsigned char) ((((long)inpram.PC_PWM_Pd*71430)+500000)/1000000);
                                                  // (period is constant, duty cycle varies with load
                                                  //  as set in pc_table[12x12])
                                                  // 71.43 is (1/0.014) ticks/millisecond
                                                  // nominal value is 293 Hertz = 3.41 milliseconds 
                                                  // -> PWMPER1 = 3.41 * 71.43 = 243 tics
  
  // set PWM period (usec) for torque converter clutch                                   
  // Not used at present - TCC is on/off
  // PWMPER3 = (unsigned char) ((((long)inpram.TCC_PWM_Pd*71430)+500000)/1000000);      
                                                  // (period is constant, DC varies from 0% to 100%)
                                                  // (period and duty cycle constant)

  // Initialize PWM Duty Cycles
  PWMDTY1 = (uchar)((90*PWMPER1)/100);   // set Sol32 PWM duty to 90%
  PWMDTY2 =    0;   // set PC PWM duty to 0%
  PWMDTY3 =    0;   // set TCC PWM duty to 0%
  PWMCNT1 = 0x00;   // clear counter (reset PWM)

  pTIC[1] = pTC0;
  ICintmask[1] = 0x01;
  pTOC[1] = pTC5;
  t_chgoff = 0xFFFFFFFF;
 
  //  vss_reset();   // DEBUG this is causing the processor to crash = -  a counter running out of range???   
 
  // Set up ADCs so they continuously convert, then read result registers
  // every millisecond
  
  /* USING THE HCS12 A/D CONVERTER 
    (* indicates the values we use for GPIO/4L60E)
     
     0. Set up which pins are analog/digital converters and which are digital
        inputs using the ATD0DIEN register, 1=digital intput, 0=ADC
     
     1. Power up A/D Converter (ADPU = 1 in ATD0CTL2)

     2. Select number of conversions per sequence (S8C S4C S2C S1C in ATD0CTL3)
          S8C S4C S2C S1C = 0001 to 0111 for 1 to 7 conversions
          S8C S4C S2C S1C = 0000 or 1xxx for 8 conversions

     3. Set up resolution in ATD0CTL4 position 7 (0-7)
         - For  8-bit mode ( 255 divisions) write 1 to ATD0CTL4 position 7
         - For 10-bit mode (1024 divisions) write 0 to ATD0CTL4 position 7
           
     4. Select DJM in ATD0CTL5 position 7
        (a) DJM = 0 => Left justified data in the result registers
        (b) DJM = 1 => Right justified data in the result registers *

     5. Select DSGN in ATD0CTL5 position 6
        (a) DSGN = 0 => Unsigned data representation in the result register *
        (b) DSGN = 1 => Signed data representation in the result register
        The Available Result Data Formats are shown in the following table:
        
        SRES8 DJM DSGN   Data Format
        1      0    0    8-bit/left justified/unsigned   - Bits 15-8
        1      0    1    8-bit/left justified/signed     - Bits 15-8
        1      1    X    8-bit/right justified/unsigned  - Bits  7-0
        0      0    0    10-bit/left justified/unsigned  - Bits 15-6
        0      0    1    10-bit/left justified/signed    - Bits 15-6
        0      1    X    10-bit/right justified/unsigned - Bits  9-0 *

     6. Select SCAN in ATD0CTL5 postion 5:
         - SCAN = 0: Convert one sequence, then stop
         - SCAN = 1: Convert continuously *

     7. Select MULT in ATD0CTL5 position 4:
         - MULT = 0: Convert one channel eight the specified number of times
         - Choose channel to convert with CC, CB, CA of ATD0CTL5.
         - MULT = 1: Convert across several channels. CC, CB, CA of ATD0CTL5
           is the first channel to be converted. *

     8. After writing to ATD0CTL5, the A/D converter starts, and the SCF bit
        is cleared. After a sequence of conversions is completed, the SCF flag in
        ATD0STAT0 is set. You can read the results in ATD0DRx [0-7]H.

     9. If SCAN = 0, you need to write to ATD0CTL5 to start a new sequence. If
        SCAN = 1, the conversions continue automatically, and you can read new
        values in ADR[0-7]H.

     10. To get an interrupt after the sequence of conversions are completed, set
         ASCIE bit of ATD0CTL2. After the sequence of conversions, the ASCIF bit
         in ATD0CTL2 will be set, and an interrupt will be generated.

     11. On HCS12 EVBU, AD0 channels 0 and 1 are used to determine start-up
         program (D-Bug12, EEPROM or bootloader). Do not use AD0 channels
         0 or 1 unless absolutely necessary (you need more thann 14 A/D
         channels).

     12. To interpret the result, ATD0DRx = (Vin - VRL) / (VRH - VRL) * 1024
         Normally, VRL = 0 V, and VRH = 5 V, so ATD0DRx = (Vin / 5 V) * 1024
         Example: ATD0DR0 = 351 => Vin = 448/1024 * 5.0 = 1.71 V
  */

/*  We want ADC on PAD02 (temp), PAD04 (line pressure), and PAD05 (non_CAN MAP) only 
switchA							 PAD00				GPI1     5			 MAP
switchB							 PAD01				GPI2     6		   IAT
Temp Sensor			     PAD02				GPI3		30			 CLT
switchC							 PAD03				EGT3    25			 TPS
line pressure sensor PAD04        EGT2    27       ---
non-CAN MAP/TPS/MAF  PAD05        EGT1    24       EGO
Paddle DOWN			     PAD06        GPI5		 4			 JS5
Brake Sense			     PAD07				GPI4     3			 Knock
*/
  ATD0DIEN = 0xCB;  // 1=digital intput, 0=ADC
                    // 0xCB = 11001011
                    // ADC on temperature sensor, line pressure sensor, optional MAP only
                    // rest are digital inputs
                    // read digital input pins from PORTAD0
  next_adc =    0;	// specifies next ADC channel to be read
  ATD0CTL2 = 0x40;  // leave interrupt disabled, set fast flag clear
  ATD0CTL3 = 0x00;  // do 8 conversions/ sequence
  ATD0CTL4 = 0x67;  // 10-bit resolution, 16 tic conversion (max accuracy),
                    // prescaler divide by 16 => 2/3 us tic x 18 tics
                    // 0x67 = 01100111
  ATD0CTL5 = 0xB0;  // right justified, unsigned, continuous conversion,
                    // sample 8 channels starting with AN0
                    // 0xB0 = 10110000
  ATD0CTL2 |= 0x80; // turn on ADC0
  
  // wait for ADC engine charge-up or P/S ramp-up
  for(ix = 0; ix < 160; ix++)  {
    while(!(ATD0STAT0 >> 7));	    // wait til conversion complete
    ATD0STAT0 = 0x80;
  }  // end for (ix = 0...
  
  // get all adc values
  first_adc = 1;
  outpc.clt = get_adc(2); // get temperature
              // get_adc takes adc port to 
              // be checked as argument, returns adcval
     if (inpram.Metric_Units) outpc.clt = (outpc.clt-32)*5/9;  // Convert to Celsius if metric units                         
  outpc.linepressure = get_adc(4); // get line pressure
  outpc.kpa = get_adc(5); // get non-CAN MAP           
  first_adc = 0;
  adc_lmms = lmms;

	/* set variable block addresses to be used for 
	    CAN communications */
  canvar_blkptr[0] = (char *)&inpram;
  canvar_blkptr[1] = (char *)&outpc;
  canvar_blkptr[2] = (char *)msvar;
  
  for(ix = 3; ix < NO_VAR_BLKS; ix++)  {	 // rest are spares for now
    canvar_blkptr[ix] = 0;
  } // end for(ix =3 ...

// Initialize outpc. variables
  flocker = 0;  
  last_LOAD = 1000;         // kpa x 10
  outpc.LOAD_short = 1000; 
  outpc.LOAD_long = outpc.LOAD_short;
  outpc.speedo = 0;
  outpc.auto_mode = 1;
  outpc.manual_gear = 99;   // set to 99 to poll immediately
  outpc.target_gear = 0;
  outpc.current_gear = 1;
  outpc.lock_TCC =0;
  outpc.PC_duty = 0;
  outpc.vss_teeth = 0;
  outpc.loop_count = 0;
  outpc.kpa = 100;
  outpc.rpm = 0;
  outpc.dbug = 0;           // zero unless set to something else

  /* Initialize CAN comms */
  can_reset = 0;
  can_id = inpram.mycan_id;
  can_varoff = 0;  
  CanInit();

  // Make IC highest priority interrupt
  // On HCS12 processors, you can promote a single interrupt source to 
  // have the highest priority by writing the LSB address of the appropriate 
  // interrupt vector to the HPRIO register.
  // The interrupt priority has meaning only when two or more interrupt requests 
  // are being processed at the same time to determine which ISR will be 
  // executed first. Once a certain ISR has started to execute, the interrupt 
  // priority does NOT mean another interrupt can interrupt the already executing ISR.
  //   HPRIO = 0xEE;
  
// enable global interrupts
ENABLE_INTERRUPTS
  
// Restart PWM counters
PWMCNT1 = 0x00;
PWMCNT2 = 0x00;  // clear counter
PWMCNT3 = 0x00;  // clear counter
PWMCNT4 = 0x00;
PWME   |= 0x06;  // enable PWM 1 & 2 (3/2 SOL, PC), 3 is TCC - on/off



// --------------------- GPIO/4L60E ----------------------------------------------------

// Initialize Switch Manifold Variables
// set to 3rd gear == 110, we will poll the switches immediately so this isn't crucial
switchA = 1;
switchB = 1;
switchC = 0;

// Initialize deboune parameters (reinitialize when shifting gears)
switchAacc = inpram.debounce >> 1;   // right shift the bits 1 position 
switchBacc = inpram.debounce >> 1;   // this is the same as dividing by 2 and discarding the remainder)
switchCacc = inpram.debounce >> 1;   // for example 01100111 (decimal 103) becomes 00110011 (51)

// VSSdivide = (long) (11250060 / inpram.no_teeth); // This is the revs/mile or revs/km
                                                 // = 281252 for 40 teeth
                                                 
VSSdivide = 62832 * inpram.tire_diam/(inpram.axle_ratio * inpram.no_teeth); // inches x 100
                                                                                // or cm x 100
     // Ex. for 26" tire, 3.08 axle, 40 teeth, VSSdivide = 1326 = 13.26 inches per 20 teeth 

// Initialize local variables
LOAD =   100;
scount =   0;
lcount =   0;

// rev_mile = (12*5280*10000/314159)/(inpram.tire_diam/100)                                                 

if (!inpram.Metric_Units)  // rev/mile
  {  
  rev_distance = (int)(2016811/(long)inpram.tire_diam);
  }
else // rev/km
  {
  rev_distance = (int)(3183099/(long)inpram.tire_diam);
  }
// with 3.08 gears, 26" tires and 40 teeth, this should be 775 revs/mile 
  
  
// Set pulse/mile speedo counters
//
// number of VSS pulses/mile is:
//
//   rev_distance * axle_ratio * no_teeth
//
//
// the speedo output total count is:
//
//   (VSS pulses/mile)/(speedo pulse mile)
//
//   
 ultmp1 = ((long)rev_distance * (long)inpram.axle_ratio)/1000;
 ultmp2 = (ultmp1 * (long)inpram.no_teeth)/(long)inpram.pulse_mile;
// speedo_total = (int)((rev_distance *inpram.axle_ratio)/(inpram.pulse_mile*100)*inpram.no_teeth);
speedo_total = (int) (ultmp2);
// these are used in the VSS interrupt routine VSS_timer0 
speedo_toggle = speedo_total/2; // we will create approximately 50% duty cycle signal
speedo_counter = 0;             // initialize speedo-counter

 
// Ex. for 26" tire, 3.08 gears and 40 teeth, with a 2002 pulse/mile output, this is:
//   speedo_total = (775 * 3.080 * 40)/2002 =  47.692 => 47
//   speedo_toggle = 23


// Initialize switch de-bounce values
upperswitch = ((inpram.debounce * 90)/100);
lowerswitch = ((inpram.debounce * 10)/100);

// Set up COP timeout for main loop
// COPCTL = 0x44;     // 01000100 = 2^20 OSCCLK cycles = 131 ms timeout for reboot
// COPCTL = 0x42;      // 0100010 = 2^16 OSCCLK cycles = 131 ms timeout for reboot         
  
// 4L60E MAIN LOOP starts here ---------------------------------------------------------------------------
// Start an infinite FOR loop to keep cycling through the main loop
//  main loop
MAIN_LOOP:
for (;;)  {

outpc.loop_count++;

// Clear COP timeout counter
// When COP is enabled, the program must write 0x0055 and 0x00AA (in this order) to the ARMCOP
// register during the selected time-out period. As soon as this is done, the COP time-out period
// is restarted. If the program fails to do this and the COP times out, the processor will reset. 
// Also, if any value other than 0x0055 or 0x00AA is written, the processor is immediately reset.
// ARMCOP = 0x0055;
// ARMCOP = 0x00AA;	
	
// - check ADCs
// - set pressure control PWM
// - compute speed
// - lookup manual_gear from switchA, switchB, switchC
//   - if manual_gear = target gear then stop shift_timer
// - if upshift button pressed, shift up
//        - start shift_timer
// - if downshift button pressed shift down
//        - start shift_timer
//
// - set TCC
// - if     current_gear > minTCC_gear && clt > noTCC_temp 
//
//   - if LOAD < noTCC_load then
//       - lock_TCC
//     - else unlock TCC unless rpm indicates high speed
//   - end if
//   - if Trans_Temperature > trans_temp_limit
//     - lock TCC 
//     - end if    
//
//
// - if auto_mode {
//    - lookup target_gear from rpm/LOAD
//    - if target_gear < current gear then
//        - shift_down
//        - start shift_timer 
//	  - else if target_geat > current_gear then
//        - shift up
//        - start shift_timer
//		}
//
// - if manual_mode {
//   - if engine_rpm < min_rpm then
//        - shift_down
//        - start shift_timer
//   - else if engine_rpm > max_rpm then
//        - shift up
//        - start shift_timer
//   }
//
//  - read/estimate engine rpm
//
//

// CHECK ADC

/*-- UPDATE ROLLING LOAD AVERAGE 

The LOAD average is kept on two levels:

- a short term LOAD average (LOAD_short) created by adding 1/LOADshortcount of the 
  difference between the current LOAD_short and measured LOAD to the previous LOAD 
  short term average (updated everytime through the main loop), used to smooth 
  the LOAD signal,
  
- a long term LOAD average (LOAD_long), used to detrermine driving mode 
  created by adding 1/LOADlongcount of the difference between the 
  current LOAD_short and LOAD_long to the previous LOAD_long term average 
  (updated everytime through the main loop) IF braking is not applied  

The LOAD averages are used to determine the 'aggressiveness' of the driving, 
and hence the shift strategy. Higher values indicate more agressive driving.

The intent is that the LOADshortcount ~ 100 to 250 (assumning a 1KHz main loop, 
This is about 0.10 to 0.25 seconds). The intent is that the LOADlongcount ~ 100 to 500 
(assuming a 1KHz main loop). 

 --*/

/*  -----  UPDATE RPM & MAP ---------------------------------------------- */
// Update outpc. rpm, battery voltage and MAP for calculations using CAN data
// received from MS-II
// (that is, all the info is already there in the msvar array, the CAN data is 
// grabbed in sync with the timer updates in ISR_Timer_Clock(), but not all of 
// info that is assigned to specific variables).
// 
// This code only uses the rpm, kpa, and vBatt data from the msvar array. (Other variables 
// are included for reference and future development. To be used, the corresponding 
// variable must be uncommented and the variable added to this code's outpc. structure
// - and the [CHANNELS] section of the MegaTune INI file if you want to log it, etc.)

// outpc.seconds        = *(int *)(msvar + 0);   // 2 bytes (int) at offset 0 in msvar[]
// outpc.pw1            = *(int *)(msvar + 2);   // 2 bytes (int) at offset 2 in msvar[]
// outpc.pw2            = *(int *)(msvar + 4);   // 2 bytes (int) at offset 4 in msvar[]
outpc.engine_rpm = *(int *)(msvar + 6);   // rpm is 2 bytes (int) at offset 6 in msvar[]
// outpc.advance        = *(int *)(msvar + 8);   // 2 bytes (int) at offset 8 in msvar[]
// outpc.squirt         = *(char *)(msvar + 10); // 1 byte (char) at offset 10 in msvar[]
// outpc.engine         = *(char *)(msvar + 11); // 1 byte (char) at offset 11 in msvar[]
// outpc.afrtgt1        = *(char *)(msvar + 12); // 1 byte (char) at offset 12 in msvar[]
// outpc.afrtgt1        = *(char *)(msvar + 13); // 1 byte (char) at offset 13 in msvar[]
// outpc.wbo2_en1       = *(char *)(msvar + 14); // 1 byte (char) at offset 14 in msvar[]
// outpc.wbo2_en2       = *(char *)(msvar + 15); // 1 byte (char) at offset 15 in msvar[]
// outpc.barometer      = *(int *)(msvar + 16);  // 2 bytes (int) at offset 16 in msvar[]
outpc.LOAD = *(int *)(msvar + 18);  // kPa is 2 bytes (int) at offset 18 in msvar[]
// outpc.mat            = *(int *)(msvar + 20);  // 2 bytes (int) at offset 20 in msvar[]
// outpc.coolant        = *(int *)(msvar + 22);  // 2 bytes (int) at offset 22 in msvar[]
// outpc.tps            = *(int *)(msvar + 24);  // 2 bytes (int) at offset 24 in msvar[]
outpc.vBatt = *(int *)(msvar + 26);  // 2 bytes (int) at offset 26 in msvar[]
// outpc.afr1           = *(int *)(msvar + 28);  // 2 bytes (int) at offset 28 in msvar[]
// outpc.afr2           = *(int *)(msvar + 30);  // 2 bytes (int) at offset 30 in msvar[]
// outpc.knock          = *(int *)(msvar + 32);  // 2 bytes (int) at offset 32 in msvar[]
// outpc.egoCorrection1 = *(int *)(msvar + 34);  // 2 bytes (int) at offset 34 in msvar[]
// outpc.egoCorrection1 = *(int *)(msvar + 36);  // 2 bytes (int) at offset 36 in msvar[]
// outpc.airCorrection  = *(int *)(msvar + 38);  // 2 bytes (int) at offset 38 in msvar[]
// outpc.warmupEnrich   = *(int *)(msvar + 40);  // 2 bytes (int) at offset 40 in msvar[]
// outpc.accelEnrich    = *(int *)(msvar + 42);  // 2 bytes (int) at offset 42 in msvar[]
// outpc.tpsfuelcut     = *(int *)(msvar + 44);  // 2 bytes (int) at offset 44 in msvar[]
// outpc.baroCorrection = *(int *)(msvar + 46);  // 2 bytes (int) at offset 46 in msvar[]
// outpc.gammaEnrich    = *(int *)(msvar + 48);  // 2 bytes (int) at offset 48 in msvar[]
// outpc.veCurr1        = *(int *)(msvar + 50);  // 2 bytes (int) at offset 50 in msvar[]
// outpc.veCurr2        = *(int *)(msvar + 52);  // 2 bytes (int) at offset 52 in msvar[]
// outpc.iacstep        = *(int *)(msvar + 54);  // 2 bytes (int) at offset 54 in msvar[]
// outpc.coldAdvDeg     = *(int *)(msvar + 56);  // 2 bytes (int) at offset 56 in msvar[]
// outpc.tpsDOT         = *(int *)(msvar + 58);  // 2 bytes (int) at offset 58 in msvar[]
// outpc.mapDOT         = *(int *)(msvar + 60);  // 2 bytes (int) at offset 60 in msvar[]
// outpc.dwell          = *(int *)(msvar + 62);  // 2 bytes (int) at offset 62 in msvar[]
// outpc.maf            = *(int *)(msvar + 64);  // 2 bytes (int) at offset 64 in msvar[]
// outpc.calcMAP        = *(int *)(msvar + 66);  // 2 bytes (int) at offset 66 in msvar[]
// outpc.fuelCorrection = *(int *)(msvar + 68);  // 2 bytes (int) at offset 68 in msvar[]
// outpc.portStatus     = *(char *)(msvar + 70); // 1 byte (char) at offset 70 in msvar[]
// outpc.knockRetard    = *(char *)(msvar + 71); // 1 byte (char) at offset 71 in msvar[]
// outpc.xTauFuelCorr1  = *(int *)(msvar + 72);  // 2 bytes (int) at offset 72 in msvar[]
// outpc.egoV1          = *(int *)(msvar + 74);  // 2 bytes (int) at offset 74 in msvar[]
// outpc.egoV2          = *(int *)(msvar + 76);  // 2 bytes (int) at offset 76 in msvar[]
// outpc.amcUpdates     = *(int *)(msvar + 78);  // 2 bytes (int) at offset 78 in msvar[]
// outpc.kpaix          = *(int *)(msvar + 80);  // 2 bytes (int) at offset 80 in msvar[]
// outpc.xTauFuelCorr2  = *(int *)(msvar + 82);  // 2 bytes (int) at offset 82 in msvar[]
// outpc.spare1         = *(int *)(msvar + 84);  // 2 bytes (int) at offset 84 in msvar[]
// outpc.spare2         = *(int *)(msvar + 86);  // 2 bytes (int) at offset 86 in msvar[]
// outpc.spare3         = *(int *)(msvar + 88);  // 2 bytes (int) at offset 88 in msvar[]
// outpc.spare4         = *(int *)(msvar + 90);  // 2 bytes (int) at offset 90 in msvar[]
// outpc.spare5         = *(int *)(msvar + 92);  // 2 bytes (int) at offset 92 in msvar[]
// outpc.spare6         = *(int *)(msvar + 94);  // 2 bytes (int) at offset 94 in msvar[]
// outpc.spare7         = *(int *)(msvar + 96);  // 2 bytes (int) at offset 96 in msvar[]
// outpc.spare8         = *(int *)(msvar + 98);  // 2 bytes (int) at offset 98 in msvar[]
// outpc.spare9         = *(int *)(msvar + 100); // 2 bytes (int) at offset 100 in msvar[]
// outpc.spare10        = *(int *)(msvar + 102); // 2 bytes (int) at offset 102 in msvar[]
// outpc.spare11        = *(int *)(msvar + 104); // 2 bytes (int) at offset 104 in msvar[]
// outpc.ospare         = *(char *)(msvar + 106);// 1 byte (cahr) at offset 106 in msvar[]
// outpc.chksum         = *(char *)(msvar + 107);// 1 byte (char) at offset 107 in msvar[]
// outpc.deltaT         = *(long *)(msvar + 108);// 4 bytes (long) at offset 108 in msvar[]

/* Check for error states */

if (inpram.error_check)
  {
    
   if (outpc.manual_gear > NO_GEARS || outpc.manual_gear < -1) // if out of range
     {
      reset_gear();
     } //end if current gear out of range

  } // end if error_check
   
  
/*-------------------------- DETERMINE_CURRENT_GEAR --------------------------------
; Determine current gear from state of SolA, SolB:
;      SolA  SolB
; 4    on    off
; 3    off   off
; 2    off   on
; 1    on    on
; 
; SolA is PE4 (on if SOLAst = 1), i.e. everywhere we change PE4, we also set SOLAst
; SolB is PM2 (on if SOLBst = 1), i.e. everywhere we change PM2, we also set SOLBst
; */ 

if (outpc.manual_gear > 0) // Shift lever in one of the forward drive positons
  { // check solA and sol B states 
   if (SOLAst > 0)  // SolA on
     {                 
       if (SOLBst > 0)             // SolB state indicator on 
         {
          outpc.current_gear = 1;  // SolA and Sol B on
         }
       else 
         {
          outpc.current_gear = 4;  // SolA on and SolB off
         };
     }
    else                           // SolA off
     {            
       if (SOLBst > 0)             // SolB state indicator on
         {
          outpc.current_gear = 2;  // SolA off and SolB on
         }
       else
         {
          outpc.current_gear = 3;  // SolA off and SolB off
         };
     } // end else (SOLAst == 1) ...                                                                                   
  }
else // park, reverse, neutral, use manual gear position
  {
    outpc.current_gear = outpc.manual_gear;
  }

 // if not braking update LOAD short term average
 // Brake input is on GPI4 = PAD07 = knk on MS-II was outpc.knock (voltsx100) -> outpc.brake
 
 if (PORTAD0 & 0x80) // 0x80 = 10000000, i.e., if PAD07 is high
  {  
  outpc.brake = 1; // braking
  }
  else
  {
  outpc.brake =0;  // not braking
  }
 
 scount++; // incrememnt short count each time through loop
    
 if (scount>(LOADshortcount-1)) {
  // reset short counter
  scount = 0;
  // increment long counter
  lcount++; // incerement lcount (long count) - can't do for loop, since we don't 
            // want the code to pause here
 if (lcount > 65000) lcount = 65000; // rail the lcount value           
    
  // Update short LOAD - only if not braking
  
  if (outpc.brake < 1 ) {
 
       outpc.LOAD_short = (outpc.LOAD_short*100 + ((outpc.LOAD-outpc.LOAD_short)/LOADshortcount)*100)/100;
 
       };  // End If brake>100
 
  // When long count reached, update LOAD_long from LOAD_short
  if (lcount>(LOADlongcount-1)) {
      // reset long counter
      lcount = 0;
      // Update long LOAD average  
      outpc.LOAD_long = outpc.LOAD_long + ((outpc.LOAD_short-outpc.LOAD_long)/LOADlongcount);
  
      } // End If (lcount > LOADlongcount)
 
   };  // End If (scount > (LOADshortcount-1))


/*-- UPDATE ROLLING SHIFT SWITCH INPUTS 

The switch average is:

- upshift button average (UP_avg) is calculated by adding or subtracting 1 
 (depending on the input state) to the previous UP_avg. This is updated 
 every time through the main loop.
  
- downshift button average (DWN_avg) is calculated by adding adding or 
  subtracting 1 (depending on the input state) to the previous DWN_avg. This is 
  updated everytime through the main loop.

A shift occurs when the button average exceeds the user specified threshold value - assuming 
'manual mode' is selected, both are not pressed, etc. (this provides a software debounce, 
and sets the 'responsiveness' of the shift command)

 --*/


// -------------------- Manual Shift Mode ------------------------------------------------ 
if (inpram.shift_mode == 0) // if manual, otherwise skip
  {   
  
  // UP button is PT6
  // if (PTT & 0x40) = 0x40 = 01000000 -> pin PT6 low (grounded), then increment UP_avg (reset if shift complete)

  if (PTT & 0x40)   // 0x04 = 01000000, is only true whenever pin 6 is equal to one 
                    // (since ANDing with zero is always zero)
    {
    UP_avg-- ;            // decrement UP_avg
    outpc.upbutton = 0;   // set datalog value
    }
  else
    {
    UP_avg++ ;            // increment UP_avg
    outpc.upbutton = 1;   // set datalog value
    }

  
  // DOWN button is PAD06 (aka. AN06), 0x40 = 01000000
  if (PORTAD0 & 0x40) 
    {
    DWN_avg--;            // decrement DWN_avg
    outpc.downbutton = 0; // set datalog value
    }
  else
    {
    DWN_avg++;            // increment DWN_avg
    outpc.downbutton = 1; // set datalog value
    }
  
  // Rail the counts
  if (UP_avg > 999)  UP_avg  = 999;   // rail the UP_avg count
  if (DWN_avg > 999) DWN_avg = 999;   // rail the DWN_avg count 

  // Act on switch states
  // Check upshift first  
  if (UP_avg > DWN_avg) // make sure we don't get negative in next if
    {
    // if we have 200 more ups than downs, request upshift  
    if ((UP_avg-DWN_avg) > 200) 
      {
      outpc.upshift_request=1;   // if UP_avg >> UP threshold, request upshift
      UP_avg  = 0;  // reset counters
      DWN_avg = 0;
      }
    }
  // Check downshift next
  if (DWN_avg > UP_avg) // make sure no negatives in following if
    {
    // if we have 200 more downs than ups, request downshift 
    if ((DWN_avg-UP_avg) > 200) 
      {
      outpc.downshift_request=1;   // if UP_avg >> UP threshold, request upshift
      UP_avg  = 0;  // reset counters
      DWN_avg = 0;
      }
    }

  // may want to check states of both buttons first to allow mode changes?
  // if (UP_avg > 400 && DWN_avg > 400)
  // { change mode, reset counters }

}     // end if shift_mode ...


/*-- SET_PRESSURE_CONTROL_PWM Duty Cycle ------------------------------------*/
// The Pressure Control solenoid controls main line pressure, pulsed at 
// 0-60% duty cycles at 293Hz.
//
// Pressure control modulation percent varies from 0% at WOT to 60% at idle.
// Note that MegaTune uses 100-PC_DC, so:
//  - 60% PC_DC  (lowest pressure) in the code is specified as  40% in MT, 
//  -  0% PC_DC (highest pressure) in the code is specified as 100% in MT 

// There is no cleaning pulse (dither) every 10 seconds 
// on this transmission (unlike the 4L80E). 

// if gear=neutral, set pressure control PWM% to 60%

if (outpc.manual_gear == 0) {

  PWMDTY2 =  (unsigned char) ((60*PWMPER2)/100);     // set PC PWM duty to 60% in neutral (max bleed - lowest pressure)
                                                     // default PWMPER2 is 244
  }   // End if  (outpc.current_gear == 0)

  else {   // In gear
  
  // Look-up pressure control solenoid (PC) duty cycle in pc_table[12x12] and set tempPC1
  // as a fraction of PWMPER2 (tempPC1 is the look-up value, tempPC2 is the voltage 
  // adjustment, these are added to get the duty cycle count)
  
  tempPC1 = 0; // reinitialize tempPCx
  tempPC2 = 0;
    
  tempPC1 = (uchar)((intrp_2dctable((uint)outpc.speedo/10, (int)outpc.LOAD/10, 
                                    (uchar)NO_MPH, (uchar)NO_LOADS, 
                                    (uint *)&inpram.vss_table[0], 
                                    (int *)&inpram.LOAD_table[0], 
                                    (uchar *)&inpram.pc_table[0][0])*PWMPER2)/100);

  // The voltage diffference is (120-vBatt)/10 since vBatt is x10
  // the correction is the voltage difference multiplied by the 
  // battery correction factor inpram.BatFac/10
  // We move the divsors around a bit to avoid over/underflows.
                                                                                                                     
if (outpc.vBatt < 120) // if less than 12.0 volts, decrease the DC (increase pressure), base is 12.0 volts x 10
  {
  tempPC2 = (120 - outpc.vBatt)*inpram.BatFac/100; // adjust for battery supply voltage less than 12 volts
  tempPC2 = tempPC2 * PWMPER2/100;                 // convert to number of PWM tics
  if ((tempPC1-tempPC2) >= 0) 
    {  
    PWMDTY2 = (uchar)(tempPC1 - tempPC2);          // set the value by subtracting DC (+pressure)
    }
  else // lees than zero
    {
    PWMDTY2 = 0;                                   // do not let value go negative
    }
  }
else  // battery voltage greater than 12.0
  {
  tempPC2 = (outpc.vBatt - 120)*inpram.BatFac/100; // adjust for battery supply voltage greater than 12 volts
  tempPC2 = tempPC2 * PWMPER2/100;                 // convert to number of PWM tics
  if ((tempPC1+tempPC2) <= PWMPER2) 
    { 
    PWMDTY2 = (uchar)(tempPC1 + tempPC2);          // set the value by adding DC (-pressure) 
    }
  else
    {
    PWMDTY2 = PWMPER2;                             // rail the value
    }
  }; // end if (outpc.vBatt <= 120)
                                                                                                                                                    
  outpc.PC_duty = (unsigned char)(100-((PWMDTY2*100)/PWMPER2));        // set datalog value (100-value to have datalog value same as PC table)
                                                                       // input/table values are 0-60%  - adjusted by MT                               
  }   // End else (outpc.current_gear == 0)

/*-- Set the Torque Converter Clutch (TCC) ----------------------------------------*/
  // if  current_gear > minTCC_gear && clt > noTCC_temp && not too hot
     if ((outpc.current_gear > inpram.minTCC_gear) && (outpc.clt > inpram.noTCC_temp) && (outpc.clt<inpram.trans_temp_limit))
      {  //  if LOAD < noTCC_load then lock_TCC on PT3
      
          if (outpc.LOAD < inpram.noTCC_load)
           {
            tcc_lock(); // function call to lock TCC 
           }
           // else unlock TCC unless rpm indicates high speed
           else {
            tcc_unlock(); // function call to unlock TCC
           }; // end if LOAD
           
      }; // end if current gear ...

  // if Trans_Temperature > trans_temp_limit (always lock TCC above min gear if trans is hot)
     if ((outpc.current_gear > inpram.minTCC_gear) && (outpc.clt>inpram.trans_temp_limit))
      {
        // lock TCC
            tcc_lock();
      }; // end if ((current_gear ... 


/*-- COMPUTE VEHICLE SPEED
 For imperial units: 
 
 Vehicle speed = VSS_rpm * tire_diam/(5280*12) * 60 minutes/hr * pi/axle_ratio  (miles/hour)

	 since miles/wheel revolution = tire_diam/(5280*12) * 60 minutes/hr * pi/axle_ratio
   the constant evaluate to 3 .1415926/(5280*12) * 60 =  1/336, so
   Vehicle speed  = VSS rpm * tire_diam/axle_ratio * 1/336 (imperial) 

--*/

// ic_period is 'tics', multiply by 5.333 �s/tic to get time 
// there are inpram.no_teeth per rev, so
//        VSS rpm = frequency (rev/min) = 60/(inpram.no_teeth * outpc.ic_period * 0.000005333)
//                  VSSdivide is set to 60/(inpram.no_teeth*0.000005333)
//                   (VSSdivide is set near start of main proc, so we don't have to keep recalculating)
// so     VSS rpm = freq = VSSdivide/ic_period  (rev/min)
//
// calculate VSSdivide = 187501 [tics/sec] * 1/no_teeth [rev/teeth] * 60 [sec/min]
//                     = 187501 * 60 / inpram.no_teeth  [rev/min]
//                     = 11250060 / inpram.no_teeth     [rev/min]
//
//    speedo = VSSdivide [revs/min][tics*teeth] / rev_mile [rev/mile]
//
//    speedo = VSSdivide * tire_diam * 1/336 * 1/ic_period 

// average last 21 periods for smoothing

// vehicle speed is delta X / delta T
// delta T is the time for 20 teeth = ic_per_acc, in 5.333 �sec tics
ic_per_acc = 0;
ic_dc_acc  = 0;
for (ix=1;ix<20;ix++)
  {
  ic_per_acc = ic_per_acc + ic_period[ix];
  ic_dc_acc = ic_dc_acc + ic_duty[ix];  
  }

outpc.ic_per_avg = (ic_per_acc * 5333)/1000; // the time for 20 teeth (microseconds); 
// delta X is the distance travelled by 20 teeth
// = tire_distance/rev * 1/axle_ratio * 20/no_teeth
// = pi*tire_diam * 1/axle_ratio * 20/no_teeth == VSSdivide
//
// so speedo = (pi*tire_diam * 1/axle_ratio * 20/no_teeth)/(ic_per_acc * constant)
//           = constant * VSSdivide/ic_per_acc

if (!inpram.Metric_Units) { 
    
     // imperial	 - tire diam in inches (mph x 10)

     outpc.speedo = (unsigned int) ((2436*VSSdivide)/(outpc.ic_per_avg));

     /* Calculate trip odometer (miles x 1000) */

     outpc.odo = (unsigned long)(((unsigned long)outpc.vss_teeth/40 * (unsigned long)VSSdivide) /(6336)); // miles x 100

     }   // End if (!inpram.Metric_Units)

else {  // metric  - tire_diam in cm (kph x 10)
     
     outpc.speedo = (unsigned int) ((9900*VSSdivide)/(outpc.ic_per_avg));
     
     /* Calculate trip odometer (km x 1000) */

     outpc.odo = (unsigned long)(((unsigned long)outpc.vss_teeth/40 * (unsigned long)VSSdivide) /(9817)); // km x 100
  
     }  // End else (!inpram.Metric_Units)


/*-- SOL32_PWM
; 3-2 Sol PWM is pulsed at 50 Hertz, with 0% duty cycle in 1st, 90% duty cycle 
; in all other gears. 
; During a 3-2 downshift, the duty cycle is decreased based on the vehicle speed; 
; lower speed, lower duty. 
--*/

if (outpc.current_gear == 1) {
  PWMDTY1 =  0;                                     // set Sol32 PWM duty to 0% in all gears but 1
}   // End if (outpc.current_gear==1)
else  {
  PWMDTY1 = (unsigned char) ((90 * PWMPER1)/100);   // set Sol32 PWM duty to 90% in first
};   // End else (outpc.current_gear==1)

/*
; Read Switches switchA, switchB, switchC and lookup manual_gear 
; i.e., the position of the manual valve - so if we get a 3, then gear can set gear 
; to 1, 2, or 3 but not 4, etc.
; The pressure switch manifold (PSM) is a multiple switch assembly consisting of 
; 3 normally open (NO) pressure switches and 2 normally closed (N/C) pressure switches.
; Fluid from various hydraulic control circuits is fed to this the pressure switch 
; manifold which allows the ECU to determine which gear the transmission is currently 
; shifted into. The switch contacts are normally open and close when fluid pressure 
; causes them to. Depending upon the circuit, the switch may provide a ground path 
; when closed. The table below shows a pin that is grounded by the PSM as a "0", 
; while an open circuit shows a 1 = 12 volt pullup.
; Set LEDs
; If manual_switch_off then goto auto-shift mode (shift_mode=1)
--*/

// Each time through the main loop, determine switch states to find the current gear
//			switchC switchB switchA
// Rev = 		1			 0			0
// Neu =    1      0      1
// 1st = 		1			 1			0
// 2nd =    1      1      1
// 3rd =    0      1      1
// 4th =    0      0      1
//
// not allowed are 0,0,0  &  0,1,0

// Switch                Port         Circuit   AMPseal
// ------                ----         -------   -------
// switchA							 PAD00				GPI1        5
// switchB							 PAD01				GPI2        6
// switchC							 PAD03				EGT3       25

// ******** Condition SWx inputs ********************************
// Set switchAacc, switchBacc, switchCacc (Switch A accumulator) to mid point 
// of user range (inpram.debounce) on start up or after a shift
// then increment/decrement by one each time through the loop 
// until all are <10 or >90% of debounce value
// then set switchA, switchB, switchC to zero if <10% and 1 if >90%

FIND_GEAR:    // If manual gear is in error state (99), loop to here until we get to current gear 

if (PORTAD0 & 0x01) {     // 0x01 = 00000001, i.e., if pin 0 is high
                          // Note that '&' is bitwise, '&&' is relational (i.e., the entire value is compared)
 if (switchAacc < 255) switchAacc = switchAacc + 1;   // most programmers would write this as switchA += 1, or switchA++
 }  // End if if (PORTAD0 & 0x01)
else {
 if (switchAacc > 0) switchAacc = switchAacc - 1;
 }  // End else (PORTAD0 & 0x01)

if (PORTAD0 & 0x02) {     // 0x02 = 00000010, i.e., if pin 1 is high
 if (switchBacc < 255) switchBacc = switchBacc + 1;
 }  // End if (PORTAD0 & 0x02)
else {
 if (switchBacc > 0) switchBacc = switchBacc - 1;
 } // End else (PORTAD0 & 0x02)

if (PORTAD0 & 0x08) {     // 0x08 = 00001000, i.e., if pin 3 is high
 if (switchCacc < 255) switchCacc = switchCacc + 1;
 }   // End if (PORTAD0 & 0x08)
 else {
 if (switchCacc > 0) switchCacc = switchCacc - 1;
 }  // End else (PORTAD0 & 0x08)

// Check if each switch is <10% or >90%
if (switchAacc > upperswitch) 
  {
    switchA = 1;
    switchAacc = inpram.debounce >> 1; 
  };
if (switchAacc < lowerswitch)
  {
    switchA = 0;
    switchAacc = inpram.debounce >> 1; 
  };
if (switchBacc > upperswitch) 
  {
    switchB = 1;
    switchBacc = inpram.debounce >> 1; 
  };
if (switchBacc < lowerswitch)
  {
    switchB = 0;
    switchBacc = inpram.debounce >> 1; 
  };
if (switchCacc > upperswitch) 
  {
    switchC = 1;
    switchCacc = inpram.debounce >> 1; 
  };
if (switchCacc < lowerswitch)
  {
    switchC = 0;
    switchCacc = inpram.debounce >> 1; 
  };

// determine the shift lever position

if (!switchA && !switchB &&  switchC) 
   { outpc.manual_gear = -1; 
     goto GOT_CGEAR; 
     }; // reverse - 100

if ( switchA && !switchB &&  switchC) 
   { outpc.manual_gear =  0; 
     goto GOT_CGEAR; 
     }; // neutral, park - 101

if (!switchA &&  switchB &&  switchC) 
   { outpc.manual_gear =  1; 
     goto GOT_CGEAR; 
     }; // first   - 110

if ( switchA &&  switchB &&  switchC) 
   { outpc.manual_gear =  2; 
     goto GOT_CGEAR; 
     }; // second  - 111

if ( switchA &&  switchB && !switchC) 
   { outpc.manual_gear =  3; 
     goto GOT_CGEAR; 
     }; // third   - 011

if ( switchA && !switchB && !switchC) 
   { outpc.manual_gear =  4; 
     goto GOT_CGEAR; 
     }; // fourth  - 001
     
if (!switchA &&  switchB && !switchC) 
   { outpc.manual_gear = 99; 
     goto GOT_CGEAR; 
     }; // invalid - 010 

if (!switchA && !switchB && !switchC) 
   { // all switches == 0 which is invalid
      // error - gear switches not reporting valid gear selected (010 and 000 not allowed)
      reset_gear();        // this will set 6th bit of error code, and reset the gear switches and switch accumulator values
      outpc.error |= 0x40; // == 64, -> also set 7th bit of error code in addition to 6th set in reset_gear
                           // so we know switch manifold values were out of range (not just current_gear)
   }  // end if (!switchA && !switchB && !switchC)

GOT_CGEAR: // Finished determining current gear (manual valve position)

/* If gear out of range*/

if (outpc.manual_gear > NO_GEARS) goto FIND_GEAR; // if in error state, loop until we have a valid gear

// -------------- Auto Shift Mode -------------------------------------------------------
// If auto_shift sequential mode, get target gear from shift table

if (inpram.shift_mode == 1 || inpram.shift_mode == 2) { 

     // SET TARGET GEAR
     // and request shift as necessary 
     // from auto_gear_lookup(
     //      x = vss - int, will be divided by 10 in subroutine to match bins
     //      y = kpa - int, will be divided by 10 in subroutine to match bins
     //      nx = [NO_MPH],
     //      ny = [NO_LOADS],
     //      x_table = vss_table - unsigned int,
     //      y_table = LOAD_table - int,
     //      z_table) = auto_table - int. 

     outpc.target_gear = auto_gear_lookup(outpc.speedo,outpc.LOAD,NO_MPH,NO_LOADS,&inpram.vss_table[0],&inpram.LOAD_table[0],&inpram.auto_table[0][0]);
     // DEBUG should be outpc.LOAD_short for averaging
     
     };  // End If shift_mode == 1 or 2

/* If manual lever in park, reverse or neutral, set solA and solB 'on', and skip gear setting. */
// Done *after* getting auto gear from table, for diagnostic reasons, 
// otherwise target gear looks 'dead' in MT

if (outpc.manual_gear < 1) // neutral, park = 0, reverse = -1
  { PORTE |= 0x10;         // turn on Sol A
    SOLAst = 1;            // set state indicator
    *pPTMpin[2] |= 0x04;   // turn on Sol B
    SOLBst = 1;            // set state indicator
    tcc_unlock();          // unlock the TCC if in P,N,R
    goto SKIP_SHIFT;       // skip gear selection code
  }; // end if (outpc.manual_gear < 1)

// Shift in auto mode (sequential) if target gear is not the current gear
if (inpram.shift_mode == 1 ) {
	outpc.upshift_request   = 0;
	outpc.downshift_request = 0;
      // set shift requests
     	if (outpc.target_gear < outpc.current_gear)    // downshift
     	  {
	       outpc.upshift_request   = 0;
         outpc.downshift_request = 1;

         // Always Unlock TCC for downshift
         TCTL2 = TCTL2 & 0xBF;    // set output lo in OL3
                                  // 0xBF = 10111111 
         };  // End if target_gear < current_gear 

	    if (outpc.target_gear > outpc.current_gear) 
	       { 
	       // if the requested gear is greater than the current manual position
	       // upshift
	         if (outpc.target_gear <= outpc.manual_gear)
	           {
	           outpc.upshift_request   = 1;
	           outpc.downshift_request = 0;
	           }
	         else  // do not shift above manual gear position
	           {
	           outpc.upshift_request   = 0;
	           outpc.downshift_request = 0;
	           }; // end else
	         
	      };  // End if target_gear > current_gear
	       
}; // End If inpram.shift mode

// If auto mode = 2, and shift required, skip direct to desired gear
// do not sequentially shift

if (inpram.shift_mode == 2) {
  switch (outpc.target_gear) {
    
  case 1:
      // Shift direct to First
      
      // Check if downshift will overrev engine, if not, proceed with shift
             
          if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[1]/inpram.gear_table[2]) < inpram.rpm_limit)) {

      /** - Unlock TCC
          - switch ON Sol A
          - switch ON Sol B
          - switch OFF 3/2 Sol
          - switch LED1 on
          - switch LED2 off
          - switch LED3 off
          - switch LED4 off
      --*/
            TCTL2 = TCTL2 & 0xBF;    // set output lo in OL3
            /* Notes: TCLT2 is defined in hcs12def.h and is memory 
               adresss (*((volatile unsigned char*)(0x0049)))
               where volatile unsigned char means that this is 
               an 8 bit value (unsigned char) that can be changed 
               externally to the code logic (volatile)
            
               By 'ANDing' TCLT2 with 0xBF, we are comparing 
               TCLT2 to BF (which is hex, the binary is 10111111 - 8 bits)
               This compares the current values bit by bit with 
               10111111, and set the corresponding TCLT2 bit to
               1 if both are 1, otherwise it is set to zero.
               
               The net result is that since bit 6 is zero 
               (read from the right - first position is zero),
               it will never be added to 1, so it sets TCLT2 bit #7 to zero. 
               Since the other values in BF are 1, the result is 1 
               if the corresponding bit in TCLT2 is 1, zero oherwise.
               
               Note that this can also be written as TCLT &= 0xBF;
            */
            
            outpc.lock_TCC = 0;  // Set the value for the datalog
						 
            // switch on Sol A
            PORTE |= 0x10; // Turn on Sol A
            SOLAst = 1;       // Set state indicator
            
            /* Notes: PORTE is defined in hcs12def.h
            
               By 'ORing' PORTE with 0x10, we are comparing 
               PORTE to 10 (the binary is 00010000)
               This compares the current values bit by bit with 
               00010000, and set the corresponding PORTE bit to
               1 if either are 1, otherwise it is set to zero.
               
               The net result is that since bit 4 is one,
               the OR will alswitchAys be one 1, so this sets PORTE bit #4 to one. 
               Since the other values in BF are 0, the result is 1 
               if the corresponding bit in PORTE is 1, zero oherwise.
               
               Note that this can also be written as PORTE = PORTE | 0x10;
            */          
            
            
            // switch on Sol B
            *pPTMpin[2] |= 0x04;     // turn on Sol B, 0x04 = 00000100, 
                                     // so set port M pin 2 to 1 
            SOLBst = 1;              // Set state indicator
            
            outpc.current_gear = 1;   // set current gear
            
            // 3/2 Sol PWM to 0%  -------------------------------------------------------- check this! -----------------
            TCTL2 = TCTL2 & 0xFB;    // set output lo in OL1, 0xFB = 11111011 - set pin 3 to zero
                       
            // switch LED1 on
            *pPTMpin[4] |= 0x10;     // 0x10 = 00010000, set port M pin 4 to 1
            /*
            pPTMpin[x] is defined as a pointer in main.c (this file), and is set:
            pPTMpin[ix] = pPTM;   for ix=0 to 7
            the index refers to each loaction in Port M in turn 
            port M and port T have similar definitions and intializations */
             
            // switch LED2 off
            *pPTMpin[3] &= ~0x08;    // 0x08 = 00001000, ~0x08 = 11110111, so set port M pin 3 = 0
            /* 
            Note: the ~ operator 'inverts' the bits, creating the compliment. 
            So a 1 becomes a zero, a zero becomes a one */
            
            // switch LED3 off
           *pPTMpin[5] &= ~0x20;    // 0x20 = 00100000, ~0x20 = 11011111, so set port M pin 5 = 0
            
            // switch LED4 off
            PORTB &= ~0x10;           // 0x10 = 00010000, set port B pin 4 to 1
          }
          
          else {
           // do not shift - will overrev engine
           outpc.error |= 0x04; // set 3 bit of error code 
          };
      break;

  case 2: 
    
  
    // Shift direct to Second
    
    // Check if downshift will underrev engine, if not, proceed with shift
        
    if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[2]/inpram.gear_table[1]) < inpram.under_rev_limit)) {
    
       /*-- unlock TCC
          - switch OFF Sol A
          - switch ON Sol B
          - 3/2 Sol PWM to 90%
          - switch LED1 on
          - switch LED2 on
          - switch LED3 off
          - switch LED4 off
          --*/

          // Unlock TCC
          TCTL2 = TCTL2 & 0xBF;    // set output lo in OL3
          outpc.lock_TCC = 0;
          
          // switch off Sol A
          PORTE &= ~0x10;          // turn off SOL32
          SOLAst = 0;              // Set state indicator
          
          // switch on Sol B
          *pPTMpin[2] |= 0x04;     // turn on Sol B
          SOLBst = 1;              // Set state indicator
          
          outpc.current_gear = 3;   // set current gear
          
				  // 3/2 Sol PWM to 90%
          TCTL2 = TCTL2 & 0xFB;    // set output lo in OL1
 
          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 off
          *pPTMpin[5] &= ~0x20;
          // switch LED4 off
          PORTB &= ~0x10;  
          }
          
          else {
           // do not shift - will underrev engine
           outpc.error |= 0x04; // set 3 bit of error code 
          };
  
     break;
  
  case 3:
        
       // Shift direct to Third
 
       if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[3]/inpram.gear_table[2]) < inpram.under_rev_limit)) { 
 
        /*- Unlock TCC
          - switch OFF Sol A
          - switch OFF Sol B
          - 3/2 Sol PWM to 90%
          - switch LED1 on
          - switch LED2 on
          - switch LED3 on
          - switch LED4 off
          --*/
          
          // Unlock TCC
          TCTL2 = TCTL2 & 0xBF;  // set output lo in OL3
          outpc.lock_TCC = 0;
          
          // switch off Sol A
          PORTE &= ~0x10;        // turn off SOLA (PE4, 0x10 = 00010000, ~0x10 = 11101111)
          SOLAst = 0;            // Set state indicator

          // switch off Sol B
          *pPTMpin[2] &= ~0x04;  // turn off Sol B
          SOLBst = 0;            // Set state indicator
          
          outpc.current_gear = 3;   // set current gear
          
          // 3/2 Sol PWM to 90%
          TCTL2 = TCTL2 & 0xFB;  // set output lo in OL1

          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 on
          *pPTMpin[5] |= 0x20; // set LED3
          // switch LED4 off
          PORTB &= ~0x10;
          } // End if (outpc.engine_rpm*inpram.
          
          else {
           // do not shift - will underrev engine
           outpc.error |= 0x04; // set 3 bit of error code 
          };  // End else
            
     break;
  
  case 4:
   
    // Shift direct to Fourth
 
       if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[4]/inpram.gear_table[3]) < inpram.under_rev_limit)) { 
     
        /*- unlock TCC
          - switch ON Sol A
          - switch OFF Sol B
          - 3/2 Sol PWM to 90%
          - switch LED1 on
          - switch LED2 on
          - switch LED3 on
          - switch LED4 on
          --*/
                   
          // Unlock TCC
          TCTL2 = TCTL2 & 0xBF;    // set output lo in OL3
          outpc.lock_TCC = 0;
          
          // switch on Sol A
          PORTE |= 0x10;           // turn on Sol A (0x10 = 00010000)
          SOLAst = 1;              // Set state indicator
 
          // switch on Sol B
          *pPTMpin[2] |= 0x04;     // turn on Sol B
          SOLBst = 1;              // Set state indicator
          
         outpc.current_gear = 4;   // set current gear
          
          // 3/2 Sol PWM to 90%
          TCTL2 = TCTL2 & 0xFB;    // set output lo in OL1

          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 on
         *pPTMpin[5] |= 0x20;     // set LED3
          // switch LED4 on
          PORTB |= 0x10;
  				}
          
          else {
           // do not shift - will underrev engine
           outpc.error |= 0x04;   // set 3 bit of error code 
          };
    break;
  
  default:  
    reset_gear();  // We have out-of-range gear, so reset
    break; 
    
  } // End switch cases
  
} // End shift_mode==2



/*-- TCC
; If current gear > 1 then
; If LOAD > TCC_kpa_limit then UNLOCK_TCC;
; If RPM > TCC_rpm_limit then UNLOCK_TCC;
; end if
; If Trans_temp > Trans_temp_limit LOCK_TCC
; If Current_gear = 4 then LOCK_TCC
; BRA MAIN_LOOP
--*/

// determine if TCC should be locked
// TCC is PT3
//

if (outpc.lock_TCC) { // lock TCC
  
    tcc_lock();

} 

else {		 // unlock TCC
  
    tcc_unlock();

}    // End if (outpc.lock_TCC)

/*-- Get shift button status, and detemine if upshift or downshift requested
//
//
--*/

/*-- SHIFT_UP
; check upshiftstatus to see if threshold has be reached, upshift if it has
; if current_gear < 4 then target_gear = current gear + 1
; Check if new gear will under-rev the engine
; If required, unlock TCC
; - TCC unlocks under certain conditions during upshifts, depending on the vehicle speed 
;   and engine LOAD.  It is safe to upshift under light load while the TCC is locked. 
;   Most OEM systems keep the torqure converter clutch applied during light throttle upshifts
--*/

if (outpc.upshift_request && !outpc.downshift_request) {
  
  switch (outpc.current_gear) {
    
  case -1:
  
    // In reverse, can't upshift
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
  
    break;
  
  case 0:
  
    // In neutral, can't upshift
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
  
    break;
  
  case 1: 
    
  
    // In 1st, upshift to second
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
    
    // Check if downshift will underrev engine, if not, proceed with shift
        
    if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[2]/inpram.gear_table[1]) < inpram.under_rev_limit)) {
    
       /*-- Shift_First_Second
          - 3/2 Sol PWM from 0% to 90%
          - switch off Sol A
          - switch LED1 on
          - switch LED2 on
          - switch LED3 off
          - switch LED4 off
          --*/

          // 3/2 Sol PWM from 0% to 90%
          TCTL2 |= 0x04;    // set output hi in OL1


          // switch off Sol A
          PORTE &= ~0x10;         // Turn off SOLA
          SOLAst = 0;             // Set state indicator
          outpc.current_gear = 2; // set current gear

          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 off
         *pPTMpin[5] &= ~0x20;
          // switch LED4 off
          PORTB &= ~0x10;  
          }   // End if (outpc.engine_rpm...
          
          else {
           // do not shift - will underrev engine
           outpc.error |= 0x04; // set third error bit 
          };  // End Else
  
    break;
  
  case 2:
        
       // In 2nd, upshift to third
       outpc.upshift_request   = 0;
       outpc.downshift_request = 0;
       
       if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[3]/inpram.gear_table[2]) < inpram.under_rev_limit)) { 
 
          /*-- Shift_Second_Third
          - switch off Sol B
          - switch LED1 on
          - switch LED2 on
          - switch LED3 on
          - switch LED4 off
          --*/

          // switch off Sol A
          PORTE &= ~0x10;         // turn off SOLA
          SOLAst = 0;             // Set state indicator
          
          // switch off Sol B
          *pPTMpin[2] &= ~0x04;   // turn off Sol B
          SOLBst = 0;             // Set state indicator
          outpc.current_gear = 3; // set current gear

          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 on
          *pPTMpin[5] |= 0x20;  // set LED3
          // switch LED4 off
          PORTB &= ~0x10;
          }
          
          else {
           // do not shift - will underrev engine
           outpc.error |= 0x04; // set third error bit 
          };
            
    break;
  
  case 3:
   
    // In 3rd, upshift to fourth
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
 
       if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[4]/inpram.gear_table[3]) < inpram.under_rev_limit)) 
        { 
     
        /*-- Shift_Third_Fourth
          - switch on Sol A
          - switch LED1 on
          - switch LED2 on
          - switch LED3 on
          - switch LED4 on
          --*/

          // switch on Sol A
          PORTE |= 0x10;           // turn on Sol A (0x10 = 00010000)
          SOLAst = 1;              // Set state indicator
          
          // switch off Sol B
          *pPTMpin[2] &= ~0x04;   // turn off Sol B
          SOLBst = 0;             // Set state indicator
          outpc.current_gear = 4; // set current gear

          // switch LED1 on
          *pPTMpin[4] |= 0x10;
          // switch LED2 on
          *pPTMpin[3] |= 0x08;
          // switch LED3 on
          *pPTMpin[5] |= 0x20;    // set LED3
          // switch LED4 on
          PORTB |= 0x10;
  				}
          
          else 
          {
           // do not shift - will underrev engine
           outpc.error |= 0x04;  // set third error bit 
          }
    break;
  
  case 4:
  
    // In 4th, can't shift up
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
  
    break;
  
  default:
    reset_gear(); // We have out-of-range gear, so reset
    break;
  
  }  // end switch current gear
  
} //end if upshift_request

/*-- SHIFT_DOWN
; check downshift status to see if threshold has be reached, upshift if it has
; if current_gear > 1 then target_gear = current gear - 1
; Check if new gear will over-rev the engine
; If required, unlock TCC
; - TCC unlocks under certain conditions during upshifts, depending on the vehicle speed 
;   and engine LOAD.  It is safe to upshift under light load while the TCC is locked. 
;   Most OEM systems keep the torqure converter clutch applied during light throttle upshifts
--*/

if (outpc.downshift_request && !outpc.upshift_request) {
  
  switch (outpc.current_gear) {
     
  case -1:
  
    //In reverse, can't downshift
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
  
    break;
  
  case 0:
  
    // In neutral, can't downshift
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
  
    break;
  
  case 1: 
    
  
    // In 1st, can't downshift
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
    
    break;

  case 2:
  
    // In 2nd, downshift to first
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
    
        /*-- Shift_Second_First
        - switch on Sol A
        - 3/2 Sol PWM from 90% to 0% 
        - switch LED1 on
        - switch LED2 off
        - switch LED3 off
        - switch LED4 off
        --*/
        
        // Check if downshift will overrev engine, if not, proceed with shift
        
          if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[1]/inpram.gear_table[2]) < inpram.rpm_limit)) {
          
				     // 3/2 Sol PWM to 0% from 90%  -------------------------------------------------------- check this! ---------
            TCTL2 = TCTL2 & 0xFB;    // set output lo in OL1
						 
            // switch onSol A
            PORTE |= 0x10;    // Turn on Sol A
            SOLAst = 1;       // Set state indicator
            outpc.current_gear = 1; // set current gear

            // switch LED1 on
            *pPTMpin[4] |= 0x10;
            // switch LED2 off
            *pPTMpin[3] &= ~0x08;
            // switch LED3 off
           *pPTMpin[5] &= ~0x20;
            // switch LED4 off
            PORTB &= ~0x10;
          }
          
          else {
           // do not shift - will overrev engine
           outpc.error |= 0x04; // set third error bit  
          }
             	   
    break;
  
  case 3:
   
    // In 3rd, downshift to second
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
    
        /*-- Shift_Third_Second
        - switch on Sol B
        - 3/2 PWM reduced from 90% based on speed and load
        - check that switchC switches on
        - switch LED1 on
        - switch LED2 on
        - switch LED3 off
        - switch LED4 off
        --*/
			         
        // Check if downshift will overrev engine, if not, proceed with shift
        
          if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[2]/inpram.gear_table[3]) < inpram.rpm_limit)) {
            // switch on Sol B
            PWMDTY1 = PWMPER1/2;    // set 3/2 Sol PWM to 50% during 3/2 downshift (from 90%)
            *pPTMpin[2] |= 0x04;    // turn on Sol B
            SOLBst = 1;             // Set state indicator
            outpc.current_gear = 2; // set current gear

            // switch LED1 on
            *pPTMpin[4] |= 0x10;
            // switch LED2 on
            *pPTMpin[3] |= 0x08;
            // switch LED3 off
            *pPTMpin[5] &= ~0x20;;  // unset LED3
            // switch LED4 off
            PORTB &= ~0x10;
          }
          
          else {
           // do not shift - will overrev engine
           outpc.error |= 0x04;    // set third error bit  
          }
              
    break;
  
  case 4:
  
    // In 4th, downshift to third
    outpc.upshift_request   = 0;
    outpc.downshift_request = 0;
    
        /*-- Shift_Fourth_Third
        - switch off Sol A
        - switch LED1 on
        - switch LED2 on
        - switch LED3 on
        - switch LED4 off
        --*/
			         
        // Check if downshift will overrev engine, if not, proceed with shift
        
          if (!inpram.rpm_check || ((outpc.engine_rpm*inpram.gear_table[3]/inpram.gear_table[4]) < inpram.rpm_limit)) {         
            // switch off Sol A
            PORTE &= ~0x10;   // Turn off Sol A
            SOLAst = 0;       // Set state indicator
            outpc.current_gear = 3; // set current gear

            // switch LED1 on
            *pPTMpin[4] |= 0x10;
            // switch LED2 on
            *pPTMpin[3] |= 0x08;
            // switch LED3 on
            *pPTMpin[5] |= 0x20;
            // switch LED4 off
            PORTB &= ~0x10;
                  }         
          else {
           // do not shift - will overrev engine
           outpc.error |= 0x04; // set third error bit 
          }
          
    break;
  
  default:
    reset_gear();
    break;
  
  } // end case
  
} //end if downshift_request

SKIP_SHIFT:
  
  DISABLE_INTERRUPTS
  ultmp = lmms;
  ENABLE_INTERRUPTS  
  if((lmms - adc_lmms) > 78)  {          // every 10 ms (78 x .128 ms clk)
    adc_lmms = lmms;
    // read 10-bit ADC results, convert to engineering units and filter
    next_adc++;
    outpc.clt = get_adc(2); // get temperature
    outpc.linepressure = get_adc(4); // get line pressure, non-CAN MAP
    outpc.kpa = get_adc(5); // non-CAN MAP

      }  // end if (lmms - adc_lmms)


/***************************************************************************
**
** Check whether to burn flash
**
**************************************************************************/
BURN_FLASH:
    if (burn_flag > 1) {
      // burn flash 512 byte(256 word) sector(s)
      fburner(tableWordFlash(burn_idx, 0), tableWordRam(burn_idx, 1), tableWords(burn_idx));
      if(burn_flag >= tableWords(burn_idx))  {
        burn_flag = 0;
        flocker = 0;
      }  // end if
      else {
        burn_flag++;
        } // end else burn_flag >=
    }  // end if (burn_flag)

/***************************************************************************
**
** Check for reinit command
**
**************************************************************************/
    if(reinit_flag)  {
      reinit_flag = 0;
      // update initialization of variables/ registers dependent on
      // user config inputs
//        trig_ret_mode = 2;
//      TCTL4 &= ~0x32;  // clear bits 1 and 4,5(won't hurt if not dual spk)
//      if(!ign_opt2)  {
 //       if(inpram.InputCaptureEdge & 0x01)  {
          // normal rising edge input  capture
//          if(!trig_ret_mode)
            // not trig ret mode
//            TCTL4 |= 0x11; // rising edge input capture
//          else
            // reverse in trigger return mode
//            TCTL4 |= 0x22; // falling edge input capture
//        }
//        else  {
          // normal falling edge input capture
//          if(!trig_ret_mode)
            // not trig ret mode
 //           TCTL4 |= 0x22; // falling edge input capture
 //         else
            // reverse in trigger return mode
 //           TCTL4 |= 0x11; // rising edge input capture
 //       }
      }   // End if(reinit_flag)
 //     else  {  
 //       if(inpram.InputCaptureEdge & 0x01)		// tach in is rising edge
 //         TCTL4 |= 0x01;
 //       else														// cam_sync is falling edge
 //         TCTL4 |= 0x20;
 //     }
 //  }


/***************************************************************************
**
**  Check for serial, CAN receiver timeout
**
**************************************************************************/

    DISABLE_INTERRUPTS
    ultmp = lmms;
    ultmp2 = rcv_timeout;
    ENABLE_INTERRUPTS
    if(ultmp > ultmp2)  {
      txmode = 0;    // break out of current receive sequence
      rcv_timeout = 0xFFFFFFFF;
    } // end if (ultmp > ultmp2)
    
    DISABLE_INTERRUPTS
    ultmp2 = ltch_CAN;
    ENABLE_INTERRUPTS
    
    if(ultmp > ultmp2)  {
      getCANdat = 0;    // break out of current receive sequence
      ltch_CAN = 0xFFFFFFFF;
    }  //end if (ultmp > ultmp2)
    
    if(kill_ser)  {
      if(outpc.seconds > kill_ser_t)  {
        kill_ser = 0;
        SCI0CR2 |= 0x24;     // = 11000 -> rcv, rcvint re-enable
      }  // end if (outpc.seconds > ...
    }  // end if (kill_ser)
  
/***************************************************************************
**
**  Check for CAN reset
**
**************************************************************************/
	  if(can_reset)  {
		  /* Re-initialize CAN comms */
		  CanInit();
		  can_reset = 0;
	  }  // end if (can_reset) ...
	  
	  /* IC_period decay */
	  // If the VSS rate drops to zero quickly, the speedo will be frozen at a large 
	  // value (large period = small frequency) that is greater than zero, since the VSS_timer only works if the 
	  // interrupt is called, so zeros are never filled into the ic_period[] arry
	  // So instead, we set this array to zero gradually - not fast enough to 
	  // affect the speedo at any reasonable input rate
	  // but enough that it eventually goes to zero if the interrupt is not called

if (outpc.speedo < 15) // if running slowly
  {
    
	  if (!(outpc.loop_count % 350))   // if there is no remainder from dividing main_loops by 1000 
	    {                               // i.e., every 350 main loops, set on of the values very high
	      if (ic_period[ic_count] < 0xFFFFF)
	        {
	        ic_period[ic_count] = ic_period[ic_count]+((0xFFFFF - ic_period[ic_count])/107);
	        ic_count++;                     // change next value next time
	        if (ic_count >20) ic_count = 1; 
	        } //end if  (ic_period[ic_count] ...
	        
	    } // end if (!(outpc.loop_count ...
	    
  } // end if (outpc.speedo ...

} // end for(;;) main loop
  
goto MAIN_LOOP;  // we should *never* be here - goto beginning if we are!
                 // might indicate wrong number of brackets.... 

} // end main() function

/** =========== FUNCTIONS ===================================================== **/

// ------------ Unlock TCC --------------------------------------------------------
void tcc_unlock(void)  {
 // Unlock without conditions
 TCTL2 = TCTL2 & 0xBF;    // set output lo in OL3
 outpc.lock_TCC = 0;      // set datalog value
 return;
} // end tcc_unlock

// ------------ Lock TCC ----------------------------------------------------------
void tcc_lock(void)  {
  // check gear, temperature, load criteria
  if (   outpc.current_gear > inpram.minTCC_gear 
      && outpc.clt          > inpram.noTCC_temp 
      && outpc.LOAD         < inpram.noTCC_load) {
    // Lock TCC if all conditions met      
    TCTL2 |= 0x40;         // set output hi in OL3
    outpc.lock_TCC = 1;    // set datalog value
    } // end if
 return;
}   // end tcc_lock


// ------------ Reset switch manifold --------------------------------------------
void reset_gear(void) 
  {
    outpc.error |= 0x20; // out of range, set 00X00000 in error code 
    outpc.current_gear=99; // set flag so we poll until have a gear lever position
    reset_switches();
    return;
  }

void reset_switches(void)
  {                
    // reset switches  to 3rd gear until we determine actual gear
    switchA = 0;
    switchB = 1;
    switchC = 1;
    // reset switch accumulator counts re-establish current gear
    switchAacc = inpram.debounce >> 1;   // right shift the bits 1 position 
    switchBacc = inpram.debounce >> 1;   // this is the same as dividing by 2 and discarding the remainder)
    switchCacc = inpram.debounce >> 1;   // for example 01100111 (decimal 103) becomes 00110011 (51)
    return; 
  }

//--------------- vss_reset -------------------------------------------------------
#pragma CODE_SEG DEFAULT

void vss_reset(void)  {
  // enter here at init and when in stall condition
  // Disable IC, Ign,Inj OC interrupts
  TIE &= 0xC0;  
  TSCR1 &= 0x7F;        // disable all timers
 
  
  // reinitialize
  t_chgoff = 0xFFFFFFFF;
  outpc.error |= 0x08;   // set 4th bit of error code (8)
  outpc.engine_rpm = 0;
  ICt_overflow[0] = 0;
  ICt_overflow[1] = 0;
  IgnOCt_overflow[0] = 0;
  IgnOCt_overflow[1] = 0;
  Fl1OCt_overflow = 0;
  Fl2OCt_overflow = 0;
  TC_ovflow = 0;
  TC_ov_ix = 0;
  t_enable_IC[0] = 0xFFFFFFFF;
  t_enable_IC[1] = 0xFFFFFFFF;

// enable Timer Input Compare (IC) timer(s) & interrupt(s)
  TSCR1 |= 0x80;
  TIE |= 0x05;
  return;
}

void reset(void)  {
/* short ix;
  no_tmgrp = 0;
  for(ix = inpram.n_init_instr;ix < inpram.n_instr; ix++)  {
    if(inpram.instr[ix].VI1.type == 90)  {
      tmgrp[no_tmgrp] = 0xFFFFFFFF;
      ixl_tmgrp[no_tmgrp] = ix - 1;  // index of instr just before this time blk starts
      no_tmgrp++;
    }
  }
  outpc.FuelAdj = 0;   // need to set!!!
  outpc.SpkAdj = 0;
  outpc.IdleAdj = 0;
  outpc.SprAdj = 0;
  FuelAdjl = 0;
  SpkAdjl = 0;
  IdleAdjl = 0;
  SprAdjl = 0;
  sendCANAdj = 0;
  first_instr = 1;
  gpio_ms = 0;
  inpram.gpio_halt = 0; */
  return;
}
#pragma CODE_SEG DEFAULT

//--------------- get_adc -------------------------------------------------------
int get_adc(char chan1)  {
// char chan;
// long adcval;

// chan = 2 is trans temperature 
// chan = 4 is line pressure
// chan = 5 is non-CAN MAP

// the ATD0DRx ADC result register contains the ten bit result of the 
// ADC conversion for channel x, we use this as an index to the 'lookup'_table 
// to get the actual value
 
    switch(chan1)  {
      case 0:
        break;
      case 1:
        break;
      case 2:
      // transmission temperature sensor
        adcval = cltfactor_table[ATD0DR2]; // deg F or C x 10
      case 3:
        break;
      case 4:
      // line pressure
        adcval = (linefactor_table[ATD0DR4]);
        break;
      case 5:
      // non-CAN MAP
        adcval = (loadfactor_table[ATD0DR5]);
        break;
      case 6:
        break;
      case 7:
        break;
      default:
        break;
    }			 // end of switch
  
  return(adcval);
}  // end get_adc

#pragma CODE_SEG DEFAULT

#pragma CODE_SEG ROM_7000

void switch_page(unsigned char sub_no)  {
// This resides in non-banked memory and is used to call subroutines
//  from another page.

  switch(sub_no)  {
          
    case 0:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
      
    case 1:
      PPAGE = 0x3C;
//      get_adc(carg1);
      PPAGE = 0x3D;
      break;

    case 2:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
    
    case 3:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
      
    default:
      break;
  }
  
  return;
}

// --------------- intrp_1dctable -------------------------------------------------------
//
// 1D Interpolation Routine
// ------------------------
// sgnx     = 
// x        = the index value for the lookup
// n		    = the number of rows in (size of) the lookup table
// x_table  = the name of the lookup table
// sgny     =
// z_table  = the 'looked up' values
// return the interpolated value as an integer

int intrp_1dctable(char sgnx, int x, unsigned char n, unsigned int * x_table, char sgnz, unsigned char * z_table)  {
  int ix;
  long interp, interp3;
  // bound input arguments
  if((sgnx && (x > x_table[n-1])) ||
     (!sgnx && ((unsigned int)x > (unsigned int)x_table[n-1])))  {
    if(!sgnz)
      return((int)z_table[n -1]);
    else
      return((int)((char)z_table[n -1]));
  }
  if((sgnx && (x < x_table[0])) ||
     (!sgnx && ((unsigned int)x < (unsigned int)x_table[0])))  {
    if(!sgnz)
      return((int)z_table[0]);
    else
      return((int)((char)z_table[0]));
  }
  for(ix = n - 2; ix > -1; ix--)  { 
    if((sgnx && (x > x_table[ix])) ||
       (!sgnx && ((unsigned int)x > (unsigned int)x_table[ix])))  {
   		break;
  	}
  }
  if(ix < 0)ix = 0;
 
  interp =	(unsigned int)x_table[ix + 1] - (unsigned int)x_table[ix];
  if(interp != 0)  {
    interp3 = (unsigned int)x - (unsigned int)x_table[ix];
    interp3 = (100 * interp3);
    interp = interp3 / interp;
  }

  return((int)((char)z_table[ix] +
	    interp * ((char)z_table[ix+1] - (char)z_table[ix])/ 100));
}


// --------------- intrp_2dctable -------------------------------------------------------

int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny,
  unsigned int * x_table, int * y_table, unsigned char * z_table)  {
  int ix,jx;
  long interp1, interp2, interp3;
  // bound input arguments
  if(x > x_table[nx-1])x = x_table[nx-1];
  else if(x < x_table[0])x = x_table[0];
  if(y > y_table[ny-1])y = y_table[ny-1];
  else if(y < y_table[0])y = y_table[0];
  // Find bounding indices in table
  for(ix = ny - 2; ix > -1; ix--)  {  // Start w highest index
	//  because will generally have least time for calculations at hi y
  	if(y > y_table[ix])  {
   		break;
  	}
  }
  if(ix < 0)ix = 0;
  for(jx = nx - 2; jx > -1; jx--)  {  // Start w highest index
	// because will generally have least time for calculations at hi x
	  if(x > x_table[jx])  {
	    break;
    }
  }
  if(jx < 0)jx = 0;
  // do 2D interpolate
  interp1 = y_table[ix + 1] - y_table[ix];
  if(interp1 != 0)  {
    interp3 = (y - y_table[ix]); 
    interp3 = (100 * interp3); 
    interp1 = interp3 / interp1; 
  }
  interp2 =	x_table[jx + 1] - x_table[jx];
  if(interp2 != 0)  {
    interp3 = (x - x_table[jx]); 
    interp3 = (100 * interp3); 
    interp2 = interp3 / interp2; 
  }
  return((int)(((100 - interp1) * (100 - interp2) * z_table[ix*nx+jx]
	  + interp1 * (100 - interp2) * z_table[(ix+1)*nx+jx]
	  + interp2 * (100 - interp1) * z_table[ix*nx+jx+1]
	  + interp1 * interp2 * z_table[(ix+1)*nx+jx+1]) / 10000));
}

// --------------- auto_gear_lookup -------------------------------------------------------

unsigned char auto_gear_lookup(int x, int y, unsigned char nx, unsigned char ny, unsigned int * x_table, int * y_table, unsigned char * z_table)  
  {
  // This function has NO INTERPOLATION, it is designed to look up target gears only 
  // (we don't want an interpolated gear, since we might interpolate to the wrong gear 
  // when the integer is truncated and returned).
  // This function is simpler than the intrp_2dctable() function above (which is used 
  // in this code to inpterpolate the PC table values), but auto_gear_lookup uses 
  // similar naming conventions, etc..
  // x is mph x10, 
  // y is kpa x10,
  // nx = 12 = NO_MPH = number of mph bins
  // ny = 12 = NO_MAP = number of kpa bins
  // * x_table is pointer to the mph bins
  // * y_table is pointer to the kpa bins
  // * x_table is pointer to the gear table
   
  int ix,jx;
  // ix is the index that contains the bin 
  //    number of the mph bin immediately 
  //    below the actual mph value
  // jx is the index that contains the bin 
  //    number of the kpa bin immediately 
  //    below the actual kpa value
  
  // multiply mph and load bins by 10 below 
  // (because values are x10, but bins are not)
    
  // bound input arguments
  if(x > (x_table[nx-1]*10))   x = (x_table[nx-1]*10); // use maximum mph
  else if(x < (x_table[0]*10)) x = (x_table[0]*10);    // use minimum mph
  
  if(y > (y_table[ny-1]*10))   y = (y_table[ny-1]*10); // use max load
  else if(y < (y_table[0]*10)) y = (y_table[0]*10);    // use min load
  
  // Find bounding indices in table
  for(ix = ny - 2; ix > -1; ix--)  {  // Start w highest index
	//  because will generally have least time for calculations at hi y
  	if(y > (y_table[ix]*10))  {
   		break;  // break when ix
  	}
  }
  if(ix < 0)ix = 0;
  for(jx = nx - 2; jx > -1; jx--)  {  // Start w highest index
	// because will generally have least time for calculations at hi x
	  if(x > (x_table[jx]*10))  {
	    break;
    }
  }
  if(jx < 0)jx = 0;
 
 return((unsigned char) (z_table[ix*nx+jx]) );  // the next higher value
}

#pragma CODE_SEG DEFAULT
#pragma CODE_SEG NON_BANKED // interrupts must be in non-banked memory, 
                            // because the vectors ar only 16 bit and 
                            // will not accomodate the appended 6 bit ppage register 
                            
// --------------- VSS Timer ----------------------------------------
// Time the period on the VSS input, set in array ic_period[]
// (also report duty cycle)

INTERRUPT void VSS_timer0(void)  {
  static unsigned short tcsav=0;
  unsigned short utmp;
  unsigned int tmp_per;
  unsigned int tmp_dc;

  TFLG1 = 0x01;
//  utmp = ((TC0 - tcsav) / 3) << 1;    // high time, us
  utmp = (TC0 - tcsav);
  // check pin value
  if(PTIT & 0x01)  {   // start at rising edge
    //  calculate last period, us
    ic_period[ic_count] = utmp;        // us
    tmp_per = utmp;    // for assembly code
    tcsav = TC0;       // save time of new rising edge
    }
  else  {              // ic_pinval = 0, falling edge
    // calculate duty cycle,  %
    if(ic_period[ic_count] != 0)  {
      asm {
        ldy utmp 
        ldd 100
        EMUL
        ldx  tmp_per
        EDIV
        sty tmp_dc
      } // end asm

      ic_duty[ic_count] = tmp_dc;
    }  // end if(outpc.ic_period...
    else 
      {   
      ic_duty[ic_count] = 0;
      }
    }  // end else
  // increment tooth counter
  outpc.vss_teeth++;
  // Set the speedo output
  speedo_counter++;

  if  (speedo_counter <= speedo_toggle)  
    {
    // Set pin PT4 high
    *pPTTpin[4] |= 0x10;  // toggle LED on PT4 (0x10 = 16 = 00010000) 
    }
  
  else  
    {
    // set pin PT4 low
    *pPTTpin[4] &= ~0x10;  // toggle LED on PT4 (~0x10 = 239 = 11101111)     
    } 
    
  if (speedo_counter >= speedo_total)
    {
    // Reset counter to zero    
    speedo_counter = 0;
    }

  ic_count++;
  if (ic_count>20) ic_count=1;  
  return;
}

#pragma CODE_SEG  NON_BANKED
INTERRUPT void ISR_Timer_Clock(void)  {
  short ix;
  
  // .128 ms clock interrupt - clear flag immediately to resume count
  CRGFLG = 0x80;  // clear RTI interrupt flag
  // also generate 1.024 ms, .10035 sec and 1.0035 sec clocks
  lmms++;         // free running clock(.128 ms tics) good for ~ 110 hrs
  can_mmsclk++;
  mms++;          // in .128 ms tics - reset every 8 tics = 1.024 ms
      
  // check mms to generate other clocks
  if(mms < 8)goto CLK_DONE;
  mms = 0;
  millisec++;     // actually 1.024 ms, for seconds count
//  gpio_ms++;       // gpio 1 ms timer

  // Get MS-II output variables via CAN. Grab 8 bytes every can_var_rate (.128 ms tics)
    /* CAN Xmt mssge ring buffer:
      can[0] is to hold Rx,TxISR messages,
      can[1] for main loop messages (so don't get clobbered by ISR)
      cxno = number of messages in queue waiting to be sent out
      cxno_in = index for inserting a msg in queue (increment after insert)
      cxno_out = index for sending out a msg (increment after loading CAN buffer)
      cx_msg_type = CMD,REQ,RESP,XSUB = set value, 
                                        request value, 
                                        respond to a request for value, 
                                        execute a subroutine.
      varblk,varoffset,varbyte = blk number of data structure, 
                                 byte offset from start of structure, 
                                 number of bytes of data.
                                 - these are prefixed by "cx_my" & "cx_dest" to represent 
                                   this CPU and other CPU respectively
      datbuf = the actual data (max of 8 bytes),
      cx_dest = CAN id number of device to which msg being sent. 
  */
  if(can_mmsclk > inpram.can_var_rate)  {
      // load xmt interrupt ring buffer(can[0]) - request data
      ix = can[0].cxno_in;
      can[0].cx_msg_type[ix] = MSG_REQ; // message type is 'request data'
      // Get all data from var. block canvar_blkptr[varblk]=outpc in MSII)      
      can[0].cx_destvarblk[ix] = 7;  // DEBUG was 6, try 7
      // Put data rcvd from MSII into var block canvar_blkptr[varblk]=msvar in GPIO)      
      can[0].cx_myvarblk[ix] = 2;    // msvar is cx_myvarblk[2] 
      can[0].cx_myvaroff[ix] = can_varoff;
      can[0].cx_dest[ix] = 0;		     // send to MS II (board 0)
      // below is offset from start of outpc in MSII
      can[0].cx_destvaroff[ix] = can_varoff;
      // calculate no bytes, and offset for next time
      can_varoff += 8;
      if(can_varoff < NO_MSVAR_BYTES)
        can[0].cx_varbyt[ix] = 8;	   // get 8 bytes
      else  {
                                     // get remaining bytes
        can[0].cx_varbyt[ix] = NO_MSVAR_BYTES - (can_varoff - 8);
        can_varoff = 0; 
      }
      // This is where (in xmt ring buffer) to put next message
      if(can[0].cxno_in < (NO_CANMSG - 1))
        can[0].cxno_in++;
      else
        can[0].cxno_in = 0;
      // increment counter
      if(can[0].cxno < NO_CANMSG)
        can[0].cxno++;
      else
        can[0].cxno = NO_CANMSG;
      
      if(!(CANTIER & 0x07))  {
        // Following will cause entry to TxIsr without sending msg
        // since when CANTIER = 0, CANTFLG left as buff empty(>0).
        // If CANTIER has at least 1 int buf enabled, will enter
        // TxIsr automatically.
        CANTBSEL = CANTFLG;
        CANTIER = CANTBSEL;
      }
      can_mmsclk = 0;
  }
  
  if(millisec > 976) {
    millisec = 0;
    // update seconds to send back to PC
    outpc.seconds++;
  }
   
CLK_DONE:
  return;
}

// --------------- ISR_TimerOverflow ------------------------------------

INTERRUPT void ISR_TimerOverflow(void)  {
  // Get display seconds from continuously running TCNT
  TC_ovflow++;
  if(TC_ovflow >= TC_ovfla[TC_ov_ix])  {
    // update seconds to send back to PC
    outpc.seconds++;     // increment secL in datalog
    if(TC_ov_ix >= 59)  {
      TC_ov_ix = 0;
      TC_ovflow = 0;
    }
    else
      TC_ov_ix++;
  }

  // Handle Ign IC stuff
//  if(pulse_no == 0)  {
  	// clear Timer Overflow inerrupt flag (TOF)
//  	TFLG2 = 0x80;
//  	return;
//  }
//  else  {
  	ICt_overflow[0]++;
  	ICt_overflow[1]++;
//  }

  // clear timer overflow interrupt flag (TOF)
  TFLG2 = 0x80;
  return;
}


/**************************************************************************
**
** SCI Communications
**
** Communications is established when the PC communications program sends
** a command character - the particular character sets the mode:
**
** "a" = send all of the realtime display variables (outpc structure) via txport.
** "w"+<offset lsb>+<offset msb>+<nobytes>+<newbytes> = 
**    receive updated data parameter(s) and write into offset location
**    relative to start of data block
** "e" = same as "w" above, followed by "r" below to echo back value
** "r"+<offset lsb>+<offset msb>+<nobytes>+<newbytes> = read and
**    send back value of a data parameter or block in offset location
** "y" = verify inpram data block = inpflash data block, return no. bytes different.
** "b" = jump to flash burner routine and burn a ram data block into a flash 
**    data block.
** "t" = receive new data for clt/mat/ego/maf tables
** "T" = receive new table data for CAN re-transmission to GPIO
** "c" = Test communications - echo back Seconds
** "Q" = Send over Embedded Code Revision Number
** "S" = Send program title.
**
**************************************************************************/
/*
To use SCI1 for 8 bit, non-parity communication:

1. Select the baud rate via SCI0BDH/L Baud Rate Registers (we did this earlier). 
2. Enable transmission and/or reception as desired by setting the TE and/or RE bits in 
the SCI0CR2 Control Register.

         bit        7       6      5     4     3     2     1     0
                   TIE     TCIE   RIE   ILIE   TE    RE   RWU   SBK
 
3. For:

- transmission - poll the TDRE flag and write data to the SCI0DRL register at 
                 address $00D7 when the TDRE flag is set. The data we write 
                 to the SCI0DRL register is the data that will be immediately 
                 output from the TX pin. 
- reception    - poll the RDRF register (SCI0SR1). When the RDRF register is 
                 set ((SCI0SR1 & 0x20) <> 0), we can then 
                 read the incoming data by reading the SCI1DRL register. The 
                 data we read in from this register is the input via the RX pin. 
*/

#pragma CODE_SEG NON_BANKED

INTERRUPT void ISR_SCI_Comm(void)  {
  char dummy,save_page;
  int ix;
  static int rd_wr,xcntr;
  static unsigned char CANid,ibuf,sendCANdat=0,next_txmode,cksum;
  unsigned char sect,nsect;
  static unsigned int txptr,tble_word,ntword;
  
#define getCANid             40
#define getTableId           41
#define setBurningParameters 99
  
  // if RDRF register not set, => transmit interrupt
  if(!(SCI0SR1 & 0x20))goto XMT_INT;

// Receive Interrupt
  // Clear the RDRF bit by reading SCISR1 register (done above), then read data
  //  (in SCIDRL reg).
  // Check if we are receiving new input parameter update
txgoal = 0;
rcv_timeout = 0xFFFFFFFF;
if(SCI0SR1 & 0x08)  {	 // check for rcv Overrun error
   txmode = 0;
   dummy = SCI0DRL;
   SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
   kill_ser = 1;
   kill_ser_t = outpc.seconds + SER_TOUT;
   return;
}

switch(txmode)  {

case 0:

  switch(SCI0DRL)  {
    case 'a':				 // send back all real time ram output variables
  next_txmode = 1;
  txmode = getCANid;
  cksum = 0;
  break;

    case 'w':		  // receive new ram input data and write into offset location;
                  // also used for forwarding CAN msgs bet. MT & auxilliary boards.
                  // In this CAN mode, no_bytes must be >0 and <=8.
  next_txmode = 5;
  txmode = getCANid;
  rd_wr = 1;
  break;

    case 'e':		  // same as 'w', but verify by echoing back values. Don't use with CAN
  next_txmode = 5;
  txmode = getCANid;
  rd_wr = 2;
  break;

    case 'r':		  // read and send back ram input data from offset location;
                  //  also used for forwarding CAN msgs bet. MT & auxilliary boards
  next_txmode = 5;
  txmode = getCANid;
  rd_wr = 0;
  cksum = 0;
  break;

    case 'y':      // Verify that a flash data block matches a
  next_txmode = 2; //  corresponding ram data block
  txmode = getCANid;
  break;
   	
    case 'b':      // burn a block of ram input values into flash;
                   // also used for forwarding CAN msgs bet. MT & auxilliary boards
  next_txmode = setBurningParameters;
  txmode      = getCANid;
  break;

    case 't':      // update a flash table with following serial data
  txmode = 20;
  cksum = 0;
  break;

    case '!':      // start receiving reinit/reboot command
  txmode = 30;
  break;

    case 'c':      // send back seconds to test comms
  txcnt = 0;         
  txmode = 1;
  txgoal = 2;      // seconds is 1st 2 bytes of outpc structure
  txbuf.seconds = outpc.seconds;
  SCI0DRL = *(char *)&txbuf;
  SCI0CR2 &= ~0x24; // rcv, rcvint disable
  SCI0CR2 |= 0x88; // xmit enable & xmit interrupt enable
  break;

    case 'Q':     // send code rev no.
  txcnt = 0;
  txmode = 4;
  txgoal = 20;
  SCI0DRL = RevNum[0]; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;

    case 'S':         // send program title
  txcnt = 0;
  txmode = 5;
  txgoal = 32;
  SCI0DRL = Signature[0]; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;
  
    case 'k':         // request checksum.
  txcnt = 0;
  txmode = 0;
  txgoal = 1;
  SCI0DRL = cksum; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;

    default:
  break;
  }     // End of switch for received command
    break;

case getCANid: // Get CAN id for current command.
   CANid = SCI0DRL;
   if (CANid < MAX_CANBOARDS)
     txmode = getTableId;
   else	 {
  	 // CANid wrong - kill comms since don't know where to send data
     txmode = 0;
     SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
     kill_ser = 1;
     kill_ser_t = outpc.seconds + SER_TOUT;
     return;
   }
   break;
case getTableId: // Get table id for current command.
   tble_idx = SCI0DRL;   // 
   if(CANid != can_id)  {
     txmode = next_txmode;
   } 
   else if(tble_idx >= NO_TBLES)  {
  	 // tble index wrong
     txmode = 0;
     //  kill comms since don't know which/ how much data
     SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
     kill_ser = 1;
     kill_ser_t = outpc.seconds + SER_TOUT;
     return;
   }
   else
     txmode = next_txmode;
   next_txmode = 0;
   if (txmode == 1) { 
     txcnt = 0;
     txgoal = tableBytes(tble_idx);
     // load output variables into txbuf to avoid incoherent word data.
     // To work all words in structure must be word aligned, all longs
     // aligned on long boundaries.
     xcntr = 0;
     txptr = 0;
     *tableWordRam(tble_idx,0) = *((unsigned int *)(&outpc) + 0);
     *tableWordRam(tble_idx,1) = *((unsigned int *)(&outpc) + 1);
     SCI0DRL = *tableByteRam(tble_idx, 0);
     cksum += SCI0DRL; 
     SCI0CR2 &= ~0x24;   // rcv, rcvint disable
     SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
   }
   else if (txmode == setBurningParameters) { // Burn command
     if(CANid != can_id)  {
  	   // set up single CAN message & forward to aux board
  	   ix = can[0].cxno_in;
  	   can[0].cx_msg_type[ix] = MSG_BURN; 
  	   can[0].cx_destvarblk[ix] = tble_idx;
  	   can[0].cx_destvaroff[ix] = 0;
  	   can[0].cx_dest[ix] = CANid;
  	   can[0].cx_varbyt[ix] = 0;		 // no data bytes
  	   // This is where (in xmt ring buffer) to put next message
  	   if(can[0].cxno_in < (NO_CANMSG - 1))
  	     can[0].cxno_in++;
  	   else
  	     can[0].cxno_in = 0;	 // overwrite oldest msg in queue
  	   // increment counter
  	   if(can[0].cxno < NO_CANMSG)
  	     can[0].cxno++;
  	   else
  	     can[0].cxno = NO_CANMSG;
  	   if(!(CANTIER & 0x07))  {
  	     // Following will cause entry to TxIsr without sending msg
  	     // since when CANTIER = 0, CANTFLG left as buff empty(>0).
  	     // If CANTIER has at least 1 int buf enabled, will enter
  	     // TxIsr automatically.
  	     CANTBSEL = CANTFLG;
  	     CANTIER = CANTBSEL;
  	   }  
     }
     else if(flocker == 0) { 
       flocker     = 0xCC;    // set semaphore to prevent burning flash thru runaway code
       burn_flag   = 1;
       burn_idx = tble_idx;
     }
     txmode      = 0;			 // handle burning in main loop
   } 
   else if(txmode == 2)  {
     vfy_flg = 1;
   	 vfy_fail = 0;
   }
   break;

case 5:
  	if(CANid != can_id)  {
  	  // MT wants to send data to aux board via CAN
  	  if(rd_wr == 1)  {
  	    sendCANdat = 1;
  	  } 
  	  // MT wants to request data from aux board via CAN
  	  else if(rd_wr == 0)  {
  	    getCANdat = 1;
  	    ltch_CAN = lmms + 3906;   // 0.5 sec timeout to get CAN data
  	  } 
  	  else  {
  	    sendCANdat = 0;
  	    getCANdat = 0;
  	  }
  	}
  	rxoffset = (SCI0DRL << 8); // byte offset(msb) from start of inpram
  	txmode++;
  	break;

case 6:
  	rxoffset |= SCI0DRL;       // byte offset(lsb) from start of inpram
  	txmode++;
  	break;
  	
case 7:
  	// nbytes must be <= 8 for CAN interaction
  	rxnbytes = (SCI0DRL << 8); 	// no. bytes (msb)
  	txmode++;
  	break;

case 8:
  	rxnbytes |= SCI0DRL;		// no. bytes (lsb)
  	rxcnt = 0;
  	// check won't blow table arrays
  	if(rxoffset + rxnbytes > tableBytes(tble_idx))  {
  	  //  kill comms since don't know how much data being sent
  	  txmode = 0;
  	  SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
  	  kill_ser = 1;
  	  kill_ser_t = outpc.seconds + SER_TOUT;
  	  return;
  	}
  	if(rd_wr == 0)  {		    // read & send back input data
  	  if(getCANdat)  {
  	    txmode = 0;         // done receiving/writing all the data
  	    // set up single CAN message & forward to aux board
  	    //   MT requesting data from aux board.
  	    //   Note: MT must only deal with 1 board at a time and wait til
  	    //   data back or timeout.
  	    ix = can[0].cxno_in;
  	    can[0].cx_msg_type[ix] = MSG_REQ; 
  	    can[0].cx_destvarblk[ix] = tble_idx;
  	    can[0].cx_myvarblk[ix] = tble_idx;  // MT will have duplicate
  	                                    // varblks for the aux boards
  	    can[0].cx_destvaroff[ix] = rxoffset;
  	    can[0].cx_myvaroff[ix] = rxoffset;
  	    can[0].cx_dest[ix] = CANid;
  	    can[0].cx_varbyt[ix] = (unsigned char)rxnbytes;
  	    // This is where (in xmt ring buffer) to put next message
  	    if(can[0].cxno_in < (NO_CANMSG - 1))
  	      can[0].cxno_in++;
  	    else
  	      can[0].cxno_in = 0;	 // overwrite oldest msg in queue
  	    // increment counter
  	    if(can[0].cxno < NO_CANMSG)
  	      can[0].cxno++;
  	    else
  	      can[0].cxno = NO_CANMSG;
  	    if(!(CANTIER & 0x07))  {
  	      // Following will cause entry to TxIsr without sending msg
  	      // since when CANTIER = 0, CANTFLG left as buff empty(>0).
  	      // If CANTIER has at least 1 int buf enabled, will enter
  	      // TxIsr automatically.
  	      CANTBSEL = CANTFLG;
  	      CANTIER = CANTBSEL;
  	    }
  	  } 
  	  else  {
  	    txcnt = 0;
  	    txmode = 3;
  	    txgoal = rxnbytes;
  	    SCI0DRL = *tableByteRam(tble_idx, rxoffset);
  	    cksum += SCI0DRL;
  	    SCI0CR2 &= ~0x24;     // rcv, rcvint disable
  	    SCI0CR2 |= 0x88;      // xmit enable & xmit interrupt enable
  	  }
  	}
  	else  {                   // write data to input data block buffer to maintain 
  	                          // coherence of inputs while awaiting serial bytes
  	  if((rxnbytes > 1) && (rxnbytes < sizeof(txbuf)))
  	    ibuf = 1;
  	  else
  	    ibuf = 0; 
  	  txmode++;
  	}
  	break;

case 9:
  	// Check for data overrun.  Note: this
  	// still not bullet proof because could write half the bytes
  	// then have overrun; need to store and write all at once if
  	// all is valid.
  	if((SCI0SR1 & 0x08) == 0)  {   // none yet- write data.
  	  if(sendCANdat)
  	    *((char *)&txbuf + rxcnt) = SCI0DRL;
  	  else if(!ibuf)
  	    *tableByteRam(tble_idx, rxoffset + rxcnt) = SCI0DRL;
  	  else
  	    *((char *)&txbuf + rxcnt) = SCI0DRL;
  	}
  	rxcnt++;
  	if(rxcnt >= rxnbytes)  {
  	  if(sendCANdat)  {
  	    txmode = 0;         // done receiving/writing all the data
  	    sendCANdat = 0;
  	    // set up single CAN message & forward to aux board
  	    //   MT sending data to aux board
  	    for(ix = 0; ix < rxnbytes; ix++)  {
  	      can[0].cx_datbuf[can[0].cxno_in][ix] = *((char *)&txbuf + ix);
  	    }
  	    ix = can[0].cxno_in;
  	    can[0].cx_msg_type[ix] = MSG_CMD; 
  	    can[0].cx_destvarblk[ix] = tble_idx;
  	    can[0].cx_destvaroff[ix] = rxoffset;
  	    can[0].cx_dest[ix] = CANid;
  	    can[0].cx_varbyt[ix] = (unsigned char)rxnbytes;
  	    // This is where (in xmt ring buffer) to put next message
  	    if(can[0].cxno_in < (NO_CANMSG - 1))
  	      can[0].cxno_in++;
  	    else
  	      can[0].cxno_in = 0;	 // overwrite oldest msg in queue
  	    // increment counter
  	    if(can[0].cxno < NO_CANMSG)
  	      can[0].cxno++;
  	    else
  	      can[0].cxno = NO_CANMSG;
  	    if(!(CANTIER & 0x07))  {
  	      // Following will cause entry to TxIsr without sending msg
  	      // since when CANTIER = 0, CANTFLG left as buff empty(>0).
  	      // If CANTIER has at least 1 int buf enabled, will enter
  	      // TxIsr automatically.
  	      CANTBSEL = CANTFLG;
  	      CANTIER = CANTBSEL;
  	    }  
  	  } 
  	  else if(ibuf)  {
  	    for(ix = 0; ix < rxnbytes; ix++)  {
  	      *tableByteRam(tble_idx,rxoffset + ix) = *((char *)&txbuf + ix);
  	    }
  	  }
  	  dummy = SCI0DRL;      // clear OR bit (read sci0sr1(done), sci0drl)
  	  if(rd_wr == 1)
  	    txmode = 0;         // done receiving/writing all the data
  	  else  {
  	    txcnt = 0;				// send back data just written
  	    txmode = 3;				//	 for verification
  	    txgoal = rxnbytes;
  	    SCI0DRL = *tableByteRam(tble_idx, rxoffset); 
  	    SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  	    SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  	  }
  	}       // finished all rxnbytes
  	break;

case 20:	 // d/load & burn tables - *** Do while engine NOT turning ***
  	       // Note: this type table has no ram copy - its not intended for tuning
  	tble_idx = SCI0DRL;    // type of table
  	if(tble_idx > NO_TBLES)	 {
  	  // tble index wrong - kill comms since don't know how much data being sent
  	  txmode = 0;
  	  SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
  	  kill_ser = 1;
  	  kill_ser_t = outpc.seconds + SER_TOUT;
  	  return;
  	}
  	ntword = 0;
  	flocker = 0xCC; // set semaphore to prevent burning flash thru runaway code
  	save_page = PPAGE;
  	PPAGE = 0x3C;
	  // erase table
  	nsect = (unsigned char)(tableWords(tble_idx)/SECTOR_WORDS);
  	if(tableWords(tble_idx) > (nsect*SECTOR_WORDS))nsect++;
  	for (sect = 0; sect < nsect; sect++)  {
  	  Flash_Erase_Sector(tableWordFlash(tble_idx, 0) + sect * SECTOR_WORDS);
  	}
  	PPAGE = save_page;
  	flocker = 0;
  	txmode++;
  	break;

case 21:
  	cksum += SCI0DRL;
  	tble_word = (SCI0DRL << 8);    // msb
  	txmode++;
  	break;

case 22:
  cksum += SCI0DRL;
  tble_word |= SCI0DRL;            // lsb
  flocker = 0xCC; // set semaphore to prevent burning flash thru runaway code
  save_page = PPAGE;
  PPAGE = 0x3C;
  // write table word
  Flash_Write_Word(tableWordFlash(tble_idx, ntword), tble_word);
  flocker = 0;
  ntword++;
  if(ntword < tableWords(tble_idx))
    txmode = 21;
  else  {
    txmode = 0;
    reset();
  }
  PPAGE = save_page;
  break;

case 30:
  	if(SCI0DRL == '!')    // receive 2nd ! for reboot signal
  	  txmode++;
  	else if(SCI0DRL == 'x')  {  // received reinit signal (!x)
  	  reinit_flag = 1;
  	  txmode = 0;
  	}
  	else
  	  txmode = 0;
  	break;

case 31:
  	if(SCI0DRL == 'x')  {   // received complete reboot signal(!!x)
  	  //if(outpc.rpm < inpram.crank_rpm)  { // check not reloading code at hi rpm
  	    PPAGE = 0x3C;
  	    // go to start of bootload pgm
  	    //asm (jmp $F800);
  	    (void)reboot();
  	  //}
  	}
  	else if(SCI0DRL == '!')  {   // received complete reload signal(!!!)
  	  /*
  	  //if(outpc.rpm < inpram.crank_rpm)  { // check not reloading code at hi rpm
  	    PPAGE = 0x3C;
  	    SCI0CR1 = 0x00;
  	    SCI0CR2 = 0x00;      // disable Tx, Rx
  	    // go to start of monitor code
  	    //asm (jmp $F842);
  	    (void)monitor();
  	  //}
  	  */
  	}
  	txmode = 0;
  	break;

default:
  dummy = SCI0DRL;   // dummy read of reg to clear int flag
  break;

  }     // End of case switch for received data
    
  if((txgoal == 0) && (txmode > 0))
    rcv_timeout = lmms + 3906;   // 0.5 sec timeout
  return;

// Transmit Interrupt
XMT_INT:
  // Clear the TDRE bit by reading SCISR1 register (done), then write data
  txcnt++;
  if(txcnt < txgoal)  {
    if(txmode == 3)  {
  	  SCI0DRL = *tableByteRam(tble_idx, rxoffset + txcnt);
  	  if(rd_wr == 0)
  	    cksum += SCI0DRL;
    }
    else if(txmode == 4)  {
  	  SCI0DRL = RevNum[txcnt];
    }
    else if(txmode == 5)  {
  	  SCI0DRL = Signature[txcnt];
    }
    else if(txmode == 2)  {
  	  SCI0DRL = *((char *)&vfy_fail + txcnt);
    } 
    else if(txmode == 6)  {
  	  // pass on CAN msg to MT
  	  SCI0DRL = *((char *)&txbuf + txcnt);
    } 
    else  {
      xcntr++;
      SCI0DRL = *tableByteRam(tble_idx, xcntr);
      cksum += SCI0DRL;
      if(xcntr >= 3)  {	 // transmitted 4 bytes, load 2 new words
        xcntr = -1;
        txptr += 2;
        *tableWordRam(tble_idx,0) = *((unsigned int *)(&outpc) + txptr);
        *tableWordRam(tble_idx,1) = *((unsigned int *)(&outpc) + txptr+1);
      }
    }
  } 
  else  {   // done transmitting
    if(txmode == 1)
//      outpc.cksum = cksum;
    txcnt = 0;
    txgoal = 0;
    txmode = 0;
    SCI0CR2 &= ~0x88;    // xmit disable & xmit interrupt disable
    SCI0CR2 |= 0x24;     // rcv, rcvint re-enable
  } 
  return;
}													 

#pragma CODE_SEG DEFAULT        

void CanInit(void)
{
unsigned char ix;
	/* Set up CAN communications */
	/* Enable CAN, set Init mode so can change registers */
	CANCTL1 |= 0x80;
	CANCTL0 |= 0x01;
	
	/* clear ring buffers */
	for(ix = 0;ix < 2;ix++)  {
	  can[ix].cxno = 0;
	  can[ix].cxno_in = 0;
	  can[ix].cxno_out = 0;
	}
	can_status = 0;
	
	while(!(CANCTL1 & 0x01));  // make sure in init mode
	
	/* Set Can enable, use IPBusclk (24 MHz),clear rest */
	CANCTL1 = 0xC0;  
	/* Set timing for .5Mbits/ sec */
	CANBTR0 = 0xC2;  /* SJW=4,BR Prescaler= 3(24MHz CAN clk) */
	CANBTR1 = 0x1C;  /* Set time quanta: tseg2 =2,tseg1=13 
	              (16 Tq total including sync seg (=1)) */
	CANIDAC = 0x00;   /* 2 32-bit acceptance filters */
	/* CAN message format:
	 Reg Bits: 7 <-------------------- 0
	  IDR0:    |---var_off(11 bits)----|  (Header bits 28 <-- 21)
	  IDR1:    |cont'd 1 1 --msg type--|  (Header bits 20 <-- 15)
	  IDR2:    |---From ID--|--To ID---|  (Header bits 14 <--  7)
	  IDR3:    |--var_blk-|--spare--rtr|  (Header bits  6 <-- 0,rtr)
	*/  
	/* Set identifier acceptance and mask registers to accept 
	     messages only for can_id or device #15 (=> all devices) */
	/* 1st 32-bit filter bank-to mask filtering, set bit=1 */
	CANIDMR0 = 0xFF;       // anything ok in IDR0(var offset)
	CANIDAR1 = 0x18;       // 0,0,0,SRR=IDE=1
	CANIDMR1 = 0xE7;		   // anything ok for var_off cont'd, msgtype
	CANIDAR2 = can_id;     // rcv msg must be to can_id, but
	CANIDMR2 = 0xF0;			 // can be from any other device
	CANIDMR3 = 0xFF;       // any var_blk, spare, rtr
	/* 2nd 32-bit filter bank */
	CANIDMR4 = 0xFF;       // anything ok in IDR0(var offset)
	CANIDAR5 = 0x18;       // 0,0,0,SRR=IDE=1
	CANIDMR5 = 0xE7;		   // anything ok for var_off cont'd, msgtype
	CANIDAR6 = 0x0F;			 // rcv msg can be to everyone (id=15), and
	CANIDMR6 = 0xF0;			 // can be from any other device
	CANIDMR7 = 0xFF;       // any var_blk, spare, rtr
	
	/* clear init mode */
	CANCTL0 &= 0xFE;  
	/* wait for synch to bus */
	while(!(CANCTL0 & 0x10));
	
	/* no xmit yet */
	CANTIER = 0x00;
	/* clear RX flag to ready for CAN recv interrupt */
	CANRFLG = 0xC3;
	/* set CAN rcv full interrupt bit */
	CANRIER = 0x01;
	return;
}


#pragma CODE_SEG  NON_BANKED

INTERRUPT void CanTxIsr(void)
{
unsigned char ix,jx,kx;

outpc.CANtx++; // increment CAN transmit counter
  
/* CAN Xmit Interrupt */
CANTBSEL = CANTFLG;    // select MSCAN xmit buffer
// Check ring buffers and xfer to MSCAN buffer
for(ix = 0;ix < 2;ix++)  {
 if(can[ix].cxno)  {
 	jx = can[ix].cxno_out;
 	/* Set up identifier registers */
	CAN_TB0_IDR0 = (unsigned char)(can[ix].cx_destvaroff[jx] >> 3);
	                      // 8 high bits in IDR0, 3 low bits in IDR1
	CAN_TB0_IDR1 = 
	  (unsigned char)((can[ix].cx_destvaroff[jx] & 0x0007) << 5) | 
	               0x18 |           // SRR=IDE=1
                 can[ix].cx_msg_type[jx];			// 3 bits
	CAN_TB0_IDR2 = (can_id << 4) | can[ix].cx_dest[jx];
	CAN_TB0_IDR3 = (can[ix].cx_destvarblk[jx] << 4);
	/* Set xmt buffer priorities (lower is > priority) */
	CAN_TB0_TBPR = 0x02;
	
	/* set data in buffer */
	switch(can[ix].cx_msg_type[jx])  {
		case MSG_CMD:  // msg for dest ecu to set a variable to val in msg
		case MSG_RSP:  // msg in reply to dest ecu's request for a var val
		  CAN_TB0_DLR = can[ix].cx_varbyt[jx];
		  for(kx = 0;kx < CAN_TB0_DLR;kx++)  {
		    *(&CAN_TB0_DSR0 + kx) = can[ix].cx_datbuf[jx][kx];
		  }
		  break;
		
		case MSG_REQ:  // msg to send back current value of variable(s)
		  // this 1st byte holds var blk for where to put rcvd data
		  // 2nd,3rd bytes hold var offset rel. to var blk and how
		  // many consecutive bytes to be sent back
		  CAN_TB0_DLR = 3;
		  CAN_TB0_DSR0 = can[ix].cx_myvarblk[jx];
		  CAN_TB0_DSR1 = (unsigned char)(can[ix].cx_myvaroff[jx] >> 3);
		  CAN_TB0_DSR2 = (unsigned char)((can[ix].cx_myvaroff[jx] & 0x0007) << 5) | 
		                                  can[ix].cx_varbyt[jx];
		  break;
		  
		case MSG_XSUB:
		  CAN_TB0_DLR = 0;
		  break;
		  
		case MSG_BURN:
		  CAN_TB0_DLR = 0;
		  break;
	}
	// This is where (in xmt buffer) to get next outgoing message
	if(can[ix].cxno_out < (NO_CANMSG - 1))
		can[ix].cxno_out++;
	else
		can[ix].cxno_out = 0;
	if(can[ix].cxno > 0)
		can[ix].cxno--;    // TB buf loaded for xmit - decrement ring buf count
	can_status &= CLR_XMT_ERR;
	/* set CAN xmt interrupt bit and clear flag to initiate transmit */
	CANTIER |= CANTBSEL;  
	CANTFLG = CANTBSEL;  // 1 clears(buffer full), 0 is ignored
	break;               // only handle 1 buffer entry at time
 }
 if(ix == 1)  {    // nothing left in either ring buffer
  CANTIER = 0x00;  // leave CANTFLG as buffer empty, but disable interrupt
                   // Note: if this last xmt in buf, will re-neter ISR, but 
                   //  then exit because can[0,1].cxno = 0. 
  break;  
 }
}		        // end for loop
if((CANRFLG & 0x0C) != 0)  {
        // Xmt error count
        can_status |= XMT_ERR;
        //can_reset = 1;
}
return;
}

INTERRUPT void CanRxIsr(void)
{
unsigned char rcv_id,msg_type,var_blk,var_byt,jx,kx;
unsigned short var_off,dvar_off;

outpc.CANrx++; // increment CAN transmit counter

    /* CAN Recv Interrupt */
    if(CANRFLG & 0x01)  {
    
        var_off = ((unsigned short)CAN_RB_IDR0 << 3) |
                       ((CAN_RB_IDR1 & 0xE0) >> 5);
        msg_type = (CAN_RB_IDR1 & 0x07);
        rcv_id = CAN_RB_IDR2 >> 4;		// message from device rcv_id
        var_blk = CAN_RB_IDR3 >> 4;
        var_byt = (CAN_RB_DLR & 0x0F); // &0x0F = 00001111, get first 4 bits

        switch(msg_type)  {
        
          case MSG_CMD:  // message for this ecu to set a variable to value in message
          case MSG_RSP:  // message in reply to this ecu's request for a variable value
        	               // value in the data buffer in received msesage
            if(getCANdat)  {
              // set up for serial xmit of the recvd CAN bytes (<= 8)
              // MegaTune getting back data requested from auxillary board
              txcnt = 0;
              txgoal = var_byt;
              if((txgoal > 0) && (txgoal <= 8))  {
                txmode = 6;
                for(jx = 0;jx < var_byt;jx++)  {
                  *((char *)&txbuf + jx) = *(&CAN_RB_DSR0 + jx);
                }
                SCI0DRL = *((char *)&txbuf);
                SCI0CR2 &= ~0x24;       // rcv, rcvint disable
                SCI0CR2 |= 0x88;        // xmit enable & xmit interrupt enable
              } 
              else  {
                txgoal = 0;
                txmode = 0;
              }
              getCANdat = 0;
              ltch_CAN = 0xFFFFFFFF;
              break;
            } 
            else  {
              // update variable value with received data
              for(jx = 0;jx < var_byt;jx++)  {
                *(canvar_blkptr[var_blk] + var_off + jx) = 
                       *(&CAN_RB_DSR0 + jx);
                       
             /* testing CAN comms */ 
             // rpm is offset 6, kpa is offset 18 (don't use baro = 16)
             //
             // var_blk is the table (block) reference ID (2 for msvar[])
             //
             // We grab 8 bytes at a time, so:
             //
             // var_off runs 0 to number of bytes in the data in 8 byte increments,
             //    ex. 0 to 104 for 112 byte MS-II outpc.
             //
             // jx runs 0 to 7 bytes (added to the var_off bytes to get the total offset)
             // 
             // So the value at offset 36 is referenced by
             // var_blk == 2  (msvar)
             // var_off == 32 (the next smaller that is evenly divisible by 8)
             // jx = 3 (which is added to 32 to get 36)
                     
                msvar[var_off + jx]  =  *(&CAN_RB_DSR0 + jx); // fill the msvar[] array

              }
            }
            break;
        		
          case MSG_REQ:  // msg to send back current value of variable(s)
            // Update related parameters for xmt ring buffer
            jx = can[0].cxno_in;
            can[0].cx_msg_type[jx] = MSG_RSP;
            // destination var blk
            can[0].cx_destvarblk[jx] = CAN_RB_DSR0;
            dvar_off = ((unsigned short)CAN_RB_DSR1 << 3) |
                                     ((CAN_RB_DSR2 & 0xE0) >> 5);
            can[0].cx_destvaroff[jx] = dvar_off;
            can[0].cx_dest[jx] = rcv_id;
            var_byt = CAN_RB_DSR2 & 0x1F;
            can[0].cx_varbyt[jx] = var_byt;
            // put variable value(s) in xmit ring buffer
            for(kx = 0;kx < var_byt;kx++)  {
              can[0].cx_datbuf[jx][kx] = 
                   *(canvar_blkptr[var_blk] + var_off + kx);
            }
            // This is where (in xmt buffer) to put next messge
            if(can[0].cxno_in < (NO_CANMSG - 1))
              can[0].cxno_in++;
            else
              can[0].cxno_in = 0;
            // increment counter
            if(can[0].cxno < NO_CANMSG)
              can[0].cxno++;
            else
              can[0].cxno = NO_CANMSG;
            if(!(CANTIER & 0x07))  {
              // Following will cause entry to TxIsr without sending msg
              // since when CANTIER = 0, CANTFLG left as buff empty.
              // If CANTIER has at least 1 int buf enabled, will enter
              // TxIsr automatically. 
              CANTBSEL = CANTFLG;
              CANTIER = CANTBSEL;
            }
            break;
        		
          case MSG_XSUB:  // msg to execute a subroutine
            switch(rcv_id)  {
              case 1:           // message to this ecu from device 1
                can_xsub01();		// execute sub immediately here (set
                                // flag if can execute in main loop) 
                break;
            }
        	  break;
        	  
          case MSG_BURN:  // msg to burn data table
            flocker     = 0xCC;    // set to prevent burning flash thru runaway code
            burn_idx = var_blk;
            burn_flag   = 1;
        	  break;
        }					 // end msg_type switch
        can_status &= CLR_RCV_ERR;
    }
    if((CANRFLG & 0x72) != 0)  {
        // Rcv error or overrun on receive
        can_status |= RCV_ERR;
        //can_reset = 1;
    }
    /* clear RX buf full, err flags to ready for next rcv int */
    /*  (Note: can't clear err count bits) */
    CANRFLG = 0xC3;
	
    return;
}
#pragma CODE_SEG DEFAULT        


#pragma CODE_SEG DEFAULT        

void can_xsub01(void)  {
  return;
}

#pragma CODE_SEG DEFAULT


// ------ Unimplemented ISRs -------------------------------------------------
#pragma CODE_SEG  NON_BANKED
INTERRUPT void UnimplementedISR(void) {
   /* Unimplemented ISRs trap.*/
   /* Do nothing and return.  */
   return;
}

#pragma CODE_SEG DEFAULT 

//------- flash burner -------------------------------------------------------
/******************************************************************************
Arguments:	

      progAdr		Pointer to the start of the destination 
			          Flash location to be programmed.
      bufferPtr	Pointer to the start of the source data. 
      size      Number of WORDS to be programmed.
                  
 This function will program non-paged flash only 
******************************************************************************/
void fburner(unsigned int* progAdr, unsigned int* bufferPtr, 
                                          unsigned int no_words)
{
unsigned char sect,no_sectors;
																
  	if(flocker != 0xCC) return; // if not set, we got here unintentionally
  	// sector = 1024 bytes (512 words)
  	if(burn_flag == 1)  {
  	  no_sectors = (unsigned char)(no_words / SECTOR_WORDS);
  	  if(no_words > (no_sectors * SECTOR_WORDS))no_sectors++;
  	  for (sect = 0; sect < no_sectors; sect++)  {  	  
  	    Flash_Erase_Sector(progAdr + sect * SECTOR_WORDS);
  	  }
  	}
  	Flash_Write_Word(progAdr + burn_flag-1, *(bufferPtr + burn_flag-1));
    	
	return;
}

//*****************************************************************************
//* Function Name: Flash_Init
//* Description : Initialize Flash NVM for HCS12 by programming
//* FCLKDIV based on passed oscillator frequency, then
//* uprotect the array, and finally ensure PVIOL and
//* ACCERR are cleared by writing to them.
//*
//*****************************************************************************
void Flash_Init(unsigned long oscclk)  {
unsigned char fclk_val;
unsigned long temp;
  /* Next, initialize FCLKDIV register to ensure we can program/erase */
  temp = oscclk;
  if (oscclk >= 12000) {
    fclk_val = oscclk/8/200 - 1; /* FDIV8 set since above 12MHz clock */
    FCLKDIV = FCLKDIV | fclk_val | FDIV8;
  }
  else
  {
    fclk_val = oscclk/8/200 - 1;
    FCLKDIV = FCLKDIV | fclk_val;
  }
  FPROT = 0xFF; /* Disable all protection (only in special modes)*/
  FSTAT = FSTAT | (PVIOL|ACCERR);/* Clear any errors */
  return;
}

void Flash_Erase_Sector(unsigned int *address)  {
  FSTAT = (ACCERR | PVIOL); // clear errors
  (*address) = 0xFFFF;/* Dummy store to page to be erased */
  FCMD = ERASE;

/*********************************************************************
;* Allow final steps in a flash prog/erase command to execute out
;* of RAM while flash is out of the memory map
;* This routine can be used for flash word-program or erase commands
;*
;********************************************************************/
/* Execute the sub out of ram  */
// Note that "asm" means the following statements -- in {} here -- are in 
// assembly language
   asm {
     ldaa  #CBEIF
     jsr  RamBurnPgm
   };
   return;
}

void Flash_Write_Word(unsigned int *address, unsigned int data)  {
  FSTAT = (ACCERR | PVIOL); // clear errors
  (*address) = data;        // Store desired data to address being programmed
  FCMD = PROG;              // Store programming command in FCMD

   /* Execute the sub out of ram  */
   asm {
     ldaa  #CBEIF
     jsr  RamBurnPgm
   };
  return;
}


// The following functions may help in debugging code 

#pragma CODE_SEG ROM_7000
void blink_gpo3(int blinkCount) 
  {
    // Flash LED on GPO3 (warm-up LED on stim) 'flashCount' times
    // at ~1.0 Hz  (PM5 LED normally on only in 3 and 4th gears)
    // This function is for debug only - can flash at certain events, or 
    // indicate current gear, etc.
    // Nothing else happens while LED is flashing.
    //        _________        ________         ________
    // ->_____         ________        _________        ______________________->
    //   i=   0        1       2       3        4       5
    // ex. for three blinks, i= 0 to 5
    
   int cycles = 65000; // 65000 is approx 1/2 second
   int ix;
   
  for(ix = 0; ix < 8; ix++)  {
    pPTMpin[ix] = pPTM;   // port M
    pPTTpin[ix] = pPTT;   // port T
  } // end for (ix...      
   
  ix=0;

START_LOOPING:    
    // PE4/VB3/FP LED on
    PORTE |= 0x10;        // initially turn on LED on PE4 (0x10 = 16 = 00010000)
    *pPTMpin[5] |= 0x20;  // toggle LED on PM5 (0x20 = 32 = 00100000)    
    // Leave LED off and wait
    waitAwhile(65000);    // wait � second
           
   // LED on     
//   PORTE &= ~0x10;        // turn off LED off PE4 (0x10 = 16 = 0010000)
//    *pPTMpin[5] &= ~0x20; // toggle LED on PM5 (0x20 = 32 = 00100000)
   // Leave LED on and wait                     
//   waitAwhile(65000);     // wait � second
         
   if (ix < blinkCount) goto START_LOOPING;    
    
    return;
     
  }   // End blink_gpo3() function
  
  
//////////////////////////////////////////////////////////////////////////////////////////////

#pragma CODE_SEG ROM_7000  
void waitAwhile(unsigned short int cycles)
// This is a 'do nothing' function that simply waits
// a cycle value of 121 is approximately 1 millisecond (need to adjust for main clock prescalar!!!)
// cycles can run from 0 to 65535, which is~541 millseconds or ~� second
  {
  unsigned short int i;
  unsigned short int start;
  i=0;
next_cycle:
    start = TCNT;
  // The timer clock ticks are counted in the variable TCNT (which is defined in the hsc12def.h file 
  // at memory location 0x0044). There are 0xFFFF 'tics' (65,536 counts).
  // In our case each 'tic' is 1/0.1875MHz  = 5.333 �s
  // However, there is some overhead associated with calling the function, comparing the values, etc.
check_again:
    if (TCNT>start) goto check_again;  // have not rolled over, proceed to next_step when do 
next_step:
    if (TCNT<start) goto next_step;  // have rolled over, and exceeded original TCNT,
                                     // so completed one cycle of TCNT 
   i++;                              // increment i
   if (i<cycles) goto next_cycle;    // and if i is less than cycles, do another
   
    return;                          // otherwise return
  }
     
  
