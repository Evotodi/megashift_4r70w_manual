/** ###################################################################
**     Filename  : GPIO_CW.C
**     Project   : GPIO_CW
**     Processor : MC9S12C64MFA25
**     Version   : Driver 01.12
**     Compiler  : CodeWarrior HC12 C Compiler
**     Date/Time : 02/09/2009, 8:37 PM
**     Abstract  :
**         Main module.
**         Here is to be placed user's code.
**     Settings  :
**     Contents  :
**         No public methods
**
**     (c) Copyright UNIS, spol. s r.o. 1997-2007
**     UNIS, spol. s r.o.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/
/* MODULE GPIO_CW */

/* Including used modules for compiling procedure */
#include "Cpu.h"
#include "Events.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

void main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */

  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END GPIO_CW */
/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 2.98 [04.12]
**     for the Freescale HCS12 series of microcontrollers.
**
** ###################################################################
*/
