;****************************************************
; This program required because stupid CW won't allow
; a jump to an address - it says "nothing more expected"
; and won't go any further.
;****************************************************
              xdef reboot
              xdef monitor
CORE1:       equ    $0000         ;ports A, B, E, modes, inits, test
INITRM:      equ    CORE1+$10     ;initialization of internal RAM position register
INITRG:      equ    CORE1+$11     ;initialization of internal registers position register
INITEE:      equ    CORE1+$12     ;initialization of internal EEPROM registers position register

reboot:
  jmp $F800						 ;go to serial mon pgm & restart ms II code
  rts
monitor:
  movb  #$00,INITRG    ;set registers at $0000 
  movb  #$39,INITRM    ;set ram to end at $3fff 
  movb  #$09,INITEE    ;set eeprom to end at $0fff
  clra;								 ; clear A reg
  sei;                 ; disable interrupts 
  jmp $F842						 ;go to serial mon pgm & wait for new code reload
  rts
