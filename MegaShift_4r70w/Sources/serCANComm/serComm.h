/**************************************************************************
**
** SCI Communications
**
** Communications is established when the PC communications program sends
** a command character - the particular character sets the mode:
**
** "a" = send all of the realtime display variables (outpc structure) via tx port.
** "w"+<offset lsb>+<offset msb>+<nobytes>+<newbytes lsb, msb> = 
**    receive updated data parameter(s) and write into offset location
**    relative to start of data block, Can write to RAM only via serial (use 't'
**    below for Flash only tables) or write via CAN to Ram or Flash-only tables.
** "e" = same as "w" above, followed by "r" below to echo back value
** "r"+<offset lsb>+<offset msb>+<nobytes lsb, msb> = read and send back values
**     of a data parameter or block in table offset location.  Can be via serial 
**     or CAN. (Will read from RAM if Ram table exists, else will read Flash,) 
** "y" = verify inpram data block = inpflash data block, return no. bytes different.
** "b" = jump to flash burner routine and burn a ram data block into a flash 
**    data block.
** "t"+<tble_idx>+<tableWords of data> = receive new data for clt/mat/ego/maf 
**    FLASH tables via serial.
** "c" = Test communications - echo back Seconds
** "Q" = Send over Embedded Code Revision Number
** "S" = Send program title.
**
**************************************************************************/
interrupt void ISR_SCI_Comm(void)  {
  char dummy,save_page;
  int ix;
  static int xcntr;
  static unsigned char rd_wr,CANid,sendCANdat=0,next_txmode;
  unsigned char sect,nsect;
  static unsigned int txptr,tble_word,ntword,crxnbytes;
  
#define getCANid             40
#define getTableId           41
#define setBurningParameters 99
  
  // if RDRF register not set, => transmit interrupt
  if(!(SCI0SR1 & 0x20))goto XMT_INT;

// Receive Interrupt
  // Clear the RDRF bit by reading SCISR1 register (done above), then read data
  //  (in SCIDRL reg).
  // Check if we are receiving new input parameter update
txgoal = 0;
rcv_timeout = 0xFFFFFFFF;
if(SCI0SR1 & 0x08)  {	 // check for rcv Overrun error
   txmode = 0;
   dummy = SCI0DRL;
   SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
   kill_ser = 1;
   kill_ser_t = outpc.seconds + SER_TOUT;
   return;
}

switch(txmode)  {

case 0:

  switch(SCI0DRL)  {
    case 'a':     // send back all real time ram output variables
  next_txmode = 1;
  txmode = getCANid;
  cksum = 0;
  break;

    case 'w':		  // receive new ram input data and write into offset location;
                  //  also used for forwarding CAN msgs bet. TS & gpio boards.
                  //  With this cmd in CAN pass-thru mode, 
  next_txmode = 5;
  txmode = getCANid;
  rd_wr = 1;
  break;

    case 'e':		  // same as 'w', but verify by echoing back values, up to 
                  //  sizeof(txbuf). 
  next_txmode = 5;    // *** Don't use with CAN. ***
  txmode = getCANid;
  rd_wr = 2;
  break;

    case 'r':		  // read and send back ram input data from offset location;
                  //  also used for forwarding CAN msgs bet. TS/ MT & gpio boards,
  next_txmode = 5;    // but *** no_bytes must be < sizeof(txbuf). ***
  txmode = getCANid;
  rd_wr = 0;
  cksum = 0;
  break;

    case 'y':      // Verify that a flash data block matches a
  next_txmode = 2; //  corresponding ram data block
  txmode = getCANid;
  break;
   	
    case 'b':        // burn a block of ram input values into flash;
                  //  also used for forwarding CAN msgs bet. MT & auxilliary boards
  next_txmode = setBurningParameters;
  txmode      = getCANid;
  break;

    case 't':        // update a flash table with following serial data
  txmode = 20;
  CANid = can_id;
  cksum = 0;
  break;

    case '!':        // start receiving reinit/reboot command
  txmode = 30;
  break;

    case 'c':        // send back seconds to test comms
  txcnt = 0;         
  txmode = 1;
  txgoal = 2;        // seconds is 1st 2 bytes of outpc structure
  tble_idx = 6;
  xcntr = 0;
  txbuf[0] = (char)(outpc.seconds & 0x00FF);
  txbuf[1] = (char)((outpc.seconds & 0xFF00) >> 4);
  SCI0DRL = txbuf[0];
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;

    case 'Q':         // send code rev no.
  txcnt = 0;
  txmode = 4;
  txgoal = 20;
  SCI0DRL = RevNum[0]; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;

    case 'S':         // send program title
  txcnt = 0;
  txmode = 5;
  txgoal = 32;
  SCI0DRL = Signature[0]; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;
  
    case 'k':         // request checksum.
  txcnt = 0;
  txmode = 0;
  txgoal = 1;
  SCI0DRL = cksum; 
  SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  break;

    default:
  break;
  }     // End of switch for received command
    break;

case getCANid: // Get CAN id for current command.
   CANid = SCI0DRL;
   if (CANid < MAX_CANBOARDS)
     txmode = getTableId;
   else	 {
  	 // CANid wrong - kill comms since don't know where to send data
     txmode = 0;
     SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
     kill_ser = 1;
     kill_ser_t = outpc.seconds + SER_TOUT;
     return;
   }
   break;
case getTableId: // Get table id for current command.
   tble_idx = SCI0DRL;
   if(CANid != can_id)  {
     txmode = next_txmode;
   } 
   else if(tble_idx >= NO_TBLES)  {
  	 // tble index wrong
     txmode = 0;
     //  kill comms since don't know which/ how much data
     SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
     kill_ser = 1;
     kill_ser_t = outpc.seconds + SER_TOUT;
     return;
   }
   else
     txmode = next_txmode;
   next_txmode = 0;
   if (txmode == 1) { 
     txcnt = 0;
     txgoal = sizeof(outpc);
     // load output variables into txbuf to avoid incoherent word data.
     // To work all words in structure must be word aligned, all longs
     // aligned on long boundaries.
     xcntr = 0;
     txptr = 0;
     *tableWordRam(tble_idx,0) = *((unsigned int *)(&outpc) + 0);
     *tableWordRam(tble_idx,1) = *((unsigned int *)(&outpc) + 1);
     SCI0DRL = *tableByteRam(tble_idx, 0);
     cksum += SCI0DRL; 
     SCI0CR2 &= ~0x24;   // rcv, rcvint disable
     SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
   }
   else if (txmode == setBurningParameters) { // Burn command
     if(CANid != can_id)  {
  	   // set up single CAN message & forward to aux board
  	   ix = can[0].cxno_in;
  	   can[0].cx_msg_type[ix] = MSG_BURN; 
  	   can[0].cx_destvarblk[ix] = tble_idx;
  	   can[0].cx_destvaroff[ix] = 0;
  	   can[0].cx_dest[ix] = CANid;
  	   can[0].cx_varbyt[ix] = 0;		 // no data bytes
  	   send_can(0);
     }
     else if(flocker == 0) { 
       flocker     = 0xCC;    // set semaphore to prevent burning flash thru runaway code
       burn_flag   = 1;
       burn_idx = tble_idx;
     }
     txmode      = 0;			 // handle burning in main loop
   } 
   else if(txmode == 2)  {
     vfy_flg = 1;
   	 vfy_fail = 0;
   }
   break;

case 5:
  	if(CANid != can_id)  {
  	  // TS wants to send data to aux board via CAN
  	  if(rd_wr >= 1)  {
  	    sendCANdat = CANid;
  	    rd_wr = 1;   // can't have echo in CAN
  	  } 
  	  // TS wants to request data from aux board via CAN
  	  else  {
  	    getCANdat = CANid;
  	  }
  	}
  	else  {          // serial only
  	  sendCANdat = 0;
  	  getCANdat = 0;
  	}
  	rxoffset = (SCI0DRL << 8); // byte offset(msb) from start of inpram
  	txmode++;
  	break;

case 6:
  	rxoffset |= SCI0DRL;    // byte offset(lsb) from start of inpram
  	txmode++;
  	break;
  	
case 7:
  	rxnbytes = (SCI0DRL << 8); 	// no. bytes (msb)
  	txmode++;
  	break;

case 8:
  	rxnbytes |= SCI0DRL;		    // no. bytes (lsb)
  	rxcnt = 0;
  	// check won't blow table arrays
  	if(CANid == can_id)  {
  	  if(rxoffset + rxnbytes > tableBytes(tble_idx))  {
  	    //  kill comms since don't know how much data being sent
  	    txmode = 0;
  	    SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
  	    kill_ser = 1;
  	    kill_ser_t = outpc.seconds + SER_TOUT;
  	    return;
  	  }
  	}
  	if(rd_wr == 0)  {		    // read & send back input data
  	  if(getCANdat)  {
  	    txmode = 0;         // done receiving all request data from TS
  	    // set up single CAN message & forward to aux board
  	    //   TS requesting data from aux board.
  	    //   Note: TS must only deal with 1 board at a time and wait til
  	    //   data back or timeout.
  	    ix = can[0].cxno_in;
  	    can[0].cx_msg_type[ix] = MSG_REQ; 
  	    can[0].cx_destvarblk[ix] = tble_idx;
  	    can[0].cx_myvarblk[ix] = 6;   // txbuf
  	    can[0].cx_destvaroff[ix] = rxoffset;
  	    can[0].cx_myvaroff[ix] = 0;
  	    can[0].cx_dest[ix] = CANid;
  	    // rxnbytes must be <= 255
  	    can[0].cx_varbyt[ix] = (unsigned char)rxnbytes;
  	    send_can(0);
  	    ltch_CAN = lmms + 3906;   // 0.5 sec timeout to get CAN data
  	    ctxboff = 0;
  	    SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  	  } 
  	  else  {               // serial read only
  	    txcnt = 0;
  	    txmode = 3;
  	    txgoal = rxnbytes;
  	    if(tables[tble_idx].addrRam != NULL)
  	      SCI0DRL = *tableByteRam(tble_idx, rxoffset);
  	    else  {
  	      save_page = PPAGE;
  	      PPAGE = tableFPage(tble_idx);
  	      SCI0DRL = *tableByteFlash(tble_idx, rxoffset);
  	      PPAGE = save_page;
  	    }
  	    cksum += SCI0DRL;
  	    // finish reading/sending rest in serial xmit mode
  	    SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  	    SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  	  }
  	}
  	else  {        // write data to input/ table data block or via CAN
  	  txmode++;
  	  crxnbytes = 0;
  	}
  	break;

case 9:
  	// Check for data overrun.  Note: this
  	// still not bullet proof because could write half the bytes
  	// then have overrun; need to store and write all at once if
  	// all is valid.
  	if((SCI0SR1 & 0x08) == 0)  {   // none yet- write data.
  	  if(sendCANdat)  {
  	    // want to buffer for 8 bytes for CAN pass thru
  	    txbuf[crxnbytes] = SCI0DRL;
  	  } 
  	  else  {   
  	    // write directly to RAM - don't need buffer since pc 
  	    //  data will be coherent
  	    *tableByteRam(tble_idx, rxoffset + rxcnt) = SCI0DRL;
  	  }
  	}
  	else  {
  	  txmode = 0;
  	  dummy = SCI0DRL;
  	  SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
  	  kill_ser = 1;
  	  kill_ser_t = outpc.seconds + SER_TOUT;
  	  return;
  	}
  	rxcnt++;
  	crxnbytes++;
  
  	if(sendCANdat)  {
  	  if((crxnbytes >= 8) || (rxcnt >= rxnbytes))  {
  	    // set up single CAN message & forward to aux board
  	    //   TS sending data to aux board
  	    // CAN faster than serial, so won't get overrun
  	    for(ix = 0; ix < crxnbytes; ix++)  {
  	      can[0].cx_datbuf[can[0].cxno_in][ix] = txbuf[ix];
  	    }
  	    ix = can[0].cxno_in;
  	    can[0].cx_msg_type[ix] = MSG_CMD; 
  	    can[0].cx_destvarblk[ix] = tble_idx;
  	    can[0].cx_destvaroff[ix] = rxoffset;
  	    can[0].cx_dest[ix] = sendCANdat;
  	    can[0].cx_varbyt[ix] = (unsigned char)crxnbytes;
  	    send_can(0);
  	    rxoffset += crxnbytes;
  	    crxnbytes = 0;
  	  }
  	}

  	if(rxcnt >= rxnbytes)  {
  	  dummy = SCI0DRL;      // clear OR bit (read sci0sr1(done), sci0drl)
  	  if(rd_wr == 2)  {
  	    txcnt = 0;         // echo mode: send back data just written
  	    txmode = 3;        //	 for verification
  	    txgoal = rxnbytes;
  	    SCI0DRL = *tableByteRam(tble_idx, rxoffset);
  	    SCI0CR2 &= ~0x24;   // rcv, rcvint disable
  	    SCI0CR2 |= 0x88;    // xmit enable & xmit interrupt enable
  	  }
  	  else  {
  	    txmode = 0;         // done receiving/writing all the data
  	    sendCANdat = 0;
  	  } 
  	}       // finished all rxnbytes
  	break;

case 20:	 // d/load & burn tables - *** Do while engine NOT turning ***
  	// Note: this type table has no ram copy - its not intended for tuning
  	tble_idx = SCI0DRL;    // type of table
  	if(tble_idx > NO_TBLES)	 {
  	  // tble index wrong - kill comms since don't know how much data being sent
  	  txmode = 0;
  	  SCI0CR2 &= ~0xAC;   // rcv, xmt disable, interrupt disable
  	  kill_ser = 1;
  	  kill_ser_t = outpc.seconds + SER_TOUT;
  	  return;
  	}
  	ntword = 0;
  	flocker = 0xCC; // set semaphore to prevent burning flash thru runaway code
  	save_page = PPAGE;
  	PPAGE = tableFPage(tble_idx);
	  // erase table
  	nsect = (unsigned char)(tableWords(tble_idx)/SECTOR_WORDS);
  	if(tableWords(tble_idx) > (nsect*SECTOR_WORDS))nsect++;
  	for (sect = 0; sect < nsect; sect++)  {
  	  Flash_Erase_Sector(tableWordFlash(tble_idx, 0) + sect * SECTOR_WORDS);
  	}
  	PPAGE = save_page;
  	flocker = 0;
  	txmode++;
  	break;

case 21:
  	cksum += SCI0DRL;
  	tble_word = (SCI0DRL << 8);    // msb
  	txmode++;
  	break;

case 22:
  cksum += SCI0DRL;
  tble_word |= SCI0DRL;            // lsb
  flocker = 0xCC; // set semaphore to prevent burning flash thru runaway code
  save_page = PPAGE;
  PPAGE = tableFPage(tble_idx);
  // write table word
  Flash_Write_Word(tableWordFlash(tble_idx, ntword), tble_word);
  flocker = 0;
  ntword++;
  if(ntword < tableWords(tble_idx))
    txmode = 21;
  else  {
    txmode = 0;
  }
  PPAGE = save_page;
  break;

case 30:
  	if(SCI0DRL == '!')    // receive 2nd ! for reboot signal
  	  txmode++;
  	else if(SCI0DRL == 'x')  {  // received reinit signal (!x)
  	  reinit_flag = 1;
  	  txmode = 0;
  	}
  	else
  	  txmode = 0;
  	break;

case 31:
  	if(SCI0DRL == 'x')  {   // received complete reboot signal(!!x)
  	  PPAGE = 0x3C;  	    
  	  // go to start of bootload pgm
  	  //asm (jmp $F800);
  	  (void)reboot();
  	}
  	else if(SCI0DRL == '!')  {   // received complete reload signal(!!!)
  	  /*
  	  PPAGE = 0x3C;  	    
  	  SCI0CR1 = 0x00;
  	  SCI0CR2 = 0x00;      // disable Tx, Rx
  	  // go to start of monitor code
  	  //asm (jmp $F842);
  	  (void)monitor();
  	  */
  	}
  	txmode = 0;
  	break;

default:
  dummy = SCI0DRL;   // dummy read of reg to clear int flag
  break;

  }     // End of case switch for received data
    
  if((txgoal == 0) && (txmode > 0))
    rcv_timeout = lmms + 3906;   // 0.5 sec timeout
  return;

// Transmit Interrupt
XMT_INT:
  // Clear the TDRE bit by reading SCISR1 register (done), then write data
  txcnt++;
  if(txcnt < txgoal)  {
    if(txmode == 3)  {     // answer serial data request via ser data xmit
  	  if(tables[tble_idx].addrRam != NULL)
  	    SCI0DRL = *tableByteRam(tble_idx, rxoffset + txcnt);
  	  else  {
  	    save_page = PPAGE;
  	    PPAGE = tableFPage(tble_idx);
  	    SCI0DRL = *tableByteFlash(tble_idx, rxoffset + txcnt);
  	    PPAGE = save_page;
  	  }
  	  if(rd_wr == 0)
  	    cksum += SCI0DRL;
    }
    else if(txmode == 4)  {
  	  SCI0DRL = RevNum[txcnt];
    }
    else if(txmode == 5)  {
  	  SCI0DRL = Signature[txcnt];
    }
    else if(txmode == 2)  {
  	  SCI0DRL = *((char *)&vfy_fail + txcnt);
    } 
    else if(txmode == 6)  {
  	  // pass on CAN msg to TS/ MT via serial xmit
  	  // make sure serial not ahead of CAN
  	  if(txcnt < ctxboff)  {
  	    SCI0DRL = txbuf[txcnt];
  	    cksum += SCI0DRL;
  	  }
    } 
    else  {    // (txmode = 1) send back outpc data via serial 
      xcntr++;
      SCI0DRL = *tableByteRam(tble_idx, xcntr);
      cksum += SCI0DRL;
      if(xcntr >= 3)  {	 // transmitted 4 bytes, load 2 new words
        xcntr = -1;
        txptr += 2;
        *tableWordRam(tble_idx,0) = *((unsigned int *)(&outpc) + txptr);
        *tableWordRam(tble_idx,1) = *((unsigned int *)(&outpc) + txptr+1);
      }
    }
  } 
  else  {   // done transmitting
    if(txmode == 1)
//      outpc.cksum = cksum;
    txcnt = 0;
    txgoal = 0;
    txmode = 0;
    SCI0CR2 &= ~0x88;    // xmit disable & xmit interrupt disable
    SCI0CR2 |= 0x24;     // rcv, rcvint re-enable
  } 
  return;
}
