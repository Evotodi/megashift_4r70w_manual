typedef struct {
  /* CAN Xmt mssge ring buffer:
      can[0] is to hold Rx,TxISR messages,
      can[1] for main loop messages (so don't get clobbered by ISR)
      cxno = no msgs in queue waiting to be sent out
      cxno_in = index for inserting a msg in queue (incr after insert)
      cxno_out = index for sending out a msg (incr after load CAN buf)
      msg_type = CMD,REQ,RESP,XSUB (= set value, request value, respond 
          to a request for value, execute a subroutine)
      varblk,varoffset,varbyte = blk no of data structure, byte offset 
          from start of structure, no. bytes of data
      datbuf = the actual data (max of 8 bytes)
      dest = id no. of device to which msg being sent. 
  */
  unsigned char cxno,cxno_in,cxno_out;
  unsigned char cx_msg_type[NO_CANMSG], cx_myvarblk[NO_CANMSG], 
	  cx_destvarblk[NO_CANMSG], cx_dest[NO_CANMSG],cx_varbyt[NO_CANMSG],
	  cx_outmsg_no[NO_CANMSG];     // only applies to OUTMSGs:
      // msg_no is an arbitray number(0-MAX_OUTMSGS) assigned to each message                              
      //  configured. When a request for this message number is received, 
      //  ECU determines the bytes and table offset needed from the outmsg structure in flash.
  unsigned short cx_myvaroff[NO_CANMSG],cx_destvaroff[NO_CANMSG];
  unsigned char cx_datbuf[NO_CANMSG][8];  // max msg data = 8 bytes
} canmsg;

typedef struct {
   unsigned short xrate;   // =0 means the message is only transmitted when requested, 
             //  otherwise it indicates the clock speed in 0.128 tics at which
             //  a CAN message will be transmitted automatically after receiving 
             //  a request for data by setting the activate_xrate mth bit= 1,
             //  where m = msg no = 0 to MAX_OUTMSGS.
   unsigned char no_bytes; // The total number of bytes of all the variables in the message.                             
                           //  Must be <= sizeof(outpc).
   unsigned char dest_varblk;   // destination ID of message (msb, 4 bits) and variable block 
                                //  (lsb, 4 bits) in destination device where message is to be put.
   unsigned char offset[MAX_BYTES_OUTMSG];    //  This holds the byte offsets of each of the    
                           //  bytes in the message relative to start of  outpc. For a short, 
                           //  specify 2 consecutive offsets in the proper order for the requesting 
                           //  processor.                            
} can_outmsg;
