// CAN prototypes and defines
void CanInit(void) ;
interrupt void CanTxIsr(void);
interrupt void CanRxIsr(void);
void send_can(unsigned char buf);
void can_xsub01(void);

#define	MSG_CMD	0
#define	MSG_REQ	1
#define	MSG_RSP	2
#define	MSG_XSUB	3
#define	MSG_BURN	4
#define	OUTMSG_REQ	5
#define	OUTMSG_RSP	6
#define	MSG_XTND 	7
// define xtended msgs from 8 on
#define NO_CANMSG 10
#define MAX_CANBOARDS 16
#define  MAX_BYTES_OUTMSG  24    // enough for 3 8-byte packets.
#define  MAX_OUTMSGS  8    // 4 GPIOs can specify 2 outmessages each,
                           // and each outmsg can hold up to 24 bytes
// Error status words: 
//    -bits 0-7 are current errors
//    -bits 8-15 are corresponding latched errors
#define	XMT_ERR			0x0101
#define	CLR_XMT_ERR		0xFFFE
#define	XMT_TOUT		0x0202
#define	CLR_XMT_TOUT	0xFFFD
#define	RCV_ERR			0x0404
#define	CLR_RCV_ERR		0xFFFB
#define	SYS_ERR		0x0808
