// Send Spark and Fuel Adjustments to Send to MS-II
void sendCANAdj(void)  
  {
    /*
     char jx;
    
    // if spark and idle inputs both zero, do not send 
    // (do not corrupt non-B&G code)

     if (!in2ram.IACAdjngear && !in2ram.AdvAdjngear) return;
    
    // send new Fuel,Spk,Idle and Spare adjustments
    if((outpc.FuelAdj != FuelAdjl) || (outpc.SpkAdj != SpkAdjl) ||
       (outpc.IdleAdj != IdleAdjl) || (outpc.SprAdj != SprAdjl))  
       {
       // there has been a change - send data via CAN
       // load main loop ring buffer(can[1]) - send data
       jx = can[1].cxno_in;
       can[1].cx_msg_type[jx] = MSG_CMD; 
       can[1].cx_datbuf[jx][0] = *((char *)&outpc.FuelAdj);
       can[1].cx_datbuf[jx][1] = *((char *)&outpc.FuelAdj + 1);
       can[1].cx_datbuf[jx][2] = *((char *)&outpc.SpkAdj);
       can[1].cx_datbuf[jx][3] = *((char *)&outpc.SpkAdj + 1);
       can[1].cx_datbuf[jx][4] = *((char *)&outpc.IdleAdj);
       can[1].cx_datbuf[jx][5] = *((char *)&outpc.IdleAdj + 1);
       can[1].cx_datbuf[jx][6] = *((char *)&outpc.SprAdj);
       can[1].cx_datbuf[jx][7] = *((char *)&outpc.SprAdj + 1);
       // Put FuelAdj, etc in ECU, in2ram.FuelAdj, etc.
       can[1].cx_destvarblk[jx] = inpram.Adj_blk;
       // below is offset of FuelAdj, etc from start of in2ram in ECU
       can[1].cx_destvaroff[jx] = inpram.Adj_varoffset;
       can[1].cx_dest[jx] = inpram.ms2canID;		// send to ECU (board 0)
       can[1].cx_varbyt[jx] = 8;	 // sending 8 bytes (4 words)
       send_can(1);
       } // end if ((outpc.FuelAdj != FuelAdjl) ....
    // Capture values so we know not to send if they have not changed
    FuelAdjl = outpc.FuelAdj;
    SpkAdjl = outpc.SpkAdj;
    IdleAdjl = outpc.IdleAdj;
    SprAdjl = outpc.SprAdj;
    */
    return;
  }