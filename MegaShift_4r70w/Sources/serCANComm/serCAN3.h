// common serial and CAN variables
unsigned char cksum;
unsigned char txbuf[100];

// serial variables
unsigned char flocker,txmode,tble_idx,burn_idx,reinit_flag;
unsigned short txcnt,txgoal,rxoffset,rxnbytes,rxcnt,kill_ser_t,
  vfy_fail;
char kill_ser,vfy_flg;
unsigned long rcv_timeout;

// CAN variables
unsigned long cansendclk,ltch_CAN=0xFFFFFFFF;
unsigned short can_status,ctxboff,Rdvaroff,Rvaroff;
unsigned char can_reset,can_id,getCANdat=0,burnCANdat=0,
  CANtx,CANrx,RFlag,Rbytes,Rvarblk;

canmsg can[2];

unsigned char n_outbytes[MAX_OUTMSGS];  // counts bytes (1-24) in an outmsg
