void CanInit(void)
{
unsigned char ix;
	/* Set up CAN communications */
	/* Enable CAN, set Init mode so can change registers */
	CANCTL1 |= 0x80;
	CANCTL0 |= 0x01;
	
	/* clear ring buffers */
	for(ix = 0;ix < 2;ix++)  {
	  can[ix].cxno = 0;
	  can[ix].cxno_in = 0;
	  can[ix].cxno_out = 0;
	}
	can_status = 0;
	getCANdat = 0;
	RFlag = 0;
	
	while(!(CANCTL1 & 0x01));  // make sure in init mode
	
	/* Set Can enable, use IPBusclk (24 MHz),clear rest */
	CANCTL1 = 0xC0;  
	/* Set timing for .5Mbits/ sec */
	CANBTR0 = 0xC2;  /* SJW=4,BR Prescaler= 3(24MHz CAN clk) */
	CANBTR1 = 0x1C;  /* Set time quanta: tseg2 =2,tseg1=13 
	              (16 Tq total including sync seg (=1)) */
	CANIDAC = 0x00;   /* 2 32-bit acceptance filters */
	/* CAN message format:
	 Reg Bits: 7 <-------------------- 0
	  IDR0:    |---var_off(11 bits)----|  (Header bits 28 <-- 21)
	  IDR1:    |cont'd 1 1 --msg type--|  (Header bits 20 <-- 15)
	  IDR2:    |---From ID--|--To ID---|  (Header bits 14 <--  7)
	  IDR3:    |--var_blk-|--spare--rtr|  (Header bits  6 <-- 0,rtr)
	*/  
	CAN_TB0_IDR3 = 0;
	/* Set identifier acceptance and mask registers to accept 
	     messages only for can_id or device #15 (=> all devices) */
	/* 1st 32-bit filter bank-to mask filtering, set bit=1 */
	CANIDMR0 = 0xFF;           // anything ok in IDR0(var offset)
	CANIDAR1 = 0x18;           // 0,0,0,SRR=IDE=1
	CANIDMR1 = 0xE7;		   // anything ok for var_off cont'd, msgtype
	CANIDAR2 = can_id;     // rcv msg must be to can_id, but
	CANIDMR2 = 0xF0;			 // can be from any other device
	CANIDMR3 = 0xFF;           // any var_blk, spare, rtr
	/* 2nd 32-bit filter bank */
	CANIDMR4 = 0xFF;           // anything ok in IDR0(var offset)
	CANIDAR5 = 0x18;           // 0,0,0,SRR=IDE=1
	CANIDMR5 = 0xE7;		   // anything ok for var_off cont'd, msgtype
	CANIDAR6 = 0x0F;			 // rcv msg can be to everyone (id=15), and
	CANIDMR6 = 0xF0;			 // can be from any other device
	CANIDMR7 = 0xFF;           // any var_blk, spare, rtr
	
	/* clear init mode */
	CANCTL0 &= 0xFE;  
	/* wait for synch to bus */
	while(!(CANCTL0 & 0x10));
	
	/* no xmit yet */
	CANTIER = 0x00;
	/* clear RX flag to ready for CAN recv interrupt */
	CANRFLG = 0xC3;
	/* set CAN rcv full interrupt bit */
	CANRIER = 0x01;
	return;
}

interrupt void CanTxIsr(void)
{
unsigned char ix,jx,jx1,kx,mtype,dest_id,msg_no,var_blk,
  var_byt,save_page;
unsigned short dvar_off;

CANtx++;
outpc.CANtx++;
  
/* CAN Xmit Interrupt */
CANTBSEL = CANTFLG;    // select MSCAN xmit buffer
// Check ring buffers and xfer to MSCAN buffer
for(ix = 0;ix < 2;ix++)  {
 if(can[ix].cxno)  {
 	jx = can[ix].cxno_out;
 	/* Set up identifier registers */
	CAN_TB0_IDR0 = (unsigned char)(can[ix].cx_destvaroff[jx] >> 3);
	                      // 8 high bits in IDR0, 3 low bits in IDR1
	CAN_TB0_IDR1 = 
	  (unsigned char)((can[ix].cx_destvaroff[jx] & 0x0007) << 5) | 
	               0x18 |           // SRR=IDE=1
                 can[ix].cx_msg_type[jx];			// 3 bits
	CAN_TB0_IDR2 = (can_id << 4) | can[ix].cx_dest[jx];
	CAN_TB0_IDR3 = (can[ix].cx_destvarblk[jx] << 4);
	/* Set xmt buffer priorities (lower is > priority) */
	CAN_TB0_TBPR = 0x02;
	
	/* set data in buffer */
	mtype = can[ix].cx_msg_type[jx];
	if(mtype == MSG_XTND)
	  mtype = can[ix].cx_datbuf[jx][0];
	switch(mtype)  {
		case MSG_CMD:  // msg for dest ecu to set a variable to val in msg
		case MSG_RSP:  // msg in reply to dest ecu's request for a var val
		  CAN_TB0_DLR = can[ix].cx_varbyt[jx];
		  for(kx = 0;kx < CAN_TB0_DLR;kx++)  {
		    *(&CAN_TB0_DSR0 + kx) = can[ix].cx_datbuf[jx][kx];
		  }
		  if(RFlag && (can[ix].cx_dest[jx] == 0))  {    
		                     // keep responding til all requested data sent
		    Rbytes -= 8;  // Rbytes = 8 1st time thru, and if < 8 will quit
		    if(Rbytes <= 8)  {
		      // this last message - turn off RFlag
		      RFlag = 0;
		      if(!Rbytes)break;   // nothing left to send
		      var_byt = Rbytes;
		    } 
		    else  {
		      var_byt = 8;
		    }
		    Rvaroff += 8;  // will not get here if just sent < 8 bytes
		    Rdvaroff += 8;
        // put next set of variable value(s) in xmit ring buffer
        jx1 = jx;             // old msg
        jx = can[0].cxno_in;  // new msg
        can[0].cx_msg_type[jx] = MSG_RSP;
        // destination var blk, offset, id
        can[0].cx_destvarblk[jx] = can[ix].cx_destvarblk[jx1];
        can[0].cx_destvaroff[jx] = Rdvaroff;
        can[0].cx_dest[jx] = can[ix].cx_dest[jx1];
        can[0].cx_varbyt[jx] = var_byt;
        if(tables[Rvarblk].addrRam != NULL)  {
          for(kx = 0;kx < var_byt;kx++)  {
            can[0].cx_datbuf[jx][kx] = 
                     *tableByteRam(Rvarblk, Rvaroff + kx);
          }
        }
        else  {
          save_page = PPAGE;
          PPAGE = tableFPage(Rvarblk);
          for(kx = 0;kx < var_byt;kx++)  {
            can[0].cx_datbuf[jx][kx] = 
                     *tableByteFlash(Rvarblk, Rvaroff + kx);
          }
          PPAGE = save_page;
        }
        send_can(0);
		  }
		  break;
		
		case MSG_REQ:  // msg to send back current value of variable(s)
		  // this 1st byte holds var blk for where to put rcvd data
		  // 2nd,3rd bytes hold var offset rel. to var blk and how
		  // many consecutive bytes to be sent back. If cx_varbyt > 8, a
		  // 4th byte added for it, otherwise it is last 5 bits of DSR2.
		  // NOTE: MSG_REQ w > 8 bytes meant only for TS.
		  // GPIOs should use outmsg for this purpose, otherwise need to
		  // have multiple RFlags = gpio ids w corresponding Rbytes, etc. 
		  CAN_TB0_DSR0 = can[ix].cx_myvarblk[jx];
		  CAN_TB0_DSR1 = (unsigned char)(can[ix].cx_myvaroff[jx] >> 3);
		  if(can[ix].cx_varbyt[jx] <= 8)  {
		    CAN_TB0_DLR = 3;
		    CAN_TB0_DSR2 = (unsigned char)((can[ix].cx_myvaroff[jx] & 0x0007) << 5) | 
		                                  can[ix].cx_varbyt[jx];
		  }
		  else  {
		    CAN_TB0_DLR = 4;
		    CAN_TB0_DSR2 = (unsigned char)((can[ix].cx_myvaroff[jx] & 0x0007) << 5); 
		    CAN_TB0_DSR3 = can[ix].cx_varbyt[jx];
		  }
		  break;
		
		case OUTMSG_REQ:  // gpio msg to ECU to send back current values of specific 
		  // outpc variables. IDR0-3(above) hold var blk, varoff for where in gpio to put
		  // the data to be sent back to gpio
		  /*   ****** sample message setup (placed in gpio mainloop or in a ISR, 
		                                                    eg timer)  ****** 
		  ix = can[0].cxno_in;
		  can[0].cx_msg_type[ix] = OUTMSG_REQ; 
		  // Put data from ECU in this gpio block      
		  can[0].cx_destvarblk[ix] = <varblk no.>;  // from tableDescriptor
		  can[0].cx_dest[ix] = 0;		// send to ECU (board 0)
		  // Below is offset from start of gpio block - it should
		  // always be 0. If more than 1 msg, ECU will send offset
		  // to count byte sent
		  can[0].cx_destvaroff[ix] = 0;
		  can[0].cx_outmsg_no[ix] = <msg no>;  // 0-MAX_OUTMSGS
		  send_can(0);
		  *******************************************************************/ 
		  CAN_TB0_DLR = 1;
		  CAN_TB0_DSR0 = can[ix].cx_outmsg_no[jx];
		  break;

		case OUTMSG_RSP:  // ECU sending msg to gpio device in reply to gpio's request for 
		  // specific values from outpc + offset to be sent to gpio destvarblk + destvaroff
		  msg_no = can[ix].cx_outmsg_no[jx];
		  if(n_outbytes[msg_no] < 8)  {
		    CAN_TB0_DLR = n_outbytes[msg_no];
		    n_outbytes[msg_no] = 0;
		  }  
		  else  {
		    CAN_TB0_DLR = 8;
		    n_outbytes[msg_no] -= 8;
		  }  
		  for(kx = 0;kx < CAN_TB0_DLR;kx++)  {
		    *(&CAN_TB0_DSR0 + kx) = can[ix].cx_datbuf[jx][kx];
		  }
		  if(n_outbytes[msg_no])  {
		    dest_id = can[ix].cx_dest[jx];
		    var_blk = can[ix].cx_destvarblk[jx];
		    // set up for next transmission
		    jx = can[0].cxno_in;
		    can[0].cx_msg_type[jx] = OUTMSG_RSP;
		    can[0].cx_outmsg_no[jx] = msg_no;
		    // destination var blk
		    can[0].cx_destvarblk[jx] = var_blk;
		    dvar_off = outmsg[msg_no].no_bytes - n_outbytes[msg_no];
		    can[0].cx_destvaroff[jx] = dvar_off;
		    can[0].cx_dest[jx] = dest_id;
		    if(n_outbytes[msg_no] >= 8)  {
		      var_byt = 8;
		    }
		    else  {
		      var_byt= n_outbytes[msg_no];
		    }
		    can[0].cx_varbyt[jx] = var_byt;
		    // put variable value(s) in xmit ring buffer as follows:
		    for(kx = 0;kx < var_byt;kx++)  {
		      can[0].cx_datbuf[jx][kx] = 
		           *((char *)&outpc + outmsg[msg_no].offset[dvar_off + kx]);
		    }
		    send_can(0);
		  }
		  break;
		  
		case MSG_XSUB:
		  CAN_TB0_DLR = 0;
		  break;
		  
		case MSG_BURN:
		  CAN_TB0_DLR = 0;
		  break;
	}     // end mtype switch
	// This is where (in xmt buffer) to get next outgoing message
	if(can[ix].cxno_out < (NO_CANMSG - 1))
		can[ix].cxno_out++;
	else
		can[ix].cxno_out = 0;
	if(can[ix].cxno > 0)
		can[ix].cxno--;    // TB buf loaded for xmit - decrement ring buf count
	can_status &= CLR_XMT_ERR;
	/* set CAN xmt interrupt bit and clear flag to initiate transmit */
	CANTIER |= CANTBSEL;  
	CANTFLG = CANTBSEL;  // 1 clears(buffer full), 0 is ignored
	break;               // only handle 1 buffer entry at time
 }
 if(ix == 1)  {    // nothing left in either ring buffer
  CANTIER = 0x00;  // leave CANTFLG as buffer empty, but disable interrupt
                   // Note: if this last xmt in buf, will re-neter ISR, but 
                   //  then exit because can[0,1].cxno = 0. 
  break;  
 }
}		        // end for loop
if((CANRFLG & 0x0C) != 0)  {
        // Xmt error count
        can_status |= XMT_ERR;
        //can_reset = 1;
}
return;
}

interrupt void CanRxIsr(void)
{
unsigned char rcv_id,mtype,var_blk,var_byt,jx,kx;
unsigned short var_off,dvar_off,tble_word,ntword;
static unsigned short canrxbytes;
unsigned char save_page,sect,nsect,msg_no;

    CANrx++;
	outpc.CANrx++;
	
    /* CAN Recv Interrupt */
    if(CANRFLG & 0x01)  {
    
        var_off = ((unsigned short)CAN_RB_IDR0 << 3) |
                       ((CAN_RB_IDR1 & 0xE0) >> 5);
        mtype = (CAN_RB_IDR1 & 0x07);
        if(mtype == MSG_XTND)
          mtype = CAN_RB_DSR0;
        rcv_id = CAN_RB_IDR2 >> 4;		// message from device rcv_id
        var_blk = CAN_RB_IDR3 >> 4;
        var_byt = (CAN_RB_DLR & 0x0F);

        switch(mtype)  {
        
          case MSG_CMD:  // msg for this ecu to set a variable to val in msg
          case MSG_RSP:  // msg in reply to this ecu's request for a var val
        	               // value in the data buffer in recvd msg
            if(getCANdat && (getCANdat == rcv_id))  {
              // set up for serial xmit of the recvd CAN bytes.
              //   TS getting back data requested from gpio board(pass-thru)
              for(jx = 0;jx < var_byt;jx++)  {
                txbuf[jx + ctxboff] = *(&CAN_RB_DSR0 + jx);
              }
              if(ctxboff == 0)  {
                // set up to start xmitting data back to TS; this process
                // will be self-sustaining because CAN data arrives faster 
                // than serial data goes out; *** sizeof(txbuf) is max size  
                // of request. *** 
                txcnt = 0;
                txgoal = rxnbytes;
                txmode = 6;
                SCI0DRL = txbuf[0];
                cksum += SCI0DRL;
                SCI0CR2 |= 0x88;        // xmit enable & xmit interrupt enable
              }
              else  {
                if(txcnt >= ctxboff)  {
                  // serial was ahead - restart xmits
                  SCI0DRL = txbuf[ctxboff];
                  cksum += SCI0DRL;
                }
              }
              ctxboff += var_byt;
              if(ctxboff >= rxnbytes - 1)  {
                // all done receiving; remaining data will be auto-serial-xmitted  
                getCANdat = 0;
                ltch_CAN = 0xFFFFFFFF;
              }
              break;
            } 
            else  {     // not pass-thru mode
              if(tables[var_blk].addrRam != NULL)  {
                // update ram variables with received data
                for(jx = 0;jx < var_byt;jx++)  {
                  *tableByteRam(var_blk, var_off + jx) =
                       *(&CAN_RB_DSR0 + jx);
                }
              } 
              else  {     // burn entire flash-only table with recvd data
                if(!burnCANdat)  {
                  // erase flash sector(s) 
                  burnCANdat = 1; 
                  canrxbytes = 0;
                  flocker = 0xCC; // set semaphore to prevent burning flash thru runaway code
                  save_page = PPAGE;
                  PPAGE = tableFPage(var_blk);
                  // erase table
                  nsect = (unsigned char)(tableWords(var_blk)/SECTOR_WORDS);
                  if(tableWords(var_blk) > (nsect*SECTOR_WORDS))nsect++;
                  for (sect = 0; sect < nsect; sect++)  {
                    Flash_Erase_Sector(tableWordFlash(var_blk, 0) + sect * SECTOR_WORDS);
                  }
                  PPAGE = save_page;
                  flocker = 0;
                }
                // update flash table with received data 
                flocker = 0xCC; 
                save_page = PPAGE;
                PPAGE = tableFPage(var_blk);
                for(jx = 0; jx < var_byt;jx += 2)  {
                  tble_word = (*(&CAN_RB_DSR0 + jx) << 8) | 
                               *(&CAN_RB_DSR0 + (jx  + 1));
							   					
                  // write table word
                  ntword = canrxbytes>>1;
                  Flash_Write_Word(tableWordFlash(var_blk, ntword), tble_word);
                  canrxbytes += 2;
                  if(canrxbytes >= tableBytes(var_blk))  {
                    burnCANdat = 0;
                    break;
                  } 
                }
                flocker = 0;
                PPAGE = save_page;
                if(burnCANdat)  {
                  // still more data - set 1 sec timeout
                  ltch_CAN = lmms + 7812;
                } 
                else  {
                  // all done
                  ltch_CAN = 0xFFFFFFFF;
                } 
              } 
            }
            break;
        		
          case MSG_REQ:  // msg to send back current value of variable(s)
            // Update related parameters for xmt ring buffer
            jx = can[0].cxno_in;
            can[0].cx_msg_type[jx] = MSG_RSP;
            // destination var blk
            can[0].cx_destvarblk[jx] = CAN_RB_DSR0;
            dvar_off = ((unsigned short)CAN_RB_DSR1 << 3) |
                                     ((CAN_RB_DSR2 & 0xE0) >> 5);
            can[0].cx_destvaroff[jx] = dvar_off;
            can[0].cx_dest[jx] = rcv_id;
            if(var_byt <= 3)  {
              var_byt = CAN_RB_DSR2 & 0x1F;
            } 
            else  {    // > 8 bytes requested (from ECU ser/TS)
              var_byt = 8;
              RFlag = 1;
              Rbytes = CAN_RB_DSR3;
              Rvarblk = var_blk;
              Rdvaroff = dvar_off;
              Rvaroff = var_off;
            }
            can[0].cx_varbyt[jx] = var_byt;
            // put variable value(s) in xmit ring buffer
            if(tables[var_blk].addrRam != NULL)  {
              for(kx = 0;kx < var_byt;kx++)  {
                can[0].cx_datbuf[jx][kx] = 
                     *tableByteRam(var_blk, var_off + kx);
              }
            }
            else  {
              save_page = PPAGE;
              PPAGE = tableFPage(var_blk);
              for(kx = 0;kx < var_byt;kx++)  {
                can[0].cx_datbuf[jx][kx] = 
                     *tableByteFlash(var_blk, var_off + kx);
              }
              PPAGE = save_page;
            }
            send_can(0);
            break;
        		
          case OUTMSG_REQ:   // ECU receiving msg from gpio device to send back 
               // various bytes in the outpc structure to destvarblk.
               // The bytes are as configured in outmsg[msg_no] where 
               // msg_no= CAN_RB_DSR0 
            // Update related parameters for xmt ring buffer
            jx = can[0].cxno_in;
            can[0].cx_msg_type[jx] = OUTMSG_RSP;
            msg_no = CAN_RB_DSR0;
            can[0].cx_outmsg_no[jx] = msg_no;
            n_outbytes[msg_no] = outmsg[msg_no].no_bytes;
            // destination var blk - for this msg the varblk is really the gpio
            //   varblk to return the data to
            can[0].cx_destvarblk[jx] = var_blk;
            can[0].cx_destvaroff[jx] = 0;   // store data starting at gpio varblk;
              // if > 8 bytes, will have subsequent messages, with varoff provided 
              // by ECU
            can[0].cx_dest[jx] = rcv_id;
            if(n_outbytes[msg_no] >= 8)  {
              var_byt = 8;
            }
            else  {
              var_byt = n_outbytes[msg_no];
            }
            can[0].cx_varbyt[jx] = var_byt;
            // put variable value(s) in xmit ring buffer as follows:
            for(kx = 0;kx < var_byt;kx++)  {
               can[0].cx_datbuf[jx][kx] = 
                   *((char *)&outpc + outmsg[msg_no].offset[kx]);
            }
            send_can(0);
            break;

          case OUTMSG_RSP:  // msg from ECU in reply to this gpio device's request 
          	// for specific values from ECU outpc to be put in gpio varblk + varoff.
            // update ram var block with received data; don't need n_outbytes[msg_no]
            // counter - ECU provides it via varoff
            for(jx = 0;jx < var_byt;jx++)  {
              *tableByteRam(var_blk, var_off + jx) =
                       *(&CAN_RB_DSR0 + jx);
            }
            break;
        		
          case MSG_XSUB:  // msg to execute a subroutine
            switch(rcv_id)  {
              case 1:           // message to this ecu from device 1
                can_xsub01();		// execute sub immediately here (set
                                // flag if can execute in main loop) 
                break;
            }
        	  break;
        	  
          case MSG_BURN:  // msg to burn ram data table into flash data table
            flocker     = 0xCC;    // set to prevent burning flash thru runaway code
            burn_idx = var_blk;
            burn_flag   = 1;
        	  break;
        }					 // end mtype switch
        can_status &= CLR_RCV_ERR;
    }
    if((CANRFLG & 0x72) != 0)  {
        // Rcv error or overrun on receive
        can_status |= RCV_ERR;
        //can_reset = 1;
    }
    /* clear RX buf full, err flags to ready for next rcv int */
    /*  (Note: can't clear err count bits) */
    CANRFLG = 0xC3;
	
    return;
}

void send_can(unsigned char buf)  {
    // buf = 0 for transmission from ISR; = 1 if from non-ISR
    // This is where (in xmt ring buffer) to put next message
    if(can[buf].cxno_in < (NO_CANMSG - 1))
      can[buf].cxno_in++;
    else
      can[buf].cxno_in = 0;	 // overwrite oldest msg in queue
    // increment counter
    if(can[buf].cxno < NO_CANMSG)
      can[buf].cxno++;
    else
      can[buf].cxno = NO_CANMSG;
    if(!(CANTIER & 0x07))  {
      // Following will cause entry to TxIsr without sending msg
      // since when CANTIER = 0, CANTFLG left as buff empty(>0).
      // If CANTIER has at least 1 int buf enabled, will enter
      // TxIsr automatically.
      CANTBSEL = CANTFLG;
      CANTIER = CANTBSEL;
    }
    return;
}

void can_xsub01(void)  {
  return;
}
