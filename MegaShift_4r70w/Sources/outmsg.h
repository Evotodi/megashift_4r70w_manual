// Outmsg structure
// Note: *** Must set same outmsg data in ECU and CAN device

const can_outmsg outmsg[MAX_OUTMSGS] = { 
{ 156,    // xrate: =0 means the message is only transmitted when requested, 
          //  otherwise it indicates the clock speed in 0.128 tics at which
          //  a CAN message will be transmitted automatically after receiving 
          //  a request for data by setting the activate_xrate mth bit= 1,
          //  where m = msg no = 0 to MAX_OUTMSGS.
  10,     // no_bytes: the total number of bytes of the variables in the message
          // excluding the xrate, no_bytes and destination ID.                             
          //  Must be <= size of outpc and <= 255
  25,     // destination id = 1 << 4, varblk = 9 (msvar).
 {2,3,    // pulse width 1
  6,7,    // rpm
  18,19,  // map
  24,25,  // tps
  26,27,  // vBatt
  {0} }
}, 
{0}       //  next 7 nessages
};
