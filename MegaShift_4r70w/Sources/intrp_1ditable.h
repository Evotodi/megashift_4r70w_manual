// --------------- intrp_1ditable -------------------------------------------------------
int intrp_1ditable(int x, unsigned char n, int * x_table, int * z_table) {
    int ix;
    long interp, interp3;

    if (x > x_table[n - 1]) {
        return ((int) z_table[n - 1]);
    }
    if (x < x_table[0]) {
        return ((int) z_table[0]);
    }
    for (ix = n - 2; ix > -1; ix--) {
        if (x > x_table[ix]) {
            break;
        }
    }
    if (ix < 0) ix = 0;

    interp = (unsigned int) x_table[ix + 1] - (unsigned int) x_table[ix];
    if (interp != 0) {
        interp3 = (unsigned int) x - (unsigned int) x_table[ix];
        interp3 = (100 * interp3);
        interp = interp3 / interp;
    }

    return ((int)((unsigned int) z_table[ix] +
        interp * ((unsigned int) z_table[ix + 1] - (unsigned int) z_table[ix]) / 100));
}