/****** INPRAM User Inputs *******************************************/
/* These are the values coming from tunerstudio  */
/* This should align with the ini file  */
typedef struct {
    // This code uses a number of variable types:
    //
    //   Type             Bits              Range               MegaTune INI Designation
    //   ----             ----              -----               ------------------------
    // - char               8            -128 to 127                    S08
    // - unsigned char      8               0 to 255                    U08
    // .................................................................................
    // - int               16          -32768 to 32767                  S16
    //   (== short)
    // - unsigned int      16               0 to 65535                  U16
    //   (== unsigned short int)
    // .................................................................................
    // - long              32            -2�� to 2�� - 1                S32
    //   (== long int)     
    // - unsigned long     32               0 to 2�� - 1                U32
    //   (== unsigned long int)
    // .................................................................................
    unsigned char ms2canID,ms2varBLK;
    unsigned long baud;
    unsigned char board_type;
    unsigned char mycan_id;
    unsigned int can_var_rate;
    unsigned int gear1_pc[NO_PC];
    unsigned int gear1_pc_rpm[NO_PC];
    unsigned int gear2_pc[NO_PC];
    unsigned int gear2_pc_rpm[NO_PC];
    unsigned int gear3_pc[NO_PC];
    unsigned int gear3_pc_rpm[NO_PC];
    unsigned char gearOD_pc;
    unsigned char gearN_pc;
    unsigned char gearR_pc;
    unsigned char gearP_pc;
    unsigned char f_brake_pc;
    unsigned char t_brake_pc;
    
    unsigned int minTCCspeed; // minimum TCC lock speed
    unsigned char gear123_tcc_tps; //bit0: gear1 tcc en
                                  //bit1: gear2 tcc en
                                  //bit2: gear3 tcc en
                                  //bit3: gear123 tps affect tcc
    unsigned int gear123_tcc_tps_val;
    
    unsigned int gear1_tcc_rpm;
    unsigned int gear1_tcc_hys;    
    unsigned int gear2_tcc_rpm;
    unsigned int gear2_tcc_hys;   
    unsigned int gear3_tcc_rpm;
    unsigned int gear3_tcc_hys;
    
    unsigned char opts1; //JRD: bit0: foot brake pc ctrl
                         //     bit1: trans brake pc ctrl
                         //     bit2: reverse tcc output

    unsigned int maxNxEnRpm;
    unsigned char vss_num_teeth;    // divide factor for input VSS pulses (teeth/rev)     ////////////Added for vss   
    unsigned char oss_num_teeth;    // divide factor for input VSS pulses (teeth/rev)     ////////////Added for vss   
    unsigned int vss_ratio;    // ratio for vss gear to trans shaft
    unsigned char vss_mask;         // vss input mask (%)
    unsigned char VSSsmoothFactor;  // smoothing factor for VSS (0-100) 
    unsigned int maxSpeed;          // maximum speed for VSS filtering 
    unsigned char minSpeed;        // minimum speed, will set to zero below this (VSS interrupt still active)
    unsigned char resetTime;        // time (seconds x10) for no VSS filtering on reset
    unsigned int gearOD_ratio;
    unsigned int gear3_ratio;
    unsigned int gear2_ratio;
    unsigned int gear1_ratio;
    unsigned int gearR_ratio;
    unsigned int axle_ratio;   		// rear axle ratio * 1000 (i.e. 3.08 = 3080),
    unsigned int tire_diam;    		// tire diameter (inches * 100)
    unsigned int vss_correction; //Correction percent to of read period to real period. e.g. read=4.7ms real=5.0ms correction=94.00% or 9400
    unsigned int dummyvar;
    unsigned char pad001; // to make a multiple of 8 for CAN pass-through
} inputs1;

typedef struct {
    unsigned char activate_xrate;
    unsigned char can_testout;
    unsigned char CAN_enabled;

    unsigned char pad102; // pad byte to make multiple of 8 for TS CAN pass-through   
} inputs2;

#pragma ROM_VAR INP_ROM

#define SECTOR_BYTES 1024
#define SECTOR_WORDS ((int)(SECTOR_BYTES / 2))
#define N_SECTORS(theStruct)((int)((sizeof(theStruct) + SECTOR_BYTES - 1) / SECTOR_BYTES))
#define N_PADDING(theStruct)((int)(N_SECTORS(theStruct) * SECTOR_BYTES - sizeof(theStruct)))

// Assign values to the in1flash parameters
const inputs1 in1flash = {
    0,
    7, // ms2canID,msvarBLK
    115200, // baud rate
    3, // board_type
    1, // mycan_id
    60, // can_var_rate
    {20,20,20,50,80,100}, // gear1_pc
    {0,1000,3000,4000,5000,6500}, // gear1_pc_rpm
    {20,20,20,50,80,100}, // gear2_pc
    {0,1000,3000,4000,5000,6500}, // gear2_pc_rpm
    {20,20,20,50,80,100}, // gear3_pc
    {0,1000,3000,4000,5000,6500}, // gear3_pc_rpm
    80, // gearOD_pc
    20, // gearN_pc
    50, // gearR_pc
    20, // gearP_pc
    20, // f_brake_pc
    70, // t_brake_pc
    
    20, //minTCCspeed
    0x06, //gear123_tcc_tps //bit0: gear1 tcc en
                            //bit1: gear2 tcc en
                            //bit2: gear3 tcc en
                            //bit3: gear123 tps affect tcc
    10, // gear123_tcc_tps_val                           
       
    4800, //gear1_tcc_rpm    
    400, //gear1_tcc_hys
    
    4800, //gear2_tcc_rpm    
    400, //gear2_tcc_hys
    
    4800, //gear3_tcc_rpm    
    400, //gear3_tcc_hys

    0x07, //opts1; bit0: foot brake pc ctrl 
          //       bit1: trans brake pc ctrl 
          //       bit2: reverse tcc output 
    2000, //maxNxEnRpm
    
    16, //vss_num_teeth
    6,  //oss_num_teeth
    444, //vss_ratio
    80, // VSS interrupt mask (0 is disabled) vss_mask
    4, //VSSsmoothFactor
    1500, // maxSpeed x10 (for VSS filtering)
    30,   // minSpeed (x10)
    15,   // resetTime (1.5 seconds)
    780, //gearOD_ratio
    1000, //gear3_ratio
    1550, //gear2_ratio
    2840, //gear1_ratio
    2320, //gearR_ratio
    3080, // axle_ratio
    2600, // tire_diam
    9340, //vss_correction
    0,  // dummyvar
    171 // pad001
};

// Pad in1flash to end of sector
const unsigned char in1padding[N_PADDING(inputs1)] = {
    0
};

// Put values in in2flash spare variables (spare1 to spare15)
const inputs2 in2flash = {
    0, // activate_xrate
    0, // can_testout
    1, // CAN_enabled
    //171, // pad101
    171 // pad102
};

// Pad in2flash to end of sector     
const unsigned char in2padding[N_PADDING(inputs2)] = {
    0
};
// 1024 bytes = 1 block flash