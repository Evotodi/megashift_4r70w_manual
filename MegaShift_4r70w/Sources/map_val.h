// --------------- mapval -------------------------------------------------------
long mapval(long x, long in_min, long in_max, long out_min, long out_max){
  long tmp1, tmp2;
  tmp1 = (x - in_min)*(out_max - out_min);
  tmp2 = (in_max - in_min) + out_min;
  return tmp1 / tmp2;  
}
