/*************************************************************************
**************************************************************************
**   Codewarrior Template Project
**
**   (C) 2007, 2008, 2009, 2013 - B. A. Bowling and A. C. Grippo and L. Gardiner
**
**   This header must appear on all derivatives of this code. Any code 
**   modifications made for any reason become the exclusive property of 
**   Bowling and Grippo. 
**   This code is intended for off-road, experimental use under controlled 
**   conditions only.
**   All code revisions must be shared with other users free of charge 
**   and without additional restrictions on the forums at 
**   http://www.msgpio.com/forums/index.php 
**   This code, nor any derivative of it, may not be used in any form, 
**   in whole or in part, on non-Bowling and Grippo hardware under any circumstances.
**   No commercial use of this code, in whole or in part, is permitted without 
**   express written permission from Bowling and Grippo
**
***************************************************************************
**************************************************************************/

/**
ABOUT THIS CODE

This code is intended primarily as a tutorial in C for embedded use on HCS12 
processors.  

This code contains many inefficiencies, and has many areas that are implemented 
in ways that experienced programmers would consider naive. Most abstractions have been 
avoided to make the code as easy to follow as possible for the beginning embedded 
programmer.
 
The code is written in a manner that a novice programmer might do, so that it can be 
understood more easily by those just starting out with embedded code on the HCS12.  

**/

/* --- General Code Layout -----------------------------------------------

This is a list of general blocks in the code.

Copyright and other notices
Compiler/Linker/Locator notes
The 9S12C64 Microcontroller Memory Notes
I/O Port Quick_Start
CAN Comms Notes
Port DDR (data direction register) Configuration Notes
Defines, Includes, ISR Definitions and Interrupt Table
Function Prototypes
Table Includes and Table Size Defines (including default values)
Revision Number and Signature
Global Variables
Function MAIN
 - define variables
 - Initialize PLL (Phase Locked Loop)
 - Copy Flash to RAM
 - Set Up Input/Output ports
 - Set Up Timer Ports
 - Set Up CRG RTI Interrupt
 - Set up SCI Serial Communications Interface
 - Set Up PWM on Timer Pins
 - Set Up ADC Channels 
 - Set variable block addresses to be used for CAN communications
 - Initialize Variables
 - MAIN LOOP
   --  
 - End MAIN LOOP
 
 Functions:
  - get_adc()
  - switch_page()
  - intrp_1dctable()
  - intrp_2dctable()
  - VSS_timer()
  - Timer_Clock_ISR()  -- this is where the millisecond and other timer operations happen
  - ISR_TimerOverflow()
  - Serial_Comm_ISR()
  - CanInit()
  - CanTxIsr()
  - CanRxIsr()
  - can_xsub01()
  - UnimplementedISR()
  - fburner()
  - Flash_Init()
  - Flash_Write_Word()
  - waitAwhile()
  - grabMS2Vars()
  
*/


/** I/O Port Quick_Start ******************************************
    --------------------

MShift Function      Port    I/O    ON    OFF   READ               NOTES
---------------      ----    ---    --    ---   ----               -----

Brake Sense				PAD07	I     --    --    (PORTAD0 & 0x80)   - on/off			
Paddle DOWN				PAD06   I     --    --    (PORTAD0 & 0x40)   - on/off   	
non-CAN MAP/TPS/MAF		PAD05   I     --    --    ATD0DR5            - ADC count   
line pressure sensor	PAD04   I     --    --    ATD0DR4            - ADC count     
switchC					PAD03	I     --    --    ATD0DR3            - ADC count  
Temp Sensor				PAD02	I     --    --    ATD0DR2            - ADC count    
switchB					PAD01	I     --    --    ATD0DR1            - ADC count  
switchA					PAD00	I     --    --    ATD0DR0            - ADC count  

Spare Output1 			PT7     O     *pPTTpin[7] |= 0x80;  *pPTTpin[7] &= ~0x80; ---
Paddle UP				PT6     I     --    --    PTT & 0x40         - on/off
ISS       				PT5     I     Interrupt ISS_timer()          - interrupt called on edge     			
Speedo Output			PT4		O			PWMDTY4 = PWMPER4;          PWMDTY4 = 0;    PWMDTY4
TCC						PT3		O		  PWMDTY3 = PWMPER3;          PWMDTY3 = 0;    PWMDTY4
PC						PT2		O		  PWMDTY2 = PWMPER2;          PWMDTY2 = 0;    PWMDTY4
Sol32					PT1     O     PWMDTY1 = PWMPER1;          PWMDTY1 = 0;    PWMDTY4
VSS						PT0		I     Interrupt VSS_timer()          - interrupt called on edge

LED4  					PB4     O     PORTB |= 0x10;        PORTB &= ~0x10;       ---

LED3  					PM5     O     *pPTMpin[5] |= 0x20;  *pPTMpin[5] &= ~0x20; ---	   
LED1					PM4     O     *pPTMpin[4] |= 0x10;  *pPTMpin[5] &= ~0x10; ---
LED2 					PM3     O     *pPTMpin[3] |= 0x08;  *pPTMpin[5] &= ~0x08; ---
Sol B					PM2     O     *pPTMpin[2] |= 0x04;  *pPTMpin[5] &= ~0x04; ---

Sol A					PE4     O    	PORTE |= 0x10;        PORTE &= ~0x10;       ---
4WD input   			PE1     I     --    --  (PORTE & 0x02)       - on/off        

Spare Output2 			PA0     O     PORTA |= 0x01;        PORTA &= ~0x01;       ---

	 
*/

/*************************************************************************

Codewarrior Compiler for Template Code for GPIO
-----------------------------------------------

This code was developed entirely on Codewarrior 4.6 Special Edition. This compiler (actually a 
compiler/linker/locator) is available for free from Freescale (which used to be called Motorola):

http://www.freescale.com/webapp/sps/site/overview.jsp?code=CW_SPECIALEDITIONS&tid=CWH

You have to register to get the software. (Note that the link may eventually become stale. If it 
does, start at www.freescale.com and search for 'codewarrior special edition')

The 'Special Edition' of Codewarrior has a 32 Kbyte code compile limit, 
and a maximum of 32 files (the number of files and code size are listed on the bottom 
left side of the Freescale Codewarrior IDE - integrated development environment). 
This code project is well under those limits at 23 files and ~20 Kbytes.

The files required to create a loadable S19 file for the GPIO board using Codewarrior are:

- main.c             (this file)
- hcs12def.h         (various register definitions, it gives names to memory addresses such as ports)
- Monitor_linker.prm (memory layout file)
- cltfactor.inc      (the file that is used to convert the trans temp ADC count into degrees Farenheit)
- loadfactor.inc     (the file that is used to convert the load signal, MAP/TPS/MAF, into 
                      volts for the controller if not using CAN)
- linefactor.inc     (the file that is used to convert the line pressure sensor output to psi)
- sprfactor.inc      (a spare file for use with the ADC ports in custom code)
- Flash.asm
- flash.h
- reset.asm
- serComm.h
- senCANAdj.h
- SerCAN0.h
- SerCAN1.h
- SerCAN2.h
- SerCAN3.h
- CANcomm.h
- flashburn.h

This project does not need the processor expert ("beans") enabled in the 
Codewarrior project, the required info is embedded into the above files. If you do decide 
to use the processor expert, you need to set it to 'MC9S12C64MFA' with the small memory model.

**************************************************************************/

/** Compile time errors *************************************************

This code *may* generate 2 messages when compiling. This is normal. 
The two errors are:

- Warning: C4201: pragma ROM_VAR was not handled 

  main.c line 1416

  Comment: "1416" might be another line where a "#pragma ROM_VAR DEFAULT" statement is. It is 
  not entirely clear why this message happens, but it does not affect the generated code. 
  Deleting the 'ignored' statement simply moves the error to another pragma statement, 
  which *might* cause problems, so this is best left alone.

- L1128: Cutting value main startup data member from 0x3C8000 to 0x8000 
 
  Comment: This is because the linker/locator has to assign actual physical addresses to 
  the code, not paged addresses (3C is the page number), so this is normal (note that it 
  is labelled a 'warning' not an 'error'). In fact we would be worried if we didn't get 
  this message while using paged code!

Any other errors should be found, understood, and fixed, of course.

**/

/**** Decimal, Binary, and Hexadecimal numbers in C *************************

Note that in Codewarrior, any number starting with 0x (or 0X) is considered a 
hexadecimal number (example 0xF1B0) - sometimes written in documentation with a 
preceding $ or a following h, and any number stating with a 1 to 9 is considered 
a decimal number. C has no native syntax for expressing a binary number in source 
code (though one can be user defined - Google it). Hexadecimal is used instead, 
and is convenient because of the smaller size, and each digit corresponds to a 
4 bit binary block.)
 
An easy way to convert between binary, hexadecimal, and decimal is to open the 
Windows calculator (Start/All Programs/Accessories/Calculator) in 'scientific' mode 
(under 'View') then click between the various formats (it has hexadecimal, decimal, 
octal, and binary). The number in the display is converted as you click different 
formats)

*/

/**** The 9S12C64 Microcontroller Memory ***************************************

The Freescale 9S12C64 microcontroller used on the GPIO board (and with this 
MegaShift code) has a 16 bit, 24 MHz processor. 16 bit means it can address memory 
locations from:
 
  0000 0000 0000 0000 to 1111 1111 1111 1111 binary locations 
= 0 to 65535 decimal 
= 0x0000 to 0xFFFF hexadecimal

The 9S12C64 microcontroller has 3 types of memory:

1) 4 kilobytes of RAM (Ramdom Access Memory) - used for values that need to be changed 
   frequently (all the values that MegaTune normally changes are changed in RAM, except the 
   thermistor and MAF tables). They can then be 'burned to flash with the 'Burn to ECU' button. 
   RAM loses its contents if the power supply to the GPIO main board is interrupted, even very 
   briefly. At start-up these values are copied from the flash memory 
   (below) into the two 1Kbyte inpram. and in2ram. structures in RAM.
   
2) 64 kilobytes of flash EEPROM memory - flash EEPROM (Electrically Eraseable Programmable Read 
   Only Memory) memory is retained even if the power is removed for long periods. Flash 
   can be read easily, but is much slower and more difficult to write (since it is erasable only 
   in 1024-byte sectors, and must be erased before even a single bit can be written), 
   so it is used for program instructions and values that don't change 
   much (like the thermistor tables). Note that the compiler may refer to flash as 
   ROM (read only memory), because though it is not 'read-only' it serves much the 
   same purpose as ROM in some other microcontrollers. (Note that rumour has it that the 9S12C64 
   actually has 128 kiloBytes of flash memory, which can be accessed using the paging mechanism
   described below.)
   
3) Registers - used to configure the microcontroller (I/O pins, timer ports, 
   pulse width modulation (PWM), and to store the status of specific operations etc.)
   
The memory is addressed as a number from 0x0000 to 0xFFFF. The memory addresses are arranged 
as:

  0x0000 to 0x03FF - Registers
  0x03FF to 0x3000 - 16K fixed Flash EEPROM (page 0x003D)
  0x3000 to 0x3FFF - RAM (mappable to any 4K boundary)
  0x4000 to 0x7FFF - 16K fixed Flash EEPROM (fixed page 0x003E)
  0x8000 to 0xBFFF - 16K fixed Flash EEPROM (ppage = 0x003C)
  0xC000 to 0xFEFF - 16K fixed Flash EEPROM (fixed page 0x003F)
  0xFF00 to 0xFFFF - Interrupt Vectors (BDM if active)
  
and the memory 'segments' are defined in the monitor_linker.prm file in 
the PRM folder as:

SEGMENTS																		 
    RAM = READ_WRITE 0x3000 TO 0x3FFF;          // 4096 bytes
    
    // unbanked FLASH ROM 
    CLT_ROM  = READ_ONLY 0x4000 TO 0x47FF;
    MAT_ROM  = READ_ONLY 0x4800 TO 0x4FFF;
    EGO_ROM  = READ_ONLY 0x5000 TO 0x53FF;
    MAF_ROM  = READ_ONLY 0x5400 TO 0x5BFF;
    INP_ROM  = READ_ONLY 0x5C00 TO 0x63FF;
    OVF_ROM  = READ_ONLY 0x6400 TO 0x6FFF;
    ROM_7000 = READ_ONLY  0x7000 TO 0x7FFF;

//  ROM_8000 = READ_ONLY  0x8000 TO 0xBFFF; 
    // banked FLASH ROM 
    PAGE_3C  = READ_ONLY  0x3C8000 TO 0x3CBFFF;  // 16384 bytes, FUNCTION_ROM, MAIN_ROM
    PAGE_3D  = READ_ONLY  0x3D8000 TO 0x3DBFFF;  // 16384 bytes, OTHER_ROM
    ROM_C000 = READ_ONLY  0xC000 TO 0xF77E;      // 14206 bytes, NON_BANKED, COPY, RUNTIME

END

In the monitor_linker.prm file is a section called 'PLACEMENT' which 
contains the user-specified labels (you can a call them whatever you 
want - except for reserved words) that the pragma code_seg directives 
use. This allows us to separate the pragma statements from the memory 
addresses, and makes things like changing to other processor variants, 
etc. much easier. 

PLACEMENT
    _PRESTART, STARTUP,
    VIRTUAL_TABLE_SEGMENT,
    DEFAULT_ROM                  INTO  ROM_7000; 
    MAIN_ROM                     INTO  PAGE_3C;
    OTHER_ROM                    INTO  PAGE_3D;
    NON_BANKED, RUNTIME,
    COPY                         INTO  ROM_C000;
    ROM_VAR, STRINGS             INTO  CLT_ROM,MAT_ROM,EGO_ROM,MAF_ROM,INP_ROM,OVF_ROM;
    DEFAULT_RAM                  INTO  RAM;
END

'pragma's are directives to the compiler at compile time (not when the code is executed)
that direct the actions of the compiler in a particular portion of a program without 
affecting the program as a whole. Most commonly, we use them to specify where the compiler 
(actually the linker) should put things in memory.

The rpm file, pragma directives, and ppage statements in the code work together. The prm file
tells the compiler how the memory is organized. The pragma statements tell the linker where to 
put sections of the code in memory. The ppage statements tell the running processor where 
to look in memory for code and values, etc.

Note that some apparently different memory areas overlap:

- Flash locations in the range 0xC000 - 0xFFFF are the same as flash 
  locations 0x3F8000 - 0x3FBFFF.
- Flash locations in the range 0x4000 - 0x7FFF are the same as flash 
  locations 0x3E8000 - 0x3EBFFF.
- Flash locations in the range 0x0000 - 0x3FFF are the same as flash 
  locations 0x3D8000 - 0x3DBFFF.

Only the FLASH area 0x0000 - 0x3FFF overlaps with Flash area 0x3D8000 - 
0x3DBFFF. (Not the SFR registers and internal RAM). This is because the 
SFR Registers and internal RAM have higher priority from Flash in the bus 
arbitrator, so when they overlap with some of the Flash memory, they win 
precedence over the Flash that occupies the same address range, and so the 
internal registers and RAM are accessed as needed, rather than the 
underlying Flash locations.

In other words, Flash portions that are not covered with SFR registers and 
internal RAM are accessible through the 0x0000 - 0x7FFF address range. 
These Flash locations are also accessible through PPAGE pages 0x3D and 0x3E 
in the 0x8000 - 0xBFFF program page window - where no Flash locations are 
usually covered by SFR registers or internal RAM (unless INITRM is set to 
this address range).

For more information on the prm file, ppaged memory, #pragmas and how to use them in C 
programming, see:

http://www.freescale.com/files/microcontrollers/doc/app_note/AN2216.pdf

or search the freescale site (http://www.freescale.com) for document AN2216.pdf 

In general, you won't need to know these unless you are doing a major re-working
of the code. For adding features, etc., you just need to know where specific 
variables are located.

Memory locations for specific variables can be found in the Monitor.map file that 
the linker creates. These locations will be very helpful (i.e., necessary) to write 
the INI file that MegaTune uses.

Also, note that all the variables you want to see in MegaTune,
that are outpc from MS-II to the laptop rather than inputs to MS-II, need 
to be in the outpc. structure to be recognized by MegaTune (they also have to be 
set up in the INI file).

If you are looking for a concise introduction to programming the HCS12 (but for the 
D256 variant) try: 

https://www.ee.nmt.edu/~rison/ee308_spr06/lectures.html 

(the site may generate a security certificate error, proceed any ways)              

For a complete textbook on programming the HCS12 family of processors, including MegaSquirt-II's 
MC9S12C64MFA, see:

"The HCS12/9S12 : An Introduction to Hardware and Software Interfacing" by Han-Way Huang 

Hardcover: 880 pages 
Publisher: Delmar Learning
Language: English 
ISBN-10: 1401898122 
ISBN-13: 978-1401898120 

(This is the standard 3rd year university electrical engineering textbook on 
this topic. It also includes a CD with freeware compilers and IDEs, among other things.)

 **/

/*
 Note that all the standard output transistors recommended for the circuits on the 
 GPIO board (TIP120 in PWMx, VB921 in VBx, ZTX450 in GPOx) are NPN - they are ON 
 (i.e., will allow current to flow) when the base (which is connected to the port pin
 through a 1K Ohm resistor) is pulled high, i.e., set to 1 in software, and 
 OFF if the port pin is set to zero. 
 (Note that if a pull-up circuit is used, 'current flowing' means an external 
 nominal voltage signal of zero (actually about 0.7 Volts) and 'no current flowing' 
 means full pull-up voltage is applied)
*/

/* ------------- CAN Communications -----------------------------------------------

CAN data is requested from the MS-II 8 bytes every can_var_rate (.128 ms tics) of the 
Timer_Clock_ISR(). The data is sent in the CanTxIsr() subroutine, and received in the
CanRxIsr() subroutine. The received data is the entire outpc. structure that MS-II 
normally sends to MegaTune. In the MShift processor, this data is placed into the msvar 
array, where the rpm and kPa are extracted for use in the program.

Every time a CAN transmit or receive routine is called, the CANtx or CANrx values in the 
outpc. structure are incremented (up to 65535, then they roll-over). This can be helpful
for de-bugging CAN communications.

*/

/*
Port DDR (data direction register) Configuration
------------------------------------------------
Many of the I/O port pins on the 9S12C64 processor can be 
configured as either an input or an output. The way you tell 
the processor which should be what is by filling zeros 
or ones in the data direction registers.

0=input, 1=output, x=not assigned (default is input)

For the GPIO trans functions, we want:

Bit		 76543210     decimal   hexadecimal
			 --------     -------   -----------
PA     xxxxxxx1			 = 01      = 0x0001
AD		 00xx0000			 = 00      = 0x0000
PB		 xxx1xxxx			 = 16      = 0x0010
PE		 xxx1xxx1			 = 16      = 0x0011
PM		 xx1111xx			 = 60      = 0x003C
PT		 10011110			 =158      = 0x009E

But the user can change them to do whatever they wish, of course!

These registers are typically defined with DDRx names. Note that 
the data direction registers are memory addresses, 
but there are convenient names defined for these addresses 
in the hcs12def.h file (open it and look for things like DDRA for 
port A, etc.).

BTW, these user configurable input/output ports are called 
"general purpose I/O" ports which is where the GPIO board gets its name.

*/


// --- Defines, Includes, ISR Definitions and Interrupt Table ----
// Note that #define statements and other compiler directives do not use a terminating semi-colon

#include "hcs12def.h"      /* common defines and macros */
                           // This is where things like PORTE are set as names rather than memory addresses.
                           // This is done entirely for programmer convenience, so that you can write 
                           // PORTE instead of  0x0008

#include "flash.h"         // flashburner defines, structures
#include <string.h>        // standard C string handling functions

// #define sets the value of the first argument:
//   - to 1 (==true) if there is no second argument
//   - to the second argument, if it exists
//
// ex1: #define TEXT1_ATTR means set TEXT1_ATTR to 'true'
//
// ex2: #define INTERRUPT interrupt means we can use uppercase or lowercase
//                                  because they are set to the same thing

#define INTERRUPT interrupt         // so we can use upper or lower case
#define FAR_TEXT1_ATTR
#define TEXT1_ATTR
#define EEPROM_ATTR
#define POST_INTERRUPT
#define ENABLE_INTERRUPTS  asm cli; // 'cli' is an assembly language directive 
                                    //  meaning 'enable interrupts'
#define DISABLE_INTERRUPTS asm sei; // 'sei' is an assembly language directive 
                                    //  meaning 'disable interrupts'
#define NEAR near                   // You can obtain more compact code by using 
                                    // near types of addressing modes that use relatively
                                    // small offsets embedded within each instruction,
                                    // i.e. if we agree to have the compiler generate 
                                    // code constrained to only access locations "nearby"
                                    // to some default location or location 0. More 
                                    // compact code also implies less instruction 
                                    // fetching per execution hence performance is better. 
#define VECT_ATTR @0xFF80           // vector interrupt space from absolute address 
                                    // $FF80 to $FFFF (65456 to 65535 decimal)

#pragma CODE_SEG NON_BANKED /* Interrupt section for this module. 
                               Placement will be in NON_BANKED area. */
                               
extern void near _Startup(void);       /* Startup routine function prototype */

/**** Interrupt Service Routines ********************************** 

When the HC9S12 processor detects an interrupt, you can have it automatically
jump to part of the program which tells it what to do outside of the normal flow 
of the program. The code an interrupt triggers a jump to is called an ISR (Interrupt 
Service Routine). The ISR is located at one of a specific set of memory 
locations called Interrupt Vectors that tell the HCS12 the address of the ISR
for each type of interrupt.

The HC9S12 know where to return to because the return address is pushed onto 
the stack before the HC9S12 jumps to the ISR. In assembly language you use 
the RTI (Return from Interrupt) instruction to pull the return address off 
of the stack when the processor exits the ISR.

Even if the ISR changes registers, all registers are pushed onto the stack 
(a set of memory location used to store the CPU status) before jumping to 
the ISR code. When the processor executes the RTI instruction at the end of 
the ISR, the registers are pulled off of the stack before returning to
program main loop execution.

To return from the ISR You must return from the ISR using the RTI instruction 
in assembly language (return; in C). The RTI instruction tells the HCS12 to 
pull all the registers off of the stack and return to the address where 
it was processing when the interrupt occurred.

MShift never call ISRs directly, ISRs run 'asynchronously' in response to 
interrupt requests. Thus, to the main loop code, each ISR appears to just 
start up on its own. Since the main loop doesn't call the ISR,
it cannot pass arguments to the ISR or accept a return value. Therefore, a 
ISR in C must be a function with no parameters and a void return type. The key 
word INTERRUPT tells the compiler that the code is an interrupt service routine, 
and also not to 'optimize' it right out of the code because it is never called 
in the main loop.
*/
                               
INTERRUPT void UnimplementedISR(void); 
INTERRUPT void VSS_timer(void);
INTERRUPT void ISS_timer(void);
INTERRUPT void Timer_Overflow_ISR(void);
INTERRUPT void Timer_Clock_ISR(void);
INTERRUPT void CanTxIsr(void);
INTERRUPT void CanRxIsr(void);

#include "serCAN0.h"
#include "serCAN1.h"
#include "serCAN2.h"

void sendCANAdj(void);

typedef void (* NEAR tIsrFunc)(void);
const tIsrFunc _vect[] VECT_ATTR = {      // Interrupt table placed at 0xFF80
        UnimplementedISR,                 // vector 63, 0xFF80
        UnimplementedISR,                 // vector 62, 0xFF82
        UnimplementedISR,                 // vector 61, 0xFF84
        UnimplementedISR,                 // vector 60, 0xFF86
        UnimplementedISR,                 // vector 59, 0xFF88
        UnimplementedISR,                 // vector 58, 0xFF8A
        UnimplementedISR,                 // vector 57, 0xFF8C - PWM emergency shut-down
        UnimplementedISR,                 // vector 56, 0xFF8E
        UnimplementedISR,                 // vector 55, 0xFF90
        UnimplementedISR,                 // vector 54, 0xFF92
        UnimplementedISR,                 // vector 53, 0xFF94
        UnimplementedISR,                 // vector 52, 0xFF96
        UnimplementedISR,                 // vector 51, 0xFF98
        UnimplementedISR,                 // vector 50, 0xFF9A
        UnimplementedISR,                 // vector 49, 0xFF9C
        UnimplementedISR,                 // vector 48, 0xFF9E
        UnimplementedISR,                 // vector 47, 0xFFA0
        UnimplementedISR,                 // vector 46, 0xFFA2
        UnimplementedISR,                 // vector 45, 0xFFA4
        UnimplementedISR,                 // vector 44, 0xFFA6
        UnimplementedISR,                 // vector 43, 0xFFA8
        UnimplementedISR,                 // vector 42, 0xFFAA
        UnimplementedISR,                 // vector 41, 0xFFAC
        UnimplementedISR,                 // vector 40, 0xFFAE              
        CanTxIsr,                         // vector 39, 0xFFB0
        CanRxIsr,                         // vector 38, 0xFFB2
        CanRxIsr,                         // vector 37, 0xFFB4
        UnimplementedISR,                 // vector 36, 0xFFB6
        UnimplementedISR,                 // vector 35, 0xFFB8
        UnimplementedISR,                 // vector 34, 0xFFBA
        UnimplementedISR,                 // vector 33, 0xFFBC
        UnimplementedISR,                 // vector 32, 0xFFBE
        UnimplementedISR,                 // vector 31, 0xFFC0
        UnimplementedISR,                 // vector 30, 0xFFC2
        UnimplementedISR,                 // vector 29, 0xFFC4
        UnimplementedISR,                 // vector 28, 0xFFC6
        UnimplementedISR,                 // vector 27, 0xFFC8
        UnimplementedISR,                 // vector 26, 0xFFCA
        UnimplementedISR,                 // vector 25, 0xFFCC
        UnimplementedISR,                 // vector 24, 0xFFCE
        UnimplementedISR,                 // vector 23, 0xFFD0
        UnimplementedISR,                 // vector 22, 0xFFD2
        UnimplementedISR,                 // vector 21, 0xFFD4
		ISR_SCI_Comm,                     // vector 20, 0xFFD6      
        UnimplementedISR,                 // vector 19, 0xFFD8
        UnimplementedISR,                 // vector 18, 0xFFDA
        UnimplementedISR,                 // vector 17, 0xFFBC
        Timer_Overflow_ISR,               // vector 16, 0xFFDE
        UnimplementedISR,                 // vector 15, 0xFFE0
        UnimplementedISR,                 // vector 14, 0xFFE2
        ISS_timer,                        // vector 13, 0xFFE4
        UnimplementedISR,                 // vector 12, 0xFFE6
        UnimplementedISR,                 // vector 11, 0xFFE8
        UnimplementedISR,                 // vector 10, 0xFFEA
        UnimplementedISR,                 // vector 09, 0xFFEC
        VSS_timer,                        // vector 08, 0xFFEE
        Timer_Clock_ISR,                  // vector 07, 0xFFF0 - RTI
        UnimplementedISR,                 // vector 06, 0xFFF2 - IRQ (PE1)
        UnimplementedISR,                 // vector 05, 0xFFF4 - XIRQ (PE0)
        UnimplementedISR,                 // vector 04, 0xFFF6 - SWI
        UnimplementedISR,                 // vector 03, 0xFFF8
        UnimplementedISR,                 // vector 02, 0xFFFA - COP failure reset
        UnimplementedISR,                 // vector 01, 0xFFFC - Clock monitor failure reset
        _Startup                          // Reset vector, 0xFFFE
   };
 

// ******************************** FUNCTION PROTOYPES **********************************   
// Functions must be 'prototyped' before they are used. A function prototype
// looks like this:
//
//    void function_name(variable_type variable_name1, variable_type variable_name2, ...);
//
// where:
//       - the first item is the type of the value the function returns 
//         ('void' means the function does not return a value), 
//       - the function_name must be unique and not one of the reserved words,
//       -  a list of the variable passed (preceded by their data type) is 
//          enclosed is round brackets. 
//
// The names in the function must match those in the function itself, but not necessarily those
// that are passed from the main program (or other function).
//
// Note that the variables passed to the function must match the variable type 
// (int, char, unsigned char, etc.) listed in the function prototypes (below).
//
// Also note that some operations will require different types for the variables being 
// operated on. The can often be temporarily 'cast' to another type using the ('type') operator. 
// This seems confusing, but the gist of it is that if you have a variable that is a char 
// that needs to be a 16 bit int to be compatible with a specific calculation, you can just 
// add "(int)" in front of it in the code.
//
// For example:
//         int integer_number;             // normally an integer number 
//    		 char letter='A';               
//				 integer_number=(int) letter;    // which works because letter is treated as an integer 
                                           // because of the cast

#pragma CODE_SEG NON_BANKED

void switch_page(unsigned char sub_no);
#pragma CODE_SEG ROM_7000

int intrp_1dctable(char sgnx, int x, unsigned char n, int * x_table, 
  char * z_table);

int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny,
  unsigned int * x_table, int * y_table, unsigned char * z_table);
unsigned int intrp_1ditable(unsigned int x, unsigned char nx, 
                  unsigned int * x_table, unsigned int * z_table);

unsigned char auto_gear_lookup(int x, int y, unsigned char nx, unsigned char ny,
unsigned int * x_table, int * y_table, unsigned char * z_table);
void grabMS2vars(void);
#pragma CODE_SEG ROM_7000

void waitAwhile(unsigned int cycles); 




#pragma CODE_SEG DEFAULT    // end CODE_SEG NON_BANKED for interrupts

// --- Table Includes and Table Size Defines ---
/* The '#include' preprocessor directive directs the compiler to process 
external header files. 

Syntax:
        #include <header-file>
   or
        #include "source-file"

When enclosing the file with < and >, then the implementation searches the known 
header directories for the file and processes it. 
When enclosed with double quotation marks, then the entire contents of the 
source-file is replaced at this point.
*/
  
#include "cltfactor.inc"    // cltfactor.inc defines a 1024 x 1 lookup table.
                            // we set the ADC level to be ten bits later: 
                            // 2�� = 0000 0100 0000 0000 = 1024 = 0x0400 
                            // Each value corresponds to one ADC increment,
                            // giving the temperature*10 in Fahrenheit 
                            // at that ADC value. (if Metric_Units is set, the 
                            // value is converted to Celsius immediately)
                            // The ADC start at 0 Volts for ADC count value of 0, 
                            // and run to 5.00 Volts at ADC 1024
                            // (so each ADC represent a difference of 5.00/1024
                            // or 0.00488 Volts/count)
                            
#include "sprfactor.inc"    // spare table for future use  
                        
#include "linefactor.inc"   // linefactor.inc defines a 1024 x 1 lookup table that turns a
                            // 0 to 5 volt signal into transmission line pressure from 0 to 
                            // 500 psi (990 = signal output out-of-range low, 999 = signal 
                            // output out of range high)
                                                     
#include "loadfactor.inc"   // loadfactor.inc defines a 1024 x 1 lookup table that takes an  
                            // ADC count, and turns it into a 0 to 5 Volt 'load' signal
                            // The input can be MAP, TPS, or MAF (used for non-CAN MAP)
                            
#define NO_TBLES 	16        // tables are cltfactor, sprfactor, presfactor, loadfactor, 
                            // in1flash, in2flash, txbuf, outpc (+blank tables)
                            
#define NO_LOADS 	12        // Number of loads in shift table and pressure control table
#define NO_MPH  	12	      // Number of MPH in shift table auto gear table
#define NO_TEMPS 	10        // number of temps in temperature table(s)
#define NO_GEARS   5        // Number of Gears (1-4 forward gears), 0 for neutral/reverse
#define NPORT      7        // Number of spare ports
#define SER_TOUT   5        // Serial time out in seconds (if corrupt data received)
#define NO_MSVAR_BYTES  112 // Number of variable bytes in the received data from MS-II

// CAN and Serial defined values
#define	MSG_CMD	        0
#define	MSG_REQ	        1
#define	MSG_RSP	        2
#define	MSG_XSUB	    3
#define	MSG_BURN	    4
#define	NO_VAR_BLKS	   16
#define NO_CANMSG      10
#define MAX_CANBOARDS  16

// Error status words: 
//    -bits 0-7 are current errors
//    -bits 8-15 are corresponding latched errors
#define	XMT_ERR		   	0x0101
#define	CLR_XMT_ERR		0xFFFE
#define	XMT_TOUT	  	0x0202
#define	CLR_XMT_TOUT	0xFFFD
#define	RCV_ERR		  	0x0404
#define	CLR_RCV_ERR		0xFFFB
#define	SYS_ERR		    0x0808

/****** INPRAM User Inputs *******************************************/
// User inputs from PC/tuning interface - 1 set in flash first, then copy into RAM
// To access the RAM variables, use the inpram. prefix

typedef struct {
// In C, variables must be 'declared' before they can be used. 
// Their 'variable type' must be given.
//
// This code uses a number of variable types:
//
//   Type             Bits              Range               MegaTune INI Designation
//   ----             ----              -----               ------------------------
// - char               8            -128 to 127                    S08
// - unsigned char      8               0 to 255                    U08
// .................................................................................
// - int               16          -32768 to 32767                  S16
//   (== short)
// - unsigned int      16               0 to 65535                  U16
//   (== unsigned short int)
// .................................................................................
// - long              32            -2�� to 2�� - 1                S32
//   (== long int)     
// - unsigned long     32               0 to 2�� - 1                U32
//   (== unsigned long int)
// .................................................................................
//
// Variables are declared with a statement like: 
//     data_type variable_name;
//            or
//     data_type variable_name = 16; 
//     (which also initializes the variable)
//
// See the application of this below.
//
// In general, the shortest variable that will do the job
// (spans the range of values needed in any circumstance by the program) 
// uses the least space, leaving more room in memory for 
// other variables and code.
//
// These variable declarations can be prefaced by: 
// - const (doesn't change ever - and can be set as 
//   constant by the compiler)
// - volatile (can be changed externally to the program - by a register result, 
//             for example, and therefore should not be 'optimized' by the compiler)
// 
// In addition to the range of values a variable type can hold, there are issues associated with
// what happens if the range is exceeded. In general, the value 'wraps around'. That is,
// it goes to the next value in the range as if the highest and lowest values were connected.
//
// For example, for an unsigned int, you would have:
// 
// 0,1,2,3,4,.....253,254,255,0,1,2,3....
//
// so adding 1 to 255 = 0, add 2 to 255 = 1, etc. This is often referred to as 'overflow'.
// Similarly, subtracting 1 from 0 is 255, and so on. This is sometimes called 'underflow'.
//
// This is easier to understand if you look at the binary representation that 
// the CPU actually uses:
//
//   255 in binary forum is: 11111111, and this uses 8 bits (short int). Add 1 to it, and you get
//   1+11111111 = 100000000
//
// But when you try to stuff that into the 8 bits of an unsigned int, the most significant 
// bit (MSB - the leftmost) is truncated, so you get 00000000
//
// This applies not just to constants stored in variables, but also to calculations. Suppose 
// you have two variables with values A=10000 and B=20000, and you want to multiply then to get C.
//
// You might expect C = A*B = 10000 * 20000 = 200000000
// However, if C is an unsigned short, then C will equal 20000000 % 255 = 95 
// (% is the modulus, the remainder after dividing the first number by the
// second number as many times as it will go)
// However, if C is an unsigned int, then C will equal 20000000 % 65535 = 11825
// Only if C is a long does it equal the expected value of 20000000.
//
// Note that this wrap-around can also happen in intermediate calculations, so you
// must be aware of this in the order you write assignments if you expect large numbers. 
// For example setting: 
// C = (A/B) * 1000000;
// may not give the same result as: 
// C = (A*1000) / (B*1000); 
// if the intermediate calculations overflow or underflow (and this can depend on the type 
// of A, B, and C).
// (Note that values grouped in parenthesis are calculated first, creating the intermediate 
// results.)
//
// Also be aware that the compiler (and the HSC12 processor) does not recognized 
// 'floating point' numbers (those with non-zero decimal portions), in either 
// constants or intermediate calculations. 
//
// In another example, suppose you want to multiply A by PI=3.1415926... to get B. 
// You cannot write 
// B = A * 3.14159; (since decimal values are not allowed), 
// instead you have to write something like 
// B = (A * 314159)/100000; (and you can see why things *might* overflow!)
//
// Finally, you should not write
// B = A * (314159/100000); since the intermediate value (314159/100000) will be 
// truncated to 3 (no floating point numbers), and the result will be B = A * 3;
// (Note: also see 'casts'.)
//
// The Codewarrior compiler will often produce a "possible loss of data" warning if
// it can determine that a calculation or variable might overflow, but it is not 
// fool-proof by any means.

unsigned char ms2canID,    // Code specific CAN variables
              ms2varBLK;  
unsigned long baud;        // baud rate

unsigned char board_type;  // board type (1-255) of this board;
                           // type=0, reserved.
                           // type=1, ECU (MS II)
                           // type=2, Router board
                           // type=3, Generic I/O board
                           // type=4, Transmission Controller, ......
unsigned char mycan_id;    // can_id (address) of this board (< MAX_CANBOARDS). Always 0 for ECU.
unsigned int can_var_rate; // rate at which to get CAN variables from MS-II -
                           // 8 bytes (4 words) per can_var_rate, 0.128 ms tics
signed char   scharvariable01;
unsigned char charvariable000;
unsigned char charvariable001;
unsigned char charvariable002;
unsigned char charvariable003;
unsigned char charvariable004;
unsigned char charvariable005;
unsigned char charvariable006;
unsigned char charvariable007;
unsigned char charvariable008;
unsigned char charvariable009;
unsigned char charvariable010;
unsigned char charvariable011;
unsigned char charvariable012;
unsigned char charvariable013;
unsigned char charvariable014;
unsigned char charvariable015;
unsigned char charvariable016;
unsigned char charvariable017;
unsigned char charvariable018;
unsigned char charvariable019;
unsigned char charvariable020;
unsigned char charvariable021;
unsigned char charvariable022;
unsigned char charvariable023;
unsigned char charvariable024;
unsigned char charvariable025;
unsigned char charvariable026;
unsigned char charvariable027;
unsigned char charvariable028;
unsigned char charvariable029;
unsigned char charvariable030;
unsigned char charvariable031;
unsigned char charvariable032;
unsigned char charvariable033;
unsigned char charvariable034;
unsigned char charvariable035;
unsigned char charvariable036;
unsigned char charvariable037;
unsigned char charvariable038;
unsigned char charvariable039;
unsigned char charvariable040;
unsigned char charvariable041;
unsigned char charvariable042;
unsigned char charvariable043;
unsigned char charvariable044;
unsigned char charvariable045;
unsigned char charvariable046;
unsigned char charvariable047;
unsigned char charvariable048;
unsigned char charvariable049;
unsigned char charvariable050;
unsigned char charvariable051;
unsigned char charvariable052;
unsigned char charvariable053;
unsigned char charvariable054;
unsigned char charvariable055;
unsigned char charvariable056;
unsigned char charvariable057;
unsigned char charvariable058;
unsigned char charvariable059;

unsigned int intvariable000;
unsigned int intvariable001;
unsigned int intvariable002;
unsigned int intvariable003;
unsigned int intvariable004;
unsigned int intvariable005;
unsigned int intvariable006;
unsigned int intvariable007;
unsigned int intvariable008;
unsigned int intvariable009;
unsigned int intvariable010;
unsigned int intvariable011;
unsigned int intvariable012;
unsigned int intvariable013;
unsigned int intvariable014;
unsigned int intvariable015;
unsigned int intvariable016;
unsigned int intvariable017;
unsigned int intvariable018;
unsigned int intvariable019;
unsigned int intvariable020;
unsigned int intvariable021;
unsigned int intvariable022;
unsigned int intvariable023;
unsigned int intvariable024;
unsigned int intvariable025;
unsigned int intvariable026;
unsigned int intvariable027;
unsigned int intvariable028;
unsigned int intvariable029;
unsigned int intvariable030;
unsigned int intvariable031;
unsigned int intvariable032;
unsigned int intvariable033;
unsigned int intvariable034;
unsigned int intvariable035;
unsigned int intvariable036;
unsigned int intvariable037;
unsigned int intvariable038;
unsigned int intvariable039;                               
unsigned int intvariable040;
unsigned int intvariable041;
unsigned int intvariable042;
unsigned int intvariable043;
unsigned int intvariable044;
unsigned int intvariable045;
unsigned int intvariable046;
unsigned int intvariable047;
unsigned int intvariable048;
unsigned int intvariable049;

unsigned long longvariable000;
unsigned long longvariable001;
unsigned long longvariable002;
unsigned long longvariable003;
unsigned long longvariable004;
unsigned long longvariable005;
unsigned long longvariable006;
unsigned long longvariable007;
unsigned long longvariable008;
unsigned long longvariable009;
unsigned long longvariable010;
unsigned long longvariable011;
unsigned long longvariable012;

unsigned char pad001; // to make a multiple of 8 for CAN pass-through

} inputs1;

 typedef struct {
  unsigned char activate_xrate; // 8 bits, one for each CAN outmsg.  When bit m set to 1, message m
                                // will be auto xmitted at rate outmsg[m].xrate.
  unsigned char can_testout;    // CAN mode to get MS-II outpc variables
                                // 0 = get entire outpc.
                                // 1 = get only outmsg variables  unsigned char ms2varBLK;   // spare3 in in2ram
  unsigned char CAN_enabled; 	// 0 = CAN not used, 1 = use CAN
  unsigned char Adj_blk;		// block for engine ECU CAN sprk/fuel/iac adjustments
  unsigned int Adj_varoffset; 	// offset within block for engine ECU CAN sprk/fuel/iac adjustments
  signed int spare8;   // spare8 in in2ram
  signed int spare9;   // spare9 in in2ram
  signed int spare10;  // spare10 in in2ram
  signed int spare11;  // spare11 in in2ram
  signed int spare12;  // spare12 in in2ram
  signed int spare13;  // spare13 in in2ram
  signed int spare14;  // spare14 in in2ram
  signed int spare15;  // spare15 in in2ram
  
  unsigned char pad101; // pad byte to make multiple of 8 for TS CAN pass-through 
  unsigned char pad102; // pad byte to make multiple of 8 for TS CAN pass-through   
 } inputs2;
 
/*typedef struct {
   unsigned short xrate;   // =0 means the message is only transmitted when requested, 
                           //  otherwise it indicates the message will be transmitted automatically 
                           //  every xrate ms after receiving the first request. [Not currently used]
   unsigned char no_bytes; // The total number of bytes of all the variables in the message.                             
                           //  Must be < 112 = size of outpc.
   unsigned char offset[MAX_BYTES_OUTMSG];    //  This holds the byte offsets of each of the    
                           //  bytes in the message relative to start of  outpc. For a short, 
                           //  specify 2 consecutive offsets in the proper order for the requesting 
                           //  processor.                            
} can_outmsg;
*/
#pragma ROM_VAR INP_ROM

// flash copy of inputs - initialized

#define SECTOR_BYTES         1024 // bytes
#define SECTOR_WORDS         ((int)(SECTOR_BYTES/2))
#define N_SECTORS(theStruct) ((int)((sizeof(theStruct)+SECTOR_BYTES-1)/SECTOR_BYTES))
#define N_PADDING(theStruct) ((int)(N_SECTORS(theStruct)*SECTOR_BYTES - sizeof(theStruct)))


// Assign values to the in1flash parameters
const inputs1 in1flash = {

0,7,    // ms2canID,msvarBLK
115200, // baud rate
3,      // board_type;  // board type (1-255) of this board;
                           // type=0, reserved.
                           // type=1, ECU (MS II)
                           // type=2, Router board
                           // type=3, Generic I/O board
                           // type=4, Transmission Controller, ......
1,     // mycan_id;    // can_id (address) of this board (< MAX_CANBOARDS). Always 0 for ECU.
60,    // can_var_rate; // rate at which to get CAN variables from MS-II -
                           // 8 bytes (4 words) per can_var_rate, 0.128 ms tics
-99,   // scharvariable01

0,     // charvariable000;
1,     // charvariable001;
2,     // charvariable002;
3,     // charvariable003;
4,     // charvariable004;
5,     // charvariable005;
6,     // charvariable006;
7,     // charvariable007;
8,     // charvariable008;
9,     // charvariable009;
10,     // charvariable010;
11,     // charvariable011;
12,     // charvariable012;
13,     // charvariable013;
14,     // charvariable014;
15,     // charvariable015;
16,     // charvariable016;
17,     // charvariable017;
18,     // charvariable018;
19,     // charvariable019;
20,     // charvariable020;
21,     // charvariable021;
22,     // charvariable022;
23,     // charvariable023;
24,     // charvariable024;
25,     // charvariable025;
26,     // charvariable026;
27,     // charvariable027;
28,     // charvariable028;
29,     // charvariable029;
30,     // charvariable030;
31,     // charvariable031;
32,     // charvariable032;
33,     // charvariable033;
34,     // charvariable034;
35,     // charvariable035;
36,     // charvariable036;
37,     // charvariable037;
38,     // charvariable038;
39,     // charvariable039;
40,     // charvariable040;
41,     // charvariable041;
42,     // charvariable042;
43,     // charvariable043;
44,     // charvariable044;
45,     // charvariable045;
46,     // charvariable046;
47,     // charvariable047;
48,     // charvariable048;
49,     // charvariable049;
50,     // charvariable050;
51,     // charvariable051;
52,     // charvariable052;
53,     // charvariable053;
54,     // charvariable054;
55,     // charvariable055;
56,     // charvariable056;
57,     // charvariable057;
58,     // charvariable058;
59,     // charvariable059;

0,     // intvariable000;
1,     // intvariable001;
2,     // intvariable002;
3,     // intvariable003;
4,     // intvariable004;
5,     // intvariable005;
6,     // intvariable006;
7,     // intvariable007;
8,     // intvariable008;
9,     // intvariable009;
10,     // intvariable010;
11,     // intvariable011;
12,     // intvariable012;
13,     // intvariable013;
14,     // intvariable014;
15,     // intvariable015;
16,     // intvariable016;
17,     // intvariable017;
18,     // intvariable018;
19,     // intvariable019;
20,     // intvariable020;
21,     // intvariable021;
22,     // intvariable022;
23,     // intvariable023;
24,     // intvariable024;
25,     // intvariable025;
26,     // intvariable026;
27,     // intvariable027;
28,     // intvariable028;
29,     // intvariable029;
30,     // intvariable030;
31,     // intvariable031;
32,     // intvariable032;
33,     // intvariable033;
34,     // intvariable034;
35,     // intvariable035;
36,     // intvariable036;
37,     // intvariable037;
38,     // intvariable038;
39,     // intvariable039;
40,     // intvariable040;
41,     // intvariable041;
42,     // intvariable042;
43,     // intvariable043;
44,     // intvariable044;
45,     // intvariable045;
46,     // intvariable046;
47,     // intvariable047;
48,     // intvariable048;
49,     // intvariable049;

0,     // longvariable000;
100000,     // longvariable001;
200000,     // longvariable002;
300000,     // longvariable003;
400000,     // longvariable004;
500000,     // longvariable005;
600000,     // longvariable006;
700000,     // longvariable007;
800000,     // longvariable008;
900000,     // longvariable009;
1000000,    // longvariable010;
1100000,    // longvariable011;
1200000,    // longvariable012;

171,		// pad001

};

// Pad in1flash to end of sector
const unsigned char in1padding[N_PADDING(inputs1)] ={0}; 

// Put values in in2flash spare variables (spare1 to spare15)
const inputs2 in2flash = {
0,     // activate_xrate: 8 bits, one for each CAN outmsg.  When bit m set to 1, message m
                                // will be auto xmitted at rate outmsg[m].xrate.
0,     // can_testout:  CAN mode to get MS-II outpc variables
       // 0 = get entire outpc, 1 = use outmsg
1,     // CAN_enabled; // 0 = CAN not used, 1 = use CAN
5,	   // Adj_blk: block for engine ECU CAN sprk/fuel/iac adjustments
946,   // Adj_varoffset: offset within block for engine ECU CAN sprk/fuel/iac adjustments 
8,
9,
10,
11,
12,
13,
14,
15,
171,   // pad101
171	   // pad102
};

// Pad in2flash to end of sector     
const unsigned char in2padding[N_PADDING(inputs2)] ={0};
                                // 1024 bytes = 1 block flash
                              
#pragma ROM_VAR DEFAULT

#pragma ROM_VAR OVF_ROM

// array of TC overflow numbers at which to increment seconds counter.
const unsigned int TC_ovfla[60] = {
23,
46,
69,
92,
114,
137,
160,
183,
206,
229,
252,
275,
298,
320,
343,
366,
389,
412,
435,
458,
481,
504,
526,
549,
572,
595,
618,
641,
664,
687,
710,
732,
755,
778,
801,
824,
847,
870,
893,
916,
938,
961,
984,
1007,
1030,
1053,
1076,
1099,
1122,
1144,
1167,
1190,
1213,
1236,
1259,
1282,
1305,
1328,
1350,
1373
};

// --- Revision Number and Signature ---
/*
const char RevNum[20] =  {    // revision no:
  // Only change for major rev and/or interface change. (The last character is added by MS-II.)
    "MShift 1.101tmp    "
  // 123456789.123456789. 
},
 Signature[32] = {            // program title.
  // Change this every time you tweak a feature.  (The last character is added by MS-II.)
    "** MShift 1.101Template B&G  **"
  // 123456789.123456789.123456789.12
 };
*/
const char RevNum[20] =  {    // revision no:
  // Only change for major rev and/or interface change. (The last character is added by GPIO.)
    "GPIO Template 1.101"
  // 123456789.123456789.
  // Put this in the title bar. 
},
 Signature[32] = {            // program title.
  // Change this every time you tweak a feature.  (The last character is added by GPIO.)
    "* GPIO Template 1.101 by B&G  *"
  // 123456789.123456789.123456789.12
 };
 
 
// CAN clocks for auto transmitting data between ECU and GPIO
unsigned short xrate_clks[MAX_OUTMSGS];     // 0.128 tics

// CAN variables
#include "serCANComm\serCAN3.h"

unsigned char sentSpkAdj;

//***** RAM copy of inputs *********************************************
// This is where the RAM variables (copied from flash memory) get 
// their inpram. & in2ram. prefixes (inpram = INPut RAM)
 
inputs1 inpram;
inputs2 in2ram;

// Outmsg structure
// Note: *** Must set same outmsg data in ECU and CAN device

#include "outmsg.h"

// --- Global Variables ----
// The variables declared below are global, because they are declared outside any function.
// As a result they are available to the main loop and *all* functions.
// These variables are used for intermediate calculations.
// That is, variables used for:
//   -- user input from the tuning software are in the inpram or in2ram structures,
//   -- user output to the tuning software are in outpc. structure
//   -- accessing input/output ports and set-up configurations are defined 
//      in the hcs12def.h header
// and are NOT declared here.
//
// So the variables used here are for user calculations that do not appear in any form 
// outside the code itself. 
unsigned char ICint,ICintmask[2],OCint,OCintmask[2];
int vss_timer_overflow;
volatile unsigned short *pTIC[2],*pTOC[2]; // The asterisk (*) in the variable declaration 
                                           // indicates this is a "pointer".
                                           // That means that these variables hold the address
                                           // in memory of another variable.
unsigned long IgnTimerComp[2],dtpred;
unsigned int mms,millisec,burn_flag,can_mmsclk,can_varoff;
unsigned char burn_msec_flag;    // flag to not burn next step of flash until milliseconds have ticked over
unsigned int TC_ovflow,TC_ov_ix;
unsigned long lmms,
  ltch_lmms,
  rcv_timeout,adc_lmms;
unsigned char flocker,tpsaclk;
unsigned char uctmp;
char jx,kx,mx;
unsigned char next_adc,first_adc,
	txmode,tble_idx,burn_idx,    // txmode is 1st received character in serial comms, burn_idx is the table to burn
	reinit_flag;
unsigned int ic1_period[21]; // VSS period array - take the average
unsigned int ic2_period[21]; // ISS period array - take the average
unsigned char ic1_count, ic1_recount;
unsigned char ic2_count;
signed int adcval;            // signed for possible negative temperatures
char kill_ser,vfy_flg;
unsigned long short_period_iss; // calculated mask times for VSS timer
unsigned long long_period_iss;  // calculated mask times for VSS timer
unsigned short tcsav1;          // Last VSS time
unsigned short tcsav2;          // Last ISS/non-CAN tach time
signed int FuelAdjl,SpkAdjl,IdleAdjl,SprAdjl; // captured engine ECU CAN adjustments
unsigned int delay_count;
unsigned int delay;
unsigned char wait_flag; // flag == 1 if we are in a wait state

// CAN variables
// unsigned long  cansendclk,canclk,ltch_CAN=0xFFFFFFFF;
char *canvar_blkptr[NO_VAR_BLKS]; // The asterisk (*) in the variable declaration 
                                  // indicates this is a "pointer".
                                  // That means that these variables hold the address 
                                  // of another variable in memory.
unsigned char first_instr;                                
unsigned short can_status;

// pointers for spare port pins
volatile unsigned char *pPTMpin[8], *pPTTpin[8];
unsigned char dummyReg,lst_pval[NPORT]; 

// allocate space in ram for flash burner core
volatile unsigned char RamBurnPgm[36]; 

// ****** Set Up OUTPC. Structure for Serial Communications with MegaTune *******
// These are the rs232 outputs to PC via the serial port, and the corresponding 
// values should be in the MegaTune INI [OutputChannels] section.

typedef struct {
unsigned int seconds;                     // clock
unsigned int pulsewidth1;                 // injector pulse width from MS-II over CAN 
                                          // (in mph or kph, depending on Metric_Units)
unsigned int engine_rpm;                  // engine rpm (from CAN or estimated from VSS and gear)
unsigned char error;                      // error status codes
unsigned int LOAD;                        // LOAD from MS-II via CAN - (kpa x 10)
unsigned long loop_count;                 // Main loop counter 
unsigned int CANtx, CANrx;                // CAN transmit & receive counters
unsigned int dbug;                        // spare debugging variable brought out in MegaTune (default is VSS tooth error count)
unsigned int vBatt;                       // battery voltage from MS-II via CAN (x10)
unsigned int adv_deg;                     // ignition advance from MS-II via CAN
unsigned int tps;                         // TPS value from MS-II over CAN
unsigned char gpi4, gpi5;                 // digital input port result
unsigned int egt1, egt2, egt3;            // ADC port input result 
unsigned int gpi1, gpi2, gpi3;            // ADC port input result 
unsigned int vr1, vr2, vr3;               // timer result
unsigned char egt4;                       // digital input port result
signed int FuelAdj;						  // Spark/Fuel/Air adjustments sent to engine ECU over CAN
signed int SpkAdj;
signed int IdleAdj;
signed int SprAdj;
signed int flashburn;                     // spare output for use while testing code            
} variables;     // end outpc. structure

variables outpc;

// Create a buffer variable to hold the MS-II output variables 
// recieved over CAN for use by GPIO
unsigned char msvar[NO_MSVAR_BYTES];
  // engine_rpm is 2 bytes at offset=6,
  // LOAD (map) is 2 bytes at offset=18, and is x10,
  // vBatt is 2 bytes at offset=26, and is x10.
  // There is much more on this below, including all the outpc variables and offsets.

#pragma ROM_VAR OVF_ROM

// Define a structure for the table descriptor that gives the address in RAM, the address 
// in Flash (ROM), and the number of bytes
typedef struct {
   unsigned int *addrRam;
   unsigned int *addrFlash;
   unsigned int  n_bytes;
   unsigned int  fpage;
} tableDescriptor;

// Use the tableDescriptor structure we just defined to create an array of table descriptions 
// sized NO_TABLES. They will hold the descriptions of the tables (and the indexes are used 
// in MegaTune to 'locate' the table with command like a06, which refers to the 6th table). 
// These are constant. That is, the descriptions - memory locations - of the tables are constant, 
// even if the elements of the tables change values.

const tableDescriptor tables[NO_TBLES] =  {
  //  RAM copy                    FLASH copy                     Table Size 
  {  NULL,                   (unsigned int *)cltfactor_table,  sizeof(cltfactor_table),    	0x3C },
  {  NULL,                   (unsigned int *)sprfactor_table,  sizeof(sprfactor_table),    	0x3C },
  {  NULL,                   (unsigned int *)linefactor_table, sizeof(linefactor_table),    0x3C }, 
  {  NULL,                   (unsigned int *)loadfactor_table, sizeof(loadfactor_table),    0x3C }, 
  { (unsigned int *)&inpram, (unsigned int *)&in1flash,        sizeof(inputs1),    			0x3C }, 
  { (unsigned int *)&in2ram, (unsigned int *)&in2flash,        sizeof(inputs2),    			0x3C },
  { (unsigned int *)&txbuf,   NULL,                            sizeof(txbuf),    			0x3C },
  { (unsigned int *)&outpc,   NULL,                            sizeof(outpc),    			0x3C },
  {  NULL,                   (unsigned int *)&outmsg,          sizeof(outmsg), 				0x3C },
  { (unsigned int *)msvar,    NULL,                            sizeof(msvar), 				0x3C },
  {  NULL,                    NULL,                            0,    						0x3C },
  {  NULL,                    NULL,                            0,    						0x3C },
  {  NULL,                    NULL,                            0,   			 			0x3C },
  {  NULL,                    NULL,                            0,    						0x3C },
  {  NULL,                   (unsigned int *)&Signature,       sizeof(Signature),  			0x3C },
  {  NULL,                   (unsigned int *)&RevNum,          sizeof(RevNum),				0x3C }
};

// In the above, the "&" before a variable name means "at the address of, 
// so &inpram is the address of the inpram table

#pragma ROM_VAR DEFAULT  // Codewarrior generates error message here, is seemingly OK though

// Calculate and define values for working with tables based on the tables array we just created.
#define tableInit(iTable) (void)memcpy(tables[iTable].addrRam, tables[iTable].addrFlash, tables[iTable].n_bytes)
#define tableByteRam(iTable, iByte)   ((unsigned char *)tables[iTable].addrRam + iByte)
#define tableWordRam(iTable, iWord)   (tables[iTable].addrRam + iWord)
#define tableByteFlash(iTable, iByte) ((unsigned char *)tables[iTable].addrFlash + iByte)
#define tableWordFlash(iTable, iWord) (tables[iTable].addrFlash + iWord)
#define tableBytes(iTable)            (tables[iTable].n_bytes)
#define tableWords(iTable)            ((tables[iTable].n_bytes+1)/2) // Round up
#define tableFPage(iTable)            ((unsigned char)tables[iTable].fpage)

#pragma CODE_SEG MAIN_ROM

// Prototypes - Note: ISRs (interrupt service routines) are prototyped above.
// These are the mandatory function prototypes.

void main(void); // the main function, i.e., where the program starts

#pragma CODE_SEG DEFAULT
void reset(void);
void VSS_reset(void);
signed int get_adc(char chan1); // this is the function that reads the ADC channels
void fburner(unsigned int* progAdr, unsigned int* bufferPtr, 
  					 unsigned int no_words); // fburner(pointer to destination, pointer to source, number of words)
void Flash_Init(unsigned long oscclk);
void Flash_Erase_Sector(unsigned int *address);
void Flash_Write_Word(unsigned int *address, unsigned int data);

#pragma CODE_SEG DEFAULT

extern void reboot(void);      // 'extern' means that the code can call the function defined in another module directly if necessary
extern void monitor(void);
extern void SpSub(void);
extern void NoOp(void);

// ---------------------------------------------------------------------------------------
// ---------------------------------- Function MAIN --------------------------------------
// ---------------------------------------------------------------------------------------
 
#pragma CODE_SEG MAIN_ROM // Put following code in MAIN_ROM == ppage 0x3C
 
void main(void) {
// This is where the program actually starts. Everything before this was mainly telling the 
// compiler what and where to put values in memory when we load the code using the serial 
// monitor. So the values above exist in the S19 file we will make (that set up both the 
// program and the defaults) but the above doesn't affect the program on a restart of 
// MegaSquirt (which is why the flash memory retains its values).

// First set-up variables, etc., then go into an infinite loop
int ix, tmpx1;
long ltmp;
unsigned long ultmp, ultmp2;

PPAGE = 0x3C;  // PPAGE register is defined in hcs12def.h 
               // as memory location 0x0030

 /* In order to access the 16K flash blocks in the address range 0x8000�.0xBFFF 
    the PPAGE register (0X0030) must be loaded with the corresponding value for 
    this range:

    PAGE    PAGE Visible with PPAGE Contents
    ----    --------------------------------    
    0x3C    $3C
    0x3D    $3D
    0x3E    $3E
    0x3F    $3F

    For the MC9S12C64, the flash page 3F is also visible in the 0xC000�.0xFFFF range 
    if ROMON is set. 
    For the MC9S12C64, the flash page 3E is also visible in the 0x4000�.0x7FFF range
    if ROMHM is cleared and ROMON is set. 
    For the MC9S12C64, the flash page 3D is also visible in the 0x0000�.0x3FFF range 
    if ROMON is set...  
    
    The main code is responsible for switching PPAGE as needed to select the 
    desired 16K block of memory to be mapped into the "window" from $8000-$BFFF.
    */ 

  //  Initialize PLL (Phase Locked Loop) - reset default 
  //  is Oscillator clock 8 MHz oscillator, 
  //  PLL freq = 48 MHz, 24 MHz bus, 
  //  divide by 16 for timer of 2/3 usec tic
  PLLCTL &= 0xBF;     // Turn off PLL so can change freq
                      // A &= B is equivalent to A  = A & B, where & means a bit-wise compare
                      // Note that this is different from A = &B, which assigns the address 
                      // of B to the variable A
  SYNR    = 0x02;     // set PLL/ Bus freq to 48/ 24 MHz
  REFDV   = 0x00;
  PLLCTL |= 0x40;     // Turn on PLL. A |= B is the same as A = A | B 
  
  // wait for PLL lock
  while (!(CRGFLG & 0x08)); // Note that & is a bitwise operator, && is the relational operator
  CLKSEL = 0x80;      // select PLL as clock
  
  // wait for clock transition to finish
  for (ix = 0; ix < 60; ix++);
  
  // Copy Flash to RAM
  // open flash programming capability
  Flash_Init(8000);
  // inp_spare used to force inpflash, flashve_table into sectors.
  // Must use in program or it won't use up the entire sector. Codewarrior 
  // ignores the pragma making this stupid statement necessary.
  ix = in1padding[0];
  ix = in2padding[0];
      
  if((int)RamBurnPgm & 0x0001)	
  	{                    							// odd address - copy to even one
    (void)memcpy((void *)RamBurnPgm,NoOp,1);        // copy noop to 1st location
    (void)memcpy((void *)&RamBurnPgm[1],SpSub,32);  // copy flashburn core program to RAM
  	}  												// End if((int)RamBurnPgm & 0x0001)
  else 
  	{                                             // even address
    (void)memcpy((void *)RamBurnPgm,SpSub,32);       // copy flashburn core program to RAM
  	}
  // load all user inputs from Flash to RAM
  // indices are from 'const tableDescriptor tables[NO_TBLES]' and start at 0
  tableInit(4);
  tableInit(5);
  
  // ** MShift I/O Port Data Direction Register Settings ***
  //
  // These set the pport pins as inputs (0) or outputs (1).
  // Default is input.
  //
	//************************************************************
	
  DDRA |= 0x01;    // port A0 - output; 0x01 = 00000001 (A0 is a spare output)
  DDRB |= 0x10;    // port B4 - output; 0x10 = 00010000
  DDRE |= 0x10;	   // port E4 - output; 0x10 = 00010000 (E0 is a input only!)  
  DDRM |= 0x3C;    // port M2, M3, M4, M5 are  outputs, full drive by default; 0x3C = 00111100 
  DDRT |= 0x9E;    // port T1, T2, T3, T4, T5 & T7 are outputs, T0, T5, & T6 are inputs; 0x9E = 10011110
  
  /** Note: ***************************************************  
  serial Tx =  PS1
  serial Rx =  PS0
  CAN    Tx =	 PM1
  CAN    Rx =  PM0
 **************************************************************/ 
   
  //  Set pointers to real port addresses
  for(ix = 0; ix < 8; ix++)  {
    pPTMpin[ix] = pPTM;   // port M
    pPTTpin[ix] = pPTT;   // port T
  } // end for (ix = 0...

  // --- Set Up Timer Ports ---
  // Enable Timer Input Capture for PT0/PT5
  // --------------------------------------------------------------------------------
  // To use the Input Capture Function:
  //      - Enable the timer subsystem (set TEN bit of TSCR1)
  //      - Set the prescaler (TSCR2)
  // The 9S12 allows you to slow down the clock which drives the counter.
  // You can slow down the clock by dividing the 24 MHz clock by 2, 4, 8, 16, 32, 64 or 128.
  // You do this by writing to the prescaler bits (PR2:0) of the Timer System Control
  // Register 2 (TSCR2) Register at address 0x004D.
  //
  //   PR  Divide    Freq          Overflow Time
  //   000    1      24 MHz          2.7307 ms
  //   001    2      12 MHz          5.4613 ms
  //   010    4       6 MHz         10.9227 ms
  //   011    8       3 MHz         21.8453 ms
  //   100   16       1.5 MHz       43.6907 ms
  //   101   32       0.75 MHz      87.3813 ms
  //   110   64       0.375 MHz    174.7627 ms
  //   111  128       0.1875 MHz   349.5253 ms
  //
  // The clock ticks are counted in the variable TCNT (which is defined in the hsc12def.h file 
  // at memory location 0x0044). There are 0xFFFF 'tics' (65,536 counts).
  // In our case each 'tic' is 1/0.1875MHz  = 5.333 �s
  //
  // To set up the timer:
  //  - Configure the processor to use a particular pin of PORTT for input
  //    capture
  //  - Configure the processor to set which edge (rising, falling, or either) you want to capture
  //  - Configure the processor to determine if you want an interrupt to be generated when the
  //    capture occurs
  
  TIOS = 0x1E;  // Input Capture or Output Compare Channel Configuration:
                //  - 0 The corresponding channel acts as an input capture
                //  - 1 The corresponding channel acts as an output compare
                // 0x1E = 00011110
                // Timer ch 0 (PT0/IOC0) = IC, 
                //       ch 5 (PT5/IOC5) = IC, 
                //       ch 1-4          = OC & PWM,
                //    - PT0 is VSS in input 
                //    - PT1 is 3/2 solenoid output (output3)
                //    - PT2 is PC output (PWM)
                //    - PT3 is TCC output  (is PWM in some cases)
                //    - PT4 is speedo output (is PWM but duty is set to zero or 100%)
                //    - PT5 is input shaft sensor input or non-CAN tach input
                //    - PT6 is paddle upshift input
                //    - PT7 is spare output1
                //    - used digital I/O
                // ch 6,7 = I/O output
  TSCR1 = 0x80; // Timer System Control Register 1, 10000000 = set port T0 to use timer 
                // (Timer Enable (TEN) bit = 1)
  TSCR2 = 0x07; // Timer System Control Register 12, 0x07 = 00000111
                // Set prescaler to divide by 128
                // 349.53 millisecond overflow time, 5.333 �sec period
                // for a 40 tooth input wheel, this is
                // 1/(0.34953*40) = 0.0715 rev/sec, less than 1/2 mile/hour
                // if teeth come is at 2 x 5.333 �sec, this is about
                // 1/(0.00001066*40) = 2344 revs/sec, much faster than your car can go!
              


  // Set the VSS Input Capture Edge
  TCTL4 = 0x02;    // 0x02 = 00000010
                   // VRx circuits are designed to trigger on rising edge
                   // Set edge to capture (EDGxB EDGxA of TCTL 3-4 regs)
                   // EDGxB EDGxA
                   //   0     0     Disabled
                   //   0     1     Rising Edge
                   //   1     0     Falling Edge
                   //   1     1     Either Edge
                   //
                   //   address  register bit7  bit6  bit5  bit4  bit3  bit2  bit1  bit0
                   //   0x004A    TCTL3   EDG7B EDG7A EDG6B EDG6A EDG5B EDG5A EDG4B EDG4A
                   //   0x004B    TCTL4   EDG4B EDG3A EDG2B EDG2A EDG1B EDG1A EDG0B EDG0A
                   // 0x02 = 00000010, ~0x04 = ~00000100 = 11111011

  TCTL3 = 0x04;    // 00000100 = EDG5B = 0, EDG5A = 1 == rising edge                                 
  TFLG1 |= 0x21;       // 0x21 = 00100001 -> Clear IC0 Flag bits 0 (PT0) and 5 (PT5) in the 'main timer interrupt flag1' register
  TIE   |= 0x21;       // 0x21 = 00100001 -> Set Timer Interrupt Enable (TIE) on pin 0 (PT0) and pin 5 (PT5)

  // turn off output pins
  *pPTMpin[2] &= ~0x04;      // 0x04 = 00000100, ~0x04 = 11111011, turn off    Sol B @ PM2
  *pPTMpin[3] &= ~0x08;      // 0x08 = 00001000, ~0x08 = 11110111, turn off     LED2 @ PM3
  *pPTMpin[4] &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off     LED1 @ PM4
  *pPTMpin[5] &= ~0x20;      // 0x20 = 00100000, ~0x20 = 11011111, turn off     LED3 @ PM5
   PORTA      &= ~0x01;      // 0x01 = 00000001, ~0x01 = 11111110, turn off   Spare2 @ PA0
   PORTB      &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off     LED4 @ PB4
   PORTE      &= ~0x10;      // 0x10 = 00010000, ~0x10 = 11101111, turn off    Sol A @ PE4
  *pPTTpin[7] &= ~0x80;      // 0x01 = 10000000, ~0x80 = 01111111, turn off   Spare1 @ PT7 
  
  // The asterisk (*) in the above statements are placed in front of 'pointers' 
  // (variables that hold the memory address of other variables). The * is the 
  // 'indirection operator' - it says to take the value at the address of the 
  // variable pointed to.
  // For example, if A is a variable at address 0x00001000, and the value is 123, 
  // then a pointer to A could be set by
  // char A, *pA  <- declare the regular and pointer variable 
  // pA = &A      <- assign the address of A to the pointer pA
  // A = 123      <- assign the value of 123 to the variable A
  // then
  // pA = 0x00001000
  // and
  // *pA = 123
  // 
  // This is more than just two ways to do the same thing though. Only single values 
  // can be passed to C functions, and passing a pointer is one way around that to 
  // pass a array. 
  // That is, we pass the address of the start of the array (pA) and let the function 
  // increment that value to access the whole array.

  // Set up the data direction ports and pull-ups
  
  DDRE  = 0x10;              // Port E all inputs, except PE4 which is output (solA)
  PUCR  = 0x00;              // disable all pull-ups, will use external pull-ups where needed
  INTCR = 0x00;              // disable interrupt on PE1  

  DDRP  = 0x00;              // port P all inputs
  PERP  = 0xFF;              // enable pullup resistance for port P;    0xFF = 255 = 11111111

  DDRJ &= 0x3F;              // port J pins 0 to 5 outputs,             0x3F =  63 = 00111111
  PERJ |= 0xC0;              // enable pullup resistance for port J6,7; 0xC0 = 192 = 11000000 

  DDRS &= 0xF3;              // port S all outputs, except 2,3;         0xF3 = 243 = 11110011
  PERS |= 0x0C;              // enable pullup resistance for port S2,3; 0x0C =  12 = 00001100

  reinit_flag = 0;

  // Set Up CRG RTI Interrupt for .128 ms clock. CRG from 8MHz oscillator.
  mms      = 0;     // 0.128 ms tics
  millisec = 0;     // 1.024 ms clock (8 tics) for adcs
  lmms     = 0;
  cansendclk = 7812;
  ltch_lmms = 0;
  outpc.seconds = 0;// (1.0035) seconds
  burn_flag  = 0;
  ic1_count=1;
  ic1_recount = 1;
  ic2_count = 1;
 
  RTICTL  = 0x10;   // load timeout register for .128 ms (smallest possible)
  CRGINT |= 0x80;   // enable interrupt
  CRGFLG  = 0x80;   // clear interrupt flag (0 writes have no effect)

  // ------ Set up SCI Serial Communications Interface (RS232) ----------------
  /* To use SCI0 for 8 bit, non-parity communication:

      1. Select the baud rate via SCI0BDL Baud Rate Register, at memory 
         location 0x00C9 - defined in hcs12def.h (set below). 
         SCI baud rate = SCI module clock / (16 x BR) =
                       = 24,000,000/ (16*baud)
                       = 1,500,000/baud
                       = for baud == 115200 this is 13.0  

      2. Enable transmission and/or reception as desired by setting the 
         TIE (Transmitter Interrupt Enable) and RIE (Receiver Full 
         Interrupt Enable Bit) bits in the SCI0CR2 Control Register,
         which is at memory location 0x00CB. 
         This is register is set up as:
         
         bit        7       6      5      4      3     2     1     0
                   TIE    TCIE    RIE    ILIE    TE    RE   RWU   SBK

      3. Transmit and Receive, in interrupt service routine Serial_Comm_ISR():

           - transmit  - poll the TDRE flag and write data to the SCI0DRL register at 
                         address $00D7 when the TDRE flag is set. The data we write 
                         to the SCI0DRL register (0x00CF) is the data that will be 
                         immediately output from the TX pin. 
           - receive   - poll the RDRF register (SCI0SR1). When the RDRF status 
                         register (0x00CC) is set ((SCI0SR1 & 0x20) <> 0), we can then 
                         read the incoming data by reading the SCI1DRL register. The 
                         data we read in from this register is the input via the RX pin. 
*/
// Set the baud rate
  SCI0BDL = (unsigned char)(1500000/inpram.baud);   // = 1500000/115200 = 13
  ltmp = (150000000/inpram.baud) - ((long)SCI0BDL*100);
  if(ltmp > 50) SCI0BDL++;   // round up

  SCI0CR1 = 0x00;   // = 00000000 -> parity even -- 0000000x 
                    //               parity disabled
                    //               idle char starts after start bit
                    //               idle line wakeup
                    //               one start bit, eight data, one stop
                    //               only meaningful if LOOPS =1
                    //               SCI enabled in wait mode
                    //               LOOP disabled -- x0000000
                    
  SCI0CR2 = 0x24;   // = 00100100 -> TIE = 0                            -- x0000000, 
                    //               RIE = 1,(enable receive interrupt) -- 00x00000,
                    //               TE  = 0 (Transmitter Enable Bit)   -- 0000x000, 
                    //               RE  = 1 (Receiver Enable Bit)      -- 00000x00,
                    //  (these will be reconfigured in Serial_Comm_ISR()) 
  txcnt     = 0;
  rxcnt     = 0;
  txmode    = 0;    // receive == 0, transmit > 0
  txgoal    = 0;
  kill_ser  = 0;
  vfy_flg   = 0;
  rcv_timeout = 0xFFFFFFFF;
  
/******************* Initialize IC_period array for VSS/ISS ****************************/
// So we start at zero speed until array is filled (which will happen very fast at any real 
// speed, but is never updated when stopped)

for (ix=1;ix<21;ix++)
  {
  ic1_period[ix] = 0x0FFFF; // set the VSS period high (speed low)  
  ic2_period[ix] = 0x0FFFF; // set the ISS period high (speed low)
  }

/******************* Set Up PWM on Timer Pins ******************************************

We want PWM on ports T1, T2, T3, & T4 (T0 is VSS input)
We want:
          Sol32:   T1 = PWM1 =  50 Hz = 0.0200 second period, variable duty cycle
          PC:      T2 = PWM2 = 293 Hz = 0.0034 second period, variable duty cycle
          TCC:     T3 = PWM3 =  50 Hz = 0.0200 second period, variable duty cycle 
                                                              (either 0% or 100% in this code, 
                                                              may be variable rate apply in 
                                                              future)
          speedout T4 = PWM4 = don't care - will use 0% or 100% only in this code
                                            (may use PWM in future for high speed signalling)         

1. Choose 8-bit mode (PWMCTL = 0x00 = default)  Could use 16 bit mode, but we don't need it.
2. Choose high polarity (PWMPOL = 0x1E for pins 1, 2, 3)
3. Choose left-aligned (PWMCAE = 0x00)
4. Select clock mode in PWMCLK = 0x1E:
     - PCLKn = 0 for 2N,
     - PCLKn = 1 for 2(N+1) � M.
5. Select N in PWMPRCLK register:
     - PCKA for channels 5, 4, 1, 0 (ISS, speedo, sol32, VSS);
     - PCKB for channels 7, 6, 3, 2 (spareoutput2, paddle up, TCC, PC).
6. If PCLKn = 1, select M = 1 to 255
     - PWMSCLA = M for channels 5, 4, 1, 0
     - PWMSCLB = M for channels 7, 6, 3, 2.
7. Select PWMPERn, normally between 100 and 255 (increased values give increased control resolution).
8. Enable desired PWM channels: PWME 1, 2, 3 and 4 for us so PWME |= 0x1E
9. Select PWMDTYn, normally between 0 and PWMPERn. Then
     Duty Cycle n = (PWMDTYn/PWMPERn) � 100%
   Change duty cycle to control speed of motor or intensity of light, etc.
10. For 0% duty cycle, choose PWMDTYn = 0
*/
 
// Set up PWM outputs - PWM 1, 2, 3, & 4
// PT0,1,4,5 are on clkA - Sol 3/2 and speedo output
// PT2,3,6,7 are on clkB - PC and TCC
  MODRR    = 0x1E; // Make Port T pins 1, 2, 3 & 4 be PWM (0x1E = 00011110), 
                   // PC and 3/2 SOL 
                   // (PT3 is on/off TCC, PT4 is on/off speedo output,
                   //  we set these by setting PWM% to 0 or 100% - may use PWM in future)
                   // The MODRR register allows for mapping 
                   // of PWM channels to port T in the absence of port P 
                   // pins for the low pin count processors (like the 
                   // 48 pin MS-II MC9S12C64 processor).
  PWME     = 0x00; // disable PWMs initially  
  PWMPOL   = 0x1E; // polarity = 1, => go hi when start
 
// Set prescaler to 16. This divides bus clk (= PLLCLK/2 = 24 MHz)
// by 16. This gives 1.5 MHz timer, 1 tic = 2/3 usec.
// PWMPERx should be between 100 and 255, as it is 8 bit number 
// (100 gives us 1% increments, 255 is absolute maximum)
// We need to scale the clocks to get a 'tic' rate that works for both 
// the 50 and 293 Hertz requirements 

  PWMCLK   = 0x1E; // select scaled clocks SB, SA
  
  PWMPRCLK = 0x22; // prescale A,B clocks = bus/4 = 24/6 = 6 MHz
                   // 0x22 = 00100010 -> divide clock by 4

  PWMSCLA  = 0xFF; // PWM clk = SA clk/(2*SCLA) = 6MHz/510 = 0.085 msec clk
                   // for  50 Hertz, we will have 235 tic periods
                   
                              
  PWMSCLB  = 0x80; // PWM clk = SB clk/(2*SCLB) =  6MHz/256 = 0.0427 msec clk
                   // for 293 Hertz, we will have 80 tic periods
                   // can range from ~10 to 255 tics, or
                   // ~2340 Hertz to 92 Hertz
                      
  PWMCAE   = 0x00; // standard left align pulse:
                   //                            -----
                   //                           |     |______
                   //                             duty
                   //                            <--period--->

  TSCR2 |= 0x80;   // enable timer overflow interrupt (TOI)
  pTIC[0] = pTC0;
  ICintmask[0] = 0x01;
  pTOC[0] = pTC5;

  TCTL2 |= 0x88;   // bit OM1,3,5 = 1. OC output line high or 
	                 // low iaw OL1,3,5 (not toggle or disable)
  TCTL1 |= 0x08;

  // set PWM period1 (usec) for 3/2 solenoid 
  PWMPER1 = 235;
                                                  // (period and duty cycle constant)
                                                  // nominal value is 50 Hertz = 20 milliseconds 
                                                  // -> PWMPER1 = 20 * 11.765 = 235 tics
                              
  // set PWM period2 for pressure control solenoid                                                       

  PWMPER2 = 80;
                                                  // nominal value is 293 Hertz = 3.41 milliseconds 
                                                  // -> PWMPER2 = 3.41 * 23.419 = 80 tics
  
  // set PWM period3 for torque converter clutch                                   

  PWMPER3 = 243;      
                                                  // 71.430 is (1/0.014) ticks/millisecond
                                                  // nominal value is 293 Hertz = 3.41 milliseconds 
                                                  // -> PWMPER2 = 3.41 * 23.419 = 243 tics  
  
  // set PWM period4 (usec) for speedo output
  
  PWMPER4 = 120; // 120 usec default for speedo output - short so we can get high frequencies 
                 //                                      because can only turn on/off at 1/2 freq

  // Initialize PWM Duty Cycles
  PWMDTY1 =    0;       // set Sol32 PWM duty to 0% (90% in 2, 3, 4)
  PWMDTY2 =    0;       // set PC PWM duty to 0%
  PWMDTY3 = 0;      // turn off TCC
  PWMDTY4 =    0;   // speedo output off
  PWMCNT1 = 0x00;   // clear counter (reset PWM)

  pTIC[1] = pTC0;
  ICintmask[1] = 0x01;
  
  
  // --- Set Up ADC Channels ---
  // Set up analog-digital converter (ADC) channels so they continuously convert, 
  // we will read one value in the ISR_timer_clock() routine every 25 milliseconds.
  
  /* USING THE HCS12 A/D CONVERTER 
    (* indicates the values we use for MShift)
     
     1. Set up which pins are analog/digital converters and which are digital
        inputs using the ATD0DIEN register, 1=digital input, 0=ADC
     
     2. Power up A/D Converter (ADPU = 1 in ATD0CTL2)

     3. Select number of conversions per sequence (S8C S4C S2C S1C in ATD0CTL3)
          S8C S4C S2C S1C = 0001 to 0111 for 1 to 7 conversions
          S8C S4C S2C S1C = 0000 or 1xxx for 8 conversions

     4. Set up resolution in ATD0CTL4 position 7 (0-7)
         - For  8-bit mode ( 255 divisions) write 1 to ATD0CTL4 position 7
         - For 10-bit mode (1024 divisions) write 0 to ATD0CTL4 position 7
           
     5. Select DJM in ATD0CTL5 position 7
        (a) DJM = 0 => Left justified data in the result registers
        (b) DJM = 1 => Right justified data in the result registers *

     6. Select DSGN in ATD0CTL5 position 6
        (a) DSGN = 0 => Unsigned data representation in the result register *
        (b) DSGN = 1 => Signed data representation in the result register
        The Available Result Data Formats are shown in the following table:
        
        SRES8 DJM DSGN   Data Format
        1      0    0    8-bit/left justified/unsigned   - Bits 15-8
        1      0    1    8-bit/left justified/signed     - Bits 15-8
        1      1    X    8-bit/right justified/unsigned  - Bits  7-0
        0      0    0    10-bit/left justified/unsigned  - Bits 15-6
        0      0    1    10-bit/left justified/signed    - Bits 15-6
        0      1    X    10-bit/right justified/unsigned - Bits  9-0 *

     7. Select SCAN in ATD0CTL5 position 5:
         - SCAN = 0: Convert one sequence, then stop
         - SCAN = 1: Convert continuously *

     8. Select MULT in ATD0CTL5 position 4:
         - MULT = 0: Convert one channel eight the specified number of times
         - Choose channel to convert with CC, CB, CA of ATD0CTL5.
         - MULT = 1: Convert across several channels. CC, CB, CA of ATD0CTL5
           is the first channel to be converted. *

     9. After writing to ATD0CTL5, the A/D converter starts, and the SCF bit
        is cleared. After a sequence of conversions is completed, the SCF flag in
        ATD0STAT0 is set. You can read the results in ATD0DRx [0-7]H.

     10. If SCAN = 0, you need to write to ATD0CTL5 to start a new sequence. If
         SCAN = 1, the conversions continue automatically, and you can read new
         values in ADR[0-7]H.

     11. To get an interrupt after the sequence of conversions are completed, set
         ASCIE bit of ATD0CTL2. After the sequence of conversions, the ASCIF bit
         in ATD0CTL2 will be set, and an interrupt will be generated.

     12. On HCS12 EVBU, AD0 channels 0 and 1 are used to determine start-up
         program (D-Bug12, EEPROM or bootloader). Do not use AD0 channels
         0 or 1 unless absolutely necessary (you need more than 14 A/D
         channels).

     13. To interpret the result, ATD0DRx = (Vin - VRL) / (VRH - VRL) * 1024
         Normally, VRL = 0 V, and VRH = 5 V, so ATD0DRx = (Vin / 5 V) * 1024
         Example: ATD0DR0 = 351 => Vin = 448/1024 * 5.0 = 1.71 V
  */

/*  We want ADC on AD0 (swA), AD1 (swB), AD2 (temp), AD3 (swC), AD4 (line pressure), 
    and AD5 (non_CAN MAP) only. The rest are digital inputs.
     
switchA							 PAD00				GPI1     5
switchB							 PAD01				GPI2     6
Temp Sensor			     PAD02				GPI3		30
switchC							 PAD03				EGT3    25
line pressure sensor PAD04        EGT2    27
non-CAN MAP/TPS/MAF  PAD05        EGT1    24
Paddle DOWN			     PAD06        GPI5		 4
Brake Sense			     PAD07				GPI4     3

-- These will be sampled by the get_adc() function, one every 25 milliseconds in 
   the Timer_Clock_ISR() function 
*/
//   - PAD00: switchA
//   - PAD01: switchB
//   - PAD02: temp sense
//   - PAD03: switchC
//   - PAD04: line pressure
//   - PAD05: non-CAN MAP
//   - PAD06: paddle downshift
//   - PAD07: brake sense
  ATD0DIEN = 0xC0;  // 1=digital input, 0=ADC
                    // 0x3F = 11000000
                    // ADC on temperature sensor, line pressure sensor, optional MAP only
                    // 3 manual shift lever inputs
                    // rest are digital inputs
                    // read digital input pins from PORTAD0
  next_adc =    0;	// specifies next ADC channel to be read
  ATD0CTL2 = 0x40;  // leave interrupt disabled, set fast flag clear
  ATD0CTL3 = 0x00;  // do 8 conversions/ sequence
  ATD0CTL4 = 0x67;  // 10-bit resolution, 16 tic conversion (max accuracy),
                    // prescaler divide by 16 => 2/3 us tic x 18 tics
                    // 0x67 = 01100111
  ATD0CTL5 = 0xB0;  // right justified, unsigned, continuous conversion,
                    // sample 8 channels starting with AN0
                    // 0xB0 = 10110000
  ATD0CTL2 |= 0x80; // turn on ADC0
  
  // wait for ADC engine charge-up or P/S ramp-up
  for(ix = 0; ix < 160; ix++)  {
    while(!(ATD0STAT0 >> 7));	    // wait until conversion complete
    ATD0STAT0 = 0x80;
  }  // end for (ix = 0...
       
  first_adc = 0;
  adc_lmms = lmms;

	/* Set Up variable block addresses to be used for 
	    CAN communications */
  canvar_blkptr[0] = (char *)&inpram; // assign the address of inpram to canvar_blkptr[0]
  canvar_blkptr[1] = (char *)&outpc;  // assign the address of outpc to canvar_blkptr[1]
  canvar_blkptr[2] = (char *)msvar;   // assign the address of msvar to canvar_blkptr[2]
  
  for(ix = 3; ix < NO_VAR_BLKS; ix++)  {	 // rest are spares for now
    canvar_blkptr[ix] = 0;
  } // end for(ix =3 ...

  // Initialize outpc. variables
  flocker = 0;  

  /* Initialize CAN comms */
  can_reset = 0;
  can_id = inpram.mycan_id;
  CanInit();   

  // Make IC highest priority interrupt
  // On HCS12 processors, you can promote a single interrupt source to 
  // have the highest priority by writing the LSB address of the appropriate 
  // interrupt vector to the HPRIO register.
  // The interrupt priority has meaning only when two or more interrupt requests 
  // are being processed at the same time to determine which ISR will be 
  // executed first. Once a certain ISR has started to execute, the interrupt 
  // priority does NOT mean another interrupt can interrupt the already executing ISR.
  //   HPRIO = 0xEE;
  
// enable global interrupts
ENABLE_INTERRUPTS
  
// Restart PWM counters
PWMCNT1 = 0x00;
PWMCNT2 = 0x00;  // clear counter
PWMCNT3 = 0x00;  // clear counter
PWMCNT4 = 0x00;
PWME   |= 0x1E;  // enable PWM 1,2,3 & 4 (0x1E = 00011110)

// Set up COP timeout for main loop
// COPCTL = 0x44;      // 01000100 = 2^20 OSCCLK cycles = 131 ms timeout for reboot
// COPCTL = 0x42;      // 01000010 = 2^16 OSCCLK cycles = 131 ms timeout for reboot
  
// ---- Template MAIN LOOP starts here -------------------------------------------
// Start an infinite FOR loop to keep cycling through the main loop until 
// there is a reset or the universe ends.
// Everything before this happens ONLY at startup - so calculations done before this 
// point require a reset to be recalculated if applicable user parameters are changed.
// Calculations after this point in the main function happen every time through the 
// main loop, so a reset isn't needed (but keeping unneeded calculations out of 
// the main loop speeds the code).

MAIN_LOOP:
for (;;)  { // start the main loop

// Increment main loop counter for datalog 
outpc.loop_count++; 

// Error codes
outpc.error = 0x01; // always set first bit - error code must be odd number otherwise 
                    // a comms error has occurred                      

// INPUT/OUTPUT EXAMPLES
// These are placed here (in the main loop) to show how to do I/O
// However these statements can be placed anywhere, in the timer ISR, 
// the VSS/ISS interrupts, etc.

// --------   READ ALL THE INPUTS -------------------------------
// (In a real program you might want to perform conversions, conditional checks, 
// etc., on the inputs, of course.)
/*
GPI4		  PAD07		I     --    --    (PORTAD0 & 0x80)   - on/off			
GPI5		  PAD06   I     --    --    (PORTAD0 & 0x40)   - on/off   	
EGT1      PAD05   I     --    --    ATD0DR5            - ADC count   
EGT2      PAD04   I     --    --    ATD0DR4            - ADC count     
EGT3      PAD03		I     --    --    ATD0DR3            - ADC count  
GPI3      PAD02		I     --    --    ATD0DR2            - ADC count    
GPI2			PAD01		I     --    --    ATD0DR1            - ADC count  
GPI1		  PAD00		I     --    --    ATD0DR0            - ADC count 
VR2			  PT6     I     --    --    PTT & 0x40         - on/off
VR3       PT5     I     Interrupt ISS_timer()          - interrupt called on edge     			
VR1				PT0			I     Interrupt VSS_timer()          - interrupt called on edge
EGT4      PE1     I     --    --  (PORTE & 0x02)       - on/off        
*/

// read GPI4 - digital
if (PORTAD0 & 0x80)  // if PORTAD0 pin 7 (x0000000) and 0x80 = 10000000
                     // are both one (PAD07 high), then this is true
                     // note that the 8 pins are numbered 0 to 7 from right to left, so:
                     // 76543210                 
	{
	outpc.gpi4 = 1;   // set set the value to one 
	}
else
	{
    outpc.gpi4 = 0;   // otherwise set it to zero
	}
   
// read GPI5 - digital
if (PORTAD0 & 0x40)  // if PORTAD0 pin 6 (0x000000) and 0x40 = 01000000
                     // are both one (PAD06 high), then this is true
   	{
   	outpc.gpi5 = 1;   // set set the value to one
   	}
	else 
	{
    outpc.gpi5 = 0;   // otherwise set it to zero   
	}

// read EGT1
outpc.egt1 = get_adc(5); // assign the value from the loadfactor.inc table, using the adc 
                         // count as an index,  to the variable for the datalog and calc's
// read EGT2
outpc.egt2 = get_adc(4); // assign the value from the linefactor.inc table, using the adc 
                         // count as an index,  to the variable for the datalog and calc's
// read EGT3
outpc.egt3 = get_adc(3); // assign the raw adc count to the variable for the datalog and calc's

// read GPI3
outpc.gpi3 = get_adc(2); // assign the value from the cltfactor.inc table, using the adc 
                         // count as an index,  to the variable for the datalog and calc's                        
// read GPI2
outpc.gpi2 = get_adc(1); // assign the raw adc count to the variable for the datalog and calc's

// read GPI1
outpc.gpi1 = get_adc(0); // assign the raw adc count to the variable for the datalog and calc's

// read VR2  - digital
if (PTT & 0x40)      // if PTT pin 6 (0x000000) and 0x40 = 01000000
                     // are both one (PAD06 high), then this is true
   	{
   	outpc.vr2 = 1;    // set the variable value to one
   	}
else
	{	 
   	outpc.vr2 = 0;    // otherwise set it to zero
	}
   
// VR3 - timer input
// read in interrupt, here will will get the average
tmpx1 = 0;
for (ix=1;ix<21;ix++)   
  {
  tmpx1 = tmpx1 +  ic2_period[ix];  // total the last twenty values
  }
outpc.vr3 = tmpx1/20;  // assign average to variable for calc's and datalog


// VR1 - timer input
// read in interrupt, here will will get the average
tmpx1 = 0;
for (ix=1;ix<21;ix++)   
  {
  tmpx1 = tmpx1 +  ic1_period[ix];  // total the last twenty values
  }
outpc.vr1 = tmpx1/20; // assign average to variable for calc's and datalog

// Read EGT4       
if (PORTE & 0x02)  // if PORTE pin 1 (000000x0) and 0x02 = 00000010
                   // are both one (PAD06 high), then this is true
   	{
   	outpc.egt4 = 1; // set variable to one if PE1 is high
   	}
else
	{
	outpc.egt4 = 0; // zero otherwise
	}

// --------  SET ALL THE OUTPUTS -------------------------------   
// Set outputs to show how it is done
/*
VR4        PT7      O     *pPTTpin[7] |= 0x80;  *pPTTpin[7] &= ~0x80;   ---
PWM1		   PT4			O			PWMDTY4 = PWMPER4;    PWMDTY4 = 0;           PWMDTY4
PWM2		   PT3			O		  PWMDTY3 = PWMPER3;    PWMDTY3 = 0;           PWMDTY4
PWM3		   PT2			O		  PWMDTY2 = PWMPER2;    PWMDTY2 = 0;           PWMDTY4
PWM4		   PT1      O     PWMDTY1 = PWMPER1;    PWMDTY1 = 0;           PWMDTY4
GPO4  	   PB4      O     PORTB |= 0x10;        PORTB &= ~0x10;         ---
GPO3  	   PM5      O     *pPTMpin[5] |= 0x20;  *pPTMpin[5] &= ~0x20;   ---	   
GPO1		   PM4      O     *pPTMpin[4] |= 0x10;  *pPTMpin[4] &= ~0x10;   ---
GPO2 		   PM3      O     *pPTMpin[3] |= 0x08;  *pPTMpin[3] &= ~0x08;   ---
VB4			   PM2      O     *pPTMpin[2] |= 0x04;  *pPTMpin[2] &= ~0x04;   ---
VB3			   PE4      O    	PORTE |= 0x10;        PORTE &= ~0x10;         ---
VB2        PA0      O     PORTA |= 0x01;        PORTA &= ~0x01;         ---
*/

// OUTPUT EXAMPLES:

// set VR4 output
// example: if intvariable000 is greater than zero OR charvariable000 is equal to longvariable000
if ((inpram.intvariable000 > 0) || (inpram.charvariable000 == inpram.longvariable000)) 
  { // turn port on
   *pPTTpin[7] |= 0x80; // if either the current port state 
                        // or 0x80 = 1000000 are one (and the 7th bit IS one), 
                        // set the 7th bit (leftmost) of the port to one (ON)
   
   // *pPTTpin[7] |= 0x80; is the same as *pPTTpin[7] = *pPTTpin[7] | 0x80;                     
  
  }
  else  // otherwise turn port off
  {
  *pPTTpin[7] &= ~0x80; // if both the current port state 
                        // and ~0x80 = 01111111 are one (and the 7th bit IS NOT one), 
                        // set the 7th bit of the port to zero (OFF)
   
   // *pPTTpin[7] &= ~0x80; is the same as *pPTTpin[7] = *pPTTpin[7] & ~0x80;
  
  }

// set PWM1 circuit output
// example: if the engine rpm is greater than 300 and the tps is greater than 900 adc counts (~4.4 Volts)
if ((outpc.engine_rpm > 300) && (outpc.tps > 900)) 
  { // turn port on
   PWMDTY4 = PWMPER4;
  }
  else  // otherwise turn port off
  {
   PWMDTY4 = 0; // set duty cycle to zero
  }


// set PWM2 circuit output
// on only between 1200 and 3600 rpm
if ((outpc.engine_rpm > 1200) && (outpc.engine_rpm < 3600) ) 
  { // turn port on
   PWMDTY3 = PWMPER3;
  }
  else  // otherwise turn port off
  {
   PWMDTY3 = 0; // set duty cycle to zero
  }


// set PWM3 circuit output  
// example: set as a function of load
if (outpc.LOAD < 100)
  {
   PWMDTY2 = (outpc.LOAD * PWMPER2)/1000; // divide by 1000 afterwards,
                                          // (LOAD is kPa x 10) 
                                          // otherwise LOAD/1000 is always zero 
                                          // in integer math if LOAD < 1000
  }
  else
  {
  PWMDTY2 = PWMPER2; // max out PWM% at 100%
  }


// set PWM4 circuit output
// set output to 50% PWM% if gpi4 is grounded, 0% otherwise
if (PORTAD0 & 0x80)  // could also have done if (outpc.gpi4 > 0)
  {  // port is high (not grounded)
  PWMDTY1 = 0;
  }
  else   // port is low (grounded by user switch)
  {
  PWMDTY1 = PWMPER1/2;
  }

// set GPO4 output
// example: set opposite to PWM3
if (PWMDTY3 == 0)
  {
   PORTB |= 0x10;   // turn ON
  }
  else
  {
   PORTB &= ~0x10;  // turn OFF
  }

// set GPO3 output
// example: flash LED on GPO3 every second
// for monitoring on a oscilloscope, for example
if (!(outpc.seconds % 2)) // % is the 'modulus' operator - it returns the remainder after division
  {
   *pPTMpin[5] |= 0x20;	// turn the port ON (flash LED)   
  }
  else
  {
   *pPTMpin[5] &= ~0x20;  // turn the port OFF
  }

// set GPO2 output
// if the ADC count from EGT4 is greater than 1/2 of the range 
// (nominally 2.5 volts, but depends on how circuit is configured)
if (outpc.egt3 > 512)
  {
   *pPTMpin[4] |= 0x10;  // turn output ON 
  }
  else
  {
   *pPTMpin[4] &= ~0x10; // turn output OFF
  }

// set GPO1 output
// if the engine is cranking or running - i.e. if the rpm is not zero
if (outpc.engine_rpm)
  {
   *pPTMpin[3] |= 0x08;  // turn output ON  
  }
  else
  {
   *pPTMpin[3] &= ~0x08; // turn output OFF
  }

// set VB4 output
// if the VR1 input is faster than a user specified period (in clock 'tics') then turn VB4 on 
// (note that we can convert the period into microseconds in the INI if desired)
if (outpc.vr1 < inpram.intvariable001)
  {
   *pPTMpin[2] |= 0x04;   // turn output ON
  }
  else
  {
   *pPTMpin[2] &= ~0x04;  // turn output OFF
  }

// set VB3 output
// if the engine rpm is greater than a user specified number
if (outpc.engine_rpm > inpram.intvariable002)
  {
   PORTE |= 0x10;        // turn output ON
  }
  else
  {
   PORTE &= ~0x10;       // turn output OFF
  }

// set VB2 output
// if we have received at least one CAN message
// (Note that CANrx = 0 is the same as 'false', and number greater than zero is 'true')
if (outpc.CANrx)
  {
   PORTA |= 0x01;       // turn output ON
  }
  else
  {
   PORTA &= ~0x01;      // turn output OFF 
  }                   

// END OF OUTPUT EXAMPLES
  
DISABLE_INTERRUPTS
ultmp = lmms;
ENABLE_INTERRUPTS  


/***************************************************************************
**
** Check whether to burn flash
**
**************************************************************************/

BURN_FLASH:
// Burn Flash if flag set
if ((outpc.loop_count % 50) == 0) // slow down the flash burns to every 50 main loops (~50/8000 = ~6 milliseconds)
  {
      // burn flash 512 byte(256 word) sector(s)
      if(burn_flag >= 1)  
        {
        fburner(tableWordFlash(burn_idx, 0), tableWordRam(burn_idx, 0), tableWords(burn_idx));
        if(burn_flag >= tableWords(burn_idx))  
          {
          burn_flag = 0;
          flocker = 0;
          }
        else
          burn_flag++;
        }
   }
    
/***************************************************************************
**
** Check for reinit command
**
**************************************************************************/
    if(reinit_flag)  
      {
      reinit_flag = 0;
      // update initialization of variables/ registers dependent on
      // user config inputs
      reset();
      // Set up SCI (rs232): SCI BR reg= BusFreq(=24MHz)/16/baudrate
      SCI0BDL = (unsigned char)(1500000/inpram.baud);
      ltmp = (150000000/inpram.baud) - ((long)SCI0BDL*100);
      if(ltmp > 50)SCI0BDL++;   // round up
      can_id = inpram.mycan_id;
      can_reset = 1;     // do this to reset id in CAN message mask register
      }   // End if(reinit_flag)



/***************************************************************************
**
**  Check for serial, CAN receiver timeout
**
**************************************************************************/

    DISABLE_INTERRUPTS
    ultmp = lmms;
    ultmp2 = rcv_timeout;
    ENABLE_INTERRUPTS
    if(ultmp > ultmp2)  
      {
      txmode = 0;    // break out of current receive sequence
      rcv_timeout = 0xFFFFFFFF;
      } // end if (ultmp > ultmp2)
    
    DISABLE_INTERRUPTS
    ultmp2 = ltch_CAN;
    ENABLE_INTERRUPTS
    
    if(ultmp > ultmp2)  
      {
      getCANdat = 0;    // break out of current receive sequence
      ltch_CAN = 0xFFFFFFFF;
      }  //end if (ultmp > ultmp2)
    
    if(kill_ser)  
      {
      if(outpc.seconds > kill_ser_t)  
        {
        kill_ser = 0;
        SCI0CR2 |= 0x24;     // = 11000 -> rcv, rcvint re-enable
        }  // end if (outpc.seconds > ...
      }  // end if (kill_ser)
  
/***************************************************************************
**
**  Check for CAN reset
**
**************************************************************************/
	  if(can_reset)  
	    {
		  /* Re-initialize CAN comms */
		  CanInit();
		  can_reset = 0;
	    }  // end if (can_reset) ...
  

} // end for(;;) main loop
  
} // end main() function

/** =========== FUNCTIONS ===================================================== **/

#pragma CODE_SEG DEFAULT

#include "flashburn.h"

//--------------- get_adc -------------------------------------------------------
signed int get_adc(char chan1)  {

// the ATD0DRx ADC result register contains the ten bit result of the 
// ADC conversion for channel x, we use this as an index to the 'lookup'_table 
// to get the actual value
   
    switch(chan1)  {
      case 0:
        adcval = ATD0DR0; // set value to ADC count for swA (manual shift lever position)
        break;
      case 1:
        adcval = ATD0DR1; // set value to ADC count for swB (manual shift lever position)
        break;
      case 2:
      // temperature sensor
        adcval = cltfactor_table[ATD0DR2]; // look up temperature in degrees F x 10 
        break;
      case 3:
        adcval = ATD0DR3; // set value to ADC count for swC (manual shift lever position)
        break;
      case 4:
      // line pressure
        adcval = linefactor_table[ATD0DR4];
        break;
      case 5:
      // non-CAN MAP
        adcval = loadfactor_table[ATD0DR5];
        break;
      case 6:
        break;
      case 7:
        break;
      default:
        break;
    }			 // end of switch
  
  return(adcval);
}  // end get_adc()

#pragma CODE_SEG DEFAULT

#pragma CODE_SEG NON_BANKED

void switch_page(unsigned char sub_no)  {
// This resides in non-banked memory and is used to call subroutines
// from another page.

  switch(sub_no)  {
          
    case 0:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
      
    case 1:
      PPAGE = 0x3C;
      PPAGE = 0x3D;
      break;

    case 2:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
    
    case 3:
      PPAGE = 0x3D;
      PPAGE = 0x3C;
      break;
      
    default:
      break;
  }
  
  return;
}

#pragma CODE_SEG ROM_7000

// --------------- intrp_1dctable -------------------------------------------------------
//
// 1D Interpolation Routine
// ------------------------
// sgnx     = 
// x        = the index value for the lookup
// n		    = the number of rows in (size of) the lookup table
// x_table  = the name of the lookup table
// sgny     =
// z_table  = the 'looked up' values
// return the interpolated value as an integer

int intrp_1dctable(char sgnx, int x, unsigned char n, int * x_table, char * z_table)  {
  int ix;
  long interp, interp3;
  // bound input arguments
  if((sgnx && (x > x_table[n-1])) ||
     (!sgnx && ((unsigned int)x > (unsigned int)x_table[n-1])))  {
      return((int)z_table[n -1]);
  }
  if((sgnx && (x < x_table[0])) ||
     (!sgnx && ((unsigned int)x < (unsigned int)x_table[0])))  {
      return((int)z_table[0]);
  }
  for(ix = n - 2; ix > -1; ix--)  { 
    if((sgnx && (x > x_table[ix])) ||
       (!sgnx && ((unsigned int)x > (unsigned int)x_table[ix])))  {
   		break;
  	}
  }
  if(ix < 0)ix = 0;
 
  interp =	(unsigned int)x_table[ix + 1] - (unsigned int)x_table[ix];
  if(interp != 0)  {
    interp3 = (unsigned int)x - (unsigned int)x_table[ix];
    interp3 = (100 * interp3);
    interp = interp3 / interp;
  }

  return((int)((char)z_table[ix] +
	    interp * ((char)z_table[ix+1] - (char)z_table[ix])/ 100));
}


// --------------- intrp_2dctable -------------------------------------------------------

int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny,
  unsigned int * x_table, int * y_table, unsigned char * z_table)  {
  int ix,jx;
  long interp1, interp2, interp3;
  // bound input arguments
  if(x > x_table[nx-1])x = x_table[nx-1];
  else if(x < x_table[0])x = x_table[0];
  if(y > y_table[ny-1])y = y_table[ny-1];
  else if(y < y_table[0])y = y_table[0];
  // Find bounding indices in table
  for(ix = ny - 2; ix > -1; ix--)  {  // Start w highest index
	//  because will generally have least time for calculations at hi y
  	if(y > y_table[ix])  {
   		break;
  	}
  }
  if(ix < 0)ix = 0;
  for(jx = nx - 2; jx > -1; jx--)  {  // Start w highest index
	// because will generally have least time for calculations at hi x
	  if(x > x_table[jx])  {
	    break;
    }
  }
  if(jx < 0)jx = 0;
  // do 2D interpolate
  interp1 = y_table[ix + 1] - y_table[ix];
  if(interp1 != 0)  {
    interp3 = (y - y_table[ix]); 
    interp3 = (100 * interp3); 
    interp1 = interp3 / interp1; 
  }
  interp2 =	x_table[jx + 1] - x_table[jx];
  if(interp2 != 0)  {
    interp3 = (x - x_table[jx]); 
    interp3 = (100 * interp3); 
    interp2 = interp3 / interp2; 
  }
  return((int)(((100 - interp1) * (100 - interp2) * z_table[ix*nx+jx]
	  + interp1 * (100 - interp2) * z_table[(ix+1)*nx+jx]
	  + interp2 * (100 - interp1) * z_table[ix*nx+jx+1]
	  + interp1 * interp2 * z_table[(ix+1)*nx+jx+1]) / 10000));
}

#pragma CODE_SEG NON_BANKED // Interrupts must be in non-banked memory, 
                            // because the vectors are only 16 bit and 
                            // will not accommodate the appended 6 bit 
                            // ppage register. 

// --------------- VSS Timer ----------------------------------------
// Time the period on the VSS input, set in array ic_period[]

INTERRUPT void VSS_timer(void)  {
  unsigned int period1;
  
  // reset interrupt
  TFLG1 = 0x01;  // write a one (00000001) to TFLG to clear flag for timer channel 0  
  period1 = (TC0 - tcsav1);   // tcsav1 is a global variable - it is the time of the last interrupt

  ic1_period[ic1_count] = period1;    // 5.333 �sec ticks
  tcsav1 = TC0;                       // save time of new trigger edge
        
  ic1_count++;   // sample counter for last 20 input capture periods
  if (ic1_count > 20) ic1_count=1;  
  
  // reset interrupt
  TFLG1 = 0x01;           // write a one (00000001) to TFLG to clear flag for timer channel 0 
  
  return;
}  // end VSS_timer()


// --------------- ISS/non-CAN Tach Timer ----------------------------------------
// Time the period on the VSS input, set in array ic_period[]

INTERRUPT void ISS_timer(void)  {
  unsigned int period2;
  
  // set interrupt received flag
  // ISS_rcvd++;     // will clear flag (set to zero) in 0.010 second section
  // reset interrupt
  TFLG1 |= 0x20;     // write a one to TFLG bit 5 (00100000) to clear flag for timer channel 0     
  period2 = (TC5 - tcsav2);   // tcsav2 is a global variable - it is the time of the last interrupt

  // reject if outside mask range of current average
  // ic2_per_avg is ~100 times the period
  // We calculate long_period and short_period in 0.010 section of timer

  if ((period2 < long_period_iss) &&    // if the period is less than 
       (period2 > short_period_iss))  // AND the period is greater than                
       {
        //  calculate last period, us
        ic2_period[ic2_count] = period2; // 5.333 �sec ticks
        tcsav2 = TC5;                    // save time of new trigger edge
       }  // end if (utmp <...

  ic2_count++;   // sample counter for last 20 input capture periods
  if (ic2_count > 20) ic2_count=1;  
  return;
}

#pragma CODE_SEG  NON_BANKED

INTERRUPT void Timer_Clock_ISR(void)  {
	short ix; 
  // .128 ms clock interrupt - clear flag immediately to resume count
  CRGFLG = 0x80;  // clear RTI interrupt flag
  // also generate 1.024 ms, .10035 sec and 1.0035 sec clocks
  lmms++;         // free running clock(.128 ms tics) good for ~ 110 hrs
  can_mmsclk++;
  mms++;          // in .128 ms tics - reset every 8 tics = 1.024 ms
      
  // check mms to generate other clocks
  if(mms < 8)goto CLK_DONE;
  mms = 0;
  
  
//////////////////////////// 0.001 millisecond section ///////////////////////

  millisec++;     // actually 1.024 ms, for seconds count
  burn_msec_flag = 0;        // reset burn timer to allow next step of flash burn

// slow down the flash burns to every ~6 milliseconds
if (!(millisec%6))
  {   
  // burn flash 512 byte(256 word) sector(s)
  if(burn_flag >= 1)  
    {
    burn_msec_flag = 1;
    fburner(tableWordFlash(burn_idx, 0), tableWordRam(burn_idx, 0), tableWords(burn_idx));
    if(burn_flag >= tableWords(burn_idx))  
      {
      burn_flag = 0;
      flocker = 0;
      }
      else
      burn_flag++;
      } 
    }

    if (burn_flag) outpc.flashburn = 1; else outpc.flashburn = 0; // set burn flag for datalog
//////////////////////////// END of 0.001 second section ///////////////////////


//////////////////////////// 0.010 second section ////////////////////////////////////////
//                                                                                      //
if (millisec % 10 == 0) // every 1/100th second (actually 10/976 = 10.24 milliseconds)  //
//                                                                                      //
//           Everything in this section will execute ~100 times/second.                 //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
  {
    // Decrement the wait states
	if (delay > 10) delay -= 10; else delay = 0;
  
	// Set up CAN transmission
	if(in2ram.activate_xrate > 0)  
		{
		// send various bytes in the outpc structure to destvarblk.
		// The bytes are as configured in outmsg[msg_no] where 
		// msg_no = CAN_RB_DSR0
		uctmp = 1; 
		for (mx = 0; mx < MAX_OUTMSGS; mx++)  
		{   // mx = msg_no

	if(outmsg[mx].xrate && (in2ram.activate_xrate & uctmp))  
		{
		// this message activated - check if time to send
		if(xrate_clks[mx] > outmsg[mx].xrate)  
		{     // time to send
		xrate_clks[mx] = 0;           // zero clk
		// Update related parameters for xmt ring buffer
		ix = can[1].cxno_in;
		can[1].cx_msg_type[ix] = OUTMSG_RSP;
		can[1].cx_outmsg_no[ix] = mx;
		n_outbytes[mx] = outmsg[mx].no_bytes;
		// destination var blk - for this msg the varblk is really the gpio
		// varblk to return the data to
		can[1].cx_destvarblk[ix] = outmsg[mx].dest_varblk & 0x0F;   
															// gpio var_blk
		can[1].cx_destvaroff[ix] = 0;   // store data starting at gpio varblk;
		// if > 8 bytes, will have subsequent messages, with varoff provided 
		// by ECU
		can[1].cx_dest[ix] = outmsg[mx].dest_varblk >> 4;    // gpio id
		if(n_outbytes[mx] >= 8)  
			{
			kx = 8;
			}
			else  
			{
			kx = n_outbytes[mx];
			}  // end if (n_outbytes...
		
		can[1].cx_varbyt[ix] = kx;
		// put variable value(s) in xmit ring buffer as follows:
		for(jx = 0;jx < kx;jx++)  
			{
			can[1].cx_datbuf[ix][jx] = 
				*((char *)&outpc + outmsg[mx].offset[jx]);
			}  // end for(jx = 0 ...
		send_can(1);
		}   // end if(xrate_clks[mx]...
	} //  end if(outmsg[mx].xrate &&...
	uctmp = uctmp << 1;    // = 2 ^^ mx
   }      // end for (mx = 0;...
  } // end if(inpram.activate_xrate)



	
//////////////////////////////////////////////////////////////////////
// Send CAN to get outpc variables from MS-II

grabMS2vars();

//////////////////////////////////////////////////////////////////////
	  
/*  -----  Update RPM, MAP & Battery Voltage with CAN ----------------------------------- */
// Update outpc. rpm, battery voltage, MAP, etc. for calculations using CAN data
// received from MS-II
// (that is, all the info is already there in the msvar array, the CAN data is 
// grabbed in sync with the timer updates in Timer_Clock_ISR(), but not all of 
// info that is assigned to specific variables).
// 
// This code only uses the rpm, kpa, adv_deg, pulsewidth1, and vBatt data from the 
// msvar array. (Other variables are included for reference and future development. To 
// be used, the corresponding variable must be uncommented and the variable added to 
// this code's outpc. structure - and the [CHANNELS] section of the INI 
// file if you want to log it, etc.)

if (in2ram.CAN_enabled) 
  {
  // outpc.seconds        = *(int *)(msvar + 0);   // 2 bytes (int) at offset 0 in msvar[]
  outpc.pulsewidth1= *(int *)(msvar + 2);   // 2 bytes (int) at offset 2 in msvar[]
  // outpc.pw2            = *(int *)(msvar + 4);   // 2 bytes (int) at offset 4 in msvar[]
  outpc.engine_rpm = *(int *)(msvar + 6);   // rpm is 2 bytes (int) at offset 6 in msvar[]
  outpc.adv_deg    = *(int *)(msvar + 8);   // 2 bytes (int) at offset 8 in msvar[]
  // outpc.squirt         = *(char *)(msvar + 10); // 1 byte (char) at offset 10 in msvar[]
  // outpc.engine         = *(char *)(msvar + 11); // 1 byte (char) at offset 11 in msvar[]
  // outpc.afrtgt1        = *(char *)(msvar + 12); // 1 byte (char) at offset 12 in msvar[]
  // outpc.afrtgt1        = *(char *)(msvar + 13); // 1 byte (char) at offset 13 in msvar[]
  // outpc.wbo2_en1       = *(char *)(msvar + 14); // 1 byte (char) at offset 14 in msvar[]
  // outpc.wbo2_en2       = *(char *)(msvar + 15); // 1 byte (char) at offset 15 in msvar[]
  // outpc.barometer      = *(int *)(msvar + 16);  // 2 bytes (int) at offset 16 in msvar[]
  outpc.LOAD       = *(int *)(msvar + 18);  // kPa is 2 bytes (int) at offset 18 in msvar[]
  // outpc.mat            = *(int *)(msvar + 20);  // 2 bytes (int) at offset 20 in msvar[]
  // outpc.coolant        = *(int *)(msvar + 22);  // 2 bytes (int) at offset 22 in msvar[]
  outpc.tps        = *(int *)(msvar + 24);  // 2 bytes (int) at offset 24 in msvar[]
  outpc.vBatt      = *(int *)(msvar + 26);  // 2 bytes (int) at offset 26 in msvar[]
  // outpc.afr1           = *(int *)(msvar + 28);  // 2 bytes (int) at offset 28 in msvar[]
  // outpc.afr2           = *(int *)(msvar + 30);  // 2 bytes (int) at offset 30 in msvar[]
  // outpc.knock          = *(int *)(msvar + 32);  // 2 bytes (int) at offset 32 in msvar[]
  // outpc.egoCorrection1 = *(int *)(msvar + 34);  // 2 bytes (int) at offset 34 in msvar[]
  // outpc.egoCorrection1 = *(int *)(msvar + 36);  // 2 bytes (int) at offset 36 in msvar[]
  // outpc.airCorrection  = *(int *)(msvar + 38);  // 2 bytes (int) at offset 38 in msvar[]
  // outpc.warmupEnrich   = *(int *)(msvar + 40);  // 2 bytes (int) at offset 40 in msvar[]
  // outpc.accelEnrich    = *(int *)(msvar + 42);  // 2 bytes (int) at offset 42 in msvar[]
  // outpc.tpsfuelcut     = *(int *)(msvar + 44);  // 2 bytes (int) at offset 44 in msvar[]
  // outpc.baroCorrection = *(int *)(msvar + 46);  // 2 bytes (int) at offset 46 in msvar[]
  // outpc.gammaEnrich    = *(int *)(msvar + 48);  // 2 bytes (int) at offset 48 in msvar[]
  // outpc.veCurr1        = *(int *)(msvar + 50);  // 2 bytes (int) at offset 50 in msvar[]
  // outpc.veCurr2        = *(int *)(msvar + 52);  // 2 bytes (int) at offset 52 in msvar[]
  // outpc.iacstep        = *(int *)(msvar + 54);  // 2 bytes (int) at offset 54 in msvar[]
  // outpc.coldAdvDeg     = *(int *)(msvar + 56);  // 2 bytes (int) at offset 56 in msvar[]
  // outpc.tpsDOT         = *(int *)(msvar + 58);  // 2 bytes (int) at offset 58 in msvar[]
  // outpc.mapDOT         = *(int *)(msvar + 60);  // 2 bytes (int) at offset 60 in msvar[]
  // outpc.dwell          = *(int *)(msvar + 62);  // 2 bytes (int) at offset 62 in msvar[]
  // outpc.maf            = *(int *)(msvar + 64);  // 2 bytes (int) at offset 64 in msvar[]
  // outpc.calcMAP        = *(int *)(msvar + 66);  // 2 bytes (int) at offset 66 in msvar[]
  // outpc.fuelCorrection = *(int *)(msvar + 68);  // 2 bytes (int) at offset 68 in msvar[]
  // outpc.portStatus     = *(char *)(msvar + 70); // 1 byte (char) at offset 70 in msvar[]
  // outpc.knockRetard    = *(char *)(msvar + 71); // 1 byte (char) at offset 71 in msvar[]
  // outpc.xTauFuelCorr1  = *(int *)(msvar + 72);  // 2 bytes (int) at offset 72 in msvar[]
  // outpc.egoV1          = *(int *)(msvar + 74);  // 2 bytes (int) at offset 74 in msvar[]
  // outpc.egoV2          = *(int *)(msvar + 76);  // 2 bytes (int) at offset 76 in msvar[]
  // outpc.amcUpdates     = *(int *)(msvar + 78);  // 2 bytes (int) at offset 78 in msvar[]
  // outpc.aux_voltsix    = *(int *)(msvar + 80);  // 2 bytes (int) at offset 80 in msvar[]
  // outpc.xTauFuelCorr2  = *(int *)(msvar + 82);  // 2 bytes (int) at offset 82 in msvar[]
  // outpc.spare1         = *(int *)(msvar + 84);  // 2 bytes (int) at offset 84 in msvar[]
  // outpc.spare2         = *(int *)(msvar + 86);  // 2 bytes (int) at offset 86 in msvar[]
  // outpc.spare3         = *(int *)(msvar + 88);  // 2 bytes (int) at offset 88 in msvar[]
  // outpc.spare4         = *(int *)(msvar + 90);  // 2 bytes (int) at offset 90 in msvar[]
  // outpc.spare5         = *(int *)(msvar + 92);  // 2 bytes (int) at offset 92 in msvar[]
  // outpc.spare6         = *(int *)(msvar + 94);  // 2 bytes (int) at offset 94 in msvar[]
  // outpc.spare7         = *(int *)(msvar + 96);  // 2 bytes (int) at offset 96 in msvar[]
  // outpc.spare8         = *(int *)(msvar + 98);  // 2 bytes (int) at offset 98 in msvar[]
  // outpc.spare9         = *(int *)(msvar + 100); // 2 bytes (int) at offset 100 in msvar[]
  // outpc.spare10        = *(int *)(msvar + 102); // 2 bytes (int) at offset 102 in msvar[]
  // outpc.spare11        = *(int *)(msvar + 104); // 2 bytes (int) at offset 104 in msvar[]
  // outpc.ospare         = *(char *)(msvar + 106);// 1 byte (char) at offset 106 in msvar[]
  // outpc.chksum         = *(char *)(msvar + 107);// 1 byte (char) at offset 107 in msvar[]
  // outpc.deltaT         = *(long *)(msvar + 108);// 4 bytes (long) at offset 108 in msvar[]
  }
} // end if (millisecond % 10 ...
//////////////////////////// END of 0.010 second section /////////////////////////////////


/////////////////////////// 0.100 second section /////////////////////////////////////
if (millisec % 98 == 0)     // 98 * 1.024 milliseconds
  {
  
/*
// Do following code 10 times per second
if (in2ram.CAN_enabled)
	{
	outpc.SpkAdj = 0;
	if (sentSpkAdj != 0) sendCANAdj();
	sentSpkAdj = 0;            
	} // end if (inpram.CAN_enabled && ...
*/     
  }  // end if (millisec % 98 ...
  
// Decrement the wait state
if (delay_count > 10 ) delay_count -= 10; else delay_count = 0;
  
//////////////////////////// END of 0.100 second section ///////////////////////  


///////////////////////// 1.000 second section ///////////////////////////////////////////
//                                                                                      //
//           Everything in this section will execute 1 time/second.                     //  
if(millisec > 976) 
 {
//                                                                                      //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
  millisec = 0;
  // update seconds to send back to PC
  outpc.seconds++;
   
  } // end 1.000 second section
   
////////////////////////  END of 1.000 second section ////////////////////////////////////

///////  Other timing increments (besides 0.001, 0.010, 0.100, and 1.000) ////////////////
  

CLK_DONE:   // Statements after this label are executed every 0.128 millisecond clock 'tic'

//////////////////////////// 0.000128 millisecond section ///////////////////////  




///////////////////////// end of 0.000128 millisecond section //////////////////// 

return;
}

// --------------- ISR_TimerOverflow ------------------------------------

INTERRUPT void Timer_Overflow_ISR(void) 
// Timer interrupt overflow service routine
// for TOI (bit 7 of TSCR2).
// Interrupts when TOF flag set if TOI == 1 
// The clock ticks are counted in the variable TCNT (which is defined in the 
// hsc12def.h file at memory location 0x0044). 
// There are 0xFFFF 'tics' (65,536 counts).
// In our case each 'tic' is 1/0.1875MHz  = 5.333 �s, so the overflow occurs 
// every 65535 * 5.3333 = 349.53 millisecond.
  {
  // Get display seconds from continuously running TCNT
  TC_ovflow++;
  if(TC_ovflow >= TC_ovfla[TC_ov_ix])  
    {
    // update seconds to send back to PC
    outpc.seconds++;     // increment secL in datalog
    if(TC_ov_ix >= 59)  
      {
      TC_ov_ix = 0;
      TC_ovflow = 0;
    }
    else
      TC_ov_ix++;
  }

  vss_timer_overflow++;

  // clear timer overflow interrupt flag (TOF)
  TFLG2 = 0x80;
  return;
}


/**************************************************************************
**
** Serial Communications (SCI)
**
** Communications is established when the PC communications program sends
** a command character - the particular character sets the mode:
**
** "a" = send all of the realtime display variables (outpc structure) via txport.
** "w"+<offset lsb>+<offset msb>+<nobytes>+<newbytes> = 
**    receive updated data parameter(s) and write into offset location
**    relative to start of data block
** "e" = same as "w" above, followed by "r" below to echo back value
** "r"+<offset lsb>+<offset msb>+<nobytes>+<newbytes> = read and
**    send back value of a data parameter or block in offset location
** "y" = verify inpram data block = inpflash data block, return no. bytes different.
** "b" = jump to flash burner routine and burn a ram data block into a flash 
**    data block.
** "t" = receive new data for clt/mat/ego/maf tables
** "T" = receive new table data for CAN re-transmission to GPIO
** "c" = Test communications - echo back Seconds
** "Q" = Send over Embedded Code Revision Number
** "S" = Send program title.
**
**************************************************************************/
/*
To use SCI1 for 8 bit, non-parity communication:

1. Select the baud rate via SCI0BDH/L Baud Rate Registers (we did this earlier). 
2. Enable transmission and/or reception as desired by setting the TE and/or RE bits in 
the SCI0CR2 Control Register.

         bit        7       6      5     4     3     2     1     0
                   TIE     TCIE   RIE   ILIE   TE    RE   RWU   SBK
 
3. For:

- transmission - poll the TDRE flag and write data to the SCI0DRL register at 
                 address $00D7 when the TDRE flag is set. The data we write 
                 to the SCI0DRL register is the data that will be immediately 
                 output from the TX pin. 
- reception    - poll the RDRF register (SCI0SR1). When the RDRF register is 
                 set ((SCI0SR1 & 0x20) <> 0), we can then 
                 read the incoming data by reading the SCI1DRL register. The 
                 data we read in from this register is the input via the RX pin. 
*/

#pragma CODE_SEG NON_BANKED

#include "serComm.h"	    
#include "CANcomm.h"

#pragma CODE_SEG DEFAULT

void reset(void)  {
outpc.FuelAdj = 0;
outpc.SpkAdj = 0;
outpc.IdleAdj = 0;
outpc.SprAdj = 0;
FuelAdjl = 0;
SpkAdjl = 0;
IdleAdjl = 0;
SprAdjl = 0;
first_instr = 1;
return;
}

// ------ Unimplemented ISRs -------------------------------------------------
#pragma CODE_SEG  NON_BANKED
INTERRUPT void UnimplementedISR(void) {
   /* Unimplemented ISRs trap.*/
   // If we got here by interrupt on IRQ (after reset in 4WD), 
   // then disable the IRQ interrupt (use as general input only)
//   INTCR = 0x00;              // disable IRQ on PE1 so it won't be called again
   return;
}

#pragma CODE_SEG ROM_7000  
void waitAwhile(unsigned int delay)
// This is a 'do nothing' function that simply waits.
// A cycle value of 250 is approximately 1 second (1 tic = ~4 milliseconds).
// Cycles can run from 0 to 65535.
  {
  	wait_flag = 1; // set wait flag so interrupts know if we are in a wait state

	if (delay_count < 65535) delay_count = delay; else delay_count = 65535;  // will decrement in 0.01 second timer
  
	while (delay > 0)
  		{
  		if (!(millisec%20)) grabMS2vars();  // get ms2 outpc variables while waiting
  		} // end while (delay_count > 0) ....
 
  	wait_flag = 0;    // clear flag
   
    return;                           // otherwise return
  }

// CAN Adjustments for Fuel/Spark/Idle on MS-II
#pragma CODE_SEG NON_BANKED

#include "serCANComm\sendCANAdj.h"

#pragma CODE_SEG ROM_7000
  
void grabMS2vars(void)
{
unsigned char ix;

if (!in2ram.can_testout)
{
// Get ECU output variables.
// Do below if want entire outpc array - very slow
// Grab 8 bytes every can_var_rate (.128 ms tics)
if(can_mmsclk > inpram.can_var_rate)  
 {
 // load xmt interrupt ring buffer(can[0]) - request data
 ix = can[1].cxno_in;
 can[1].cx_msg_type[ix] = MSG_REQ; 
 // Get all data from var. block outpc in ECU)      
 can[1].cx_destvarblk[ix] = inpram.ms2varBLK; // (was 7) is outpc on MS-II
 // Put data rcvd from ECU into var block msvar in GPIO)      
 can[1].cx_myvarblk[ix] = 9;   // msvar table number for this GPIO (from tableDescriptor)
 can[1].cx_myvaroff[ix] = can_varoff;
 can[1].cx_dest[ix] = inpram.ms2canID; // (was 0) send to ECU (board 0)
 // below is offset from start of outpc in ECU
 can[1].cx_destvaroff[ix] = can_varoff;
 // calculate no bytes, and offset for next time
 can_varoff += 8;
	
 if(can_varoff < NO_MSVAR_BYTES)
	{
	can[1].cx_varbyt[ix] = 8;	 // get 8 bytes
	}
	else  
	{  // get remaining bytes                       
	can[1].cx_varbyt[ix] = NO_MSVAR_BYTES - (can_varoff - 8);
	can_varoff = 0; 
	}
 send_can(1);
 can_mmsclk = 0;
   } // end if(can_mmsclk > inpram.can_var_rate)
 }     // end if (!inpram.can_testout)
 else // if (inpram.can_testout)
 {
 // Do below if want specific, non-contiguous vars from outpc array                                                           _
 // Grab all bytes every can_var_rate (.128 ms tics)
 // Note: *** Must set same outmsg data in ECU and CAN device, or at 
 // least have the same no_bytes and offsets.
	if(can_mmsclk > (inpram.can_var_rate*2)) // double can_var_rate for outmsg  
	{
		ix = can[1].cxno_in;
		can[1].cx_msg_type[ix] = OUTMSG_REQ; 
		// Put data from ECU in this gpio block      
		// can[1].cx_destvarblk[ix] = 3;  // from tableDescriptor
		can[1].cx_destvarblk[ix] = 9;  // into msvar
			// above really myvarblk, destvarblk always outpc
		can[1].cx_dest[ix] = 0;		// send to ECU (board 0)
		// Below is offset from start of gpio block - it should
		// always be 0. If more than 1 msg, ECU will send offset
		// to count byte sent
		can[1].cx_destvaroff[ix] = 0;
		can[1].cx_outmsg_no[ix] = 0;  // 0 to < MAX_OUTMSGS
		send_can(1);
	can_mmsclk = 0;
	}
 } // end else (!inpram.can_testout)

return;
}  // end grabMS2var()
