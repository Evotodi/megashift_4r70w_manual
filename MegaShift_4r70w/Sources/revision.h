// --- Revision Number and Signature ---
const char RevNum[20] =  {    // revision no:
  // Only change for major rev and/or interface change. (The last character is added by GPIO.)
    "GPIO Template 1.101"
  // 123456789.123456789.
  // Put this in the title bar. 
},
  Signature[32] = {            // program title.
    // Change this every time you tweak a feature.  (The last character is added by GPIO.)
      "* GPIO Template 1.101 by B&G  *"
    // 123456789.123456789.123456789.12
  };