//--------------- get_adc -------------------------------------------------------
signed int get_adc(char chan1) {
        switch (chan1) {
        case 0:
            adcval = ATD0DR0;
            break;
        case 1:
            adcval = ATD0DR1;
            break;
        case 2:
            adcval = ATD0DR2;
            break;
        case 3:
            adcval = ATD0DR3;
            break;
        case 4:
            adcval = ATD0DR4;
            break;
        case 5:
            adcval = ATD0DR5;
            break;
        case 6:
            adcval = ATD0DR6;
            break;
        case 7:
            adcval = ATD0DR7;
            break;
        default:
            break;
        } // end of switch

        return (adcval);
    } // end get_adc()
