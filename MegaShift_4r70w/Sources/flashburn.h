//------- flash burner -------------------------------------------------------
/******************************************************************************
Arguments:	

      progAdr		Pointer to the start of the destination 
			          Flash location to be programmed.
      bufferPtr	Pointer to the start of the source data. 
      size      Number of WORDS to be programmed.
                  
 This function will program non-paged flash only 
******************************************************************************/
void fburner(unsigned int* progAdr, unsigned int* bufferPtr, 
                                          unsigned int no_words)
{
unsigned char sect,no_sectors;
																
  	if(flocker != 0xCC) return; // if not set, we got here unintentionally
  	// sector = 1024 bytes (512 words)
  	if(burn_flag == 1)  
  	  {
  	  no_sectors = (unsigned char)(no_words / SECTOR_WORDS);
  	  if(no_words > (no_sectors * SECTOR_WORDS))no_sectors++;
  	  for (sect = 0; sect < no_sectors; sect++)  
  	    {  	  
  	    Flash_Erase_Sector(progAdr + sect * SECTOR_WORDS);
  	    }
  	   }  // end if(burn_flag == 1) 

  	Flash_Write_Word(progAdr + burn_flag-1, *(bufferPtr + burn_flag-1));
   	
	return;
}

//*****************************************************************************
//* Function Name: Flash_Init
//* Description : Initialize Flash NVM for HCS12 by programming
//* FCLKDIV based on passed oscillator frequency, then
//* uprotect the array, and finally ensure PVIOL and
//* ACCERR are cleared by writing to them.
//*
//*****************************************************************************
void Flash_Init(unsigned long oscclk)  {
unsigned char fclk_val;
unsigned long temp;
  /* Next, initialize FCLKDIV register to ensure we can program/erase */
  temp = oscclk;
  if (oscclk >= 12000) {
    fclk_val = oscclk/8/200 - 1; /* FDIV8 set since above 12MHz clock */
    FCLKDIV = FCLKDIV | fclk_val | FDIV8;
  }
  else
  {
    fclk_val = oscclk/8/200 - 1;
    FCLKDIV = FCLKDIV | fclk_val;
  }
  FPROT = 0xFF; /* Disable all protection (only in special modes)*/
  FSTAT = FSTAT | (PVIOL|ACCERR);/* Clear any errors */
  return;
}

void Flash_Erase_Sector(unsigned int *address)  {
  FSTAT = (ACCERR | PVIOL); // clear errors
  (*address) = 0xFFFF;/* Dummy store to page to be erased */
  FCMD = ERASE;

/*********************************************************************
;* Allow final steps in a flash prog/erase command to execute out
;* of RAM while flash is out of the memory map
;* This routine can be used for flash word-program or erase commands
;*
;********************************************************************/
/* Execute the sub out of ram  */
// Note that "asm" means the following statements -- in {} here -- are in 
// assembly language
   asm {
     ldaa  #CBEIF
     jsr  RamBurnPgm
   };
   return;
}

void Flash_Write_Word(unsigned int *address, unsigned int data)  {
  FSTAT = (ACCERR | PVIOL); // clear errors
  (*address) = data;        // Store desired data to address being programmed
  FCMD = PROG;              // Store programming command in FCMD
   /* Execute the sub out of ram  */
   asm {
     ldaa  #CBEIF
     jsr  RamBurnPgm
   };  
   return;
}