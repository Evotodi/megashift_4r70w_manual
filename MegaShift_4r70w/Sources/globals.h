// --- Global Variables ----
unsigned char ICintmask[2], OCintmask[2];
int vss_timer_overflow;
volatile unsigned short * pTIC[2], * pTOC[2];
unsigned int mms, millisec, burn_flag, can_mmsclk, can_varoff;
unsigned char burn_msec_flag;
unsigned int TC_ovflow, TC_ov_ix;
unsigned long lmms,
ltch_lmms,
rcv_timeout, adc_lmms;
unsigned char flocker;
unsigned char uctmp;
char jx, kx, mx;
unsigned char next_adc, first_adc,
txmode, tble_idx, burn_idx,
reinit_flag;
unsigned long ic1_period[21]; // VSS period array - take the average
unsigned int ic2_period[21]; // ISS period array - take the average
unsigned char ic1_count, ic1_recount;
unsigned char ic2_count;
signed int adcval;
char kill_ser, vfy_flg;
unsigned long short_period_iss; // calculated mask times for VSS timer
unsigned long long_period_iss; // calculated mask times for VSS timer
unsigned short tcsav1; // Last VSS time
unsigned short tcsav2; // Last ISS/non-CAN tach time
unsigned int delay_count;
unsigned int delay;
unsigned char wait_flag;
unsigned int temp_uint_calc; //JRD: Used for temp calcs
unsigned char temp_uchar_calc; //JRD: Used for temp calcs
unsigned int mpls_range; //JRD: what position the mpls is in. 1 2 D N R P = 1 2 3 4 5 6
unsigned char f_brake, t_brake, nx_en, nx_stg1, nx_stg2, f_brake_pc_use, t_brake_pc_use, tcc_out_rev;
unsigned char gear1_tcc_en, gear2_tcc_en, gear3_tcc_en;
unsigned char gear123_tcc_tps_en;

long ic_per_acc,ic_dc_acc;    // averaging subtotals
long VSSdivide;               // Calculated from number of teeth on VSS sensor, etc. 
unsigned long ic_period[20],ic_duty[20]; // VSS period and duty cycle 
                                          // array - take the average
unsigned char ic_count;

char SolAState; // SolA output state indicator
char SolBState; // SolB output state indicator
char NxStg1State; //Nitrous Stage 1 output state indicator
char NxStg2State; //Nitrous Stage 2 output state indicator
char NxEnState; //Nitrous Enable output state indicator
char TBrakeState; //TransBrake output state indicator
char TCCState; //TCC output state indicator

unsigned int trantmp; //Transmission Temperature temp
unsigned int GearRatioTmp;
unsigned long ConverterTmp;
unsigned long SpeedTmp; 
unsigned char tccLockTmp, tccGearLockTmp;

