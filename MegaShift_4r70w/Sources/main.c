/*
 * HERE IS WHAT IS GOING ON!
 * working speedo at line 719
*/

/*
 * TODO
 * Create code for Tcc lockup  (var gear2_tc_rpmA)    ????????
 
 
*/


// --- Defines, Includes, ISR Definitions and Interrupt Table ----
#include "hcs12def.h" /* common defines and macros */ 
#include "flash.h"
#include <string.h>

#define INTERRUPT interrupt
#define FAR_TEXT1_ATTR
#define TEXT1_ATTR
#define EEPROM_ATTR
#define POST_INTERRUPT
#define ENABLE_INTERRUPTS asm cli;
#define DISABLE_INTERRUPTS asm sei;
#define NEAR near
#define VECT_ATTR @0xFF80
         
#pragma CODE_SEG NON_BANKED

extern void near _Startup(void);

/**** Interrupt Service Routines ***********************************/

INTERRUPT void UnimplementedISR(void);
INTERRUPT void VSS_timer(void);
INTERRUPT void ISS_timer(void);
INTERRUPT void Timer_Overflow_ISR(void);
INTERRUPT void Timer_Clock_ISR(void);
INTERRUPT void CanTxIsr(void);
INTERRUPT void CanRxIsr(void);

#include "serCAN0.h"
#include "serCAN1.h"
#include "serCAN2.h"

void sendCANAdj(void);

typedef void( * NEAR tIsrFunc)(void);
const tIsrFunc _vect[] VECT_ATTR = {
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    CanTxIsr,
    CanRxIsr,
    CanRxIsr,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    ISR_SCI_Comm,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    Timer_Overflow_ISR,
    UnimplementedISR,
    UnimplementedISR,
    ISS_timer,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    VSS_timer,
    Timer_Clock_ISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    UnimplementedISR,
    _Startup
};

// ******************************** FUNCTION PROTOYPES **********************************   

#pragma CODE_SEG NON_BANKED

void switch_page(unsigned char sub_no);

#pragma CODE_SEG ROM_7000

int intrp_1ditable(int x, unsigned char n, int * x_table, int * z_table);
int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny, unsigned int * x_table, int * y_table, unsigned char * z_table);
long mapval(long x, long in_min, long in_max, long out_min, long out_max);

void grabMS2vars(void);

#pragma CODE_SEG ROM_7000

void waitAwhile(unsigned int cycles);

#pragma CODE_SEG DEFAULT // end CODE_SEG NON_BANKED for interrupts

// --- Table Includes and Table Size Defines ---
#include "charrev.inc"

// --- Coolant Factor Table ---
#include "cltfactor.inc"    // cltfactor.inc defines a 1024 x 1 lookup table.
                            // we set the ADC level to be ten bits later: 
                            // 2�� = 0000 0100 0000 0000 = 1024 = 0x0400 
                            // Each value corresponds to one ADC increment, giving the temperature*10 in Fahrenheit 
                            // at that ADC value. (if Metric_Units is set, the value is converted to Celsius immediately)
                            // The ADC start at 0 Volts for ADC count value of 0, and run to 5.00 Volts at ADC 1024
                            // (so each ADC represent a difference of 5.00/1024 or 0.00488 Volts/count)

#define NO_TBLES 16
#define NO_PC 6
#define NO_TC 6
#define NO_TCHYS 2
#define NPORT 7 // Number of spare ports
#define SER_TOUT 5 // Serial time out in seconds (if corrupt data received)
#define NO_MSVAR_BYTES 112 // Number of variable bytes in the received data from MS-II

// CAN and Serial defined values
#define MSG_CMD 0
#define MSG_REQ 1
#define MSG_RSP 2
#define MSG_XSUB 3
#define MSG_BURN 4
#define NO_VAR_BLKS 16
#define NO_CANMSG 10
#define MAX_CANBOARDS 16

// Error status words: 
#define XMT_ERR 0x0101
#define CLR_XMT_ERR 0xFFFE
#define XMT_TOUT 0x0202
#define CLR_XMT_TOUT 0xFFFD
#define RCV_ERR 0x0404
#define CLR_RCV_ERR 0xFFFB
#define SYS_ERR 0x0808

#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

#include "ports_pins.h"

// Inpram (INPut RAM definitions)
#include "inpram.h"

#pragma ROM_VAR DEFAULT

#pragma ROM_VAR OVF_ROM

#include "TC_ovfla.inc"

// --- Revision Number and Signature ---
#include "revision.h"

// CAN clocks for auto transmitting data between ECU and GPIO
unsigned short xrate_clks[MAX_OUTMSGS]; // 0.128 tics

// CAN variables
#include "serCANComm\serCAN3.h"

//***** RAM copy of inputs *********************************************
inputs1 inpram;
inputs2 in2ram;

// Outmsg structure
#include "outmsg.h"

// --- Global Variables ----
#include "globals.h"

// CAN variables
#include "can_vars.h"

// pointers for spare port pins
volatile unsigned char * pPTMpin[8], * pPTTpin[8];

// allocate space in ram for flash burner core
volatile unsigned char RamBurnPgm[36];

// ****** Set Up OUTPC. Structure for Serial Communications with MegaTune *******
#include "outpc.h"

// Create a buffer variable to hold the MS-II output variables 
unsigned char msvar[NO_MSVAR_BYTES];

#pragma ROM_VAR OVF_ROM

// Define a structure for the table descriptor that gives the address in RAM, the address 
typedef struct {
    unsigned int * addrRam;
    unsigned int * addrFlash;
    unsigned int n_bytes;
    unsigned int fpage;
}
tableDescriptor;

// Use the tableDescriptor structure we just defined to create an array of table descriptions 
// sized NO_TABLES. They will hold the descriptions of the tables (and the indexes are used 
// in MegaTune to 'locate' the table with command like a06, which refers to the 6th table). 
// These are constant. That is, the descriptions - memory locations - of the tables are constant, 
// even if the elements of the tables change values.

const tableDescriptor tables[NO_TBLES] =  {
    //  RAM copy                    FLASH copy                          Table Size 
    {  NULL,                   (unsigned int *)cltfactor_table,     sizeof(cltfactor_table),    0x3C }, // table 0
    {  NULL,                    NULL,                               0,                          0x3C }, // table 1
    {  NULL,                    NULL,                               0,                          0x3C }, // table 2
    {  NULL,                   (unsigned int *)charrev_table,       sizeof(charrev_table),      0x3C }, // table 3
    { (unsigned int *)&inpram, (unsigned int *)&in1flash,           sizeof(inputs1),            0x3C }, // table 4
    { (unsigned int *)&in2ram, (unsigned int *)&in2flash,           sizeof(inputs2),            0x3C }, // table 5
    { (unsigned int *)&txbuf,   NULL,                               sizeof(txbuf),              0x3C }, // table 6
    { (unsigned int *)&outpc,   NULL,                               sizeof(outpc),              0x3C }, // table 7
    {  NULL,                   (unsigned int *)&outmsg,             sizeof(outmsg),             0x3C }, // table 8
    { (unsigned int *)msvar,    NULL,                               sizeof(msvar),              0x3C }, // table 9
    {  NULL,                    NULL,                               0,                          0x3C }, // table 10
    {  NULL,                    NULL,                               0,                          0x3C }, // table 11
    {  NULL,                    NULL,                               0,                          0x3C }, // table 12
    {  NULL,                    NULL,                               0,                          0x3C }, // table 13
    {  NULL,                   (unsigned int *)&Signature,          sizeof(Signature),          0x3C }, // table 14
    {  NULL,                   (unsigned int *)&RevNum,             sizeof(RevNum),             0x3C }  // table 15
};

// In the above, the "&" before a variable name means "at the address of, 
// so &inpram is the address of the inpram table

#pragma ROM_VAR DEFAULT // Codewarrior generates error message here, is seemingly OK though

// Calculate and define values for working with tables based on the tables array we just created.
#define tableInit(iTable)(void) memcpy(tables[iTable].addrRam, tables[iTable].addrFlash, tables[iTable].n_bytes)
#define tableByteRam(iTable, iByte)((unsigned char * ) tables[iTable].addrRam + iByte)
#define tableWordRam(iTable, iWord)(tables[iTable].addrRam + iWord)
#define tableByteFlash(iTable, iByte)((unsigned char * ) tables[iTable].addrFlash + iByte)
#define tableWordFlash(iTable, iWord)(tables[iTable].addrFlash + iWord)
#define tableBytes(iTable)(tables[iTable].n_bytes)
#define tableWords(iTable)((tables[iTable].n_bytes + 1) / 2)
#define tableFPage(iTable)((unsigned char) tables[iTable].fpage)

#pragma CODE_SEG MAIN_ROM

// Prototypes - Note: ISRs (interrupt service routines) are prototyped above.
// These are the mandatory function prototypes.

void main(void); // the main function, i.e., where the program starts

#pragma CODE_SEG DEFAULT
void reset(void);
void VSS_reset(void);
signed int get_adc(char chan1); // this is the function that reads the ADC channels
void fburner(unsigned int * progAdr, unsigned int * bufferPtr, unsigned int no_words);
void Flash_Init(unsigned long oscclk);
void Flash_Erase_Sector(unsigned int * address);
void Flash_Write_Word(unsigned int * address, unsigned int data);
void SolAOutput(unsigned char);
void SolBOutput(unsigned char);
void StatusLedOutput(unsigned char);
void StatusLedOutputToggle(void);
void TBrakeOutput(unsigned char);
void NxStage1Output(unsigned char);
void NxStage2Output(unsigned char);
char FBrakeInput(void);
char TBrakeInput(void);
char NxEnInput(void);
char bit_read(unsigned char bit, unsigned char byte);
void bit_reset(unsigned char bit, unsigned char *byte);
void bit_set(unsigned char bit, unsigned char *byte); 


#pragma CODE_SEG DEFAULT

extern void reboot(void);
extern void monitor(void);
extern void SpSub(void);
extern void NoOp(void);

// ---------------------------------------------------------------------------------------
// ---------------------------------- Function MAIN --------------------------------------
// ---------------------------------------------------------------------------------------

#pragma CODE_SEG MAIN_ROM // Put following code in MAIN_ROM == ppage 0x3C

void main(void) {

        int ix;
        long tmpx1;
        long ltmp;
        unsigned long ultmp, ultmp2;
        unsigned int pc_percent_tmp;
        unsigned long tire_circ;

        PPAGE = 0x3C; // PPAGE register is defined in hcs12def.h 
        //  Initialize PLL (Phase Locked Loop) - reset default 
        PLLCTL &= 0xBF; // Turn off PLL so can change freq
        SYNR = 0x02; // set PLL/ Bus freq to 48/ 24 MHz
        REFDV = 0x00;
        PLLCTL |= 0x40; // Turn on PLL. A |= B is the same as A = A | B 

        // wait for PLL lock
        while (!(CRGFLG & 0x08));
        CLKSEL = 0x80; // select PLL as clock

        // wait for clock transition to finish
        for (ix = 0; ix < 60; ix++);

        // Copy Flash to RAM
        Flash_Init(8000);
        ix = in1padding[0];
        ix = in2padding[0];

        if ((int) RamBurnPgm & 0x0001) {
            (void) memcpy((void * ) RamBurnPgm, NoOp, 1);
            (void) memcpy((void * ) & RamBurnPgm[1], SpSub, 32);
        } else {
            (void) memcpy((void * ) RamBurnPgm, SpSub, 32);
        }
        // load all user inputs from Flash to RAM
        tableInit(4);
        tableInit(5);

        // ** MShift I/O Port Data Direction Register Settings ***
        // inputs (0) or outputs (1)
        DDRA |= 0x01;   // 00000001
        DDRB |= 0x10;   // 00010000
        DDRE |= 0x10;   // 00010000
        DDRM |= 0x3C;   // 00111100
        DDRT |= 0x1E;   // 00011110

        //  Set pointers to real port addresses
        for (ix = 0; ix < 8; ix++) {
            pPTMpin[ix] = pPTM; // port M
            pPTTpin[ix] = pPTT; // port T
        } // end for (ix = 0...

        // --- Set Up Timer Ports ---
        TIOS = 0x1E; //Timer Input Capture/Output Compare Select 0x1e = 00011110; 1->Input Capture, 0->Output Compare 
        TSCR1 = 0x80; //Timer System Control Register1 0x80 = TEN; Timer active
        TSCR2 = 0x07; //Timer System Control Register2 0x07 = 00000111 = PR0-2 = Bus Clock/128
        // Set the VSS Input Capture Edge
        TCTL4 = 0x02; //Timer Control Register4 0x02 = 00000010; Capture on falling edges only
        TCTL3 = 0x04; //Timer Control Register3 0x04 = 00000100; Capture on rising edges only
        TFLG1 |= 0x21; //Main Timer Interrupt Flag1 0x21 = 00100001; Clears flags at the 1s
        TIE |= 0x21; //Timer Interrupt Enable Register 0x21 = 00100001; Enable interrups on 1s

        // turn off output pins
        SolBOutput(0);
        SolAOutput(0);
        StatusLedOutput(0);
        NxStage1Output(0);
        NxStage2Output(0);
        

        // Set up the data direction ports and pull-ups
        DDRE = 0x10; // Port E all inputs, except PE4 which is output (solA) // 00010000
        PUCR = 0x00; // disable all pull-ups, will use external pull-ups where needed
        INTCR = 0x00; // disable interrupt on PE1  

        DDRP = 0x00; // port P all inputs
        PERP = 0xFF; // enable pullup resistance for port P;    0xFF = 255 = 11111111

        DDRJ &= 0x3F; // port J pins 0 to 5 outputs,             0x3F =  63 = 00111111
        PERJ |= 0xC0; // enable pullup resistance for port J6,7; 0xC0 = 192 = 11000000 

        DDRS &= 0xF3; // port S all outputs, except 2,3;         0xF3 = 243 = 11110011
        PERS |= 0x0C; // enable pullup resistance for port S2,3; 0x0C =  12 = 00001100

        reinit_flag = 0;

        // Set Up CRG RTI Interrupt for .128 ms clock. CRG from 8MHz oscillator.
        mms = 0; // 0.128 ms tics
        millisec = 0; // 1.024 ms clock (8 tics) for adcs
        lmms = 0;
        cansendclk = 7812;
        ltch_lmms = 0;
        outpc.seconds = 0; // (1.0035) seconds
        burn_flag = 0;
        ic1_count = 1;
        ic1_recount = 1;
        ic2_count = 1;

        RTICTL = 0x10;  //CRG RTI Control Register 0x10 = RTR4 = Real-Time Interrupt Prescale Rate Select Bits
        CRGINT |= 0x80; //CRG Interrupt Enable Register 0x80 = RTIE = Real-Time Interrupt Enable Bit; 1->disabled
        CRGFLG = 0x80;  //CRG Flags Register 0x80 = RTIF = Real-Time Interrupt Flag; 1->occurred

        // ------ Set up SCI Serial Communications Interface (RS232) ----------------
        // Set the baud rate
        SCI0BDL = (unsigned char)(1500000 / inpram.baud);
        ltmp = (150000000 / inpram.baud) - ((long) SCI0BDL * 100);
        if (ltmp > 50) SCI0BDL++;

        SCI0CR1 = 0x00;
        SCI0CR2 = 0x24;
        txcnt = 0;
        rxcnt = 0;
        txmode = 0;
        txgoal = 0;
        kill_ser = 0;
        vfy_flg = 0;
        rcv_timeout = 0xFFFFFFFF;

        //******************* Initialize IC_period array for VSS/ISS ****************************/
        for (ix = 1; ix < 21; ix++) {
            ic1_period[ix] = 0x0FFFF; // set the VSS period high (speed low)  
            ic2_period[ix] = 0x0FFFF; // set the ISS period high (speed low)
        }

        //******************* Set Up PWM on Timer Pins ******************************************
        MODRR = 0x1E; // Make Port T pins 1, 2, 3 & 4 be PWM (0x1E = 00011110), 
        PWME = 0x00; // disable PWMs initially  
        PWMPOL = 0x1E; // polarity = 1, => go hi when start
        PWMCLK = 0x1E; // select scaled clocks SB, SA
        PWMPRCLK = 0x22; // prescale A,B clocks = bus/4 = 24/6 = 6 MHz
        PWMSCLA = 0xFF; // PWM clk = SA clk/(2*SCLA) = 6MHz/510 = 0.085 msec clk
        PWMSCLB = 0x80; // PWM clk = SB clk/(2*SCLB) =  6MHz/256 = 0.0427 msec clk
        PWMCAE = 0x00; // standard left align pulse:
        TSCR2 |= 0x80; // enable timer overflow interrupt (TOI)
        pTIC[0] = pTC0;
        ICintmask[0] = 0x01;
        pTOC[0] = pTC5;
        TCTL2 |= 0x88; //Timer Control Register2 0x88 
        TCTL1 |= 0x08; //Timer Control Register1 0x08

        // set PWM period1 (usec) for 3/2 solenoid 
        PWMPER1 = 255;
        // set PWM period2 for pressure control solenoid                                                       
        PWMPER2 = 255;
        // set PWM period3 for torque converter clutch                                   
        PWMPER3 = 255;
        // set PWM period4 (usec) for speedo output
        PWMPER4 = 255; // 120 usec default for speedo output - short so we can get high frequencies 

        // Initialize PWM Duty Cycles
        PWMDTY1 = 0; // set Sol32 PWM duty to 0% (90% in 2, 3, 4)
        PcPwm = 0; // set PC PWM duty to 0%
        TccPwm = 0; // turn off TCC
        PWMDTY4 = 0; // speedo output off
        PWMCNT1 = 0x00; // clear counter (reset PWM)

        pTIC[1] = pTC0;
        ICintmask[1] = 0x01;

        // --- Set Up ADC Channels ---
        ATD0DIEN = 0xC0; // 1=digital input, 0=ADC
        next_adc = 0; // specifies next ADC channel to be read
        ATD0CTL2 = 0x40; // leave interrupt disabled, set fast flag clear
        ATD0CTL3 = 0x00; // do 8 conversions/ sequence
        ATD0CTL4 = 0x67; // 10-bit resolution, 16 tic conversion (max accuracy),
        ATD0CTL5 = 0xB0;
        ATD0CTL2 |= 0x80; // turn on ADC0

        // wait for ADC engine charge-up or P/S ramp-up
        for (ix = 0; ix < 160; ix++) {
            while (!(ATD0STAT0 >> 7));
            ATD0STAT0 = 0x80;
        } // end for (ix = 0...

        first_adc = 0;
        adc_lmms = lmms;

        /* Set Up variable block addresses to be used for CAN communications */
        canvar_blkptr[0] = (char * ) & inpram;
        canvar_blkptr[1] = (char * ) & outpc;
        canvar_blkptr[2] = (char * ) msvar;

        for (ix = 3; ix < NO_VAR_BLKS; ix++) {
            canvar_blkptr[ix] = 0;
        } // end for(ix =3 ...

        // Initialize outpc. variables
        flocker = 0;

        /* Initialize CAN comms */
        can_reset = 0;
        can_id = inpram.mycan_id;
        CanInit();

        // enable global interrupts
        ENABLE_INTERRUPTS

        // Restart PWM counters
        PWMCNT1 = 0x00;
        PWMCNT2 = 0x00;
        PWMCNT3 = 0x00;
        PWMCNT4 = 0x00;
        PWME |= 0x1E; // enable PWM 1,2,3 & 4 (0x1E = 00011110)
        
        tire_circ = ((long)inpram.tire_diam*314)/100;
        
        tcc_out_rev = FALSE;
        if(inpram.opts1 & 0x04){
          tcc_out_rev = TRUE;
        }
        
        f_brake_pc_use = FALSE;
        if(inpram.opts1 & 0x01){
          f_brake_pc_use = TRUE;
        }
        
        t_brake_pc_use = FALSE;
        if(inpram.opts1 & 0x02){
          t_brake_pc_use = TRUE;
        }

        gear1_tcc_en = FALSE;
        if(inpram.gear123_tcc_tps & 0x01){
          gear1_tcc_en = TRUE;
        }
        
        gear2_tcc_en = FALSE;
        if(inpram.gear123_tcc_tps & 0x02){
          gear2_tcc_en = TRUE;
        }
        
        gear3_tcc_en = FALSE;
        if(inpram.gear123_tcc_tps & 0x04){
          gear3_tcc_en = TRUE;
        }
        
        gear123_tcc_tps_en = FALSE;
        if(inpram.gear123_tcc_tps & 0x08){
          gear123_tcc_tps_en = TRUE;
        }

        
        StatusLedOutput(1); // Turn on status led
        // Set up COP time out for main loop
        MAIN_LOOP:
            for (;;) { // start the main loop

                // Increment main loop counter for datalog 
                outpc.loop_count++;

                // Error codes
                outpc.error = 0x01;
                
                //Read brake switch
                //Brake applied grounds input wire
                if(FBrakeInput()){
                  outpc.switches &= ~0x01;
                  f_brake = FALSE;
                }else{
                  outpc.switches |= 0x01;
                  f_brake = TRUE;
                }
                
                //Read Trans Brake switch
                //On is connected to 12v
                if(TBrakeInput() && mpls_range == 1){
                  outpc.switches |= 0x02;
                  t_brake = TRUE;
                  TBrakeState = TRUE;
                }else{
                  outpc.switches &= ~0x02;
                  t_brake = FALSE;
                  TBrakeState = FALSE;
                }
                
                //Read nx enable switch
                //On is connected to 12v
                if(NxEnInput() && outpc.engine_rpm <= inpram.maxNxEnRpm){
                  outpc.switches |= 0x04;
                  nx_en = TRUE;
                  NxEnState = TRUE;
                  bit_reset(1, &outpc.RemotePort3); //low is enable
                }
                
                if(NxEnInput() == FALSE){
                  outpc.switches &= ~0x04;
                  nx_en = FALSE;
                  NxEnState = FALSE;
                  bit_set(1, &outpc.RemotePort3); //high is disable
                }
                
                //Read the RemotePort1 bits
                if(bit_read(0, outpc.RemotePort1)) {
                    nx_stg1 = TRUE;
                }else{
                    nx_stg1 = FALSE;
                }
                
                if(bit_read(1, outpc.RemotePort1)) {
                    nx_stg2 = TRUE;
                }else{
                    nx_stg2 = FALSE;
                }
                
                //Do the nitrous stage 1 & 2 outputs
                if(nx_en && outpc.engine_rpm >= inpram.maxNxEnRpm) {
                    if(nx_stg1) {
                        NxStage1Output(1);
                    }else{
                        NxStage1Output(0);
                    }
                    
                    if(nx_stg2) {
                        NxStage2Output(1);
                    }else{
                        NxStage2Output(0);
                    }
                }else{
                    NxStage1Output(0);
                    NxStage2Output(0);
                }
                
                // common conditions for tcc
                tccGearLockTmp = FALSE;
                if(gear1_tcc_en || gear2_tcc_en || gear3_tcc_en){
                  tccGearLockTmp = TRUE;
                }
                
                tccLockTmp = FALSE;
                if(outpc.speedo >= inpram.minTCCspeed && f_brake == FALSE && tccGearLockTmp){
                  tccLockTmp = TRUE;
                }
                
                //outpc.tcc_stat = tccLockTmp; // Testing
                                
                // Read MPLS and set sols
                temp_uint_calc = get_adc(MplsAdc);
                
                if (temp_uint_calc <= 128) { // 1st Gear
                    mpls_range = 1;
                    SolAOutput(1); // turn ON sol A
                    SolBOutput(0); // turn vb4/pm2 output OFF sol B  
                    outpc.gearX = 0x01;
                    GearRatioTmp = inpram.gear1_ratio;
                    //Set Transmission Pressure
                    if(f_brake && f_brake_pc_use){
                        pc_percent_tmp = inpram.f_brake_pc;
                    }else if(t_brake && t_brake_pc_use){
                        pc_percent_tmp = inpram.t_brake_pc;
                    }else{
                        pc_percent_tmp = intrp_1ditable((int) outpc.engine_rpm, (char) NO_PC, (int * ) & inpram.gear1_pc_rpm[0], (int * ) & inpram.gear1_pc[0]);  
                    } 
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                    
                    if(tccLockTmp && gear1_tcc_en){
                      if(outpc.engine_rpm >= inpram.gear1_tcc_rpm){
                        TCCState = TRUE;
                      }
                      if(outpc.engine_rpm <= (inpram.gear1_tcc_rpm - inpram.gear1_tcc_hys)){
                        TCCState = FALSE;
                      }
                      if(outpc.tps <= (inpram.gear123_tcc_tps_val * 10) && gear123_tcc_tps_en){
                        TCCState = FALSE;
                      }
                    }else{
                      TCCState = FALSE;
                    }
                    
                } else if (temp_uint_calc >= 129 && temp_uint_calc <= 233) { // 2nd Gear
                    mpls_range = 2;
                    SolAOutput(0); // turn vb3/pe4 output OFF sol A
                    SolBOutput(0); // turn vb4/pm2 output OFF sol B
                    outpc.gearX = 0x02;
                    GearRatioTmp = inpram.gear2_ratio;
                    //Set Transmission Pressure
                    if(f_brake && f_brake_pc_use){
                        pc_percent_tmp = inpram.f_brake_pc;
                    }else{
                        pc_percent_tmp = intrp_1ditable((int) outpc.engine_rpm, (char) NO_PC, (int * ) & inpram.gear2_pc_rpm[0], (int * ) & inpram.gear2_pc[0]);  
                    }                    
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                    
                    if(tccLockTmp && gear2_tcc_en){
                      if(outpc.engine_rpm >= inpram.gear2_tcc_rpm){
                        TCCState = TRUE;
                      }
                      if(outpc.engine_rpm <= (inpram.gear2_tcc_rpm - inpram.gear2_tcc_hys)){
                        TCCState = FALSE;
                      }
                      if(outpc.tps <= (inpram.gear123_tcc_tps_val * 10) && gear123_tcc_tps_en){
                        TCCState = FALSE;
                      }
                    }else{
                      TCCState = FALSE;
                    }

                } else if (temp_uint_calc >= 234 && temp_uint_calc <= 363) { //3rd Gear or Drive
                    mpls_range = 3;
                    SolAOutput(0); // turn vb3/pe4 output OFF sol A
                    SolBOutput(1); // turn vb4/pm2 output ON sol B
                    outpc.gearX = 0x04;
                    GearRatioTmp = inpram.gear3_ratio;
                    //Set Transmission Pressure
                    if(f_brake && f_brake_pc_use){
                        pc_percent_tmp = inpram.f_brake_pc;
                    }else{
                        pc_percent_tmp = intrp_1ditable((int) outpc.engine_rpm, (char) NO_PC, (int * ) & inpram.gear3_pc_rpm[0], (int * ) & inpram.gear3_pc[0]);  
                    }    
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                                    
                    if(tccLockTmp && gear3_tcc_en){
                      if(outpc.engine_rpm >= inpram.gear3_tcc_rpm){
                        TCCState = TRUE;
                      }
                      if(outpc.engine_rpm <= (inpram.gear3_tcc_rpm - inpram.gear3_tcc_hys)){
                        TCCState = FALSE;
                      }
                      if(outpc.tps <= (inpram.gear123_tcc_tps_val * 10) && gear123_tcc_tps_en){
                        TCCState = FALSE;
                      }
                    }else{
                      TCCState = FALSE;
                    }
                
                } else if (temp_uint_calc >= 364 && temp_uint_calc <= 526) { //Neutral
                    mpls_range = 4;
                    SolAOutput(1); // turn vb3/pe4 output ON sol A
                    SolBOutput(0); // turn vb4/pm2 output OFF sol B
                    outpc.gearX = 0x10;
                    GearRatioTmp = inpram.gear3_ratio;
                    //Set Transmission Pressure
                    pc_percent_tmp = inpram.gearN_pc;
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                    TCCState = FALSE;

                } else if (temp_uint_calc >= 527 && temp_uint_calc <= 708) { //Reverse
                    mpls_range = 5;
                    SolAOutput(1); // turn vb3/pe4 output ON sol A
                    SolBOutput(0); // turn vb4/pm2 output OFF sol B
                    outpc.gearX = 0x20;
                    GearRatioTmp = inpram.gearR_ratio;
                    //Set Transmission Pressure
                    pc_percent_tmp = inpram.gearR_pc;
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                    TCCState = FALSE;

                } else if (temp_uint_calc >= 709) { //Park
                    mpls_range = 6;
                    SolAOutput(1); // turn vb3/pe4 output ON sol A
                    SolBOutput(0); // turn vb4/pm2 output OFF sol B
                    outpc.gearX = 0x40;
                    GearRatioTmp = inpram.gear3_ratio;                    
                    //Set Transmission Pressure
                    pc_percent_tmp = inpram.gearP_pc;
                    temp_uchar_calc = (char)mapval((long)pc_percent_tmp, 0, 100, 0, 255);
                    PcPwm = (char)charrev_table[(int)temp_uchar_calc];
                    TCCState = FALSE;

                }
                
                // VR1 - timer input
                // read in interrupt, here will will get the average
                tmpx1 = 0;
                for (ix=1;ix<21;ix++) {
                  tmpx1 = tmpx1 +  ic1_period[ix];  // total the last twenty values
                }
                
                outpc.vss = (((tmpx1/20)/2)*10000)/inpram.vss_correction;
                outpc.os_rpm = ((((100000/outpc.vss)*100)  /inpram.vss_num_teeth)*60)/100; // correct and average period1, then convert to freq, divivde by vss teeth, multiply by 60 seconds
                ConverterTmp = outpc.engine_rpm;
                ConverterTmp = (((((ConverterTmp*1000)/GearRatioTmp)*1000)/outpc.os_rpm)-1000);
                
                //////DEBUG///////
                outpc.dbug = tccLockTmp;
                //////DEBUG///////
                
                if (ConverterTmp < 1000){
                  outpc.converter_slip = (unsigned int)ConverterTmp;  
                }else{
                  outpc.converter_slip = 1000;
                }
                
                SpeedTmp = (((((outpc.os_rpm*1000)/inpram.axle_ratio)*tire_circ)/100)*60)/63360  ;
                if ((unsigned int) SpeedTmp <= 4){
                  outpc.speedo = 0;  
                }else{
                  outpc.speedo = (unsigned int)SpeedTmp;
                }
                
                if(TCCState){
                  if(tcc_out_rev){
                    TccPwm = 0;  
                  }else{
                    TccPwm = 255;
                  }
                }else{
                  if(tcc_out_rev){
                    TccPwm = 255;  
                  }else{
                    TccPwm = 0;
                  }
                }
                
                
                
                
                //Update outpc vals
                outpc.mpls = mpls_range;
                outpc.pc_percent = pc_percent_tmp;
                outpc.tcc_stat = TCCState;
                
                

       

                DISABLE_INTERRUPTS
                ultmp = lmms;
                ENABLE_INTERRUPTS

                //***************************************************************************
                //**
                //** Check whether to burn flash
                //**
                //**************************************************************************

                BURN_FLASH:
                    // Burn Flash if flag set
                    if ((outpc.loop_count % 50) == 0) // slow down the flash burns to every 50 main loops (~50/8000 = ~6 milliseconds)
                    {

                        if (burn_flag >= 1) {
                            fburner(tableWordFlash(burn_idx, 0), tableWordRam(burn_idx, 0), tableWords(burn_idx));
                            if (burn_flag >= tableWords(burn_idx)) {
                                burn_flag = 0;
                                flocker = 0;
                            } else
                                burn_flag++;
                        }
                    }

                    //***************************************************************************
                    //**
                    //** Check for reinit command
                    //**
                    //***************************************************************************
                if (reinit_flag) {
                    reinit_flag = 0;

                    reset();

                    SCI0BDL = (unsigned char)(1500000 / inpram.baud);
                    ltmp = (150000000 / inpram.baud) - ((long) SCI0BDL * 100);
                    if (ltmp > 50) SCI0BDL++;
                    can_id = inpram.mycan_id;
                    can_reset = 1;
                } // End if(reinit_flag)

                //***************************************************************************
                //**
                //**  Check for serial, CAN receiver timeout
                //**
                //***************************************************************************

                DISABLE_INTERRUPTS
                ultmp = lmms;
                ultmp2 = rcv_timeout;
                ENABLE_INTERRUPTS
                if (ultmp > ultmp2) {
                    txmode = 0;
                    rcv_timeout = 0xFFFFFFFF;
                } // end if (ultmp > ultmp2)

                DISABLE_INTERRUPTS
                ultmp2 = ltch_CAN;
                ENABLE_INTERRUPTS

                if (ultmp > ultmp2) {
                    getCANdat = 0;
                    ltch_CAN = 0xFFFFFFFF;
                } //end if (ultmp > ultmp2)

                if (kill_ser) {
                    if (outpc.seconds > kill_ser_t) {
                        kill_ser = 0;
                        SCI0CR2 |= 0x24;
                    } // end if (outpc.seconds > ...
                } // end if (kill_ser)

                //***************************************************************************
                //**
                //**  Check for CAN reset
                //**
                //***************************************************************************
                if (can_reset) {
                    /* Re-initialize CAN comms */
                    CanInit();
                    can_reset = 0;
                } // end if (can_reset) ...

            } // end for(;;) main loop

    } // end main() function

/** =========== FUNCTIONS ===================================================== **/

#pragma CODE_SEG DEFAULT

# include "flashburn.h"

//--------------- get_adc -------------------------------------------------------
#include "get_adc.h"

#pragma CODE_SEG DEFAULT

# pragma CODE_SEG NON_BANKED

void switch_page(unsigned char sub_no) {
    switch (sub_no) {

    case 0:
        PPAGE = 0x3D;
        PPAGE = 0x3C;
        break;

    case 1:
        PPAGE = 0x3C;
        PPAGE = 0x3D;
        break;

    case 2:
        PPAGE = 0x3D;
        PPAGE = 0x3C;
        break;

    case 3:
        PPAGE = 0x3D;
        PPAGE = 0x3C;
        break;

    default:
        break;
    }

    return;
}

#pragma CODE_SEG ROM_7000

// --------------- intrp_1ditable -------------------------------------------------------
#include "intrp_1ditable.h"

// --------------- intrp_2dctable -------------------------------------------------------
#include "intrp_2dctable.h"

// --------------- mapval -------------------------------------------------------
#include "map_val.h"

#pragma CODE_SEG NON_BANKED // Interrupts must be in non-banked memory, 
// --------------- VSS Timer ----------------------------------------
// Time the period on the VSS input, set in array ic_period[]

INTERRUPT void VSS_timer(void) {
    unsigned long period1;
    TFLG1 = 0x01; // write a one (00000001) to TFLG to clear flag for timer channel 0  
    period1 = (TC0 - tcsav1); // tcsav1 is a global variable - it is the time of the last interrupt

    ic1_period[ic1_count] = period1; // 5.333 �sec ticks
    tcsav1 = TC0; // save time of new trigger edge

    ic1_count++; // sample counter for last 20 input capture periods
    if (ic1_count > 20) ic1_count = 1;
    // reset interrupt
    TFLG1 = 0x01; // write a one (00000001) to TFLG to clear flag for timer channel 0 

    return; 
        
} // end VSS_timer()

// --------------- ISS/non-CAN Tach Timer ----------------------------------------
// Time the period on the VSS input, set in array ic_period[]

INTERRUPT void ISS_timer(void) {
    unsigned int period2;

    // set interrupt received flag
    // ISS_rcvd++;     // will clear flag (set to zero) in 0.010 second section
    // reset interrupt
    TFLG1 |= 0x20; // write a one to TFLG bit 5 (00100000) to clear flag for timer channel 0     
    period2 = (TC5 - tcsav2); // tcsav2 is a global variable - it is the time of the last interrupt

    // reject if outside mask range of current average
    // ic2_per_avg is ~100 times the period
    // We calculate long_period and short_period in 0.010 section of timer

    if ((period2 < long_period_iss) && // if the period is less than 
        (period2 > short_period_iss)) // AND the period is greater than                
    {
        //  calculate last period, us
        ic2_period[ic2_count] = period2; // 5.333 �sec ticks
        tcsav2 = TC5; // save time of new trigger edge
    } // end if (utmp <...

    ic2_count++; // sample counter for last 20 input capture periods
    if (ic2_count > 20) ic2_count = 1;
    return;
}

#pragma CODE_SEG NON_BANKED

INTERRUPT void Timer_Clock_ISR(void) {
        short ix;
        //unsigned long tmp;
        
        // .128 ms clock interrupt - clear flag immediately to resume count
        CRGFLG = 0x80; // clear RTI interrupt flag
        // also generate 1.024 ms, .10035 sec and 1.0035 sec clocks
        lmms++; // free running clock(.128 ms tics) good for ~ 110 hrs
        can_mmsclk++;
        mms++; // in .128 ms tics - reset every 8 tics = 1.024 ms

        // check mms to generate other clocks
        if (mms < 8) goto CLK_DONE;
        mms = 0;

        //////////////////////////// 0.001 millisecond section ///////////////////////

        millisec++; // actually 1.024 ms, for seconds count
        burn_msec_flag = 0; // reset burn timer to allow next step of flash burn

        // slow down the flash burns to every ~6 milliseconds
        if (!(millisec % 6)) {
            // burn flash 512 byte(256 word) sector(s)
            if (burn_flag >= 1) {
                burn_msec_flag = 1;
                fburner(tableWordFlash(burn_idx, 0), tableWordRam(burn_idx, 0), tableWords(burn_idx));
                if (burn_flag >= tableWords(burn_idx)) {
                    burn_flag = 0;
                    flocker = 0;
                } else {
                    burn_flag++;
                }
            }

            if (burn_flag) outpc.flashburn = 1;
            else outpc.flashburn = 0; // set burn flag for datalog
            //////////////////////////// END of 0.001 second section ///////////////////////

            //////////////////////////// 0.010 second section ////////////////////////////////////////
            //                                                                                      //
            if (millisec % 10 == 0) // every 1/100th second (actually 10/976 = 10.24 milliseconds)  //
            //                                                                                      //
            //           Everything in this section will execute ~100 times/second.                 //
            //                                                                                      //
            //////////////////////////////////////////////////////////////////////////////////////////
            {
                // Decrement the wait states
                if (delay > 10) delay -= 10;
                else delay = 0;

                // Set up CAN transmission
                if (in2ram.activate_xrate > 0) {
                    uctmp = 1;
                    for (mx = 0; mx < MAX_OUTMSGS; mx++) { // mx = msg_no

                        if (outmsg[mx].xrate && (in2ram.activate_xrate & uctmp)) {
                            if (xrate_clks[mx] > outmsg[mx].xrate) { // time to send
                                xrate_clks[mx] = 0; // zero clk
                                ix = can[1].cxno_in;
                                can[1].cx_msg_type[ix] = OUTMSG_RSP;
                                can[1].cx_outmsg_no[ix] = mx;
                                n_outbytes[mx] = outmsg[mx].no_bytes;
                                can[1].cx_destvarblk[ix] = outmsg[mx].dest_varblk & 0x0F;
                                // gpio var_blk
                                can[1].cx_destvaroff[ix] = 0; // store data starting at gpio varblk;
                                can[1].cx_dest[ix] = outmsg[mx].dest_varblk >> 4; // gpio id
                                if (n_outbytes[mx] >= 8) {
                                    kx = 8;
                                } else {
                                    kx = n_outbytes[mx];
                                } // end if (n_outbytes...

                                can[1].cx_varbyt[ix] = kx;
                                for (jx = 0; jx < kx; jx++) {
                                    can[1].cx_datbuf[ix][jx] = * ((char * ) & outpc + outmsg[mx].offset[jx]);
                                } // end for(jx = 0 ...
                                send_can(1);
                            } // end if(xrate_clks[mx]...
                        } //  end if(outmsg[mx].xrate &&...
                        uctmp = uctmp << 1; // = 2 ^^ mx
                    } // end for (mx = 0;...
                } // end if(inpram.activate_xrate)

                //////////////////////////////////////////////////////////////////////
                // Send CAN to get outpc variables from MS-II

                grabMS2vars();

                //////////////////////////////////////////////////////////////////////


                /*-- COMPUTE VEHICLE SPEED
                  
                   We do this here so that the speedo updates regardles of any delays in the code
                   (like the shift and pressure delays).
                  
                   For imperial units: 
                 
                   Vehicle speed = VSS_rpm * tire_diam/(5280*12) * 60 minutes/hr * pi/axle_ratio  (miles/hour)

                     since miles/wheel revolution = tire_diam/(5280*12) * 60 minutes/hr * pi/axle_ratio
                     the constant evaluate to 3 .1415926/(5280*12) * 60 =  1/336, so
                     Vehicle speed  = VSS rpm * tire_diam/axle_ratio * 1/336 (imperial) 

                  ic_period is 'tics', multiply by 5.333 �s/tic to get time 
                  there are inpram.vss_num_teeth per rev, so
                        VSS rpm = frequency (rev/min) = 60/(inpram.vss_num_teeth * outpc.ic_period * 0.000005333)
                                  VSSdivide is set to 60/(inpram.vss_num_teeth*0.000005333)
                                   (VSSdivide is set near start of main proc, so we don't have to keep recalculating)
                  so    VSS rpm = freq = VSSdivide/ic_period  (rev/min)

                  calculate VSSdivide = 187501 [tics/sec] * 1/vss_num_teeth [rev/teeth] * 60 [sec/min]
                                      = 187501 * 60 / inpram.vss_num_teeth  [rev/min]
                                      = 11250060 / inpram.vss_num_teeth     [rev/min]

                     speedo = VSSdivide [revs/min][tics*teeth] / rev_mile [rev/mile]

                     speedo = VSSdivide * tire_diam * 1/336 * 1/ic_period 
                  --*/

                // average last 21 periods for smoothing

                // vehicle speed is delta X / delta T
                // delta T is the time for 20 teeth = ic_per_acc, in 5.333 �sec tics
                /* ic_per_acc = 0;
                ic_dc_acc  = 0;
                for (ix=1;ix<20;ix++)
                  {
                  ic_per_acc = ic_per_acc + ic_period[ix];
                  ic_dc_acc = ic_dc_acc + ic_duty[ix];  
                  }

                outpc.ic_per_avg = (ic_per_acc * 5333)/1000; // the time for 20 teeth (microseconds);  */
                // delta X is the distance travelled by 20 teeth
                // = tire_distance/rev * 1/axle_ratio * 20/vss_num_teeth
                // = pi*tire_diam * 1/axle_ratio * 20/vss_num_teeth == VSSdivide
                //
                // so speedo = (pi*tire_diam * 1/axle_ratio * 20/vss_num_teeth)/(ic_per_acc * constant)
                //           = constant * VSSdivide/ic_per_acc


                    
                     

                //outpc.speedo = (unsigned int) ((2530*VSSdivide)/(outpc.ic_per_avg)); // imperial     - tire diam in inches (mph x 10)
                //outpc.speedo = (unsigned int) ((9900*VSSdivide)/(outpc.ic_per_avg));  // metric  - tire_diam in cm (kph x 10)
                       

                // unlock the TCC if the speed has fallen 5 mph/kph below user set lock-up threshold
                //if (outpc.speedo < (inpram.minTCCspeed-50)) tcc_unlock();
                  
                // Calculate converter slippage (100% is no slip)
                //tmp = outpc.current_gear; 
                //outpc.os_rpm = (unsigned int)(((1000000/outpc.ic_per_avg) * (2000/inpram.vss_num_teeth)) * 600/1050);
                //outpc.is_rpm = (unsigned int)(((long)outpc.os_rpm * (long)inpram.gear_table[tmp])/1000); 
                //outpc.converter_slip = (uchar)((long)outpc.engine_rpm*100/(long)outpc.is_rpm);














                /*  -----  Update RPM, MAP & Battery Voltage with CAN ----------------------------------- */
                if (in2ram.CAN_enabled) {
                    outpc.pulsewidth1 = * (int * )(msvar + 2);
                    outpc.engine_rpm = * (int * )(msvar + 6);
                    outpc.adv_deg = * (int * )(msvar + 8);
                    
                    outpc.tps = * (int * )(msvar + 24);
                    
                }
            } // end if (millisecond % 10 ...
            //////////////////////////// END of 0.010 second section /////////////////////////////////

            /////////////////////////// 0.100 second section /////////////////////////////////////
            if (millisec % 98 == 0) // 98 * 1.024 milliseconds
            {
              outpc.trans_temp = cltfactor_table[get_adc(TranTmpAdc)]; //Get transmission temperature
            } // end if (millisec % 98 ...

            // Decrement the wait state
            if (delay_count > 10) delay_count -= 10;
            else delay_count = 0;

            //////////////////////////// END of 0.100 second section ///////////////////////  

            ///////////////////////// 1.000 second section ///////////////////////////////////////////
            //                                                                                      //
            //           Everything in this section will execute 1 time/second.                     //  
            if (millisec > 976) {
                //                                                                                      //
                //                                                                                      //
                //////////////////////////////////////////////////////////////////////////////////////////
                millisec = 0;
                // update seconds to send back to PC
                outpc.seconds++;
                
                StatusLedOutputToggle(); // Toggle status led
                
            } // end 1.000 second section

            ////////////////////////  END of 1.000 second section ////////////////////////////////////

            ///////  Other timing increments (besides 0.001, 0.010, 0.100, and 1.000) ////////////////

            CLK_DONE: // Statements after this label are executed every 0.128 millisecond clock 'tic'

                //////////////////////////// 0.000128 millisecond section ///////////////////////  

                ///////////////////////// end of 0.000128 millisecond section //////////////////// 

                return;
        }
    }
    // --------------- ISR_TimerOverflow ------------------------------------

INTERRUPT void Timer_Overflow_ISR(void) {
    // Get display seconds from continuously running TCNT
    TC_ovflow++;
    if (TC_ovflow >= TC_ovfla[TC_ov_ix]) {
        // update seconds to send back to PC
        outpc.seconds++; // increment secL in datalog
        if (TC_ov_ix >= 59) {
            TC_ov_ix = 0;
            TC_ovflow = 0;
        } else
            TC_ov_ix++;
    }

    vss_timer_overflow++;

    // clear timer overflow interrupt flag (TOF)
    TFLG2 = 0x80;
    return;
}

//** Serial Communications (SCI)
#pragma CODE_SEG NON_BANKED

#include "serComm.h"
#include "CANcomm.h"

#pragma CODE_SEG DEFAULT

void reset(void) {
    first_instr = 1;
    return;
}

#include "functs.h"

// ------ Unimplemented ISRs -------------------------------------------------
#pragma CODE_SEG NON_BANKED
INTERRUPT void UnimplementedISR(void) {
    return;
}

#pragma CODE_SEG ROM_7000
void waitAwhile(unsigned int delay) {
    wait_flag = 1;

    if (delay_count < 65535) delay_count = delay;
    else delay_count = 65535;

    while (delay > 0) {
        if (!(millisec % 20)) grabMS2vars();
    } // end while (delay_count > 0) ....

    wait_flag = 0;

    return;
}

// CAN Adjustments for Fuel/Spark/Idle on MS-II
#pragma CODE_SEG NON_BANKED

#include "serCANComm\sendCANAdj.h"

#pragma CODE_SEG ROM_7000

void grabMS2vars(void) {
        unsigned char ix;

        if (!in2ram.can_testout) {
            if (can_mmsclk > inpram.can_var_rate) {
                ix = can[1].cxno_in;
                can[1].cx_msg_type[ix] = MSG_REQ;
                can[1].cx_destvarblk[ix] = inpram.ms2varBLK;
                can[1].cx_myvarblk[ix] = 9;
                can[1].cx_myvaroff[ix] = can_varoff;
                can[1].cx_dest[ix] = inpram.ms2canID;
                can[1].cx_destvaroff[ix] = can_varoff;
                can_varoff += 8;

                if (can_varoff < NO_MSVAR_BYTES) {
                    can[1].cx_varbyt[ix] = 8;
                } else {
                    can[1].cx_varbyt[ix] = NO_MSVAR_BYTES - (can_varoff - 8);
                    can_varoff = 0;
                }
                send_can(1);
                can_mmsclk = 0;
            }
        } else {
            if (can_mmsclk > (inpram.can_var_rate * 2)) {
                ix = can[1].cxno_in;
                can[1].cx_msg_type[ix] = OUTMSG_REQ;

                can[1].cx_destvarblk[ix] = 9;

                can[1].cx_dest[ix] = 0;

                can[1].cx_destvaroff[ix] = 0;
                can[1].cx_outmsg_no[ix] = 0;
                send_can(1);
                can_mmsclk = 0;
            }
        } // end else (!inpram.can_testout)

        return;
    } // end grabMS2var()