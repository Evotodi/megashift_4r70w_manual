// This header defines all the I/O port assignments so that
// the main.c, etc. are clear of direct hardware references.

/*****************************************************************************************************


Function              Port     I/O    ON                      OFF                     READ               NOTES
--------              ----     ---    --                      ---                     ----               -----

SolAOut               PE4       O     PORTE &= 0x10;          PORTE &= ~0x10;
SolBOut               PM2       O     *pPTMpin[2] &= 0x04;    *pPTMpin[2] &= ~0x04;
StatLedOut            PM3       O     *pPTMpin[3] &= 0x08;    *pPTMpin[3] &= ~0x08;
NxStg1Out             PM4       O     *pPTMpin[4] &= 0x10;    *pPTMpin[4] &= ~0x10;
NxStg2Out             PM5       O     *pPTMpin[5] &= 0x20;    *pPTMpin[5] &= ~0x20;
FBrakeIn              PT7       I     --                      --                      PTT & 0x80
TBrakeIn              PE0       I     --                      --                      PORTE & 0x01
NxEnIn                PT6       I     --                      --                      PTT & 0x40
MplsAdc               PE1       ADC   --                      --                      get_adc(1)
TranTmpAdc			  AD2		ADC													  get_adc(2)
TccPwm                PT3       PWM    
PcPwm                 PT2       PWM


*****************************************************************************************************/


// Output: Solenoid A
#define SolAOut PORTE
#define SolAOutPin 0x10

// Output: Solenoid B
#define SolBOut *pPTMpin[2]
#define SolBOutPin 0x04

// Output: Status LED
#define StatLedOut *pPTMpin[3]
#define StatLedOutPin 0x08

// Output: Nitrous Stage 1
#define NxStg1Out *pPTMpin[4]
#define NxStg1OutPin 0x10

// Output: Nitrous Stage 2
#define NxStg2Out *pPTMpin[5]
#define NxStg2OutPin 0x20

// Input: Foot Brake
#define FBrakeIn PTT
#define FBrakeInPin 0x80

// Input: Trans Brake
#define TBrakeIn PORTE
#define TBrakeInPin 0x01

// Input: Nitrous Enable
#define NxEnIn PTT
#define NxEnInPin 0x40

// ADC: Manual Position Lever Sensor (MPLS)
#define MplsAdc 1

// ADC: Transmission Temperature Sensor
#define TranTmpAdc 2

// PWM: Torque Converter Clutch (TCC)
#define TccPwm PWMDTY2
#define TccPwmOn PWMPER2
#define TccPwmOff 0

// PWM: Pressure Control Solenoid (PC)
#define PcPwm PWMDTY3
#define PcPwmOn PWMPER3
#define PcPwmOff 0









