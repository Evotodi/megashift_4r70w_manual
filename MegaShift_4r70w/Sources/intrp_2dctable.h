// --------------- intrp_2dctable -------------------------------------------------------
int intrp_2dctable(unsigned int x, int y, unsigned char nx, unsigned char ny,
    unsigned int * x_table, int * y_table, unsigned char * z_table) {
    int ix, jx;
    long interp1, interp2, interp3;

    if (x > x_table[nx - 1]) x = x_table[nx - 1];
    else if (x < x_table[0]) x = x_table[0];
    if (y > y_table[ny - 1]) y = y_table[ny - 1];
    else if (y < y_table[0]) y = y_table[0];

    for (ix = ny - 2; ix > -1; ix--) {

        if (y > y_table[ix]) {
            break;
        }
    }
    if (ix < 0) ix = 0;
    for (jx = nx - 2; jx > -1; jx--) {

        if (x > x_table[jx]) {
            break;
        }
    }
    if (jx < 0) jx = 0;

    interp1 = y_table[ix + 1] - y_table[ix];
    if (interp1 != 0) {
        interp3 = (y - y_table[ix]);
        interp3 = (100 * interp3);
        interp1 = interp3 / interp1;
    }
    interp2 = x_table[jx + 1] - x_table[jx];
    if (interp2 != 0) {
        interp3 = (x - x_table[jx]);
        interp3 = (100 * interp3);
        interp2 = interp3 / interp2;
    }
    return ((int)(((100 - interp1) * (100 - interp2) * z_table[ix * nx + jx] + interp1 * (100 - interp2) * z_table[(ix + 1) * nx + jx] + interp2 * (100 - interp1) * z_table[ix * nx + jx + 1] + interp1 * interp2 * z_table[(ix + 1) * nx + jx + 1]) / 10000));
}
