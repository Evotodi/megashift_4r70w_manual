// ****** Set Up OUTPC. Structure for Serial Communications with MegaTune *******
typedef struct {
    unsigned int seconds; // clock
    unsigned int pulsewidth1; // injector pulse width from MS-II over CAN 
    unsigned int engine_rpm; // engine rpm (from CAN or estimated from VSS and gear)
    unsigned char error; // error status codes
    unsigned long loop_count; // Main loop counter 
    unsigned int CANtx, CANrx; // CAN transmit & receive counters
  	unsigned char RemotePort1, RemotePort2, RemotePort3; //Port1 = outputs from MS, Port2-3 are inputs to MS. Do not move any vars from here up as will effect the CAN Parameters in TS.
    unsigned long dbug; // spare debugging variable brought out in MegaTune
    unsigned int adv_deg; // ignition advance from MS-II via CAN
    unsigned int tps; // TPS value from MS-II over CAN
    unsigned int mpls;
    signed int flashburn;
    unsigned char gearX; //Bits=gears, 0=1, 1=2, 2=3, 3=OD, 4=N, 5=R, 6=P //MPLS Gear
    unsigned int pc_percent; //trans pressure percent 0-100
    unsigned char switches; //JRD: 1=on 0=off; bit0: foot brake;  bit1: trans brake; bit2: nx enable;
	  unsigned int trans_temp; //Trans temp in deg f
    unsigned long vss; //JRD
    
    unsigned long ic_per_avg, ic_dc_avg;      // average of last 20  
    unsigned int converter_slip;
    unsigned long vss_teeth;                  // VSS tooth counter
    unsigned long odo;                        // trip odometer (x1000)
    unsigned long os_rpm;              // clock, output shaft rpm (x10)
    unsigned int speedo;                      // vehicle speed (x10) 
    unsigned char tcc_stat; //bit0 is used
}
variables; // end outpc. structure

variables outpc;
