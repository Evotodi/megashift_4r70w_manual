//Output functions

void SolAOutput(unsigned char set_to) {
  if (set_to == 1) {
  	SolAOut |= SolAOutPin; // turn on
  	SolAState = 1;            // Set state indicator
  } else {
  	SolAOut &= ~SolAOutPin;// turn off
  	SolAState =0;             // Set state indicator
  }
  return;
}

void SolBOutput(unsigned char set_to) {
  if (set_to == 1) {
  	SolBOut |= SolBOutPin; // turn on
  	SolBState = 1;            // Set state indicator
  } else {
  	SolBOut &= ~SolBOutPin;// turn off
  	SolBState =0;             // Set state indicator
  }
  return;
}

void StatusLedOutput(unsigned char set_to) {
  if (set_to == 1) {
	StatLedOut |= StatLedOutPin; // turn on
  } else {
  	StatLedOut &= ~StatLedOutPin;// turn off
  }
  return;
}

void StatusLedOutputToggle() {
	StatLedOut ^= StatLedOutPin; // toggle
	return;
}

void NxStage1Output(unsigned char set_to) {
  if (set_to == 1) {
  	NxStg1Out |= NxStg1OutPin; // turn on
  	NxStg1State = 1;            // Set state indicator
  } else {
  	NxStg1Out &= ~NxStg1OutPin;// turn off
  	NxStg1State =0;             // Set state indicator
  }
  return;
}

void NxStage2Output(unsigned char set_to) {
  if (set_to == 1) {
  	NxStg2Out |= NxStg2OutPin; // turn on
  	NxStg2State = 1;            // Set state indicator
  } else {
  	NxStg2Out &= ~NxStg2OutPin;// turn off
  	NxStg2State =0;             // Set state indicator
  }
  return;
}

// Input functions

char FBrakeInput(void) {
	return FBrakeIn & FBrakeInPin;
}

char TBrakeInput(void) {
	return TBrakeIn & TBrakeInPin;
}

char NxEnInput(void) {
	return NxEnIn & NxEnInPin;
}

// Other Functions

char bit_read(unsigned char bit, unsigned char byte) {
    bit = 1 << bit;
    return(bit & byte);
}

void bit_reset(unsigned char bit, unsigned char *byte) {
    bit = 1 << bit;
    bit ^= 0xff;
    *byte = *byte & bit;
}

void bit_set(unsigned char bit, unsigned char *byte) {
    bit = 1 << bit;
    *byte = *byte | bit;
}