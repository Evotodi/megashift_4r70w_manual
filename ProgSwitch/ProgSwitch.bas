#REM

Program Mode Switcher For GPIO
 
Version 1.0.0
Date 12/30/2017
Evotodi

#ENDREM

DISABLEBOD
setfreq m4

symbol sw = pinC.3
symbol btn = C.3
symbol led = C.4
symbol pwr = C.1
symbol prg = C.2
symbol btnvar = b0
symbol state = b1
#define offdly 2000

output pwr
output prg
output led
input btn
	
main:
	;sertxd("-Init-",cr,lf)	
	high led
	pause 1000
	state = 0 ;Run mode
	high pwr
	low prg
	low led
	
	;sertxd("-Main-",cr,lf)	
	
loop1:
	pause 100
	button btn, 0, 255, 255, btnvar, 1, mode
	;if sw = 0 then goto mode
	goto loop1
	
mode:
	;sertxd("-Mode Change-",cr,lf)

	select case state
		case 0
			state = 1
			low pwr
			pause offdly
			high prg
			pause 500
			high pwr
			high led
			;sertxd("Mode: Programming",cr,lf)
			pause 3000
			goto loop1
		case 1
			state = 0
			low pwr
			pause offdly
			low prg
			pause 500
			high pwr
			low led
			;sertxd("Mode: Run",cr,lf)
			pause 3000
			goto loop1
	endselect


		
		